package tdp.mt.backend.movistartotal;

import java.util.Map;
import java.util.Properties;
import javax.servlet.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import tdp.mt.backend.movistartotal.commonms.common.util.DatabasePropertiesSource;
import tdp.mt.backend.movistartotal.commonservice.connection.Database;
import tdp.mt.backend.movistartotal.commonservice.context.VentaFijaContextPersistenceFilter;


@SpringBootApplication
@EnableScheduling
public class MovistarTotalApplication {

	private static final Logger logger = LogManager.getLogger();
	private static final String USER_SERVICE_CODE = "USER";

	public static void main(String[] args) {

		SpringApplication.run(MovistarTotalApplication.class, args);
		System.out.println("Movistar Total - Backend Fija - Running!");

	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		DatabasePropertiesSource propertiesSource = new DatabasePropertiesSource(Database.dataSource());
		Map<String, Object> props = propertiesSource.loadProperties();
		System.out.print(props);
		pspc.setIgnoreResourceNotFound(true);
		pspc.setIgnoreUnresolvablePlaceholders(true);
		Properties propps = new Properties();
		propps.putAll(props);
		pspc.setProperties(propps);
		return pspc;
	}

	@Bean
	public Filter ventaFijaContextPersistenceFilter() {
		return new VentaFijaContextPersistenceFilter(USER_SERVICE_CODE);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**");
			}
		};
	}

}
