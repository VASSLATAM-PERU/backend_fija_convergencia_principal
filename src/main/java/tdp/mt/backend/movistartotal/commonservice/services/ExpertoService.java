package tdp.mt.backend.movistartotal.commonservice.services;

import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.clients.experto.OffersApiExpertoRequestBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.experto.OffersApiExpertoResponseBody;

/**
 * Service to make api calls to tdp
 * @author jvilcayp
 *
 */
public interface ExpertoService {

	/**
	 * Makes request to Experto service on tdp
	 * @param apiExpertoRequestBody the body of the request
	 * @return the api response when obtained
	 * @throws ClientException when an error happens
	 */
	ApiResponse<OffersApiExpertoResponseBody> scoring(OffersApiExpertoRequestBody apiExpertoRequestBody) throws ClientException;
}
