package tdp.mt.backend.movistartotal.commonservice.services;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonservice.services.config.ApplicationConfiguration;

@Service
public class TwilioService {
	private static Logger logger = LogManager.getLogger(TwilioService.class);
	@Autowired
	private ApplicationConfiguration applicationConfiguration;
	
	/**
	 * This method will send messageText to the phoneNumber
	 * @param phoneNumber to whom we send the message Ej. +51999999999
	 * @param messageText the message to send
	 */
	public void sendSms(String phoneNumber, String messageText) {
		Twilio.init(applicationConfiguration.getTwilioUsername(), applicationConfiguration.getTwilioPassword());
		Message message = Message.creator(new PhoneNumber(phoneNumber), new PhoneNumber(applicationConfiguration.getTwilioPhoneNumberSender()), messageText).create();
	}

}
