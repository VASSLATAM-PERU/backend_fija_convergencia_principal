package tdp.mt.backend.movistartotal.commonservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.TdpServicePedido;

@Repository
@Transactional
public interface TdpServicePedidoRepository extends JpaRepository<TdpServicePedido, String> {

    TdpServicePedido findOneByPedidoCodigo(String pedidoCodigo);

}
