package tdp.mt.backend.movistartotal.commonservice.services;

import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.exception.ApiClientException;
import tdp.mt.backend.movistartotal.commonms.common.exception.ApplicationException;
import tdp.mt.backend.movistartotal.commonservice.domain.dto.HDECResponse;

/**
 * Service para hacer controlar las llamadas a HDEC.
 * Se maneja un limite de reintentos (configurado en base datos, no soporta cambios en caliente).
 * @author      glazaror
 * @since       1.5
 */
public interface HDECRetryService {

    /**
     * Permite enviar datos de venta al servicio de HDEC.
     * @param orderId identificador de la venta.
     * @return respuesta de la llamada a HDEC
     */
    public HDECResponse sendToHDEC(String orderId) throws ApiClientException, ApplicationException, ClientException;

    /**
     * Permite enviar datos de venta al servicio de HDEC una determinada cantidad de veces.
     * @param orderId identificador de la venta.
     * @return respuesta de la llamada a HDEC
     */
    public HDECResponse sendToHDECWithRetry(String orderId) throws ApiClientException, ApplicationException, ClientException;
}
