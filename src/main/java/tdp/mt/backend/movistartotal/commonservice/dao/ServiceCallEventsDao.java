package tdp.mt.backend.movistartotal.commonservice.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

@Repository
public class ServiceCallEventsDao {
    private Logger LOGGER = LogManager.getLogger(ServiceCallEventsDao.class);
    private DataSource datasource;

    @Autowired
    public ServiceCallEventsDao(DataSource datasource) {
        this.datasource = datasource;
    }


   public void registerEvent(ServiceCallEvent event) {
        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP - interval '5' hour,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "SERVICE_CALL");
            ps.setString(2, event.getUsername());
            ps.setString(3, event.getMsg());
            ps.setString(4, event.getOrderId());
            ps.setString(5, event.getDocNumber());
            ps.setString(6, event.getServiceCode());
            ps.setString(7, event.getServiceUrl());
            ps.setString(8, event.getServiceRequest());
            ps.setString(9, event.getServiceResponse());
            ps.setString(10, event.getSourceApp());
            ps.setString(11, event.getSourceAppVersion());
            ps.setString(12, event.getResult());

            ps.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada a API", e);
        }
    }


    public void registerEventEnd(ServiceCallEvent event, Timestamp timeRequest) {
        try (Connection con = datasource.getConnection()) {

            String sql = "INSERT INTO public.service_call_events (datetime_request,datetime_response, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (?,CURRENT_TIMESTAMP - interval '5' hour,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = con.prepareStatement(sql);
            LogVass.daoQuery(LOGGER, ps.toString());

            ps.setTimestamp(1,  timeRequest);
            ps.setString(2, "SERVICE_CALL");
            ps.setString(3, event.getUsername());
            ps.setString(4, event.getMsg());
            ps.setString(5, event.getOrderId());
            ps.setString(6, event.getDocNumber());
            ps.setString(7, event.getServiceCode());
            ps.setString(8, event.getServiceUrl());
            ps.setString(9, event.getServiceRequest());
            ps.setString(10, event.getServiceResponse());
            ps.setString(11, event.getSourceApp());
            ps.setString(12, event.getSourceAppVersion());
            ps.setString(13, event.getResult());

            ps.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de respuesta a API", e);
        }
    }

}
