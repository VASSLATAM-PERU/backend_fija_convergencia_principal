package tdp.mt.backend.movistartotal.commonservice.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

	@Value("${twilio.api.sms.username}")
	private String twilioUsername;
	@Value("${twilio.api.sms.password}")
	private String twilioPassword;
	@Value("${twilio.api.sms.phonenumbersender}")
	private String twilioPhoneNumberSender;
	
	@Value("${google.api.storage.bucket}")
	private String googleStorageBucket;
	@Value("${google.api.auth.projectid}")
	private String googleApiProjectId;

	public String getTwilioUsername() {
		return twilioUsername;
	}

	public String getTwilioPassword() {
		return twilioPassword;
	}

	public String getGoogleStorageBucket() {
		return googleStorageBucket;
	}

	public String getTwilioPhoneNumberSender() {
		return twilioPhoneNumberSender;
	}

	public String getGoogleApiProjectId() {
		return googleApiProjectId;
	}
}
