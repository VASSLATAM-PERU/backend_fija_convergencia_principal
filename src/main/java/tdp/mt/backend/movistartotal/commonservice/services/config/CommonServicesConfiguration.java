package tdp.mt.backend.movistartotal.commonservice.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonServicesConfiguration {

	@Value("${google.api.auth.credential}")
	private String googleApiJsonCredential;
	@Value("${google.api.auth.applicationname}")
	private String googleApiApplicationName;
	
	@Bean
    public MethodInvokingFactoryBean methodInvokingFactoryBean() {
        MethodInvokingFactoryBean mifb = new MethodInvokingFactoryBean();
        mifb.setStaticMethod("tdp.mt.backend.movistartotal.commonservice.services.config.StorageFactory.setMeta");
        mifb.setArguments(new String[] { this.googleApiJsonCredential, this.googleApiApplicationName });
        return mifb;
    }

}
