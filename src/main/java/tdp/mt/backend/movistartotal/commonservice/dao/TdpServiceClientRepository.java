package tdp.mt.backend.movistartotal.commonservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.TdpServiceClient;

import java.util.List;

@Repository
@Transactional
public interface TdpServiceClientRepository extends JpaRepository<TdpServiceClient, String> {

    List<TdpServiceClient> findAllByClientTipoDocAndClientDocument(String clientTipoDoc, String clientDocument);
    List<TdpServiceClient> findAllByProductTypeAndClientTipoDocAndClientDocument(String productType, String clientTipoDoc, String clientDocument);

}
