package tdp.mt.backend.movistartotal.commonservice.context;

import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContext;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextHolder;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextImpl;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class VentaFijaContextPersistenceFilter implements Filter {
	private static final String X_HEADER_USUARIO = "X_HTTP_USUARIO";
	private static final String X_HEADER_ORDERID = "X_HTTP_ORDERID";
	private static final String X_HEADER_DOCIDENT = "X_HTTP_DOCIDENT";
	private static final String X_HEADER_APPSOURCE = "X_HTTP_APPSOURCE";
	private static final String X_HEADER_APPVERSION = "X_HTTP_APPVERSION";
	
	private String serviceCode;

	public VentaFijaContextPersistenceFilter(String serviceCode) {
		super();
		this.serviceCode = serviceCode;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		
		String username = httpRequest.getHeader(X_HEADER_USUARIO);
		String orderId = httpRequest.getHeader(X_HEADER_ORDERID);
		String docNumber = httpRequest.getHeader(X_HEADER_DOCIDENT);
		String appSource = httpRequest.getHeader(X_HEADER_APPSOURCE);
		String appVersion = httpRequest.getHeader(X_HEADER_APPVERSION);
		
		ServiceCallEvent event = new ServiceCallEvent();
		event.setUsername(username);
		event.setOrderId(orderId);
		event.setServiceCode(serviceCode);
		event.setDocNumber(docNumber);
		event.setSourceApp(appSource);
		event.setSourceAppVersion(appVersion);
		
		try {
			VentaFijaContext ctx = new VentaFijaContextImpl();
			ctx.setServiceCallEvent(event);
			
			VentaFijaContextHolder.setContext(ctx);
			chain.doFilter(request, response);
		} finally {
			VentaFijaContextHolder.clearContext();
		}
	}

	@Override
	public void destroy() {}

}
