package tdp.mt.backend.movistartotal.commonservice.domain.dto;

public enum HDECResponse {
    SATISFACTORIO,
    ERROR_CONTROLADO,
    ERROR_GENERAL;
}
