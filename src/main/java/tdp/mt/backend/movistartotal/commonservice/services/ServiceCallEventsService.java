package tdp.mt.backend.movistartotal.commonservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonservice.dao.ServiceCallEventsDao;

import java.sql.Timestamp;


@Service
public class ServiceCallEventsService {
	ServiceCallEventsDao dao;
	
	@Autowired
	public ServiceCallEventsService (ServiceCallEventsDao dao) {
		this.dao = dao;
	}

	public void registerEvent (ServiceCallEvent event) {this.dao.registerEvent(event);}

	public void registerEventEnd (ServiceCallEvent event, Timestamp id) {
		this.dao.registerEventEnd(event,id);
	}



}
