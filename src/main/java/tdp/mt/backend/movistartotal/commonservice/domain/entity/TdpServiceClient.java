package tdp.mt.backend.movistartotal.commonservice.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tdp_service_client", schema = "public")
public class TdpServiceClient {

    @Column(name = "product_type")
    private String productType;
    @Id
    @Column(name = "product_service")
    private String productService;
    @Column(name = "product_ps")
    private String productPs;
    @Column(name = "producto_descripcion")
    private String productoDescripcion;
    @Column(name = "product_origen")
    private String productOrigen;
    @Column(name = "product_linea")
    private String productLinea;
    @Column(name = "product_internet")
    private String productInternet;
    @Column(name = "product_tv")
    private String productTv;
    @Column(name = "client_tipo_doc")
    private String clientTipoDoc;
    @Column(name = "client_document")
    private String clientDocument;
    @Column(name = "client_cms")
    private String clientCms;
    @Column(name = "departamento_code")
    private String departamentoCode;
    @Column(name = "departamento")
    private String departamento;
    @Column(name = "provincia_code")
    private String provinciaCode;
    @Column(name = "provincia")
    private String provincia;
    @Column(name = "distrito_code")
    private String distritoCode;
    @Column(name = "distrito")
    private String distrito;
    @Column(name = "coordenada_x")
    private BigDecimal coordenadaX;
    @Column(name = "coordenada_y")
    private BigDecimal coordenadaY;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "service_estado_code")
    private String serviceEstadoCode;
    @Column(name = "service_estado")
    private String serviceEstado;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "auditoria_create")
    private Date auditoriaCreate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "auditoria_modify")
    private Date auditoriaModify;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductService() {
        return productService;
    }

    public void setProductService(String productService) {
        this.productService = productService;
    }

    public String getProductPs() {
        return productPs;
    }

    public void setProductPs(String productPs) {
        this.productPs = productPs;
    }

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public String getProductOrigen() {
        return productOrigen;
    }

    public void setProductOrigen(String productOrigen) {
        this.productOrigen = productOrigen;
    }

    public String getProductLinea() {
        return productLinea;
    }

    public void setProductLinea(String productLinea) {
        this.productLinea = productLinea;
    }

    public String getProductInternet() {
        return productInternet;
    }

    public void setProductInternet(String productInternet) {
        this.productInternet = productInternet;
    }

    public String getProductTv() {
        return productTv;
    }

    public void setProductTv(String productTv) {
        this.productTv = productTv;
    }

    public String getClientTipoDoc() {
        return clientTipoDoc;
    }

    public void setClientTipoDoc(String clientTipoDoc) {
        this.clientTipoDoc = clientTipoDoc;
    }

    public String getClientDocument() {
        return clientDocument;
    }

    public void setClientDocument(String clientDocument) {
        this.clientDocument = clientDocument;
    }

    public String getClientCms() {
        return clientCms;
    }

    public void setClientCms(String clientCms) {
        this.clientCms = clientCms;
    }

    public String getDepartamentoCode() {
        return departamentoCode;
    }

    public void setDepartamentoCode(String departamentoCode) {
        this.departamentoCode = departamentoCode;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvinciaCode() {
        return provinciaCode;
    }

    public void setProvinciaCode(String provinciaCode) {
        this.provinciaCode = provinciaCode;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistritoCode() {
        return distritoCode;
    }

    public void setDistritoCode(String distritoCode) {
        this.distritoCode = distritoCode;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public BigDecimal getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(BigDecimal coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    public BigDecimal getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(BigDecimal coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getServiceEstadoCode() {
        return serviceEstadoCode;
    }

    public void setServiceEstadoCode(String serviceEstadoCode) {
        this.serviceEstadoCode = serviceEstadoCode;
    }

    public String getServiceEstado() {
        return serviceEstado;
    }

    public void setServiceEstado(String serviceEstado) {
        this.serviceEstado = serviceEstado;
    }

    public Date getAuditoriaCreate() {
        return auditoriaCreate;
    }

    public void setAuditoriaCreate(Date auditoriaCreate) {
        this.auditoriaCreate = auditoriaCreate;
    }

    public Date getAuditoriaModify() {
        return auditoriaModify;
    }

    public void setAuditoriaModify(Date auditoriaModify) {
        this.auditoriaModify = auditoriaModify;
    }
}
