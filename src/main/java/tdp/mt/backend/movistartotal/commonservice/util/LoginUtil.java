package tdp.mt.backend.movistartotal.commonservice.util;

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import javax.ws.rs.NotAuthorizedException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Map;

//glazaror utilitario para generar y validar token
public class LoginUtil {
    private static final Logger logger = LogManager.getLogger();

    //key debe ser igual al verifier
    private static JWTSigner jwtSigner = new JWTSigner("TOKEN-KEY");

    //el key verificador debe ser el mismo del signer
    private static JWTVerifier verifier = new JWTVerifier("TOKEN-KEY");

    public static String generarToken(Map<String, Object> payload) {
        final String token = jwtSigner.sign(payload, new JWTSigner.Options().setAlgorithm(Algorithm.HS512));
        return token;
    }

    public static Map<String, Object> obtenerUsuario(String token) {
        try {
            logger.info("ObtenerUsuario => token: " + token);
            //logger.info("---->>> LoginUtil.obtenerUsuario.token::: " );
            if (token == null) {
                return null;
            }
            if (token.contains("Bearer")) {
                token = token.substring("Bearer".length()).trim();
            }
            //objeto verify tiene la info empaquetada previamente
            Map<String, Object> datosUsuario = verifier.verify(token);
            JSONObject obj = new JSONObject(datosUsuario);
            logger.info("ObtenerUsuario => datosUsuario: " + obj.toString());
            //System.out.println("verify::: " + datosUsuario);
            return datosUsuario;

        } catch (SignatureException e) {
            e.printStackTrace();
            logger.error("Error ObtenerUsuario Util", e);

        } catch (InvalidKeyException e) {
            e.printStackTrace();
            logger.error("Error ObtenerUsuario Util", e);
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
            logger.error("Error ObtenerUsuario Util", e);
        } catch (IllegalStateException e) {

            e.printStackTrace();
            logger.error("Error ObtenerUsuario Util", e);
        } catch (IOException e) {

            e.printStackTrace();
            logger.error("Error ObtenerUsuario Util", e);
            throw new NotAuthorizedException("Llamada inconsistente");
        } catch (JWTVerifyException e) {

            e.printStackTrace();
            logger.info("Error ObtenerUsuario Util", e);
            throw new NotAuthorizedException("Llamada inconsistente");
        }
        return null;
    }
}
