package tdp.mt.backend.movistartotal.commonservice.domain.dto;

import java.math.BigInteger;

public class UserOrderResponse {

	private String status;
	private BigInteger quantity;
	
	public UserOrderResponse() {
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getQuantity() {
		return quantity;
	}

	public void setQuantity(BigInteger quantity) {
		this.quantity = quantity;
	}
	
	
	
}
