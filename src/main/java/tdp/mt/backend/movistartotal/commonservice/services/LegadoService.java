package tdp.mt.backend.movistartotal.commonservice.services;

import tdp.mt.backend.movistartotal.commonservice.domain.entity.TdpServicePedido;

public interface LegadoService {

    String getEstadoCmsCodeWithTry(String codigoPedido);

    String getEstadoAtisCodeWithTry(String codigoPedido);

    String getEstadoAtisCodePedido(String codigoPedido);


    TdpServicePedido getEstadoCms(String codigoPedido);
    TdpServicePedido getEstadoAtis(String codigoPedido);
}
