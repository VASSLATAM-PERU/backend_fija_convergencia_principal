package tdp.mt.backend.movistartotal.commonservice.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadoatis.ConsultaEstadoAtisClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadoatis.DuplicateApiAtisRequestBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadoatis.DuplicateApiAtisResponseBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadocms.ConsultaEstadoCmsClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadocms.DuplicateApiCmsRequestBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadocms.DuplicateApiCmsResponseBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.constant.LegadosConstants;
import tdp.mt.backend.movistartotal.commonms.common.constant.ParametersConstants;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.commonservice.dao.ParametersRepository;
import tdp.mt.backend.movistartotal.commonservice.dao.TdpServiceClientRepository;
import tdp.mt.backend.movistartotal.commonservice.dao.TdpServicePedidoRepository;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.TdpServicePedido;
import tdp.mt.backend.movistartotal.commonservice.services.LegadoService;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class LegadoServiceImpl implements LegadoService {
    private static final Logger logger = LogManager.getLogger();

    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private TdpServiceClientRepository tdpServiceClientRepository;
    @Autowired
    private TdpServicePedidoRepository tdpServicePedidoRepository;



    private ApiHeaderConfig apiHeaderConfig;
    private ServiceCallEventsService serviceCallEventsService;

    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.atis}")
    private String contingenciaStopEstadoAtis;
    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.cms}")
    private String contingenciaStopEstadoCms;

    @Autowired
    public LegadoServiceImpl(ApiHeaderConfig apiHeaderConfig, ServiceCallEventsService serviceCallEventsService) {
        this.apiHeaderConfig = apiHeaderConfig;
        this.serviceCallEventsService = serviceCallEventsService;
    }

    @Override
    public String getEstadoAtisCodeWithTry(String codigoPedido) {
        String estadoPedido = null;

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        Integer triesTotal = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.atis");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_ATIS);
            TdpServicePedido tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (contingenciaStopEstadoAtis.indexOf("|" + estadoPedido + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }else{
                        estadoPedido = tdpServicePedidos.getPedidoEstadoCode();
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                Integer estadoAtisDowns = 0;
                Parameters tries = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TRY, "ESTADO", "try.estado.atis");
                triesTotal = tries.getStrValue() == null || tries.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(tries.getStrValue());

                while (estadoAtisDowns < triesTotal && estadoPedido == null) {

                    try {
                        DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                        apiRequestBody.setPeticion(codigoPedido);
                        ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                        if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                            if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                                if (tdpServicePedidos == null) {
                                    tdpServicePedidos = new TdpServicePedido();
                                    tdpServicePedidos.setAuditoriaCreate(dateHoy);
                                }
                                tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_ATIS);
                                tdpServicePedidos.setPedidoCodigo(codigoPedido);
                                tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());
                                tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(apiResponse.getBodyOut().getEstadoPeticion(), translateList));

                                tdpServicePedidos.setAuditoriaModify(dateHoy);

                                estadoPedido = tdpServicePedidos.getPedidoEstadoCode();

                                tdpServicePedidoRepository.save(tdpServicePedidos);
                            }
                        } else estadoAtisDowns++;

                        ServiceCallEvent datos= new ServiceCallEvent();
                        datos = cargarDatosServiceCallEvent(apiResponse.getBodyOut(), tdpServicePedidos, apiRequestBody);
                        serviceCallEventsService.registerEvent(datos);

                    } catch (Exception apiconnect) {
                        estadoAtisDowns++;
                        logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                    }

                }

            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoAtis Service.", ex);
        }

        return estadoPedido;
    }

    @Override
    public String getEstadoAtisCodePedido(String codigoPedido) {
        String estadoPedido=null;
        TdpServicePedido tdpServicePedidos=null;
                    try {
                        DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                        apiRequestBody.setPeticion(codigoPedido);
                        ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                        if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                            if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                                if (tdpServicePedidos == null) {
                                    tdpServicePedidos = new TdpServicePedido();
                                }
                                tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());


                                estadoPedido = tdpServicePedidos.getPedidoEstadoCode();

                            }
                        } else {

                            ServiceCallEvent datos = new ServiceCallEvent();
                            datos = cargarDatosServiceCallEvent(apiResponse.getBodyOut(), tdpServicePedidos, apiRequestBody);
                            serviceCallEventsService.registerEvent(datos);
                        }

                    } catch (Exception apiconnect) {
                        logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                    }


        return estadoPedido;
    }

    @Override
    public String getEstadoCmsCodeWithTry(String codigoPedido) {
        String estadoPedido = null;

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        Integer triesTotal = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.cms");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_CMSR);
            TdpServicePedido tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (contingenciaStopEstadoCms.indexOf("|" + estadoPedido + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }else{
                        estadoPedido = tdpServicePedidos.getPedidoEstadoCode();
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                Integer estadoCmsDowns = 0;
                Parameters tries = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TRY, "ESTADO", "try.estado.cms");
                triesTotal = tries.getStrValue() == null || tries.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(tries.getStrValue());

                while (estadoCmsDowns < triesTotal && estadoPedido == null) {

                    try {
                        DuplicateApiCmsRequestBody apiRequestBody = new DuplicateApiCmsRequestBody();
                        apiRequestBody.setCodigoRequerimiento(codigoPedido);
                        ApiResponse<DuplicateApiCmsResponseBody> objCms = getConsultaEstadoCms(apiRequestBody);
                        if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                            if (objCms.getBodyOut().getEstadoRequerimiento() != null) {
                                if (tdpServicePedidos == null) {
                                    tdpServicePedidos = new TdpServicePedido();
                                    tdpServicePedidos.setAuditoriaCreate(dateHoy);
                                }
                                tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_CMS);
                                tdpServicePedidos.setPedidoCodigo(codigoPedido);
                                tdpServicePedidos.setPedidoEstadoCode(objCms.getBodyOut().getEstadoRequerimiento());
                                tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(objCms.getBodyOut().getEstadoRequerimiento(), translateList));

                                tdpServicePedidos.setAuditoriaModify(dateHoy);

                                estadoPedido = tdpServicePedidos.getPedidoEstadoCode();

                                tdpServicePedidoRepository.save(tdpServicePedidos);

                            }
                        } else estadoCmsDowns++;

                        ServiceCallEvent datos= new ServiceCallEvent();
                        datos = cargarDatosServiceCallEventCms(objCms.getBodyOut(), tdpServicePedidos, apiRequestBody);
                        serviceCallEventsService.registerEvent(datos);

                    } catch (Exception apiconnect) {
                        estadoCmsDowns++;
                        logger.error("Error obtenerEstadoCms Service. apiconnect.", apiconnect);
                    }

                }


            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoCms Service.", ex);
        }

        return estadoPedido;
    }

    private ServiceCallEvent cargarDatosServiceCallEvent(DuplicateApiAtisResponseBody bodyOut, TdpServicePedido tdpServicePedidos, DuplicateApiAtisRequestBody apiRequestBody) {
        logger.info("ValidacionAtis generacion de Log para HDEC: ");
        ServiceCallEvent data = new ServiceCallEvent();
        JSONObject obj = new JSONObject(apiRequestBody);


        data.setMsg("OK");
        data.setUsername(tdpServicePedidos.getPedidoEstado());
        data.setOrderId(tdpServicePedidos.getPedidoCodigo());
        data.setDocNumber(tdpServicePedidos.getPedidoType());
        data.setServiceCode("ESTADO-ATIS");
        data.setServiceUrl(tdpServicePedidos.getPedidoEstado());
        data.setServiceRequest(""+obj);
        data.setServiceResponse(bodyOut.getEstadoPeticion() + " / " + tdpServicePedidos.getPedidoEstado());
        data.setSourceApp("WEBVF");
        data.setSourceAppVersion("1.0");
        data.setResult("OK");
        logger.info("ValidacionAtis Json apiRequestBody: " + obj);
        logger.info("ValidacionAtis data: " + data);
        return data;
    }


    private ServiceCallEvent cargarDatosServiceCallEventCms(DuplicateApiCmsResponseBody objCms, TdpServicePedido tdpServicePedidos, DuplicateApiCmsRequestBody apiRequestBody) {
        logger.info("ValidacionCms Azuki.");
        ServiceCallEvent data = new ServiceCallEvent();
        JSONObject obj = new JSONObject(apiRequestBody);

        data.setMsg("OK");
        data.setUsername(tdpServicePedidos.getPedidoEstado());
        data.setOrderId(tdpServicePedidos.getPedidoCodigo());
        data.setDocNumber(tdpServicePedidos.getPedidoType());
        data.setServiceCode("ESTADO-CMS");
        data.setServiceUrl(tdpServicePedidos.getPedidoEstado());
        data.setServiceRequest(""+obj);
        data.setServiceResponse(objCms.getEstadoRequerimiento() + " / " + tdpServicePedidos.getPedidoEstado());
        data.setSourceApp("WEBVF");
        data.setSourceAppVersion("1.0");
        data.setResult("OK");
        return data;
    }

    private String getDescriptionByElementScoring(String element, List<Parameters> cmssParameterList) {
        String description = "SIN VALOR";

        for (Parameters parameter : cmssParameterList) {
            if (element.equals(parameter.getElement())) {
                description = parameter.getStrValue();
            }
        }

        return description;
    }

    @Override
    public TdpServicePedido getEstadoCms(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.cms");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_CMSR);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (tdpServicePedidos.getPedidoEstadoCode() != null && contingenciaStopEstadoCms.indexOf("|" + tdpServicePedidos.getPedidoEstadoCode() + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                try {
                    DuplicateApiCmsRequestBody apiRequestBody = new DuplicateApiCmsRequestBody();
                    apiRequestBody.setCodigoRequerimiento(codigoPedido);
                    ApiResponse<DuplicateApiCmsResponseBody> objCms = getConsultaEstadoCms(apiRequestBody);
                    if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                        if (objCms.getBodyOut().getEstadoRequerimiento() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_CMS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(objCms.getBodyOut().getEstadoRequerimiento());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(objCms.getBodyOut().getEstadoRequerimiento(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoCms Service. apiconnect.", apiconnect);
                }
            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoCms Service.", ex);
        }

        return tdpServicePedidos;
    }

    private ApiResponse<DuplicateApiCmsResponseBody> getConsultaEstadoCms(DuplicateApiCmsRequestBody apiRequestBody) throws
            ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getCmsConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaEstadoCmsClient consultaEstadoCmsClient = new ConsultaEstadoCmsClient(config);
        ClientResult<ApiResponse<DuplicateApiCmsResponseBody>> result = consultaEstadoCmsClient.post(apiRequestBody);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    private ApiResponse<DuplicateApiAtisResponseBody> getConsultaEstadoAtis(DuplicateApiAtisRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAtisConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getAtisConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getAtisConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS)
                .build();

        ConsultaEstadoAtisClient consultaEstadoAtisClient = new ConsultaEstadoAtisClient(config);
        ClientResult<ApiResponse<DuplicateApiAtisResponseBody>> result = consultaEstadoAtisClient.post(apiRequestBody);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    @Override
    public TdpServicePedido getEstadoAtis(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.atis");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_ATIS);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (tdpServicePedidos.getPedidoEstadoCode() != null && contingenciaStopEstadoAtis.indexOf("|" + tdpServicePedidos.getPedidoEstadoCode() + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                try {
                    DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                    apiRequestBody.setPeticion(codigoPedido);
                    ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                    if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                        if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_ATIS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(apiResponse.getBodyOut().getEstadoPeticion(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                }
            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoAtis Service.", ex);
        }

        return tdpServicePedidos;
    }

}
