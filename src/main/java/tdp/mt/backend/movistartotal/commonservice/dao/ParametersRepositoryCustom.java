package tdp.mt.backend.movistartotal.commonservice.dao;

import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;

public interface ParametersRepositoryCustom {

    Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

}
