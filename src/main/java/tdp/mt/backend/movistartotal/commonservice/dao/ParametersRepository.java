package tdp.mt.backend.movistartotal.commonservice.dao;

		import org.springframework.data.jpa.repository.JpaRepository;
		import org.springframework.data.jpa.repository.Modifying;
		import org.springframework.data.jpa.repository.Query;
		import org.springframework.stereotype.Repository;
		import org.springframework.transaction.annotation.Transactional;
		import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;

		import java.util.List;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Integer>, ParametersRepositoryCustom {

	Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

	List<Parameters> findByDomainAndCategoryAndElement(String domain, String category, String element);
	List<Parameters> findByDomainAndCategoryOrderByAuxiliarAsc(String domain, String category);

	List<Parameters> findByDomainAndCategoryAndStrfilter(String domain, String category, String strfilter);

	@Query("SELECT p FROM Parameters p WHERE p.domain = 'GEOCODIFICAR_DIRECCION'")
	List<Parameters> readTypeViaAndUrb();

	@Query("SELECT p FROM Parameters p WHERE p.element = 'mt.azure.legados18.container'")
	Parameters getNameContainer();

	@Transactional
	@Modifying
	@Query("update Parameters p set p.strValue=?1 where p.element = ?2")
	void UpdateContainerAzureParameters(String container, String acceso);
	
	Parameters findByDomain(String domain);
}
