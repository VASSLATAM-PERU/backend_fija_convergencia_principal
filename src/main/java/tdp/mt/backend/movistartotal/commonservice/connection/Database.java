package tdp.mt.backend.movistartotal.commonservice.connection;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import tdp.mt.backend.movistartotal.main.util.PropertiesUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

@Configuration
public class Database {
    private static final Logger logger = LogManager.getLogger();
    //private static HikariDataSource datasource;
    static Properties prop = new Properties();
    static Connection con;

    /*
    @Bean(name = "dataSource", destroyMethod = "close")
    public static DataSource datasource() {

        if (datasource == null) {
            final HikariConfig config = new HikariConfig();
            config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
            config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
            config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
            config.setPassword(System.getenv("TDP_FIJA_DB_PW"));

            //config.addDataSourceProperty("ApplicationName", System.getenv("TDP_FIJA_DB_APLICATION_NAME"));
            config.setMinimumIdle(Integer.parseInt(System.getenv("TDP_FIJA_DB_MINIMUM_IDLE")));
            config.setMaximumPoolSize(Integer.parseInt(System.getenv("TDP_FIJA_DB_POOLING")));
            config.setIdleTimeout(Integer.parseInt(System.getenv("TDP_FIJA_DB_TIMEOUT_IDLE")));

            datasource = new HikariDataSource(config);
        }
        return datasource;
    }
    */

    @Bean
    public static DataSource dataSource() {
        boolean isProduction = System.getenv("FIJA_ENVIROMENT")!=null?System.getenv("FIJA_ENVIROMENT").equals("production"):false;
        isProduction = true;
        String propertiesFile = isProduction ? "applicationPrd.properties" : "application.properties";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(PropertiesUtil.getPropertyFromFile(propertiesFile,"spring.datasource.driver-class-name"));
        dataSource.setUrl(PropertiesUtil.getPropertyFromFile(propertiesFile,"spring.datasource.url"));
        dataSource.setUsername(PropertiesUtil.getPropertyFromFile(propertiesFile,"spring.datasource.username"));
        dataSource.setPassword(PropertiesUtil.getPropertyFromFile(propertiesFile,"spring.datasource.password"));
        return dataSource;
    }
}