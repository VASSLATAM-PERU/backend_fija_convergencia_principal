package tdp.mt.backend.movistartotal.commonservice.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ParametersRepositoryImpl implements ParametersRepositoryCustom {

    private static final Logger logger = LogManager.getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element) {

        List<Parameters> data = new ArrayList<>();
        try {
            String sql = "select p.auxiliar, p.domain, p.category, p.element, p.strvalue, p.strfilter "
                       + "from parameters p "
                       + "where p.domain = :domain and p.category = :category AND p.element = :element "
                       + "order by p.auxiliar desc";

            LogVass.daoQuery(logger, sql
                    .replace(":domain", "'" + domain + "'")
                    .replace(":category", "'" + category + "'")
                    .replace(":element", "'" + element + "'"));

            Query q = em.createNativeQuery(sql);
            q.setParameter("domain", domain);
            q.setParameter("category", category);
            q.setParameter("element", element);

            List<Object[]> result = q.getResultList();
            for (Object[] row : result) {
                Parameters parameters = mapRowToParameters(row);

                data.add(parameters);
            }
        } catch (Exception e) {
            logger.error("Error findOneByDomainAndCategoryAndElement DAO.", e);
        }
        return data.get(0);
    }


    private Parameters mapRowToParameters(Object[] row) {

        Integer auxiliar = (Integer) row[0];
        String domain = (String) row[1];
        String category = (String) row[2];
        String element = (String) row[3];
        String strvalue = (String) row[4];
        String strfilter = (String) row[5];

        Parameters parameters = new Parameters();
        parameters.setAuxiliar(auxiliar);
        parameters.setDomain(domain);
        parameters.setCategory(category);
        parameters.setElement(element);
        parameters.setStrValue(strvalue);
        parameters.setStrfilter(strfilter);

        return parameters;
    }

}
