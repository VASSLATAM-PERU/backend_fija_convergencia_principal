package tdp.mt.backend.movistartotal.commonservice.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.clients.experto.ExpertoClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.experto.OffersApiExpertoRequestBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.experto.OffersApiExpertoResponseBody;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.commonservice.services.ExpertoService;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;
import tdp.mt.backend.movistartotal.main.util.ServiceUrlUtil;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.sql.Timestamp;

@Service
public class ExpertoServiceImpl implements ExpertoService {
	private ApiHeaderConfig apiHeaderConfig;
	//private ServiceCallEventsService serviceCallEventsService;

	@Autowired
	public ExpertoServiceImpl (ApiHeaderConfig apiHeaderConfig) {
		this.apiHeaderConfig = apiHeaderConfig;
	}

	@Autowired
	private ServiceCallEventsService serviceCallEventsService;
/*
	@Autowired
	public ExpertoServiceImpl (ApiHeaderConfig apiHeaderConfig, ServiceCallEventsService serviceCallEventsService) {
		this.apiHeaderConfig = apiHeaderConfig;
		this.serviceCallEventsService = serviceCallEventsService;
	}
*/
	@Override
	public ApiResponse<OffersApiExpertoResponseBody> scoring(OffersApiExpertoRequestBody apiExpertoRequestBody) throws ClientException {

		ExpertoClient expertoClient = new ExpertoClient(new ClientConfig());
		Timestamp t = UtilMethods.getFechaActual();
		ClientResult<ApiResponse<OffersApiExpertoResponseBody>> result = expertoClient.post(apiExpertoRequestBody);
		ApiResponse<OffersApiExpertoResponseBody> objExperto = result.getResult();
		serviceCallEventsService.registerEventEnd(result.getEvent(), t);

		if (!result.isSuccess()) {
			throw result.getE();
		}
		return objExperto;
	}

}
