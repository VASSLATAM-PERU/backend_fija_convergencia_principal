package tdp.mt.backend.movistartotal.main.restclient.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfoAuditoriaWSInput {

    @JsonProperty("ipServidor")
    private String ipServidor;
    @JsonProperty("hostName")
    private String hostName;
    @JsonProperty("fechaInvocacion")
    private String fechaInvocacion;
    @JsonProperty("usuarioIdentificador")
    private String usuarioIdentificador;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The ipServidor
     */
    @JsonProperty("ipServidor")
    public String getIpServidor() {
        return ipServidor;
    }

    /**
     * 
     * @param ipServidor
     *     The ipServidor
     */
    @JsonProperty("ipServidor")
    public void setIpServidor(String ipServidor) {
        this.ipServidor = ipServidor;
    }

    /**
     * 
     * @return
     *     The hostName
     */
    @JsonProperty("hostName")
    public String getHostName() {
        return hostName;
    }

    /**
     * 
     * @param hostName
     *     The hostName
     */
    @JsonProperty("hostName")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * 
     * @return
     *     The fechaInvocacion
     */
    @JsonProperty("fechaInvocacion")
    public String getFechaInvocacion() {
        return fechaInvocacion;
    }

    /**
     * 
     * @param fechaInvocacion
     *     The fechaInvocacion
     */
    @JsonProperty("fechaInvocacion")
    public void setFechaInvocacion(String fechaInvocacion) {
        this.fechaInvocacion = fechaInvocacion;
    }

    /**
     * 
     * @return
     *     The usuarioIdentificador
     */
    @JsonProperty("usuarioIdentificador")
    public String getUsuarioIdentificador() {
        return usuarioIdentificador;
    }

    /**
     * 
     * @param usuarioIdentificador
     *     The usuarioIdentificador
     */
    @JsonProperty("usuarioIdentificador")
    public void setUsuarioIdentificador(String usuarioIdentificador) {
        this.usuarioIdentificador = usuarioIdentificador;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
