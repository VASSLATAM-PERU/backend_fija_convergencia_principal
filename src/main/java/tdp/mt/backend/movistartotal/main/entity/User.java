package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user", schema = "ibmx_a07e6d02edaf552")
public class User {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "pwd")
    private String pwd;
    @Column(name = "phone")
    private String phone;
    @Column(name = "imei")
    private String imei;
    @Column(name = "resetpwd")
    private String resetPwd;
    @Column(name = "previouspwd")
    private String previousPwd;
    @Column(name = "registeredtime")
    private Date registeredTime;
    @Column(name = "lastupdatetime")
    private Date lastUpdateTime;
    @Column(name = "status")
    private Character status;
    @Column(name = "nrointentos")
    private Integer numeroIntentos;
    @Column(name = "lastaccess")
    private Date ultimoAcceso;

    public Integer getNumeroIntentos() {
        return numeroIntentos;
    }

    public void setNumeroIntentos(Integer numeroIntentos) {
        this.numeroIntentos = numeroIntentos;
    }

    public Date getUltimoAcceso() {
        return ultimoAcceso;
    }

    public void setUltimoAcceso(Date ultimoAcceso) {
        this.ultimoAcceso = ultimoAcceso;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getResetPwd() {
        return resetPwd;
    }

    public void setResetPwd(String resetPwd) {
        this.resetPwd = resetPwd;
    }

    public String getPreviousPwd() {
        return previousPwd;
    }

    public void setPreviousPwd(String previousPwd) {
        this.previousPwd = previousPwd;
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}
