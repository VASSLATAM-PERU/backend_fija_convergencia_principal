package tdp.mt.backend.movistartotal.main.service.impl;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.file.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonservice.dao.ParametersRepository;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;
import tdp.mt.backend.movistartotal.main.dao.MTAzureBlobStorageDao;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.entity.AutomatizadorSalesService;
import tdp.mt.backend.movistartotal.main.entity.MTAzureFileStorage;
import tdp.mt.backend.movistartotal.main.entity.OrdenMT;
import tdp.mt.backend.movistartotal.main.entity.TdpVisor;
import tdp.mt.backend.movistartotal.main.repository.AutomatizadorRequestSaleService;
import tdp.mt.backend.movistartotal.main.repository.MTAzureBlobStorageRepository;
import tdp.mt.backend.movistartotal.main.repository.MTAzureFileStorageRepository;
import tdp.mt.backend.movistartotal.main.repository.OrdenMTRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpOrderRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpVisorRepository;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.AutomatizadorSaleRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.InsertResponseBodyAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.dto.ApiResponseAuto;
import tdp.mt.backend.movistartotal.main.service.AutomatizadorService;
import tdp.mt.backend.movistartotal.main.service.BatchService;
import tdp.mt.backend.movistartotal.main.service.TGestionaService;
import tdp.mt.backend.movistartotal.main.util.ExcelWriter;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;

import java.io.*;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;


@Service
public class BatchServiceImpl implements BatchService {

    @Autowired
    private MTAzureFileStorageRepository mtAzureFileStorageRepository;

    @Autowired
    private MTAzureBlobStorageDao mtAzureBlobStorageDao;

    private static final Log log = LogFactory.getLog(BatchServiceImpl.class);

    @Value("${mt.azure.legados18.protocol}")
    private String protocolAzure;
    @Value("${mt.azure.legados18.name}")
    private String nameAzure;
    @Value("${mt.azure.legados18.key}")
    private String keyAzure;
    @Value("${mt.azure.legados18.suffix}")
    private String suffixAzure;
    //@Value("${mt.azure.legados18.container}")
    //private String containerAzure;
    @Value("${mt.batch.tgestiona.cantidad}")
    private String tgestionaReintento;

    @Value("${mt.batch.azure.endtime}")
    private String endtime;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    @Autowired
    private ParametersRepository parametersRepository;

    @Autowired
    private TdpVisorRepository tdpVisorRepository;

    @Autowired
    private TGestionaService tGestionaService;

    @Autowired
    private MTAzureBlobStorageRepository mtAzureBlobStorageRepository;

    @Autowired
    private OrdenMTRepository ordenMTRepository;

    @Autowired
    private AzureEmailServiceImpl azureEmailService;
    
    @Autowired
    private AutomatizadorRequestSaleService automatizadorRepository;
    
    @Autowired
    private AutomatizadorServiceImpl automatizadorService;
    
    @Value("${tdp.automatizador.errBusParametrizable}")
	private String errBusParametrizable;
    @Autowired
	private TdpOrderRepository tdpOrderRepository;
    
    @Override
    public void reintentoTgestiona() {
        List<Object[]> listMT = tdpVisorRepository.searchMTEnviando();
        if (listMT.size() > 0) {
            for (Object[] mt : listMT) {
                int reint = mt[3] == null ? 0 : Integer.parseInt(mt[3] + "");
                String idVisor = mt[0] + "";
                if (reint < Integer.parseInt(tgestionaReintento)) {
                    FijaHDECResponse fijaHDECResponse = tGestionaService.sendTGestionaInsertOrder(idVisor);
                    switch (fijaHDECResponse.getEstadoTGestiona()) {
                        case ServiceConstants.TGESTIONA_MT_PENDIENTE:
                            tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_PENDIENTE, idVisor);
                            break;
                        case ServiceConstants.TGESTIONA_MT_ENVIANDO:
                            tdpVisorRepository.updateCntIntentosHdecVentaMT(ServiceConstants.TGESTIONA_MT_ENVIANDO, reint + 1, idVisor);
                            break;
                        case ServiceConstants.TGESTIONA_MT_CAIDA:
                            tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_CAIDA, idVisor);
                            break;
                    }
                } else {
                    //CAIDA
                    tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_CAIDA, idVisor);
                }
            }
        }
    }

    @Override
    public void procesarExcelCustodiaToAzure() throws Exception {
        List<String> _date = getDate();
        String container = _date.get(0) + _date.get(1) + _date.get(2);

        //Credenciales azure FILE SHARE
        String storageConnectionString = storageConnectionString(protocolAzure, nameAzure, keyAzure, suffixAzure);

        //Listar los audios procesados
        List<Object[]> procesadosExcelGSM = mtAzureBlobStorageRepository.getFilesProccessAzureBlob(container);
        List<Object[]> procesadosExcelPdfPNG = mtAzureBlobStorageRepository.getPdfAndPngProccessAzureBlob(container);

        if (procesadosExcelGSM.size() > 0 || procesadosExcelPdfPNG.size() > 0) {

            //Generar Archivo excel Local
            String file_xls = exportarPedido(procesadosExcelGSM, procesadosExcelPdfPNG);

            //Conecatarse a un blobClient - Azure
            CloudBlobClient blobClient = getBlobClient(storageConnectionString);

            //Subir archivo al container blob - Azure
            uploadFileXLSInBlobStorage(blobClient, file_xls, container);

            //Eliminar archivo xls
            deleteFileS(file_xls);
            log.info("Exportación de Excel finalizado.");
        }
    }

    @Override
    public void proccessFilesToAzure() throws Exception {
        List<String> _date = getDate();
        String container = _date.get(0) + _date.get(1) + _date.get(2);

        //Credenciales azure FILE SHARE
        String storageConnectionString = storageConnectionString(protocolAzure, nameAzure, keyAzure, suffixAzure);


        List<MTAzureFileStorage> fileStoragesGeneral = mtAzureFileStorageRepository.getListFileStorageAudio();

        List<MTAzureFileStorage> fileStorages = new ArrayList<>();

        if (fileStoragesGeneral.size() >= 5) {
            for (int i = 0; i < 5; i++) {
                fileStorages.add(fileStoragesGeneral.get(i));
            }
        } else {
            for (int i = 0; i < fileStoragesGeneral.size(); i++) {
                fileStorages.add(fileStoragesGeneral.get(i));
            }
        }

        if (fileStorages.size() > 0) {
            for (MTAzureFileStorage fileStorage : fileStorages) {
                try {
                    //Conectarse a un fileClient - Azure
                    CloudFileClient fileClient = getFileClient(storageConnectionString);

                    //Descargar File gsm desde nuestro local y eliminar del fileStorage
                    File gsm = downloadFileToFileStorageInLocal(fileClient, fileStorage);

                    if (gsm == null) {
                        continue;
                    }

                    //Conecatarse a un blobClient - Azure
                    CloudBlobClient blobClient = getBlobClient(storageConnectionString);

                    //Subir un File desde nuestro local hacia un Blob Storage y eliminar del fileStorage
                    boolean upload = uploadFileToLocalInBlobStorage(blobClient, fileStorage, container);

                    //Eliminar el archivo local
                    deleteFile(gsm);

                    //Cambiar estado de procesamiento (mt_azure_file_storage --> mt_azure_blob_storage)
                    mtAzureBlobStorageDao.registerAzureBlobStorage(fileStorage.getMtorder(), container);

                    //Eliminar registro de mt_azure_file_storage
                    mtAzureFileStorageRepository.deleteMTAzureFileStorage(fileStorage.getMtorder());

                    //Eliminar el archivo del Azure - File Storage
                    if (upload) {
                        deleteFileToFileStorageInLocal(fileClient, fileStorage);
                    }
                } catch (InvalidKeyException ex) {
                    log.error("InvalidKeyException: " + ex.getMessage());
                } catch (URISyntaxException ex) {
                    log.error("URISyntaxException: " + ex.getMessage());
                } catch (StorageException ex) {
                    log.error("StorageException: " + ex.getMessage());
                } catch (IOException e) {
                    log.error("StorageException: " + e.getMessage());
                }
            }
        }

    }

    @Override
    public void procesarFilesPDFAndPNGToAzure() throws Exception {
        List<String> _date = getDate();
        String container = _date.get(0) + _date.get(1) + _date.get(2);
        //Credenciales azure FILE SHARE
        String storageConnectionString = storageConnectionString(protocolAzure, nameAzure, keyAzure, suffixAzure);

        List<MTAzureFileStorage> fileStoragesGeneral = mtAzureFileStorageRepository.getListFileStorageRetail();

        List<MTAzureFileStorage> fileStorages = new ArrayList<>();
        if (fileStoragesGeneral.size() >= 10) {
            for (int i = 0; i < 10; i++) {
                fileStorages.add(fileStoragesGeneral.get(i));
            }
        } else {
            for (int i = 0; i < fileStoragesGeneral.size(); i++) {
                fileStorages.add(fileStoragesGeneral.get(i));
            }
        }

        if (fileStorages.size() > 0) {
            for (MTAzureFileStorage fileStorage : fileStorages) {
                try {
                    boolean upload = false;
                    CloudFileClient fileClient = null;
                    CloudBlobClient blobClient = null;

                    switch (fileStorage.getFileExtension()) {
                        case ServiceConstants.EXTENSION_PDF:
                            //Conectarse a un fileClient - Azure
                            fileClient = getFileClient(storageConnectionString);

                            //Descargar File gsm desde nuestro local y eliminar del fileStorage
                            File pdf = downloadFileToFileStorageInLocalWithExtension(fileClient, fileStorage, ServiceConstants.EXTENSION_PDF);

                            if (pdf == null) {
                                continue;
                            }

                            //Conecatarse a un blobClient - Azure
                            blobClient = getBlobClient(storageConnectionString);

                            //Subir un File desde nuestro local hacia un Blob Storage y eliminar del fileStorage
                            upload = uploadFileToLocalInBlobStorageWithExtension(blobClient, fileStorage, container, ServiceConstants.EXTENSION_PDF);

                            //Eliminar el archivo local
                            deleteFile(pdf);

                            //Eliminar el archivo del Azure - File Storage --- ENVIAR CORREO CONTRATO
                            if (upload) {
                                azureEmailService.sendRetailEmailProcess(fileStorage.getCodeMt());
                                //deleteFileToFileStorageInLocalWithExtension(fileClient, fileStorage, ServiceConstants.EXTENSION_PDF);
                            }

                            //Cambiar estado de procesamiento (mt_azure_file_storage --> mt_azure_blob_storage)
                            mtAzureBlobStorageDao.registerAzureBlobStorageRetail(fileStorage.getMtorder(), container);

                            //Eliminar registro de mt_azure_file_storage
                            mtAzureFileStorageRepository.deleteMTAzureFileStorageRetail(fileStorage.getMtorder());
                            break;
                        case ServiceConstants.EXTENSION_PNG:
                            //Conectarse a un fileClient - Azure
                            fileClient = getFileClient(storageConnectionString);

                            //Descargar File gsm desde nuestro local y eliminar del fileStorage
                            File png = downloadFileToFileStorageInLocalWithExtension(fileClient, fileStorage, ServiceConstants.EXTENSION_PNG);

                            if (png == null) {
                                continue;
                            }

                            //Conecatarse a un blobClient - Azure
                            blobClient = getBlobClient(storageConnectionString);

                            //Subir un File desde nuestro local hacia un Blob Storage y eliminar del fileStorage
                            upload = uploadFileToLocalInBlobStorageWithExtension(blobClient, fileStorage, container, ServiceConstants.EXTENSION_PNG);

                            //Eliminar el archivo local
                            deleteFile(png);

                            //Eliminar el archivo del Azure - File Storage
                            if (upload) {
                                //deleteFileToFileStorageInLocalWithExtension(fileClient, fileStorage, ServiceConstants.EXTENSION_PNG);
                            }

                            //Cambiar estado de procesamiento (mt_azure_file_storage --> mt_azure_blob_storage)
                            mtAzureBlobStorageDao.registerAzureBlobStorageRetail(fileStorage.getMtorder(), container);

                            //Eliminar registro de mt_azure_file_storage
                            mtAzureFileStorageRepository.deleteMTAzureFileStorageRetail(fileStorage.getMtorder());
                            break;
                    }
                } catch (InvalidKeyException ex) {
                    log.error("InvalidKeyException: " + ex.getMessage());
                } catch (URISyntaxException ex) {
                    log.error("URISyntaxException: " + ex.getMessage());
                } catch (StorageException ex) {
                    log.error("StorageException: " + ex.getMessage());
                } catch (IOException e) {
                    log.error("StorageException: " + e.getMessage());
                }
            }
        }
    }


    private void deleteFileS(String filename) {
        File file = new File(filename);
        file.delete();
    }

    private String exportarPedido(List<Object[]> listSalesAudio, List<Object[]> listSalesRetail) throws IOException, InvalidFormatException {
        String[] columns = {
                "ID_GRABACION",
                "IDENTIFICADOR_UNICO",
                "CANAL_VENTA_PROV",
                "NUMERO_CELULAR_PROV",
                "NUMERO_LINEA_PROV",
                "FECHA_DOCUMENTO_PROV",
                "NOMBRE_PROVEEDOR_PROV",
                "NOMBRE_CAMPAÑA_PROV",
                "NUMERO_DOCUMENTO_PROV",
                "NOMBRE_CLIENTE_PROV",
                "NEGOCIO_PROV",
                "DESAFILIACION_DE_PAGINAS_BLANCAS_PROV",
                "AFILIACION_FACTURA_DIGITAL_PROV",
                "PROTECCION_DE_DATOS_PROV"
        };

        List<String[]> list = new ArrayList<>();
        List<Object[]> listAudio = listSalesAudio;
        for (int i = 0; i < listAudio.size(); i++) {
            String[] item = {
                    listAudio.get(i)[0] != null ? listAudio.get(i)[0].toString() : "",
                    listAudio.get(i)[1] != null ? listAudio.get(i)[1].toString() : "",
                    listAudio.get(i)[2] != null ? listAudio.get(i)[2].toString() : "",
                    listAudio.get(i)[3] != null ? listAudio.get(i)[3].toString() : "",
                    listAudio.get(i)[4] != null ? listAudio.get(i)[4].toString() : "",
                    listAudio.get(i)[5] != null ? listAudio.get(i)[5].toString() : "",
                    "TGESTIONA",
                    listAudio.get(i)[6] != null ? listAudio.get(i)[6].toString() : "",
                    listAudio.get(i)[7] != null ? listAudio.get(i)[7].toString() : "",
                    listAudio.get(i)[8] != null ? listAudio.get(i)[8].toString() : "",
                    "MOVIL",
                    listAudio.get(i)[9] != null ? listAudio.get(i)[9].toString() : "",
                    listAudio.get(i)[10] != null ? listAudio.get(i)[10].toString() : "",
                    listAudio.get(i)[11] != null ? listAudio.get(i)[11].toString() : "",
            };
            list.add(item);
        }

        if (listSalesRetail.size() > 0) {
            List<Object[]> listRetail = listSalesRetail;
            for (int i = 0; i < listRetail.size(); i++) {
                String[] item = {
                        listRetail.get(i)[0] != null ? listRetail.get(i)[0].toString() : "",
                        listRetail.get(i)[1] != null ? listRetail.get(i)[1].toString() : "",
                        listRetail.get(i)[2] != null ? listRetail.get(i)[2].toString() : "",
                        listRetail.get(i)[3] != null ? listRetail.get(i)[3].toString() : "",
                        listRetail.get(i)[4] != null ? listRetail.get(i)[4].toString() : "",
                        listRetail.get(i)[5] != null ? listRetail.get(i)[5].toString() : "",
                        "TGESTIONA",
                        listRetail.get(i)[6] != null ? listRetail.get(i)[6].toString() : "",
                        listRetail.get(i)[7] != null ? listRetail.get(i)[7].toString() : "",
                        listRetail.get(i)[8] != null ? listRetail.get(i)[8].toString() : "",
                        "MOVIL",
                        listRetail.get(i)[9] != null ? listRetail.get(i)[9].toString() : "",
                        listRetail.get(i)[10] != null ? listRetail.get(i)[10].toString() : "",
                        listRetail.get(i)[11] != null ? listRetail.get(i)[11].toString() : "",
                };
                list.add(item);
            }
        }

        String strDate = getDate().get(0) + getDate().get(1) + getDate().get(2);
        String filename = strDate + ".xlsx";
        ExcelWriter excelWriter = new ExcelWriter(columns, filename);
        excelWriter.Create(list);
        return filename;
    }


    private boolean uploadFileToLocalInBlobStorage(CloudBlobClient blobClient, MTAzureFileStorage fileStorage, String container) {
        boolean flag = false;
        try {
            CloudBlobContainer blobContainer = blobClient.getContainerReference(container);
            if (blobContainer.createIfNotExists()) {
                log.info("New container created");
            }
            final String path = fileStorage.getMtorder() + ".gsm";
            CloudBlockBlob blob = blobContainer.getBlockBlobReference(fileStorage.getMtorder() + ".gsm");
            File source = new File(path);
            FileInputStream fileInputStream = new FileInputStream(source);
            blob.upload(fileInputStream, source.length());
            log.info("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            fileInputStream.close();
            flag = true;
            ServiceCallEvent log = new ServiceCallEvent();
            log.setServiceCode("AZURE_BLOB_MT");
            log.setServiceRequest("Orden MT: " + fileStorage.getMtorder());
            log.setServiceResponse("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            log.setOrderId(fileStorage.getMtorder());
            log.setResult("OK");
            log.setMsg("Archivo cargado: " + path);
            serviceCallEventsService.registerEvent(log);
            return flag;
        } catch (IOException e) {
            log.error("Erro: " + e.getMessage());
        } catch (StorageException e) {
            log.error("Erro: " + e.getMessage());
        } catch (URISyntaxException e) {
            log.error("Erro: " + e.getMessage());
        }
        return flag;
    }

    private boolean uploadFileToLocalInBlobStorageWithExtension(CloudBlobClient blobClient, MTAzureFileStorage fileStorage, String container, String extension) {
        boolean flag = false;
        try {
            CloudBlobContainer blobContainer = blobClient.getContainerReference(container);
            if (blobContainer.createIfNotExists()) {
                log.info("New container created");
            }
            final String path = fileStorage.getMtorder() + extension;
            CloudBlockBlob blob = blobContainer.getBlockBlobReference(fileStorage.getMtorder() + extension);
            File source = new File(path);
            FileInputStream fileInputStream = new FileInputStream(source);
            blob.upload(fileInputStream, source.length());
            log.info("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            fileInputStream.close();
            flag = true;
            ServiceCallEvent log = new ServiceCallEvent();
            log.setServiceCode("AZURE_BLOB_MT");
            log.setServiceRequest("Orden MT: " + fileStorage.getMtorder());
            log.setServiceResponse("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            log.setOrderId(fileStorage.getMtorder());
            log.setResult("OK");
            log.setMsg("Archivo cargado: " + path);
            serviceCallEventsService.registerEvent(log);
            return flag;
        } catch (IOException e) {
            log.error("Erro: " + e.getMessage());
        } catch (StorageException e) {
            log.error("Erro: " + e.getMessage());
        } catch (URISyntaxException e) {
            log.error("Erro: " + e.getMessage());
        }
        return flag;
    }

    private void uploadFileXLSInBlobStorage(CloudBlobClient blobClient, String archivo, String container) {

        try {
            CloudBlobContainer blobContainer = blobClient.getContainerReference(container);
            if (blobContainer.createIfNotExists()) {
                log.info("New container created");
            }
            final String path = archivo;
            CloudBlockBlob blob = blobContainer.getBlockBlobReference(archivo);
            File source = new File(path);
            FileInputStream fileInputStream = new FileInputStream(source);
            blob.upload(fileInputStream, source.length());
            log.info("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            fileInputStream.close();
            ServiceCallEvent log = new ServiceCallEvent();
            log.setServiceCode("AZURE_BLOB_MT");
            log.setServiceRequest(archivo);
            log.setServiceResponse("Archivo cargado: " + path + ", incluido en el contenedor " + container);
            log.setOrderId(archivo);
            log.setResult("OK");
            log.setMsg("Archivo cargado: " + path);
            serviceCallEventsService.registerEvent(log);
        } catch (IOException e) {
            log.error("Erro: " + e.getMessage());
        } catch (StorageException e) {
            log.error("Erro: " + e.getMessage());
        } catch (URISyntaxException e) {
            log.error("Erro: " + e.getMessage());
        }
    }


    private String storageConnectionString(String DefaultEndpointsProtocol, String AccountName, String AccountKey, String EndpointSuffix) {
        String _DefaultEndpointsProtocol = "DefaultEndpointsProtocol=" + DefaultEndpointsProtocol;
        String _AccountName = ";AccountName=" + AccountName;
        String _AccountKey = ";AccountKey=" + AccountKey;
        String _EndpointSuffix = ";EndpointSuffix=" + EndpointSuffix;
        String storageConnectionString = _DefaultEndpointsProtocol + _AccountName + _AccountKey + _EndpointSuffix;

        return storageConnectionString;
    }

    private CloudFileClient getFileClient(String storageConnectionString) throws URISyntaxException, InvalidKeyException {
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
        CloudFileClient fileClient = storageAccount.createCloudFileClient();
        return fileClient;
    }

    private CloudBlobClient getBlobClient(String storageConnectionString) throws URISyntaxException, InvalidKeyException {
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
        CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
        return blobClient;
    }

    private File downloadFileToFileStorageInLocal(CloudFileClient fileClient, MTAzureFileStorage fileStorage) throws URISyntaxException, StorageException, IOException {
        for (CloudFileShare share : fileClient.listShares()) {
            if (share.getName().equals(fileStorage.getFileDate())) {
                Iterable<ListFileItem> results = share.getRootDirectoryReference().listFilesAndDirectories();
                for (Iterator<ListFileItem> itr = results.iterator(); itr.hasNext(); ) {
                    ListFileItem item = itr.next();
                    if (item.getUri().toString().contains(fileStorage.getMtorder())) {
                        //Descargar el archivo desde Azure FileStorage
                        CloudFileDirectory rootDir = share.getRootDirectoryReference();
                        CloudFile file = rootDir.getFileReference(fileStorage.getMtorder() + ".gsm");
                        File sourceFile = new File(fileStorage.getMtorder() + ".gsm");
                        file.downloadToFile(sourceFile.getAbsolutePath());
                        log.info("El archivo " + fileStorage.getMtorder() + ".gsm se descargo completamente y viene del directorio " + fileStorage.getFileDate());
                        //file.delete(); //Eliminar archivo desde el File Storage
                        return sourceFile;
                    }
                }
            }
        }

        log.info("El archivo " + fileStorage.getMtorder() + ".gsm no se encuetra en el directorio " + fileStorage.getFileDate());
        return null;
    }

    private File downloadFileToFileStorageInLocalWithExtension(CloudFileClient fileClient, MTAzureFileStorage fileStorage, String extension) throws URISyntaxException, StorageException, IOException {
        for (CloudFileShare share : fileClient.listShares()) {
            if (share.getName().equals(fileStorage.getFileDate())) {
                Iterable<ListFileItem> results = share.getRootDirectoryReference().listFilesAndDirectories();
                for (Iterator<ListFileItem> itr = results.iterator(); itr.hasNext(); ) {
                    ListFileItem item = itr.next();
                    if (item.getUri().toString().contains(fileStorage.getMtorder())) {
                        //Descargar el archivo desde Azure FileStorage
                        CloudFileDirectory rootDir = share.getRootDirectoryReference();
                        CloudFile file = rootDir.getFileReference(fileStorage.getMtorder() + extension);
                        File sourceFile = new File(fileStorage.getMtorder() + extension);
                        file.downloadToFile(sourceFile.getAbsolutePath());
                        log.info("El archivo " + fileStorage.getMtorder() + extension + " se descargo completamente y viene del directorio " + fileStorage.getFileDate());
                        //file.delete(); //Eliminar archivo desde el File Storage
                        return sourceFile;
                    }
                }
            }
        }

        log.info("El archivo " + fileStorage.getMtorder() + extension + " no se encuetra en el directorio " + fileStorage.getFileDate());
        return null;
    }

    private void deleteFileToFileStorageInLocal(CloudFileClient fileClient, MTAzureFileStorage fileStorage) throws URISyntaxException, StorageException, IOException {
        for (CloudFileShare share : fileClient.listShares()) {
            if (share.getName().equals(fileStorage.getFileDate())) {
                Iterable<ListFileItem> results = share.getRootDirectoryReference().listFilesAndDirectories();
                for (Iterator<ListFileItem> itr = results.iterator(); itr.hasNext(); ) {
                    ListFileItem item = itr.next();
                    if (item.getUri().toString().contains(fileStorage.getMtorder())) {
                        //Descargar el archivo desde Azure FileStorage
                        CloudFileDirectory rootDir = share.getRootDirectoryReference();
                        CloudFile file = rootDir.getFileReference(fileStorage.getMtorder() + ".gsm");
                        //File sourceFile = new File(fileStorage.getMtorder() + ".gsm");
                        //file.downloadToFile(sourceFile.getAbsolutePath());
                        file.delete(); //Eliminar archivo desde el File Storage
                        log.info("El archivo " + fileStorage.getMtorder() + ".gsm se elimino completamente y viene del directorio " + fileStorage.getFileDate());
                        return;
                    }
                }
            }
        }
        log.info("El archivo " + fileStorage.getMtorder() + ".gsm no se encuetra en el directorio " + fileStorage.getFileDate());
        return;
    }

    private void deleteFileToFileStorageInLocalWithExtension(CloudFileClient fileClient, MTAzureFileStorage fileStorage, String extension) throws URISyntaxException, StorageException, IOException {
        for (CloudFileShare share : fileClient.listShares()) {
            if (share.getName().equals(fileStorage.getFileDate())) {
                Iterable<ListFileItem> results = share.getRootDirectoryReference().listFilesAndDirectories();
                for (Iterator<ListFileItem> itr = results.iterator(); itr.hasNext(); ) {
                    ListFileItem item = itr.next();
                    if (item.getUri().toString().contains(fileStorage.getMtorder())) {
                        //Descargar el archivo desde Azure FileStorage
                        CloudFileDirectory rootDir = share.getRootDirectoryReference();
                        CloudFile file = rootDir.getFileReference(fileStorage.getMtorder() + extension);
                        file.delete(); //Eliminar archivo desde el File Storage
                        log.info("El archivo " + fileStorage.getMtorder() + extension + " se elimino completamente y viene del directorio " + fileStorage.getFileDate());
                        return;
                    }
                }
            }
        }
        log.info("El archivo " + fileStorage.getMtorder() + extension + " no se encuetra en el directorio " + fileStorage.getFileDate());
        return;
    }

    private void deleteFile(File file) {
        log.info(file.getAbsolutePath());
        if (file.delete()) {
            log.info("El archivo data fue eliminado.");
        } else {
            log.info("El archivo data no existe.");
        }
        if (file != null) {
            file.delete();
        }
    }


    private List<String> getDate() {
        List<String> list = new ArrayList<>();

        Date today = new Date(); // Fri Jun 17 14:54:28 PDT 2016
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
        cal.setTime(today); // don't forget this if date is arbitrary e.g. 01-01-2014

        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH); // 17
        int month = cal.get(Calendar.MONTH); // 5
        int year = cal.get(Calendar.YEAR); // 2016
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);

        list.add(String.format("%02d", dayOfMonth));
        list.add(String.format("%02d", month + 1));
        list.add(String.valueOf(year));
        list.add(String.format("%02d", hour));
        list.add(String.format("%02d", minute));
        list.add(String.format("%02d", second));

        return list;
    }

    private List<String> getDatePost() {
        List<String> list = new ArrayList<>();

        Date today = new Date(); // Fri Jun 17 14:54:28 PDT 2016
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
        cal.setTime(today); // don't forget this if date is arbitrary e.g. 01-01-2014
        cal.add(Calendar.DAY_OF_MONTH, 1);

        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH); // 17
        int month = cal.get(Calendar.MONTH); // 5
        int year = cal.get(Calendar.YEAR); // 2016
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);

        list.add(String.format("%02d", dayOfMonth));
        list.add(String.format("%02d", month + 1));
        list.add(String.valueOf(year));
        list.add(String.format("%02d", hour));
        list.add(String.format("%02d", minute));
        list.add(String.format("%02d", second));

        return list;
    }
    
    @Override
    public void reintentoAutomatizador() {
    	Parameters param=parametersRepository.findByDomain(ServiceConstants.INDICADOR_LIMIT);
        List<TdpVisor> listMT1 = tdpVisorRepository.findByEstadoSolicitud(ServiceConstants.INDICADOR_BATCH_REINTENTOS, Integer.parseInt(param.getStrValue()));
        
        
        Parameters parameter=parametersRepository.findByDomain(ServiceConstants.INDICADOR_NUM_REINTENTO);
        
        int numReitento=Integer.parseInt(parameter.getStrValue());
        String[] erroresBuss = errBusParametrizable.split(",");
        int contReintento=0;
        
        if(listMT1.size()>0) {
        	boolean batch=false;
        	for(TdpVisor visor:listMT1) {
        		AutomatizadorSalesService auto=automatizadorRepository.findByCodvtappcd(visor.getIdVisor());
        		contReintento=visor.getCntIntentosAuto()==null?0:visor.getCntIntentosAuto();
        		if(auto!=null) {
        			batch=true;
        			
        		}else {
        			tdpVisorRepository.delete(visor.getIdVisor());
        		}
        		if(batch) {
        		//Envio Automatizador
        		if(contReintento<numReitento) {
        			AutomatizadorSaleRequestBody request = seteoRequestAuto(auto);
        			//automatizadorRepository.delete(auto);
        			try {
        				 //tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_PENDIENTE, visor.getIdVisor());
						ApiResponseAuto<InsertResponseBodyAuto> response =automatizadorService.sendInsertOrder(request);
						
						 if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
			                     response.getHeaderOut().getMsgType().equalsIgnoreCase("RESPONSE")) {
			                
			                 tdpOrderRepository.updateAutomatizadorOkTdpOrder(1, request.getOrderId());
			             }
						
						if (response != null && response.getBodyOut() != null) {
			                 if (response.getBodyOut().getServerException() != null) {
			                     for (String i : erroresBuss) {
			                         int res = Integer.parseInt(i);
			                         if (response.getBodyOut().getServerException().getExceptionCode() == res) {
			                                      
			                            //Cambiar estado de TDP_Visor a Enviando_Auto_MT
			                            //tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.INDICADOR_BATCH_REINTENTOS, request.getOrderId());
			                            tdpVisorRepository.updateCntIntentosAutoentaMT(ServiceConstants.INDICADOR_BATCH_REINTENTOS, contReintento+1, request.getOrderId());
			                            
			                         }

			                     }
			                 }
			             }
						 
					} catch (ClientException e) {
						e.printStackTrace();
						tdpVisorRepository.updateCntIntentosAutoentaMT(ServiceConstants.INDICADOR_BATCH_REINTENTOS, contReintento+1, request.getOrderId());
					}
        		//Envio TGestiona
	        		}else {
	        				FijaHDECResponse fijaHDECResponse = tGestionaService.sendTGestionaInsertOrder(visor.getIdVisor());
		           			 switch (fijaHDECResponse.getEstadoTGestiona()) {
		                        case ServiceConstants.TGESTIONA_MT_PENDIENTE:
		                            tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_PENDIENTE, visor.getIdVisor());
		                            break;
		                        case ServiceConstants.TGESTIONA_MT_ENVIANDO:
		                        	 tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_ENVIANDO, visor.getIdVisor());
		                          
		                            break;
		                        case ServiceConstants.TGESTIONA_MT_CAIDA:
		                            tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.TGESTIONA_MT_CAIDA, visor.getIdVisor());
		                            break;
		                    }
	        			 
	        		}
        		
        		}
        	}
        }
        
    }
    
    
    public AutomatizadorSaleRequestBody seteoRequestAuto(AutomatizadorSalesService auto) {
    	
    	AutomatizadorSaleRequestBody autoRequest= new AutomatizadorSaleRequestBody();
    	
    	autoRequest.setClienteNombre(auto.getNom_ds());
    	autoRequest.setClienteApellidoPaterno(auto.getPri_ape_ds());
    	autoRequest.setClienteApellidoMaterno(auto.getSeg_ape_ds());
        autoRequest.setClienteNumeroDoc(auto.getNum_doc_cli_cd());
        autoRequest.setClienteRucDigitoControl(auto.getDig_ctl_doc_cd());
        autoRequest.setClienteTipoDoc(auto.getTip_doc_cd());
        autoRequest.setClienteTelefono1(auto.getTel_pri_cot_cd());
        autoRequest.setClienteTelefono2(auto.getTel_seg_cot_cd());
        autoRequest.setClienteRucActividadCodigo(auto.getCod_act_eco_cd());
        autoRequest.setClienteRucSubActividadCodigo(auto.getCod_sac_eco_cd());
        autoRequest.setAddressCalleAtis(auto.getTip_cal_ati_cd());
        autoRequest.setAddressCalleNombre(auto.getNom_cal_ds());
        autoRequest.setAddressCalleNumero(auto.getNum_cal_nu());
        autoRequest.setAddressCCHHCodigo(auto.getCod_cnj_hbl_cd());
        autoRequest.setAddressCCHHTipo(auto.getTip_cnj_hbl_cd());
        autoRequest.setAddressCCHHNombre(auto.getNom_cnj_hbl_no());
        autoRequest.setAddressCCHHCompTipo1(auto.getPri_tip_cmp_cd());
        autoRequest.setAddressCCHHCompNombre1(auto.getPri_cmp_dir_ds());
        autoRequest.setAddressCCHHCompTipo2(auto.getSeg_tip_cmp_cd());
        autoRequest.setAddressCCHHCompNombre2(auto.getSeg_cmp_dir_ds());
        autoRequest.setAddressCCHHCompTipo3(auto.getTrc_tip_cmp_cd());
        autoRequest.setAddressCCHHCompNombre3(auto.getTrc_cmp_dir_ds());
        autoRequest.setAddressCCHHCompTipo4(auto.getCao_tip_cmp_cd());
        autoRequest.setAddressCCHHCompNombre4(auto.getCao_cmp_dir_ds());
        autoRequest.setAddressCCHHCompTipo5(auto.getQui_tip_cmp_cd());
        autoRequest.setAddressCCHHCompNombre5(auto.getQui_cmp_dir_ds());
        autoRequest.setAddressViaCompTipo1(auto.getSet_tip_cmp_cd());
        autoRequest.setAddressViaCompNombre1(auto.getSet_cmp_dir_ds());
        autoRequest.setAddressViaCompTipo2(auto.getSpt_tip_cmp_cd());
        autoRequest.setAddressViaCompNombre2(auto.getSpt_cmp_dir_ds());
        autoRequest.setAddressViaCompTipo3(auto.getOct_tip_cmp_cd());
        autoRequest.setAddressViaCompNombre3(auto.getOct_cmp_dir_ds());
        autoRequest.setAddressViaCompTipo4(auto.getNov_tip_cmp_cd());
        autoRequest.setAddressViaCompNombre4(auto.getNov_cmp_dir_ds());
        autoRequest.setAddressViaCompTipo5(auto.getDco_tip_cmp_cd());
        autoRequest.setAddressViaCompNombre5(auto.getDco_cmp_dir_ds());
        autoRequest.setOrderGisXDSL(auto.getMsx_cbr_voi_ges_in());
        autoRequest.setOrderGisHFC(auto.getMsx_cbr_voi_gis_in());
        autoRequest.setOrderGisTipoSenial(auto.getMsx_ind_snl_gis_cd());
        autoRequest.setOrderGisGPON(auto.getMsx_ind_gpo_gis_cd());
        autoRequest.setUserAtis(auto.getCod_fza_ven_cd());
        autoRequest.setUserFuerzaVenta(auto.getCod_fza_gen_cd());
        autoRequest.setUserCanalCodigo(auto.getCod_cnl_ven_cd());
        autoRequest.setProductoSenial(auto.getCod_ind_sen_cms());
        autoRequest.setProductoCabecera(auto.getCod_cab_cms());
        autoRequest.setProductoCodigo(auto.getId_cod_pro_cd());
        autoRequest.setProductoPSAdmin1(auto.getPs_adm_dep_1());
        autoRequest.setProductoPSAdmin2(auto.getPs_adm_dep_2());
        autoRequest.setProductoPSAdmin3(auto.getPs_adm_dep_3());
        autoRequest.setProductoPSAdmin4(auto.getPs_adm_dep_4());
        autoRequest.setProductoSVACodigo1(auto.getId_cod_sva_cd_1());
        autoRequest.setProductoSVACodigo2(auto.getId_cod_sva_cd_2());
        autoRequest.setProductoSVACodigo3(auto.getId_cod_sva_cd_3());
        autoRequest.setProductoSVACodigo4(auto.getId_cod_sva_cd_4());
        autoRequest.setProductoSVACodigo5(auto.getId_cod_sva_cd_5());
        autoRequest.setProductoSVACodigo6(auto.getId_cod_sva_cd_6());
        autoRequest.setProductoSVACodigo7(auto.getId_cod_sva_cd_7());
        autoRequest.setProductoSVACodigo8(auto.getId_cod_sva_cd_8());
        autoRequest.setProductoSVACodigo9(auto.getId_cod_sva_cd_9());
        autoRequest.setProductoSVACodigo10(auto.getId_cod_sva_cd_10());
        autoRequest.setOrderId(auto.getCodvtappcd());
        autoRequest.setOrderFecha(auto.getFec_vta_ff());
        autoRequest.setOrderParqueTelefono(auto.getNum_ide_tlf());
        autoRequest.setClienteCMSCodigo(auto.getCod_cli_ext_cd());
        autoRequest.setOrderOperacionComercial(auto.getTip_trx_cmr_cd());
        autoRequest.setClienteCMSServicio(auto.getCod_srv_cms_cd());
        autoRequest.setClienteNacimiento(auto.getFec_nac_ff());
        autoRequest.setClienteSexo(auto.getCod_sex_cd());
        autoRequest.setClienteCivil(auto.getCod_est_civ_cd());
        autoRequest.setClienteEmail(auto.getDes_mai_cli_ds());
        autoRequest.setClienteTelefono1Area(auto.getCod_are_te1_cd());
        autoRequest.setClienteTelefono2Area(auto.getCod_are_te2_cd());
        autoRequest.setClienteCMSFactura(auto.getCod_cic_fac_cd());
        autoRequest.setAddressDepartamentoCodigo(auto.getCod_dep_cd());
        autoRequest.setAddressProvinciaCodigo(auto.getCod_prv_cd());
        autoRequest.setAddressDistritoCodigo(auto.getCod_dis_cd());
        autoRequest.setOrderGisNTLT(auto.getCod_fac_tec_cd());
        autoRequest.setOrderGPSX(auto.getNum_cod_x_cd());
        autoRequest.setOrderGPSY(auto.getNum_cod_y_cd());
        autoRequest.setUserCSMPuntoVenta(auto.getCod_ven_cms_cd());
        autoRequest.setUserCMS(auto.getCod_sve_cms_cd());
        autoRequest.setUserNombre(auto.getNom_ven_ds());
        autoRequest.setAddressReferencia(auto.getRef_dir_ent_ds());
        autoRequest.setOrderCallTipo(auto.getCod_tip_cal_cd());
        autoRequest.setOrderCallDesc(auto.getDes_tip_cal_cd());
        autoRequest.setOrderModelo(auto.getTip_mod_equ_cd());
        autoRequest.setUserDNI(auto.getNum_doc_ven_cd());
        autoRequest.setUserTelefono(auto.getNum_tel_ven_ds());
        autoRequest.setOrderContrato(auto.getInd_env_con_cd());
        autoRequest.setOrderIDGrabacion(auto.getInd_id_gra_cd());
        autoRequest.setOrderModalidadAcep(auto.getMod_ace_ven_cd());
        autoRequest.setOrderEntidad(auto.getDet_can_ven_ds());
        autoRequest.setOrderRegion(auto.getCod_reg_cd());
        autoRequest.setOrderCIP(auto.getCod_cip_cd());
        autoRequest.setOrderExperto(auto.getCod_exp_cd());
        autoRequest.setOrderZonal(auto.getCod_zon_ven_cd());
        autoRequest.setOrderWebParental(auto.getInd_web_par_cd());
        autoRequest.setOrderCodigoSTC6I(auto.getCod_stc());
        autoRequest.setClienteNacionalidad(auto.getDes_nac_ds());
        autoRequest.setOrderModoVenta(auto.getCod_mod_ven_cd());
        autoRequest.setOrderCantidadEquipos(auto.getCan_equ_ven());
    	
    	
    	return autoRequest;
    }

}
