package tdp.mt.backend.movistartotal.main.restclient.automatizador.cliente;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.connection.Api;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextHolder;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.ApiRequestAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.ApiRequestHeaderAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.AutomatizadorSaleRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.dto.ApiResponseAuto;

public abstract class AbstractClientAuto<T, R> {
	public static final String SERVICE_CODE_CONSULTA_ESTADO_CMS = "ESTADOCMS";
    public static final String SERVICE_CODE_CONSULTA_ESTADO_ATIS = "ESTADOATIS";
    public static final String SERVICE_CODE_RENIEC = "RENIEC";
    public static final String SERVICE_CODE_BIOMETRIC = "BIOMETRIC";
    public static final String SERVICE_CODE_TGESTIONA = "TGESTIONA_MT";
    public static final String SERVICE_CODE_TGESTIONA2 = "TGESTIONA2";
	protected ClientConfig config;
    protected ServiceCallEvent event;
    
	public AbstractClientAuto(ClientConfig config) {
		super();
		this.config=config;
	}
	 protected ServiceCallEvent getEvent() {
	        if (event == null) {
	            if (VentaFijaContextHolder.getContext().getServiceCallEvent()==null) {
	                event = new ServiceCallEvent();
	            } else {
	                event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
	            }
	            event.setServiceUrl(config.getUrl());
	            event.setServiceCode(getServiceCodeAuto());
	        }
	        return event;
	    }
	 protected abstract String getServiceCodeAuto();
	 protected abstract ApiResponseAuto<R> getResponseAuto(String json) throws Exception;

	public ClientResult<ApiResponseAuto<R>> postAuto(AutomatizadorSaleRequestBody body) {
        ClientResult<ApiResponseAuto<R>> result = new ClientResult<>();

        try {

            String json = doRequestAuto(body);

            ApiResponseAuto<R> apiResponse = getResponseAuto(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
        	if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }
	
	protected String doRequestAuto(AutomatizadorSaleRequestBody body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequestAuto(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }
	
	protected ApiRequestAuto<T> getApiRequestAuto(AutomatizadorSaleRequestBody body) {
        ApiRequestAuto<T> apiRequest = new ApiRequestAuto<>();
        ApiRequestHeaderAuto apiRequestHeader = getHeaderAutomatizador(config.getOperation(), config.getDestination());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }
	 protected ApiRequestHeaderAuto getHeaderAutomatizador(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
	        ApiRequestHeaderAuto apiRequestHeader = new ApiRequestHeaderAuto();
	        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY_AUTO);
	        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG_AUTO);
	        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY_AUTO);
	        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM_AUTO);
	        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM_AUTO);
	        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR_AUTO);
	        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER_AUTO);
	        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USERID_AUTO);
	        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WSID_AUTO);
	        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WSIP_AUTO);
	        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
	        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
	        apiRequestHeader.setPid(Constants.API_REQUEST_HEADER_PID_AUTO);
	        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID);
	        apiRequestHeader.setMsgId(Constants.API_REQUEST_HEADER_MSGID_AUTO);
	        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
	        apiRequestHeader.setTimestamp(sdf.format(new Date()) + "-05:00");
	        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSGTYPE_AUTO);

	        return apiRequestHeader;
	    }
}
