package tdp.mt.backend.movistartotal.main.restclient.mailing;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserMailRequest {

    @JsonProperty("idConversation")
    private Integer idConversation;
    @JsonProperty("values")
    private Values_ values;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("idConversation")
    public Integer getIdConversation() {
        return idConversation;
    }

    @JsonProperty("idConversation")
    public void setIdConversation(Integer idConversation) {
        this.idConversation = idConversation;
    }

    @JsonProperty("values")
    public Values_ getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(Values_ values) {
        this.values = values;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}