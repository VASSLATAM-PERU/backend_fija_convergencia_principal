package tdp.mt.backend.movistartotal.main.domain.MTFija;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the mt_client_package_log database table.
 *
 */
public class MtClientPackageReq implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idPackage;

	//bi-directional many-to-one association to MtFixedProductReq
	private List<MtFixedProductReq> mtFixedProductReqs;

	public MtClientPackageReq() {
	}

	public Long getIdPackage() {
		return this.idPackage;
	}

	public void setIdPackage(Long idPackage) {
		this.idPackage = idPackage;
	}

	public List<MtFixedProductReq> getMtFixedProductReqs() {
		return this.mtFixedProductReqs;
	}

	public void setMtFixedProductReqs(List<MtFixedProductReq> mtFixedProductReqs) {
		this.mtFixedProductReqs = mtFixedProductReqs;
	}

	public MtFixedProductReq addMtFixedProductReq(MtFixedProductReq mtFixedProductReq) {
		getMtFixedProductReqs().add(mtFixedProductReq);

		return mtFixedProductReq;
	}

	public MtFixedProductReq removeMtFixedProductReq(MtFixedProductReq mtFixedProductReq) {
		getMtFixedProductReqs().remove(mtFixedProductReq);

		return mtFixedProductReq;
	}


}