package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "HeaderIn", "PreventaRequestBody" })
public class ApiRequestAuto<E> {

  private ApiRequestHeaderAuto HeaderIn;
  private AutomatizadorSaleRequestBody BodyIn;

  @JsonProperty("HeaderIn")
  public ApiRequestHeaderAuto getHeaderIn() {
    return HeaderIn;
  }

  public void setHeaderIn(ApiRequestHeaderAuto headerIn) {
    HeaderIn = headerIn;
  }

  @JsonProperty("BodyIn")
  public AutomatizadorSaleRequestBody getBodyIn() {
    return BodyIn;
  }

  public void setBodyIn(AutomatizadorSaleRequestBody bodyIn) {
    BodyIn = bodyIn;
  }  
  
}
