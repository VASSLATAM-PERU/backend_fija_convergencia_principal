package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.entity.TdpVisor;

import java.util.Date;
import java.util.List;

@Repository
public interface TdpVisorRepository extends JpaRepository<TdpVisor, String> {

    List<TdpVisor> findAllByCodigoPedidoAndDni(String codigoPedido, String dni);

    TdpVisor findOneByIdVisor(String idVisor);

    List<TdpVisor> findAllByDepartamentoAndProvinciaAndDistritoAndTipoDocumentoAndDniAndNombreProductoAndOperacionComercial(String departamento, String provincia, String distrito, String tipoDocumento, String dni, String nombreProducto, String operacionComercial);

    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_visor WHERE tipo_documento = ?1 AND dni = ?2 AND UPPER(operacion_comercial) IN ?3 AND fecha_grabacion > now() - INTERVAL '2 month' AND UPPER(estado_solicitud) != 'CAIDA'", nativeQuery = true)
    List<TdpVisor> findAllByTipoDocumentoAndDni(String tipoDocumento, String dni, List<String> operacionComercial);

    @Modifying
    @Query("update TdpVisor v set v.estadoSolicitud = 'ENVIANDO', v.motivoEstado = 'En proceso de recupero', v.telefono = ?1 where v.idVisor = ?2")
    void updateTelefonoAndIdVisor(String telefono, String idVisor);

    @Modifying
    @Query(value = "update TdpVisor v set v.estadoSolicitud = 'ENVIANDO', v.motivoEstado = 'En proceso de recupero', v.direccion = ?1 where v.idVisor = ?2")
    void updateDireccionTdpVisor(String direccion, String idVisor);

    @Modifying
    @Query("update TdpVisor v set v.estadoSolicitud = 'ENVIANDO', v.motivoEstado = 'En proceso de recupero' where v.idVisor = ?1")
    void updateStateTdpVisor(String idVisor);

    @Query(value = "SELECT " +
            "v.id_visor, v.estado_solicitud, v.nombre_producto, v.sub_producto, " +
            "to_char(v.fecha_grabacion, 'YYYY-MM-DD HH24:MI:SS') registrationdate, " +
            "o.id, o.appcode, v.motivo_estado, v.codigo_pedido  " +
            "FROM ibmx_a07e6d02edaf552.tdp_visor v " +
            "LEFT JOIN ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
            "WHERE v.tipo_documento = ?1 " +
            "AND v.dni = ?2 " +
            "AND v.fecha_grabacion >= ?3 "
            //"AND v.estado_solicitud not in ('NO EFECTIVO','CAIDA') "
            //"AND v.fecha_instalado IS NULL"
            , nativeQuery = true)
    List<Object[]> findAllFijaOrders(String tipoDocumento, String dni, Date dateLimit);

    @Query(value = "SELECT VIS.ID_VISOR, " +//0
            "VIS.FECHA_GRABACION, " +       //1
            "VIS.ESTADO_SOLICITUD, " +      //2
            "VIS.CNT_INTENTOS_HDEC, " +     //3
            "VIS.DNI, " +                   //4
            "ORD.USERID AS COD_ATIS " +     //5
            "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
            "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.id_visor " +
            "INNER JOIN ibmx_a07e6d02edaf552.TDP_SALES_AGENT SA ON SA.codATIS = ORD.userID "+
            "WHERE VIS.ESTADO_SOLICITUD = 'ENVIANDO' AND VIS.id_visor LIKE 'MT%' " +
            "ORDER BY VIS.FECHA_GRABACION ASC LIMIT 100", nativeQuery = true)
    List<Object[]> searchMTEnviando();

    @Transactional
    @Modifying
    @Query("UPDATE TdpVisor v SET v.estadoSolicitud =?1 WHERE v.idVisor =?2")
    void updateEstadoSolicitudVentaMT(String estadoSolicitud, String idVisor);

    @Transactional
    @Modifying
    @Query("UPDATE TdpVisor v SET v.estadoSolicitud =?1, v.cntIntentosHdec = ?2 WHERE v.idVisor =?3")
    void updateCntIntentosHdecVentaMT(String estadoSolicitud, Integer cntIntentosHdec, String idVisor);

    @Query(value = "SELECT v.id_visor, " +      //0
            "v.codigo_pedido, " +               //1
            "v.estado_solicitud, " +            //2
            "v.motivo_estado, " +               //3
            "v.visor_update, " +                //4
            "v.estado_anterior " +              //5
            "FROM tdp_visor v " +
            "WHERE v.id_visor = ?1", nativeQuery = true)
    List<Object[]> getVisorInfoReporteBBII(String mtOrder);
    
    @Query(value = "select *  from tdp_visor p where estado_solicitud = ?1 limit ?2 " , nativeQuery = true)
    List<TdpVisor> findByEstadoSolicitud(String estadoSolicitud, int limit);
    
    @Transactional
    @Modifying
    @Query("UPDATE TdpVisor v SET v.estadoSolicitud =?1, v.cntIntentosAuto = ?2 WHERE v.idVisor =?3")
    void updateCntIntentosAutoentaMT(String estadoSolicitud, Integer cntIntentosAuto, String idVisor);

}
