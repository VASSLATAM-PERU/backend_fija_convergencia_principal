package tdp.mt.backend.movistartotal.main.util;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author VASSLATAM
 */
public class ExcelWriter {
    
    private static String[] columns;
    private static String filename;

    public ExcelWriter(String[] columns, String filename) {
        this.columns = columns;
        this.filename = filename;
    }

    public void Create(List<String[]> data) throws IOException,
            InvalidFormatException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("datos");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);
        Integer columns_size = columns.length;

        for (int i = 0; i < columns_size; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with contacts data
        int rowNum = 1;

        for (int i = 0; i < data.size(); i++) {
            Row row = sheet.createRow(rowNum++);
            for (int j = 0; j < columns_size; j++) {
                row.createCell(j).setCellValue(data.get(i)[j]);
            }
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(filename);
        workbook.write(fileOut);
        fileOut.close();
    }   
}