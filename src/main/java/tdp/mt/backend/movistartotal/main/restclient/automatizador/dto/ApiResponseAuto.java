package tdp.mt.backend.movistartotal.main.restclient.automatizador.dto;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseHeader;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ClientException;

@Service
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseAuto<E> {
	@JsonProperty("HeaderOut")
	  private ApiResponseHeaderAuto HeaderOut;
	  @JsonProperty("BodyOut")
	  private E BodyOut;
	  @JsonProperty("ClientException")
	  private ClientException clientException;

	  @JsonProperty("httpCode")
	  private String httpCode;
	  @JsonProperty("httpMessage")
	  private String httpMessage;
	  @JsonProperty("moreInformation")
	  private String moreInformation;

	  

	  public ApiResponseHeaderAuto getHeaderOut() {
		return HeaderOut;
	}

	public void setHeaderOut(ApiResponseHeaderAuto headerOut) {
		HeaderOut = headerOut;
	}

	public E getBodyOut() {
	    return BodyOut;
	  }

	  public void setBodyOut(E bodyOut) {
	    BodyOut = bodyOut;
	  }

	  public ClientException getClientException() {
	    return clientException;
	  }

	  public void setClientException(ClientException clientException) {
	    this.clientException = clientException;
	  }

	  public String getHttpCode() {
	    return httpCode;
	  }

	  public void setHttpCode(String httpCode) {
	    this.httpCode = httpCode;
	  }

	  public String getHttpMessage() {
	    return httpMessage;
	  }

	  public void setHttpMessage(String httpMessage) {
	    this.httpMessage = httpMessage;
	  }

	  public String getMoreInformation() {
	    return moreInformation;
	  }

	  public void setMoreInformation(String moreInformation) {
	    this.moreInformation = moreInformation;
	  }
}
