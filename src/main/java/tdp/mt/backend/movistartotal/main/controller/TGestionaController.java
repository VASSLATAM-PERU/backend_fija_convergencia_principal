package tdp.mt.backend.movistartotal.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.entity.TdpVisor;
import tdp.mt.backend.movistartotal.main.repository.TdpVisorRepository;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.FijaHDECResponseAuto;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.service.AutomatizadorService;
import tdp.mt.backend.movistartotal.main.service.TGestionaService;

import java.util.Map;

@RestController
public class TGestionaController {

    private static final Logger logger = LogManager.getLogger(TGestionaController.class);

    @Autowired
    private TGestionaService tGestionaService;
    @Autowired
    private AutomatizadorService automatizadorService;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tgestiona/insertOrder", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<FijaHDECResponse> insertOrder (@RequestBody FijaHDECRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        logger.info("info insertOrder", request);
        Response<FijaHDECResponse> response = new Response<>();
        		
        		
        		TdpVisor tdpVisor= tdpVisorRepository.findOneByIdVisor(request.getOrderId());
        
        		if(tdpVisor!=null){
        			automatizadorService.EliminarTdpVisor(request.getOrderId());
        		}
            	
            	response = tGestionaService.saveAndSendTGestiona(request, codigoAplicacion);
     
            

        return response;
    }
}
