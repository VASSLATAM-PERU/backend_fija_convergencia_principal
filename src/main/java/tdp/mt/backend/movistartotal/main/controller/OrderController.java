package tdp.mt.backend.movistartotal.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tdp.mt.backend.movistartotal.commonms.common.util.CustomerConstants;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;
import tdp.mt.backend.movistartotal.main.domain.FijaDatesBD.FijaDatesVendedorRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaOrderStatus.FijaOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaReporteBBIIRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.MTOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.service.OrderService;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;

import java.util.Map;

@RestController
public class OrderController {

    private static final Logger logger = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getMTOrderStatus", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> getMTOrderStatus(@RequestBody MTOrderStatusRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "getMTOrderStatus", request);
        Response<Map<String, String>> response = new Response<>();

        try {
            response = orderService.getMTOrderStatus(request);
        } catch (Exception e) {
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Error en getMTOrderStatus: " + e.getMessage());
        }

        LogVass.finishController(logger, "getMTOrderStatus", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getFijaOrder", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> getFijaOrder(@RequestBody FijaOrderStatusRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "getFijaOrder", request);
        Response<Map<String, String>> response = new Response<>();

        try {
            response = orderService.getFijaOrder(request);
        } catch (Exception e) {
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Error en getFijaOrder: " + e.getMessage());
        }

        LogVass.finishController(logger, "getFijaOrder", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getDateInFija", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> getDateInFija(@RequestBody FijaDatesVendedorRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "getDateInFija", request);
        Response<Map<String, String>> response = new Response<>();
        try {
            response = orderService.getDateInFija(request);
        } catch (Exception e) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Error en getDateInFija: " + e.getMessage());
        }
        LogVass.finishController(logger, "getDateInFija", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getFijaDataReporteBBII", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, Map<String,String>>> getFijaDataReporteBBII(@RequestBody FijaReporteBBIIRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "getFijaDataReporteBBII", request);
        Response<Map<String, Map<String,String>>> response = new Response<>();
        try {
            response = orderService.getFijaDataReporteBBII(request);
        } catch (Exception e) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Error en getFijaDataReporteBBII: " + e.getMessage());
        }
        LogVass.finishController(logger, "getFijaDataReporteBBII", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getFijaDataReporteBBIITest", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public void getFijaDataReporteBBIITest()  {
        //LogVass.startController(logger, "getFijaDataReporteBBII");
        Response<Map<String, Map<String,String>>> response = new Response<>();
        try {
              orderService.getFijaDataReporteBBIITest();
        } catch (Exception e) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Error en getFijaDataReporteBBII: " + e.getMessage());
        }
        LogVass.finishController(logger, "getFijaDataReporteBBII", response);
    }
}
