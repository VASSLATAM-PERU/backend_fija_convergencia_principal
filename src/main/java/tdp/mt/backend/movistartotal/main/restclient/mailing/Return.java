package tdp.mt.backend.movistartotal.main.restclient.mailing;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Return {

    @JsonProperty("idRecord")
    private String idRecord;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("idRecord")
    public String getIdRecord() {
        return idRecord;
    }

    @JsonProperty("idRecord")
    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
