package tdp.mt.backend.movistartotal.main.service.impl;


import java.security.SecureRandom;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;

import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.commonms.common.util.OrderConstants;
import tdp.mt.backend.movistartotal.commonservice.dao.ParametersRepository;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.domain.Automatizador.EstadoSolicitudRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.CustomerMTParser;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.MTParser;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.OrderDetailMTParser;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.OrderMTParser;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.VendedorMTParser;
import tdp.mt.backend.movistartotal.main.entity.Automatizador;
import tdp.mt.backend.movistartotal.main.entity.AutomatizadorSalesService;
import tdp.mt.backend.movistartotal.main.entity.Customer;
import tdp.mt.backend.movistartotal.main.entity.Order;
import tdp.mt.backend.movistartotal.main.entity.OrderDetail;
import tdp.mt.backend.movistartotal.main.entity.TdpOrder;
import tdp.mt.backend.movistartotal.main.entity.TdpSalesAgent;
import tdp.mt.backend.movistartotal.main.entity.TdpUbigeoEquiv;
import tdp.mt.backend.movistartotal.main.entity.User;
import tdp.mt.backend.movistartotal.main.repository.AddressDao;
import tdp.mt.backend.movistartotal.main.repository.AutomatizadorOrderRepository;
import tdp.mt.backend.movistartotal.main.repository.AutomatizadorRepository;
import tdp.mt.backend.movistartotal.main.repository.AutomatizadorRequestSaleService;
import tdp.mt.backend.movistartotal.main.repository.CustomerRepository;
import tdp.mt.backend.movistartotal.main.repository.MTAzureFileStorageRepository;
import tdp.mt.backend.movistartotal.main.repository.OrdenMTRepository;
import tdp.mt.backend.movistartotal.main.repository.OrderDetailRepository;
import tdp.mt.backend.movistartotal.main.repository.OrderRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpOrderRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpSalesAgentRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpUbigeoEquivRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpVisorRepository;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.AutomatizadorSaleRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.BeanAddress;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.InsertOrderClientAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.InsertRequestBodyAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.InsertResponseBodyAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.NormalizadorModelVO;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.dto.ApiResponseAuto;
import tdp.mt.backend.movistartotal.main.service.AutomatizadorService;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

@Service
public class AutomatizadorServiceImpl implements AutomatizadorService {
	
	  private static final Logger logger = LogManager.getLogger();
	  private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");
	  @Autowired
	  private TdpSalesAgentRepository tdpSalesAgentRepository;
	  @Autowired
	  private ServiceCallEventsService serviceCallEventsService;
	  @Autowired
	  private TGestionaServiceImpl tgestiona;
	  @Autowired
	  private OrderRepository orderRepository;
	  @Autowired
	   private TdpOrderRepository tdpOrderRepository;
	  @Autowired
	  private CustomerRepository customerRepository;

	  @Autowired
	  private OrderDetailRepository orderDetailRepository;
	  @Autowired
	  private ParametersRepository parametersRepository;
	  @Autowired
	  private AddressDao addressDao;
	  @Autowired
	  private AutomatizadorOrderRepository automatizadorOrderRepository;
	  @Autowired
	  private TdpUbigeoEquivRepository tdpUbigeoEquivRepository;
	  @Value("${tdp.sw.automatizador.tgestiona}")
	  private Boolean enabled;
	  @Value("${tdp.automatizador.errBusParametrizable}")
	  private String errBusParametrizable;
	  @Autowired
	  private ApiHeaderConfig apiHeaderConfig;
	  @Autowired
	  private AutomatizadorRequestSaleService automatizadorRequestSaleService;
	  @Autowired 
	  private ApiResponseAuto<InsertResponseBodyAuto> resultadoFinal;
	  @Autowired
	  private AutomatizadorRepository automatizadorRepository;
	  @Autowired
	  private TdpVisorRepository tdpVisorRepository;
	  @Autowired
	  private MTAzureFileStorageRepository mtAzureFileStorageRepository;
	  @Autowired
	  private OrdenMTRepository ordenMTRepository;
	  
	@Override
	public Response<FijaHDECResponse> saveAndSendAutomatizador(FijaHDECRequest request, String codigoAplicacion) {
		Response<FijaHDECResponse> response = new Response<>();
	        //Variables
	       
	        try {
	        TdpSalesAgent vendedor = tdpSalesAgentRepository.findOneByCodigoAtis(request.getSellerAtisCode());
	        if (vendedor == null) {
                response.setResponseCode(ServiceConstants.SERVICE_ERROR);
                response.setResponseMessage("No se encuentra infomacion del vendedor " + request.getSellerAtisCode());
                return response;
            }
	        	
	        	 Order order =saveOrderMT(request, codigoAplicacion);
	        	 
	        	 FijaHDECResponse fijaHDECResponse = sendTAutomatizadorInsertOrder(order, request);
	        	 
	        	 if(fijaHDECResponse.getEstadoTGestiona().equalsIgnoreCase("OK")) {
	        		 boolean isRetail = vendedor.getCanalEquivalenciaCampania().equals(ServiceConstants.USER_CANAL_RETAIL);
	                 boolean isOrigen = ServiceConstants.USER_CANAL_CALL.contains(vendedor.getCanalEquivalenciaCampania());//request.getOrigen().equals(ServiceConstants.MT_CALL);
	                 
	                 if (isOrigen) {
	                     mtAzureFileStorageRepository.insertMTAzureFileStorage(order.getId(), fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_GSM);
	                 }
	                 
	                 if (isRetail) {
	                     ordenMTRepository.insertMtOrderRetail(request.getMtOrden(), order.getId(), true, ServiceConstants.MODE_RETAIL);
	                     mtAzureFileStorageRepository.insertMTAzureFileStorageRetail(order.getId() + ServiceConstants.CUSTODIA_PDF, fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_PDF, order.getId());
	                     mtAzureFileStorageRepository.insertMTAzureFileStorageRetail(order.getId() + ServiceConstants.CUSTODIA_PNG, fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_PNG, order.getId());
//	                 } else {
//	                     ordenMTRepository.insertMtOrder(request.getMtOrden(), order.getId(), request.getMtAMDOCSMovil1(), request.getMtAMDOCSMovil2(), isOrigen);
	                 }

//	                 orderRepository.updatesStatusLegacyOrder(fijaHDECResponse.getEstadoTGestiona(), order.getId());
	        	 }
	        	 
	        response.setResponseCode(fijaHDECResponse.getCode());
	        response.setResponseMessage(fijaHDECResponse.getMessage());
	        response.setResponseData(fijaHDECResponse);
	        }catch(Exception e) {
	          logger.error(e.getMessage());
	        }
	        
	    
		return response;
	}
	
	
	public Order saveOrderMT(FijaHDECRequest request, String codigoAplicacion) throws Exception {
        logger.info("OrderMTService => entro metodo [saveOrderMT]");

        DateTime dt = new DateTime(new Date());

        Customer customer = saveCustomer(request);

        MTParser<Order> parserOrder = new OrderMTParser();
        Order order = parserOrder.parse(request);
        order.setCustomer(customer);
        order.setId(request.getOrderId());
        order.setAppcode(codigoAplicacion);
        order.setRegisteredTime(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        order.setRegistrationDate(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        order.setServiceType("ATIS");
        
        order.setMtOperacionComercial1("?");
        order.setMtIncluyeEquipo1("?");
        order.setMtTelefono1("?");
        order.setMtNumeroOrden1("?");
        order.setMtOperacionComercial2("?");
        order.setMtIncluyeEquipo2("?");
        order.setMtTelefono2("?");
        order.setMtNumeroOrden2("?");
        order.setMtEstadoMovil1("?");
        order.setMtEstadoMovil2("?");
        
        if (UtilMethods.validString(request.getOperacionComercial1()) && UtilMethods.validString(request.getIncluyeEquipo1())) {
            if (!request.getOperacionComercial1().equals(ServiceConstants.MT_SAME_OFFER_ACTION) || !request.getIncluyeEquipo1().equals("SI")) {
                order.setMtOperacionComercial1(request.getOperacionComercial1());
                order.setMtIncluyeEquipo1(request.getIncluyeEquipo1());
                order.setMtTelefono1(request.getTelefono1());
                order.setMtNumeroOrden1((request.getMtAMDOCSMovil1() == null ? "" : request.getMtAMDOCSMovil1()).equals("") ? "?" : request.getMtAMDOCSMovil1().trim());
                order.setMtEstadoMovil1((request.getEstadoMovil1() == null ? "?" : request.getEstadoMovil1().equals("") ? "?" : request.getEstadoMovil1()));
            }
        }
        if (UtilMethods.validString(request.getOperacionComercial2()) && UtilMethods.validString(request.getIncluyeEquipo2())) {
            if (!request.getOperacionComercial2().equals(ServiceConstants.MT_SAME_OFFER_ACTION) || !request.getIncluyeEquipo2().equals("SI")) {
                order.setMtOperacionComercial2(request.getOperacionComercial2());
                order.setMtIncluyeEquipo2(request.getIncluyeEquipo2());
                order.setMtTelefono2(request.getTelefono2());
                order.setMtNumeroOrden2((request.getMtAMDOCSMovil2() == null ? "" : request.getMtAMDOCSMovil2()).equals("") ? "?" : request.getMtAMDOCSMovil2().trim());
                order.setMtEstadoMovil2((request.getEstadoMovil2() == null ? "?" : request.getEstadoMovil2().equals("") ? "?" : request.getEstadoMovil2()));
            }
        }
        
        order.setMtPlanMovil(UtilMethods.validString(request.getPlanMovil()) ? request.getPlanMovil() : "?");
//        order.setMtFlag(request.getFlag() == null ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag().equals("") ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag());

        User u = new User();
        u.setId(request.getSellerAtisCode());
        order.setUser(u);
        order.setCampaign(request.getCampaing());
        orderRepository.save(order);

        MTParser<OrderDetail> parserOrderDetail = new OrderDetailMTParser();
        OrderDetail orderDetail = parserOrderDetail.parse(request);

        Long id = orderDetailRepository.findByOrderId(order.getId());
        orderDetail.setId(id);
        orderDetail.setRegisteredTime(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        orderDetail.setOrderId(order.getId());
        orderDetailRepository.save(orderDetail);

        MTParser<TdpSalesAgent> parserVendedor = new VendedorMTParser();
        TdpSalesAgent vendedor = parserVendedor.parse(request);
        if (vendedor != null) {
            if (tdpSalesAgentRepository.findOneByCodigoAtis(vendedor.getCodigoAtis()) != null) {
                logger.info("Vendedor si esta registrado");
            } else {
                logger.info("Vendedor no registrado");
            }
        }

        TdpOrder tdpOrder = new TdpOrder();
        tdpOrder.setOrderId(request.getOrderId());
        tdpOrder.setClientNumeroDoc(request.getClientDocNumber());
        tdpOrder.setClientTipoDoc(request.getClientDocType().toUpperCase());
        tdpOrder.setClientEmail(request.getClientEmail());
        tdpOrder.setClientApellidoMaterno(customer.getLastName2());
        tdpOrder.setClientApellidoPaterno(customer.getLastName1());
        tdpOrder.setClientNombre(customer.getFirstName());
        tdpOrder.setClientTelefono1Area("");
        tdpOrder.setClientTelefono1(customer.getCustomerPhone());
        tdpOrder.setClientTelefono2Area("");
        tdpOrder.setClientTelefono2(customer.getCustomerPhone2());
        tdpOrder.setClientNationality(customer.getNationality());
        tdpOrder.setClientCivil(customer.getMaritalstatus());
        tdpOrder.setClientNacimiento(customer.getBirthDate());
        tdpOrder.setClientSexo(customer.getSex());
        tdpOrder.setProductCodigo(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getProductCode());
        //tdpOrder.setAutomatizadorOK(1);

        tdpOrder.setClientCmsServicio(order.getCmsServiceCode());
        tdpOrder.setClientCmsFactura(0);
        
        
        tdpOrder.setOrderOrigen("WEBMT");
        tdpOrder.setAffiliationDataProtection("No");
        tdpOrder.setAffiliationElectronicInvoice("Si");
        tdpOrder.setAffiliationDataProtection("No");
        tdpOrder.setDisaffiliationWhitePagesGuide("No");
        tdpOrder.setOrderOperationCommercial(order.getCommercialOperation());
        tdpOrder.setOrderOrigen(order.getAppcode());
        tdpOrder.setOrderGpsX(order.getCoordinateX());
        tdpOrder.setOrderGpsY(order.getCoordinateY());
        tdpOrder.setProductPs(order.getProductCode());
        tdpOrder.setProductPrecioNormal(order.getPrice());
        tdpOrder.setProductPrecioPromo(order.getPromPrice());
        tdpOrder.setProductPrecioPromoMes(order.getMonthPeriod());
        tdpOrder.setProductInternetPromo(order.getPromoSpeed());
        tdpOrder.setProductInternetPromoTiempo(order.getPeriodoPromoSpeed());
        tdpOrder.setProductCampaing(order.getCampaign());
        tdpOrder.setProductType(order.getProductType());
        tdpOrder.setProductCategory(order.getProductCategory());
        tdpOrder.setProductPayMethod(order.getPaymentMode());
        tdpOrder.setProductPayPrice(order.getCashPrice());
        tdpOrder.setProductSenal(orderDetail.getCod_ind_sen_cms());
        tdpOrder.setProductCabecera(orderDetail.getCod_cab_cms());

        TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(u.getId());
        
        if (tdpSalesAgent != null) {
        tdpOrder.setUserAtis(u.getId());
        tdpOrder.setUserCms(Integer.parseInt(tdpSalesAgent.getCodCms()));
        tdpOrder.setUserCanalCodigo(tdpSalesAgent.getUser_canal_codigo());
        tdpOrder.setUserNombre(tdpSalesAgent.getNombre() + " " + tdpSalesAgent.getApePaterno() + " " + tdpSalesAgent.getApeMaterno());
        tdpOrder.setUserDni(tdpSalesAgent.getDni());
        tdpOrder.setUserEntidad(tdpSalesAgent.getEntidad());
        tdpOrder.setUserZonal(tdpSalesAgent.getZonal());
        tdpOrder.setUserFuerzaVenta(tdpSalesAgent.getUserFuerzaVenta());
        tdpOrder.setUserRegion(tdpSalesAgent.getZona());
        }

        tdpOrder.setAuditoriaCreate(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        tdpOrder.setAuditoriaModify(UtilMethods.sumarRestarHorasFecha(new Date(), -5));

        Integer operationNumber = 0;
        if (order.getCommercialOperation() != null && order.getServiceType() != null && order.getCampaign() != null) {
            if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("ATIS")
                    && order.getCampaign().toUpperCase().equalsIgnoreCase("CIUDAD SITIADA")) {
                operationNumber = 1;
            } else if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("ATIS")) {
                operationNumber = 1;
            } else if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("CMS")) {
                operationNumber = 6;
            } else if (order.getCommercialOperation().equalsIgnoreCase("SVAS")
                    && order.getServiceType().equalsIgnoreCase("ATIS")) {
                operationNumber = 2;
            } else if (order.getCommercialOperation().equalsIgnoreCase("SVAS")
                    && order.getServiceType().equalsIgnoreCase("CMS")) {
                operationNumber = 7;
            } else {
                operationNumber = 0;
            }
        }
        
        tdpOrder.setOrderExperto(orderDetail.getExpertoCode());
        tdpOrder.setOrderParqueTelefono(order.getMigrationPhone());
        tdpOrder.setOrderGisXdsl(orderDetail.getMsx_cbr_voi_ges_in());
        String indHDC="N";
        String indGPN="N";
        
        if(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage().equalsIgnoreCase(Constants.INDICADOR_GPON_HFC)) {
        	indHDC="S";
        	indGPN="G";
        }else if(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage().equalsIgnoreCase(Constants.INDICADOR_HFC)) {
        	indHDC="S";
        	indGPN="H";
        }else if(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage().equalsIgnoreCase(Constants.INDICADOR_GPON)) {
        	indHDC="S";
        	indGPN="G";
        }
        tdpOrder.setOrderGisHfc(indHDC);
        tdpOrder.setOrderGisCable(Constants.TIPO_SENIAL_DIGITAL);
        tdpOrder.setProductPsAdmin1(Constants.PS_ADM_DEP_1);
        tdpOrder.setProductPsAdmin2(Constants.PS_ADM_DEP_2);
        tdpOrder.setProductPsAdmin3(Constants.PS_ADM_DEP_3);
        tdpOrder.setProductPsAdmin4(Constants.PS_ADM_DEP_4);
//        tdpOrder.setOrderGisGpon(orderDetail.getMsx_ind_gpo_gis_cd());
        tdpOrder.setOrderGisGpon(indGPN);
        tdpOrder.setOrderGisNtlt(orderDetail.getCod_fac_tec_cd());
        tdpOrder.setOrderCallTipo("");
        tdpOrder.setOrderCallDesc("");
        tdpOrder.setOrderModelo("");
        tdpOrder.setAffiliationParentalProtection(orderDetail.getParentalProtection());
        tdpOrder.setOrderContrato(orderDetail.getSendContracts().toUpperCase().equalsIgnoreCase("SI") ? "M" : "F");
        tdpOrder.setOrderGrabacion("S");
        tdpOrder.setOrderModalidadAcept("G");
        tdpOrder.setOrderStc6i(orderDetail.getCod_stc());
        
        tdpOrder.setOrderOperationNumber(operationNumber);
        tdpOrder.setAddressDepartamento(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDepartment());
        tdpOrder.setAddressProvincia(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getProvince());
        tdpOrder.setAddressDistrito(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDistrict());
        tdpOrder.setOrderFechaHora(orderDetail.getRegisteredTime());
        if(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getFixedSystemType()!=null) {
        	tdpOrder.setProductTipoReg(request.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getFixedSystemType());
        }else {
        	tdpOrder.setProductTipoReg(order.getServiceType());
        }
        
        tdpOrder.setUserAtis(request.getSellerAtisCode());
        tdpOrder.setAddressDepartamentoCod("");
        tdpOrder.setAddressProvinciaCod("");
        tdpOrder.setAddressDistritoCod("");
        tdpOrder.setAddressPrincipal(orderDetail.getAddress());
        tdpOrder.setAddressCalleNumero(orderDetail.getNum_cal_nu());
        tdpOrder.setAddressReferencia(orderDetail.getAddressComplement());
        
        
        if(request.getMtOfferLog()!=null){
            if(request.getMtOfferLog().get(0)!=null){
                if(request.getMtOfferLog().get(0).getSvaList()!=null) {
                    if (request.getMtOfferLog().get(0).getSvaList().size() > 0) {
                        tdpOrder.setSvaCodigo1(request.getMtOfferLog().get(0).getSvaList().get(0).getProductCode());
                        tdpOrder.setSvaNombre1(request.getMtOfferLog().get(0).getSvaList().get(0).getSvaName());
                        tdpOrder.setSvaCantidad1(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(0).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(0).getSold()));
                        tdpOrder.setSvaPrecio1(request.getMtOfferLog().get(0).getSvaList().get(0).getSvaPrice());
                        if (request.getMtOfferLog().get(0).getSvaList().size() > 1) {
                            tdpOrder.setSvaCodigo2(request.getMtOfferLog().get(0).getSvaList().get(1).getProductCode());
                            tdpOrder.setSvaNombre2(request.getMtOfferLog().get(0).getSvaList().get(1).getSvaName());
                            tdpOrder.setSvaCantidad2(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(1).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(1).getSold()));
                            tdpOrder.setSvaPrecio2(request.getMtOfferLog().get(0).getSvaList().get(1).getSvaPrice());
                            if (request.getMtOfferLog().get(0).getSvaList().size() > 2) {
                                tdpOrder.setSvaCodigo3(request.getMtOfferLog().get(0).getSvaList().get(2).getProductCode());
                                tdpOrder.setSvaNombre3(request.getMtOfferLog().get(0).getSvaList().get(2).getSvaName());
                                tdpOrder.setSvaCantidad3(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(2).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(2).getSold()));
                                tdpOrder.setSvaPrecio3(request.getMtOfferLog().get(0).getSvaList().get(2).getSvaPrice());
                                if (request.getMtOfferLog().get(0).getSvaList().size() > 3) {
                                    tdpOrder.setSvaCodigo4(request.getMtOfferLog().get(0).getSvaList().get(3).getProductCode());
                                    tdpOrder.setSvaNombre4(request.getMtOfferLog().get(0).getSvaList().get(3).getSvaName());
                                    tdpOrder.setSvaCantidad4(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(3).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(3).getSold()));
                                    tdpOrder.setSvaPrecio4(request.getMtOfferLog().get(0).getSvaList().get(3).getSvaPrice());
                                    if (request.getMtOfferLog().get(0).getSvaList().size() > 4) {
                                        tdpOrder.setSvaCodigo5(request.getMtOfferLog().get(0).getSvaList().get(4).getProductCode());
                                        tdpOrder.setSvaNombre5(request.getMtOfferLog().get(0).getSvaList().get(4).getSvaName());
                                        tdpOrder.setSvaCantidad5(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(4).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(4).getSold()));
                                        tdpOrder.setSvaPrecio5(request.getMtOfferLog().get(0).getSvaList().get(4).getSvaPrice());
                                        if (request.getMtOfferLog().get(0).getSvaList().size() > 5) {
                                            tdpOrder.setSvaCodigo6(request.getMtOfferLog().get(0).getSvaList().get(5).getProductCode());
                                            tdpOrder.setSvaNombre6(request.getMtOfferLog().get(0).getSvaList().get(5).getSvaName());
                                            tdpOrder.setSvaCantidad6(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(5).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(5).getSold()));
                                            tdpOrder.setSvaPrecio6(request.getMtOfferLog().get(0).getSvaList().get(5).getSvaPrice());
                                            if (request.getMtOfferLog().get(0).getSvaList().size() > 6) {
                                                tdpOrder.setSvaCodigo7(request.getMtOfferLog().get(0).getSvaList().get(6).getProductCode());
                                                tdpOrder.setSvaNombre7(request.getMtOfferLog().get(0).getSvaList().get(6).getSvaName());
                                                tdpOrder.setSvaCantidad7(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(6).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(6).getSold()));
                                                tdpOrder.setSvaPrecio7(request.getMtOfferLog().get(0).getSvaList().get(6).getSvaPrice());
                                                if (request.getMtOfferLog().get(0).getSvaList().size() > 7) {
                                                    tdpOrder.setSvaCodigo8(request.getMtOfferLog().get(0).getSvaList().get(7).getProductCode());
                                                    tdpOrder.setSvaNombre8(request.getMtOfferLog().get(0).getSvaList().get(7).getSvaName());
                                                    tdpOrder.setSvaCantidad8(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(7).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(7).getSold()));
                                                    tdpOrder.setSvaPrecio8(request.getMtOfferLog().get(0).getSvaList().get(7).getSvaPrice());
                                                    if (request.getMtOfferLog().get(0).getSvaList().size() > 8) {
                                                        tdpOrder.setSvaCodigo9(request.getMtOfferLog().get(0).getSvaList().get(8).getProductCode());
                                                        tdpOrder.setSvaNombre9(request.getMtOfferLog().get(0).getSvaList().get(8).getSvaName());
                                                        tdpOrder.setSvaCantidad9(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(8).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(8).getSold()));
                                                        tdpOrder.setSvaPrecio9(request.getMtOfferLog().get(0).getSvaList().get(8).getSvaPrice());
                                                        if (request.getMtOfferLog().get(0).getSvaList().size() > 9) {
                                                            tdpOrder.setSvaCodigo10(request.getMtOfferLog().get(0).getSvaList().get(9).getProductCode());
                                                            tdpOrder.setSvaNombre10(request.getMtOfferLog().get(0).getSvaList().get(9).getSvaName());
                                                            tdpOrder.setSvaCantidad10(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(9).getSold().equalsIgnoreCase("null")?"0":request.getMtOfferLog().get(0).getSvaList().get(9).getSold()));
                                                            tdpOrder.setSvaPrecio10(request.getMtOfferLog().get(0).getSvaList().get(9).getSvaPrice());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        List<BeanAddress> list= new ArrayList<BeanAddress>();
        
        list=agregarList(Constants.TIPO_SVA_DHD, Constants.TIPO_DHD, list);
        list=agregarList(Constants.TIPO_SVA_DSHD, Constants.TIPO_DSHD, list);
        list=agregarList(Constants.TIPO_SVA_DVR, Constants.TIPO_DVR, list);
        
        
        
        
        for(BeanAddress dat:list) {
        	if(tdpOrder.getSvaNombre1()!=null) {
        		if(tdpOrder.getSvaNombre1().equalsIgnoreCase(dat.getNombre())) {
            		tdpOrder.setSvaCode1(dat.getAbreviatura());
            	}else if(tdpOrder.getSvaCode1()==null) {
            		tdpOrder.setSvaCode1("");
            	}
        	}
        	
        	if(tdpOrder.getSvaNombre2()!=null) {
	        	if(tdpOrder.getSvaNombre2().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode2(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode2()==null) {
	        		tdpOrder.setSvaCode2("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre3()!=null) {
	        	if(tdpOrder.getSvaNombre3().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode3(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode3()==null) {
	        		tdpOrder.setSvaCode3("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre4()!=null) {
	        	if(tdpOrder.getSvaNombre4().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode4(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode4()==null) {
	        		tdpOrder.setSvaCode4("");
	        	}
        	}
        	if(tdpOrder.getSvaNombre5()!=null) {
	        	if(tdpOrder.getSvaNombre5().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode5(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode5()==null) {
	        		tdpOrder.setSvaCode5("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre6()!=null) {
	        	if(tdpOrder.getSvaNombre6().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode6(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode6()==null) {
	        		tdpOrder.setSvaCode6("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre7()!=null) {
	        	if(tdpOrder.getSvaNombre7().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode7(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode7()==null) {
	        		tdpOrder.setSvaCode7("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre8()!=null) {
	        	if(tdpOrder.getSvaNombre8().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode8(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode8()==null) {
	        		tdpOrder.setSvaCode8("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre9()!=null) {
	        	if(tdpOrder.getSvaNombre9().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode9(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode9()==null) {
	        		tdpOrder.setSvaCode9("");
	        	}
        	}
        	
        	if(tdpOrder.getSvaNombre10()!=null) {
	        	if(tdpOrder.getSvaNombre10().equalsIgnoreCase(dat.getNombre())) {
	        		tdpOrder.setSvaCode10(dat.getAbreviatura());
	        	}else if(tdpOrder.getSvaCode10()==null) {
	        		tdpOrder.setSvaCode10("");
	        	}
        	}
        }
        
        
        tdpOrderRepository.save(tdpOrder);
        return order;

    }
	
	 private Customer saveCustomer(FijaHDECRequest request) throws Exception {

	        MTParser<Customer> parserCustomer = new CustomerMTParser();
	        Customer cust = parserCustomer.parse(request);

	        Long duplicateCustomer = customerRepository.loadIdByDocTypeAndDocNumber(cust.getDocType(), cust.getDocNumber());
	        cust.setId(duplicateCustomer);
	        Random rand = new SecureRandom();
	        cust.setSex(rand.nextInt(1) == 0 ? "M" : "F");

	        String estadoCivilList = "SCVD";
	        cust.setMaritalstatus(String.valueOf(estadoCivilList.charAt(rand.nextInt(estadoCivilList.length()))));

	        customerRepository.save(cust);

	        return cust;
	    }
	
	public FijaHDECResponse sendTAutomatizadorInsertOrder(Order order,FijaHDECRequest request) {
		
		String orderId=order.getId();
		FijaHDECResponse response = new FijaHDECResponse();
		Boolean estado=false;
   	 	try {
   	 		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   	 		String strDate = df.format(new Date());
   	 		String operacionComercial=order.getCommercialOperation();
   	 		
   	 		orderRepository.insertTdpVisor(orderId,Constants.SALE_STATUS_PENDING , strDate);

   	   	 	 estado=preRegisterSaleWeb(orderId,operacionComercial,request);
   	   	 	 if(!estado) {
   	   	 	 Integer duplicado = addressDao._consultarDuplicado(orderId);
   	   	 	 
		   	   	 if(duplicado.equals(1)){
		             logger.error("Error Duplicado en Automatizador", duplicado);
		         }
			   	  if(duplicado.equals(2)){
			   		  String   visorStatus = "ENVIANDO_AUT_MT";
	                  
	                  logger.error("Error de Bus", duplicado);
	              }
   	   	 	 }
			
			 //Validar Respuesta  	 	
   	   
  	 			 if(resultadoFinal.getHeaderOut().getMsgType().equalsIgnoreCase(Constants.RESPONSE)) {
  	 				 if(resultadoFinal.getBodyOut().getRespuestaMsj().equalsIgnoreCase(Constants.INDICADOR_RESPUESTA_AUTOMATIZADOR)) {
  	 					 
  	 					 
  	 				response.setCode(ServiceConstants.SERVICE_SUCCESS);
                   response.setMessage("1");
                   response.setEstadoTGestiona("PENDIENTE");
  	 				 }
  	 				 
  	 			 } else if (resultadoFinal.getBodyOut().getClientException() != null) {
                if (resultadoFinal.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage().equals("Error: CODIGO_UNICO ya existe => CODIGO_UNICO") ||
                		resultadoFinal.getBodyOut().getClientException().getAppDetail().getExceptionAppCode().equals(1)) {
                    response.setCode(ServiceConstants.SERVICE_SUCCESS);
                    response.setMessage(resultadoFinal.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage());
                    response.setEstadoTGestiona("PENDIENTE");
                } else {
                    response.setCode(ServiceConstants.SERVICE_ERROR);
                    response.setMessage("Error " + resultadoFinal.getBodyOut().getClientException().getExceptionCode() + ": " + resultadoFinal.getBodyOut().getClientException().getExceptionDetail());
                    response.setEstadoTGestiona("CAIDA");
                    return response;
                }
            } else if (resultadoFinal.getBodyOut().getServerException() != null) {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                if(resultadoFinal.getBodyOut().getServerException().getExceptionCode()==4000) {
                	 response.setEstadoTGestiona("CAIDA BUS");
                	 response.setMessage("Error " + resultadoFinal.getBodyOut().getServerException().getExceptionCode() + ": " + resultadoFinal.getBodyOut().getServerException().getExceptionDetail());
                }else if(resultadoFinal.getBodyOut().getServerException().getExceptionCode()==500) {
                	response.setEstadoTGestiona("CAIDA SERVICIO, no conecta con endpoint");
               	 response.setMessage("Error " + resultadoFinal.getBodyOut().getServerException().getExceptionCode() + ": " + resultadoFinal.getBodyOut().getServerException().getExceptionDetail());
                }
                else {
                	 response.setEstadoTGestiona("ENVIANDO");
                	 response.setMessage("Error " + resultadoFinal.getBodyOut().getServerException().getExceptionCode() + ": " + resultadoFinal.getBodyOut().getServerException().getExceptionDetail());
                }
                
               
                return response;
            } else {
            	response.setCode("ERR-0000");
	 			response.setMessage("PENDIENTE");
	 			response.setEstadoTGestiona("OK");
	 		}
            
		} catch (Exception e) {
	            response.setCode(ServiceConstants.SERVICE_ERROR);
	            response.setMessage("ERROR CAIDA");
	            response.setEstadoTGestiona("CAIDA");
	            return response;
		}	
   	 
   	 return response;
	}
	
	
	
	/**
	 * Metodo de Automatización para el request
	 */
	 public boolean preRegisterSaleWeb(String orderId, String type,FijaHDECRequest fijaRequest) {
		 
	  try {
		 //Lista de parametros que tiene automatizador
		 List<String> automatizadorAux = automatizadorExtraValidacion();
		 
		 boolean automatizadorSegmentacionSwitch = Boolean.parseBoolean(automatizadorAux.get(2));
		 if (!automatizadorSegmentacionSwitch) {
             return false;
         }
		 
		//validar si paso por normalizador
//        boolean normalizador = addressDao.goToNormalizador(orderId);
		 
         //Verifica si existe venta en visor
         boolean _exitsVentaVisor = addressDao._exitsVentaVisor(orderId);
         
         //Si existe venta en visor se actualiza el orderID con codigo de error 
         if (_exitsVentaVisor) {
             automatizadorOrderRepository.updateAutomatizadorOrder("ERR-0004", orderId);
             return false;
         }
         
         Boolean isEnabled = false;
         Boolean isSVA=false;
         //Lista de parametros que tenga automatizador en categoria enable la lista tiene Alta, SVA y Migración.
         List<Parameters> enableList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "ENABLE");
         for(Parameters obj: enableList) {
        	 if (type.equalsIgnoreCase(OrderConstants.ALTA_PURA) && obj.getElement().equalsIgnoreCase("auto.altapura")
                     && obj.getStrValue().equalsIgnoreCase("true")) {
                 isEnabled = true;
                 break;
             }
             if (type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && obj.getElement().equalsIgnoreCase("auto.altasva")
                     && obj.getStrValue().equalsIgnoreCase("true")) {
                 isEnabled = true;
                 isSVA=true;
                 break;
             }
             if (type.equalsIgnoreCase(OrderConstants.MIGRACION) && obj.getElement().equalsIgnoreCase("auto.migracion")
                     && obj.getStrValue().equalsIgnoreCase("true")) {
                 isEnabled = true;
                 break;
             }
        	 
         }
         
         if (!isEnabled) {
             logger.info("Automatizador de encuentra DESACTIVADO.");
             return false;
         }
         //Consulta a la tabla order obtener datos de order
         TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(orderId);
         //Consulta de punto de venta cms
         String cmsPuntoVentaDepa = addressDao.autoDepaCmsPunto(tdpOrder.getUserAtis(), tdpOrder.getAddressDepartamento());
		 
         if (cmsPuntoVentaDepa != null) {
             if (cmsPuntoVentaDepa.isEmpty()) {
                 cmsPuntoVentaDepa = "0";
             }
         }
         tdpOrder.setUserFuerzaVenta(cmsPuntoVentaDepa);
         
         
         //Validar Si RUC es 20 , Si es no llama a automatizador
         if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
             String rucNum = tdpOrder.getClientNumeroDoc();
             String sub = rucNum.substring(0,2);
             if(sub.equals("20")){
                 return false;
             }
         }
         
         boolean switchRuc = Boolean.parseBoolean(automatizadorAux.get(3));
       //TODO: Aplicar validacion
         if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
             if (!switchRuc) {
                 logger.info("Automatizador de encuentra DESACTIVADO para RUC.");
                 return false;
             }else{
                 if(tdpOrder.getClientNumeroDoc().trim().substring(0,2).equals("20")){
                     return false;
             }
           }
         }
         
         boolean isPaquetizacion=false;
         if(tdpOrder.getClientCmsCodigo() != null && tdpOrder.getClientCmsServicio()!=null && type.equalsIgnoreCase(OrderConstants.MIGRACION)) {
        	 isPaquetizacion=true;
         }
         
         if(type.equalsIgnoreCase(OrderConstants.MIGRACION) && isPaquetizacion) {
        	 logger.info("Automatizador se encuentra DESACTIVADO para Migraciones porque no es paquetización");
			 return false;
         }
         
         
         SimpleDateFormat sdfDatetime= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
         SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
         
         AutomatizadorSaleRequestBody request = new AutomatizadorSaleRequestBody();
         
         request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta() != null ? tdpOrder.getUserFuerzaVenta() : "");
         request.setProductoSenial(tdpOrder.getProductSenal() != null ? tdpOrder.getProductSenal() : "");
         request.setUserNombre(tdpOrder.getUserNombre() != null ? tdpOrder.getUserNombre() : "");
         request.setAddressReferencia(tdpOrder.getAddressReferencia() != null ? tdpOrder.getAddressReferencia() : "");
         request.setOrderCallTipo(tdpOrder.getOrderCallTipo() != null ? tdpOrder.getOrderCallTipo() : "");
         request.setOrderCallDesc(tdpOrder.getOrderCallDesc() != null ? tdpOrder.getOrderCallDesc() : "");
         request.setOrderModelo(tdpOrder.getOrderModelo() != null ? tdpOrder.getOrderModelo() : "");
         request.setUserDNI(tdpOrder.getUserDni() != null ? tdpOrder.getUserDni() : "");
         request.setUserTelefono(tdpOrder.getUserTelefono() != null ? tdpOrder.getUserTelefono() : "");
         request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");
         request.setOrderIDGrabacion(tdpOrder.getOrderGrabacion() != null ? tdpOrder.getOrderGrabacion() : "");
         request.setOrderModalidadAcep(tdpOrder.getOrderModalidadAcept() != null ? tdpOrder.getOrderModalidadAcept() : "");
         request.setOrderEntidad(tdpOrder.getUserEntidad() != null ? tdpOrder.getUserEntidad() : "");
         request.setOrderRegion(tdpOrder.getUserRegion() != null ? tdpOrder.getUserRegion() : "");

         request.setOrderCIP(tdpOrder.getOrderCip() != null ? tdpOrder.getOrderCip() : "");
         request.setOrderExperto(tdpOrder.getOrderExperto() != null ? tdpOrder.getOrderExperto() : "");
         request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");
         request.setOrderWebParental(tdpOrder.getAffiliationParentalProtection() != null ? tdpOrder.getAffiliationParentalProtection() : "");
         request.setOrderCodigoSTC6I(tdpOrder.getOrderStc6i() != null ? tdpOrder.getOrderStc6i() : "");
         request.setClienteNacionalidad(tdpOrder.getClientNationality()!=null? tdpOrder.getClientNationality():"");
         //Origen de venta que es de la WEBMT web movistar total
         
         String ordenModoVenta="5";
         request.setOrderModoVenta(ordenModoVenta);
         
         String cantidadEquipos=getCantidadEquipos(tdpOrder);
         request.setOrderCantidadEquipos(cantidadEquipos);
         request.setOrderGisNTLT(tdpOrder.getOrderGisNtlt()!=null? tdpOrder.getOrderGisNtlt():"");
         request = SetGeneralAutomatizador(request, tdpOrder, sdfDatetime, isPaquetizacion);
         
         if (request != null) {
             if (tdpOrder.getProductTipoReg().equalsIgnoreCase(Constants.TIPO_PRODUCTO_ATIS)) {
                 request = setAtisAutomatizador(request, tdpOrder);
             }

             if (tdpOrder.getProductTipoReg().equalsIgnoreCase(Constants.TIPO_PRODUCTO_CMS)) {
                 request = setCMSAutomatizador(request, tdpOrder, sdfDate);
             }
         }
         
         if (request == null) {
             return false;
         }
         
         //Comienzo del automatizador
         
         String[] segmentacionCanalSplit = automatizadorAux.get(1).split(",");
         SimpleDateFormat formatPresentDay = new SimpleDateFormat("yyyy-MM-dd");
         String presentDay = formatPresentDay.format(new Date());
         
         Boolean autoSend = false;

         for (String i : segmentacionCanalSplit) {
             if (tdpOrder.getUserCanalCodigo().equalsIgnoreCase(i)) {
                 autoSend = true;
             }
         }
         
         if (!autoSend) {
             return false;
         }
         
         //Validamos los datos con tabla normalizador
         //y según lo obtenido agregamos lo que falta.

         NormalizadorModelVO normalizadorModelVO= new NormalizadorModelVO();
         if(fijaRequest.getNormalizador()!=null) {
        	 normalizadorModelVO= fijaRequest.getNormalizador();
         }
   
         List<BeanAddress> listNormaTipo= new ArrayList<BeanAddress>();
         
         listNormaTipo= agregarList("URB", "UR", listNormaTipo);
         listNormaTipo= agregarList("POBLADO", "PO", listNormaTipo);
         listNormaTipo= agregarList("VILLA MILITAR", "VM", listNormaTipo);
         listNormaTipo= agregarList("URBANIZACION POPULAR", "UP", listNormaTipo);
         listNormaTipo= agregarList("RES", "RS", listNormaTipo);
         listNormaTipo= agregarList("PPJJ", "PJ", listNormaTipo);
         listNormaTipo= agregarList("PARCELA", "PA", listNormaTipo);
         listNormaTipo= agregarList("LOT", "LO", listNormaTipo);
         listNormaTipo= agregarList("GRUPO", "GR", listNormaTipo);
         listNormaTipo= agregarList("FUNDO", "FU", listNormaTipo);
         listNormaTipo= agregarList("COOP", "CV", listNormaTipo);
         listNormaTipo= agregarList("CIUDAD UNIVERSITARIA", "CU", listNormaTipo);
         listNormaTipo= agregarList("CIUDAD SATELITE", "CS", listNormaTipo);
         listNormaTipo= agregarList("CH", "CO", listNormaTipo);
         listNormaTipo= agregarList("COMITE", "CM", listNormaTipo);
         listNormaTipo= agregarList("CIUDADELA", "CD", listNormaTipo);
         listNormaTipo= agregarList("BARRIO", "BA", listNormaTipo);
         listNormaTipo= agregarList("ASOC", "AS", listNormaTipo);
         listNormaTipo= agregarList("AGR", "AG", listNormaTipo);
         listNormaTipo= agregarList("COND", "CDM", listNormaTipo);
         
         List<BeanAddress> listNormaTipoVia= new ArrayList<BeanAddress>();
         
         listNormaTipoVia= agregarList("PSJ", "PJ", listNormaTipo);
         listNormaTipoVia= agregarList("AUTOPISTA", "AU", listNormaTipo);
         listNormaTipoVia= agregarList("SENDERO", "SD", listNormaTipo);
         listNormaTipoVia= agregarList("CUESTA", "CT", listNormaTipo);
         listNormaTipoVia= agregarList("CAMINO", "CM", listNormaTipo);
         listNormaTipoVia= agregarList("PLAZUELA", "PZ", listNormaTipo);
         listNormaTipoVia= agregarList("PASEO", "PO", listNormaTipo);
         listNormaTipoVia= agregarList("PZ", "PL", listNormaTipo);
         listNormaTipoVia= agregarList("PSJ", "PJ", listNormaTipo);
         listNormaTipoVia= agregarList("OVALO", "OV", listNormaTipo);
         listNormaTipoVia= agregarList("ML", "MA", listNormaTipo);
         listNormaTipoVia= agregarList("GAL", "GL", listNormaTipo);
         listNormaTipoVia= agregarList("CALLEJON", "CJ", listNormaTipo);
         listNormaTipoVia= agregarList("CAR", "CA", listNormaTipo);
         listNormaTipoVia= agregarList("COND", "CDM", listNormaTipo);
         
         if(normalizadorModelVO.getTipo() != null){
             if (normalizadorModelVO.getTipo().equalsIgnoreCase("1")) {

                 if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                     if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                    	 for(BeanAddress bean:listNormaTipo) {
                    		 if(normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase(bean.getNombre())) {
                    			 request.setAddressCCHHTipo(bean.getAbreviatura());
                    			 break;
                    		 }else {
                    			 request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                    		 }
                    	 }
                     }
                 }
                 
                 
                 if (normalizadorModelVO.getTipoVia() != null) {
                     if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                    	 for(BeanAddress bean:listNormaTipoVia) {
                    		 if(normalizadorModelVO.getTipoVia().equalsIgnoreCase(bean.getNombre())) {
                    			  request.setAddressCalleAtis(bean.getAbreviatura());
                    			  break;
                    		 }else {
                    			 request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                    		 }
                    			 
                    	 }
                     }
                 
                 }
                 
                 
                 
                 
                 if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                     if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                         request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                     }
                 }

                 if (normalizadorModelVO.getNombreVia() != null) {
                     if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                         request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                     } else {
                         request.setAddressCalleNombre(".");
                     }
                 }
                 
                 if (request.getAddressViaCompTipo1() != null) {
                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(".");
                     }


                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");
                     }

                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");

                         request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                         request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                     }
                 }
                 
                 if (request.getAddressViaCompTipo1() != null) {
                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3("0");
                     }
                 }

                 if (request.getAddressViaCompTipo2() != null) {

                     if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(".");

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3("0");
                     }
                 }

                 if (normalizadorModelVO.getManzana() != null) {
                     if (!normalizadorModelVO.getManzana().isEmpty()) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                     }

                 }

                 if (normalizadorModelVO.getLote() != null) {
                     if (!normalizadorModelVO.getLote().isEmpty()) {

                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                     }
                 }

                 if (normalizadorModelVO.getCuadra() != null) {
                     if (!normalizadorModelVO.getCuadra().isEmpty()) {

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                     }
                 }


                 if (request.getAddressViaCompNombre1() != null) {
                     if (!request.getAddressViaCompNombre1().isEmpty()) {
                         request.setAddressViaCompTipo1("MZ");
                     }

                 }

                 if (request.getAddressViaCompNombre2() != null) {
                     if (!request.getAddressViaCompNombre2().isEmpty()) {
                         request.setAddressViaCompTipo2("LT");
                     }
                 }


                 if (request.getAddressViaCompNombre3() != null) {
                     if (!request.getAddressViaCompNombre3().isEmpty()) {
                         request.setAddressViaCompTipo3("CDR");
                     }
                 }

                 if (request.getAddressViaCompTipo1() == null) {
                     if (request.getAddressViaCompTipo2() != null) {
                         if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                             request.setAddressViaCompTipo1("MZ");
                             request.setAddressViaCompNombre1(".");
                         }
                     }
                 }

                 if (request.getAddressViaCompTipo2() == null) {
                     if (request.getAddressViaCompTipo1() != null) {
                         if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                             request.setAddressViaCompTipo2("LT");
                             request.setAddressViaCompNombre2(".");
                         }
                     }
                 }
                 
                 if (!normalizadorModelVO.getNormalizador()) {
                     if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                         return false;
                     }
                     if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                         request = new AutomatizadorSaleRequestBody();

                         request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                         request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());

                         //TODO: Aplicar validacion
                         if (tdpOrder.getClientTipoDoc() != null) {
                             switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
                                 case "DNI":
                                     request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_DNI);
                                     break;
                                 case "CE":
                                     request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_CE);
                                     break;
                                 case "PAS":
                                     request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_PAS);
                                     break;
                                 case "RUC":
                                     request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_RUC);
                                     break;
                                 default:
                                     request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_NDE);
                                     break;
                             }
                         }


                         request.setUserAtis(tdpOrder.getUserAtis());
                         request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                         request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                         request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                         request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                         request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                         request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                         request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                         request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                         request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                         request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                         request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                         request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                         request.setOrderId(tdpOrder.getOrderId());

                         String dateOrderStr = "";
                         Date dateOrder = tdpOrder.getOrderFechaHora();

                         if (dateOrder != null) {
                             dateOrderStr = sdfDatetime.format(dateOrder);
                         }
                         if (dateOrderStr != null) {
                             request.setOrderFecha(dateOrderStr);
                         }

                         request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                         request.setOrderOperacionComercial("2");


                         //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                         if (tdpOrder.getClientNombre() != null) {
                             request.setClienteNombre(tdpOrder.getClientNombre());
                         }

                         if (tdpOrder.getClientApellidoPaterno() != null) {
                             if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                 request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                             } else {
                                 request.setClienteApellidoPaterno(".");
                             }
                         } else {
                             request.setClienteApellidoPaterno(".");
                         }

                         if (tdpOrder.getClientApellidoMaterno() != null) {
                             if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                 request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                             } else {
                                 request.setClienteApellidoMaterno(".");
                             }
                         } else {
                             request.setClienteApellidoMaterno(".");
                         }

                         request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                         request.setClienteEmail(tdpOrder.getClientEmail());
                         request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                         request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                         request.setUserDNI(tdpOrder.getUserDni());

                         request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                         request.setOrderModoVenta(ordenModoVenta);

                         if (tdpOrder.getUserFuerzaVenta() != null) {
                             request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                         }

                     }
                 
                 
             }
                 
             }else {
                 if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                     if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                    	 for(BeanAddress bean:listNormaTipo) {
                    		 if(normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase(bean.getNombre())) {
                    			 request.setAddressCCHHTipo(bean.getAbreviatura());
                    			 break;
                    		 }else {
                    			 request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                    		 }
                    	 }
                    	 
                     }
                 }

                 if (normalizadorModelVO.getTipoVia() != null) {
                     if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                    	 for(BeanAddress bean:listNormaTipoVia) {
                    		 if(normalizadorModelVO.getTipoVia().equalsIgnoreCase(bean.getNombre())) {
                    			  request.setAddressCalleAtis(bean.getAbreviatura());
                    			  break;
                    		 }else {
                    			 request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                    		 }
                    			 
                    	 }
                     }
                 }

                 if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                     if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                         request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                     }
                 }

                 if (normalizadorModelVO.getNombreVia() != null) {
                     if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                         request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                     } else {
                         request.setAddressCalleNombre(".");
                     }
                 }

                 if (request.getAddressViaCompTipo1() != null) {
                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(".");
                     }


                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");
                     }

                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");

                         request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                         request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                     }
                 }

                 if (request.getAddressViaCompTipo1() != null) {
                     if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(".");

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3("0");
                     }
                 }

                 if (request.getAddressViaCompTipo2() != null) {

                     if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(".");

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3("0");
                     }
                 }

                 if (normalizadorModelVO.getManzana() != null) {
                     if (!normalizadorModelVO.getManzana().isEmpty()) {

                         request.setAddressViaCompTipo1("MZ");
                         request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                     }

                 }

                 if (normalizadorModelVO.getLote() != null) {
                     if (!normalizadorModelVO.getLote().isEmpty()) {

                         request.setAddressViaCompTipo2("LT");
                         request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                     }
                 }

                 if (normalizadorModelVO.getCuadra() != null) {
                     if (!normalizadorModelVO.getCuadra().isEmpty()) {

                         request.setAddressViaCompTipo3("CDR");
                         request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                     }
                 }


                 if (request.getAddressViaCompNombre1() != null) {
                     if (!request.getAddressViaCompNombre1().isEmpty()) {
                         request.setAddressViaCompTipo1("MZ");
                     }

                 }

                 if (request.getAddressViaCompNombre2() != null) {
                     if (!request.getAddressViaCompNombre2().isEmpty()) {
                         request.setAddressViaCompTipo2("LT");
                     }
                 }


                 if (request.getAddressViaCompNombre3() != null) {
                     if (!request.getAddressViaCompNombre3().isEmpty()) {
                         request.setAddressViaCompTipo3("CDR");
                     }
                 }

                 if (request.getAddressViaCompTipo1() == null) {
                     if (request.getAddressViaCompTipo2() != null) {
                         if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                             request.setAddressViaCompTipo1("MZ");
                             request.setAddressViaCompNombre1(".");
                         }
                     }
                 }

                 if (request.getAddressViaCompTipo2() == null) {
                     if (request.getAddressViaCompTipo1() != null) {
                         if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                             request.setAddressViaCompTipo2("LT");
                             request.setAddressViaCompNombre2(".");
                         }
                     }
                 }


                 if (!normalizadorModelVO.getNormalizador()) {
                     if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                         return false;
                     }
                     if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                         request = new AutomatizadorSaleRequestBody();

                         request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                         request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());

                         //TODO: Aplicar validacion
                         if (tdpOrder.getClientTipoDoc() != null) {
                             switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
                                 case "DNI":
                                     request.setClienteTipoDoc("DNI");
                                     break;
                                 case "CE":
                                     request.setClienteTipoDoc("CEX");
                                     break;
                                 case "PAS":
                                     request.setClienteTipoDoc("CEX");
                                     break;
                                 case "RUC":
                                     request.setClienteTipoDoc("RUC");
                                     break;
                                 default:
                                     request.setClienteTipoDoc("NDE");
                                     break;
                             }
                         }

                         request.setUserAtis(tdpOrder.getUserAtis());
                         request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                         request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                         request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                         request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                         request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                         request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                         request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                         request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                         request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                         request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                         request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                         request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                         request.setOrderId(tdpOrder.getOrderId());

                         String dateOrderStr = "";
                         Date dateOrder = tdpOrder.getOrderFechaHora();

                         if (dateOrder != null) {
                             dateOrderStr = sdfDatetime.format(dateOrder);
                         }
                         if (dateOrderStr != null) {
                             request.setOrderFecha(dateOrderStr);
                         }

                         request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                         request.setOrderOperacionComercial("2");


                         //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                         if (tdpOrder.getClientNombre() != null) {
                             request.setClienteNombre(tdpOrder.getClientNombre());
                         }

                         if (tdpOrder.getClientApellidoPaterno() != null) {
                             if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                 request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                             } else {
                                 request.setClienteApellidoPaterno(".");
                             }
                         } else {
                             request.setClienteApellidoPaterno(".");
                         }

                         if (tdpOrder.getClientApellidoMaterno() != null) {
                             if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                 request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                             } else {
                                 request.setClienteApellidoMaterno(".");
                             }
                         } else {
                             request.setClienteApellidoMaterno(".");
                         }

                         request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                         request.setClienteEmail(tdpOrder.getClientEmail());
                         request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                         request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                         request.setUserDNI(tdpOrder.getUserDni());

                         request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                         request.setOrderModoVenta(ordenModoVenta);

                         if (tdpOrder.getUserFuerzaVenta() != null) {
                             request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                         }


                     }
                 }
             }
            }
         
         
         
         
         if(request.getAddressCCHHTipo() != null) {
        	 if(request.getAddressCCHHTipo().isEmpty()) {
        		 request.setAddressCCHHTipo("");
                 request.setAddressCCHHNombre("");
             }
             if(request.getAddressCCHHTipo().equalsIgnoreCase(".")){
                 request.setAddressCCHHTipo("");
                 request.setAddressCCHHNombre("");
             }
         }
         
         if(request.getAddressCCHHCompTipo1()!=null) {
        	 if(request.getAddressCCHHCompNombre1()==null) {
        		 request.setAddressCCHHCompNombre1(".");
        	 }
         }else{
        	 request.setAddressCCHHCompTipo1("");
        	 request.setAddressCCHHCompNombre1("");
         }
         
         if(request.getAddressCCHHCompTipo2()!=null) {
        	 if(request.getAddressCCHHCompNombre2()==null) {
        		 request.setAddressCCHHCompNombre2(".");
        	 }
         }else {
        	 request.setAddressCCHHCompTipo2("");
        	 request.setAddressCCHHCompNombre2("");
         }
         
         if(request.getAddressCCHHCompTipo3()!=null) {
        	 if(request.getAddressCCHHCompNombre3()==null) {
        		 request.setAddressCCHHCompNombre3(".");
        	 }
         }else {
        	 request.setAddressCCHHCompTipo3("");
        	 request.setAddressCCHHCompNombre3("");
         }
         
         if(request.getAddressCCHHCompTipo4()!=null) {
        	 if(request.getAddressCCHHCompNombre4()==null) {
        		 request.setAddressCCHHCompNombre4(".");
        	 }
         }else {
        	 request.setAddressCCHHCompTipo4("");
        	 request.setAddressCCHHCompNombre4("");         
        }
         
         if(request.getAddressCCHHCompTipo5()!=null) {
        	 if(request.getAddressCCHHCompNombre5()==null) {
        		 request.setAddressCCHHCompNombre5(".");
        	 }
         }else {
        	 request.setAddressCCHHCompTipo5("");
        	 request.setAddressCCHHCompNombre5("");
         }
         
         
         if(request.getAddressViaCompTipo1()!=null) {
        	 if(request.getAddressViaCompNombre1()==null) {
        		 request.setAddressViaCompNombre1(".");
        	 }
         }else {
        	 
        	request.setAddressViaCompTipo1("");
        	request.setAddressViaCompNombre1("");
         }
         
         if(request.getAddressViaCompTipo2()!=null) {
        	 if(request.getAddressViaCompNombre2()==null) {
        		 request.setAddressViaCompNombre2(".");
        	 }
         }else {
        	 request.setAddressViaCompTipo2("");
        	 request.setAddressViaCompNombre2("");
         }
         
         if(request.getAddressViaCompTipo3()!=null) {
        	 if(request.getAddressViaCompNombre3()==null) {
        		 request.setAddressViaCompNombre3(".");
        	 }
         }else {
        	 request.setAddressViaCompTipo3("");
        	 request.setAddressViaCompNombre3("");
         }
                
         if(request.getAddressViaCompTipo4()!=null) {
        	 if(request.getAddressViaCompNombre4()==null) {
        		 request.setAddressViaCompNombre4(".");
        	 }
         }else {
        	 request.setAddressViaCompTipo4("");
        	 request.setAddressViaCompNombre4("");
         }
         
         if(request.getAddressViaCompTipo5()!=null) {
        	 if(request.getAddressViaCompNombre5()==null) {
        		 request.setAddressViaCompNombre5(".");
        	 }
         }else {
        	 request.setAddressViaCompTipo5("");
        	 request.setAddressViaCompNombre5("");
         }
         
         if(fijaRequest.getNormalizador().getPiso()!=null && fijaRequest.getNormalizador().getPiso()!="") {
        	 request.setAddressViaCompTipo4(Constants.COD_TIPO_PISO);
        	 request.setAddressViaCompNombre4(fijaRequest.getNormalizador().getPiso());
         }
         if(fijaRequest.getNormalizador().getInterior()!=null && fijaRequest.getNormalizador().getInterior()!="") {
        	 request.setAddressViaCompTipo5(Constants.COD_TIPO_PISO);
        	 request.setAddressViaCompNombre5(fijaRequest.getNormalizador().getPiso());
         }
         
         if(request.getClienteRucDigitoControl()==null) {
        	 request.setClienteRucDigitoControl("");
         }
         
         if(request.getClienteRucActividadCodigo()==null) {
        	 request.setClienteRucActividadCodigo("");
         }
         if(request.getClienteRucSubActividadCodigo()==null) {
        	 request.setClienteRucSubActividadCodigo("");
         }
         request.setClienteCMSCodigo("");
         
         
         if(request.getOrderGisTipoSenial()==null) {
        	 request.setOrderGisTipoSenial(".");
         }
         
         if(request.getProductoCabecera()==null) {
        	 request.setProductoCabecera("");
         }
         if(request.getProductoCodigo()==null) {
        	 request.setProductoCodigo(".");
         }
         
         if(isSVA) {
        	 request.setOrderParqueTelefono(fijaRequest.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getServiceNumber());
         }
         if(request.getOrderParqueTelefono()==null) {
        	 request.setOrderParqueTelefono("");
         }
         if(request.getClienteCMSServicio()==null) {
        	 request.setClienteCMSServicio("");
         }
         if(request.getClienteNacimiento()==null) {
        	 request.setClienteNacimiento("");
         }
         if(request.getClienteTelefono1Area()==null) {
        	 request.setClienteTelefono1Area("");
         }
         if(request.getClienteTelefono2Area()==null) {
        	 request.setClienteTelefono2Area("");
         }
         if(request.getClienteCMSFactura()==null) {
    	   request.setClienteCMSFactura("");
         }
         
         if(request.getAddressCCHHCodigo()==null) {
        	 request.setAddressCCHHCodigo("");
         }
         // Fin de validacion de Automatizador
         
         
         String[] erroresBuss = errBusParametrizable.split(",");

         String mensaje_duplicado = "";
         boolean result = false;
         
         
         try {
             ApiResponseAuto<InsertResponseBodyAuto> response = sendInsertOrder(request);
             resultadoFinal=response;
             if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                     response.getHeaderOut().getMsgType().equalsIgnoreCase("RESPONSE")) {
                 result = true;
                 mensaje_duplicado = "OK";
                 tdpOrderRepository.updateAutomatizadorOkTdpOrder(1, request.getOrderId());
             }
             
             if (response != null && response.getBodyOut() != null) {
                 if (response.getBodyOut().getServerException() != null) {
                     for (String i : erroresBuss) {
                         int res = Integer.parseInt(i);
                         if (response.getBodyOut().getServerException().getExceptionCode() == res) {
                             mensaje_duplicado = "ERR_BUS";
                             
                            //Cambiar estado de TDP_Visor a Enviando_Auto_MT
                            tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.INDICADOR_BATCH_REINTENTOS, request.getOrderId());
                            
                         }

                     }
                 }
             }
             
             if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                     response.getHeaderOut().getMsgType().equalsIgnoreCase("ERROR")) {

                 if (response.getBodyOut() != null) {
                     if (response.getBodyOut() != null) {
                         if (response.getBodyOut().getClientException() != null) {

                             if (response.getBodyOut().getClientException().getAppDetail().getExceptionAppCode().equalsIgnoreCase("ERR-0004")) {
                                 addressDao._registerDuplicado(orderId);
                                 mensaje_duplicado = "ERR-0004";
                             }


                         }
                     }
                 }
             }
         }catch (Exception ex) {
        	 ex.printStackTrace();
             tdpVisorRepository.updateEstadoSolicitudVentaMT(ServiceConstants.INDICADOR_BATCH_REINTENTOS, request.getOrderId());
		}

         
         addressDao.insertAuto(orderId);
         if (mensaje_duplicado.equalsIgnoreCase("ERR-0004")) {
             automatizadorOrderRepository.updateAutomatizadorOrder(mensaje_duplicado, orderId);
         }
         
         if (mensaje_duplicado.equalsIgnoreCase("ERR_BUS")) {
             automatizadorOrderRepository.updateAutomatizadorOrder(mensaje_duplicado, orderId);
             //guardar el request
             AutomatizadorSalesService automatizadorSaleService = poblarRequest(request);

             automatizadorRequestSaleService.save(automatizadorSaleService);
         }
         return result;
	  }catch (Exception e) {
		  return false;
	  }
		
	 }

	//Cantidad de equipos que se obtiene del total de SVA que se tiene.
	 public String getCantidadEquipos(TdpOrder tdpOrder) {
		 
		 Integer cantidad=0;
		 
		 if (tdpOrder.getSvaCode1() != null && (tdpOrder.getSvaCode1().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode1().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode1().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad1();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
		 if (tdpOrder.getSvaCode2() != null && (tdpOrder.getSvaCode2().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode2().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode2().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad2();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
		 if (tdpOrder.getSvaCode3() != null && (tdpOrder.getSvaCode3().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode3().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode3().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad3();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode4() != null && (tdpOrder.getSvaCode4().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode4().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode4().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad4();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode5() != null && (tdpOrder.getSvaCode5().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode5().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode5().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad5();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode6() != null && (tdpOrder.getSvaCode6().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode6().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode6().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad6();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode7() != null && (tdpOrder.getSvaCode7().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode7().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode7().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad7();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode8() != null && (tdpOrder.getSvaCode8().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode8().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode8().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad8();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode9() != null && (tdpOrder.getSvaCode9().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode9().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode9().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad9();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
	        if (tdpOrder.getSvaCode10() != null && (tdpOrder.getSvaCode10().equalsIgnoreCase("DHD") ||
	                tdpOrder.getSvaCode10().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode10().equalsIgnoreCase("DVR"))) {
	            try {
	                cantidad = cantidad + tdpOrder.getSvaCantidad10();
	            } catch (Exception ex) {
	            	logger.error("Error:"+ex.getMessage());
	            }
	        }
		 
		 return cantidad.toString();
	 }
	/**
	 * Metodo para el seteo general de la region
	 * @param request
	 * @param tdpOrder
	 * @param sdfDatetime
	 * @param isPaquetizacion
	 * @return
	 */
	 
	 public AutomatizadorSaleRequestBody SetGeneralAutomatizador(AutomatizadorSaleRequestBody request,
             TdpOrder tdpOrder,
             SimpleDateFormat sdfDatetime,
             boolean isPaquetizacion) {
		 
		 
		 List<String> automatizadorAux = automatizadorExtraValidacion();
		 
		 if (((tdpOrder.getAddressCchhTipo() != null)
	                && (tdpOrder.getAddressCchhNombre() != null)
	                && (tdpOrder.getAddressCalleAtis() != null)
	                && (tdpOrder.getAddressCalleNombre() != null)
	                && (tdpOrder.getAddressCalleNumero() != null)) &&
	                ((tdpOrder.getAddressCchhTipo() != "")
	                        && (tdpOrder.getAddressCchhNombre() != "")
	                        && (tdpOrder.getAddressCalleAtis() != "")
	                        && (tdpOrder.getAddressCalleNombre() != "")
	                        && (tdpOrder.getAddressCalleNumero() != ""))
	                ) {
	            logger.info("Campos completos en la lógica de direcciones, no entra en la contingencia.");
	        } else {
	            //no tiene conjunto habitacional ni nombre de conjunto habitacional
	            if (tdpOrder.getAddressCchhTipo() == null && tdpOrder.getAddressCchhNombre() == null) {
	                //tiene tipo de vía(calle) y nombre de vía(calle)
	                if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() != null) {
	                    //no tiene número de puerta
	                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
	                        //número de puerta asignale un "."
	                        tdpOrder.setAddressCalleNumero("0");
	                    }

	                }
	                //tiene tipo de vía(calle) y pero no nombre de vía(calle)
	                else if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() == null) {
	                    //seteamos nombre vía(calle)
	                    tdpOrder.setAddressCalleNombre(".");
	                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
	                        //número de puerta asignale un "."
	                        tdpOrder.setAddressCalleNumero("0");
	                    }

	                }
	                else if (tdpOrder.getAddressCalleAtis() == null || tdpOrder.getAddressCalleAtis().isEmpty()) {
	                    tdpOrder.setAddressCalleAtis("CL");

	                    //si se tiene manzana y no se tiene lote se envía en lote un "."
	                    if (tdpOrder.getAddressViaCompTip1() != null ) {
	                        //si no se tiene lote
	                        if (tdpOrder.getAddressViaCompNom2() == null || tdpOrder.getAddressViaCompNom2() == "") {
	                            tdpOrder.setAddressViaCompNom2(".");
	                        }
	                    }
	                    //si se tiene lote pero no se tiene manzana se envia en manzana un "."
	                    if (tdpOrder.getAddressViaCompTip2() != null) {
	                        if (tdpOrder.getAddressViaCompTip1() == null || tdpOrder.getAddressViaCompTip1() == "") {
	                            tdpOrder.setAddressViaCompNom1(".");
	                        }
	                    }

	                }
	                //ni no tiene nombre vía (calle)
	                if (tdpOrder.getAddressCalleNombre() == null || tdpOrder.getAddressCalleNombre().isEmpty()) {
	                    tdpOrder.setAddressCalleNombre(".");
	                }
	                if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
	                    tdpOrder.setAddressCalleNumero("0");
	                }
	                //si tene conjunto habitacional y nombre de conjunto habitacional
	            } else if (tdpOrder.getAddressCchhTipo() != null && tdpOrder.getAddressCchhNombre() != null) {
	                //tiene tipo de vía(calle) y nombre de vía(calle)
	                if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() != null) {
	                    //no tiene número de puerta
	                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
	                        //número de puerta asignale un "."
	                        tdpOrder.setAddressCalleNumero("0");
	                    }
	                } else {
	                    //Si no tiene tipo de via (calle)
	                    if (tdpOrder.getAddressCalleAtis() == null || tdpOrder.getAddressCalleAtis().isEmpty()) {
	                        tdpOrder.setAddressCalleAtis("CL");

	                        //si se tiene manzana y no se tiene lote se envía en lote un "."
	                        if (tdpOrder.getAddressViaCompTip1() != null || tdpOrder.getAddressViaCompTip1() != "") {
	                            //si no se tiene lote
	                            if (tdpOrder.getAddressViaCompNom2() == null || tdpOrder.getAddressViaCompNom2() == "") {
	                                tdpOrder.setAddressViaCompNom2(".");
	                            }
	                        }
	                        //si se tiene lote pero no se tiene manzana se envia en manzana un "."
	                        if (tdpOrder.getAddressViaCompTip2() != null || tdpOrder.getAddressViaCompTip2() != "") {
	                            if (tdpOrder.getAddressViaCompTip1() == null || tdpOrder.getAddressViaCompTip1() == "") {
	                                tdpOrder.setAddressViaCompNom1(".");
	                            }
	                        }

	                    }
	                    //ni no tiene nombre vía (calle)
	                    if (tdpOrder.getAddressCalleNombre() == null) {
	                        tdpOrder.setAddressCalleNombre(".");
	                    }

	                    if(tdpOrder.getAddressCalleNombre() != null){
	                        if(tdpOrder.getAddressCalleNombre().isEmpty()){
	                            tdpOrder.setAddressCalleNombre(".");
	                        }
	                    }


	                    if (tdpOrder.getAddressCalleNumero() == null) {
	                        tdpOrder.setAddressCalleNumero("0");
	                    }

	                    if(tdpOrder.getAddressCalleNumero() != null){
	                        if(tdpOrder.getAddressCalleNumero().isEmpty()){
	                            tdpOrder.setAddressCalleNumero("0");
	                        }
	                    }

	                }
	            }
	            if(tdpOrder.getAddressViaCompNom1()!= null){
	                if(tdpOrder.getAddressViaCompNom1().isEmpty()){
	                    if(tdpOrder.getAddressViaCompNom1().equalsIgnoreCase(".")){
	                        tdpOrder.setAddressViaCompTip1("MZ");
	                    }
	                }
	            }

	            if(tdpOrder.getAddressViaCompNom2()!= null){
	                if(tdpOrder.getAddressViaCompNom2().isEmpty()){
	                    if(tdpOrder.getAddressViaCompNom2().equalsIgnoreCase(".")){
	                        tdpOrder.setAddressViaCompTip2("LT");
	                    }
	                }
	            }

	            if (tdpOrder.getAddressCalleNumero() == null) {

	                tdpOrder.setAddressCalleNumero("0");
	            }
	            if(tdpOrder.getAddressCalleNumero() != null){
	                if(tdpOrder.getAddressCalleNumero().isEmpty()){
	                    tdpOrder.setAddressCalleNumero("0");
	                }
	            }

	            //ni no tiene nombre vía (calle)
	            if (tdpOrder.getAddressCalleNombre() == null) {
	                tdpOrder.setAddressCalleNombre(".");
	            }

	            if(tdpOrder.getAddressCalleNombre() !=null){
	                if(tdpOrder.getAddressCalleNombre().isEmpty()){
	                    tdpOrder.setAddressCalleNombre(".");
	                }
	            }
	        }
		 	
		 //Valida que el campo nombre de cliente no sea null.
		 if (tdpOrder.getClientNombre() != null) {
	            request.setClienteNombre(tdpOrder.getClientNombre());
	        }
		 
		 //Valida que el campo apellido paterno no sea null.
		 if (tdpOrder.getClientApellidoPaterno() != null ) {
	            if(!tdpOrder.getClientApellidoPaterno().isEmpty()){
	                request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
	            }else{
	                request.setClienteApellidoPaterno(".");
	            }
	        } else {
	            request.setClienteApellidoPaterno(".");
	        }
		 //Valida que el campo apellido Materno no sea null.
		 if (tdpOrder.getClientApellidoMaterno() != null) {
	            if(!tdpOrder.getClientApellidoMaterno().isEmpty()){
	                request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
	            } else {
	                request.setClienteApellidoMaterno(".");
	            }
	        } else {
	            request.setClienteApellidoMaterno(".");
	        }
		 //valida que el campo numero de documento no sea null.
		 if (tdpOrder.getClientNumeroDoc() != null) {
	            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
	        } else if ((tdpOrder.getClientNumeroDoc() == null)) {
	            return null;
	        }
		 //Valida el tipo de documento para setearlo
		 if (tdpOrder.getClientTipoDoc() != null) {
	            switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
	                case "DNI":
	                    request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_DNI);
	                    break;
	                case "CE":
	                    request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_CE);
	                    break;
	                case "PAS":
	                    request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_PAS);
	                    break;
	                case "RUC":
	                    request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_RUC);
	                    break;
	                default:
	                    request.setClienteTipoDoc(Constants.TIPO_DOCUMENTO_NDE);
	                    break;
	            }
	        } else {
	            return null;
	        }
		 
		 Boolean telefonoValidate = Boolean.parseBoolean(automatizadorAux.get(6));
		 
		 //Valida el campo telefono 1 del cliente.
		 if(telefonoValidate){
	            if (tdpOrder.getClientTelefono1() != null) {
	                if(!tdpOrder.getClientTelefono1().isEmpty()){
	                    request.setClienteTelefono1(tdpOrder.getClientTelefono1());
	                } else{
	                    return null;
	                }
	            }else{
	                return null;
	            }
	        }else{
	            if (tdpOrder.getClientTelefono1() != null) {
	                request.setClienteTelefono1(tdpOrder.getClientTelefono1());
	            }
	        }
		 //Valida el campo telefono 2 si es null.
		  if (tdpOrder.getClientTelefono2() != null) {
	            request.setClienteTelefono2(tdpOrder.getClientTelefono2());
	        }
		 
		  List<BeanAddress> list= new ArrayList<BeanAddress>();
		  list= agregarList("PSJ", "PJ",list);
		  list=agregarList("AUTOPISTA", "AU", list);
		  list= agregarList("SENDERO", "SD",list);
		  list=agregarList("CUESTA", "CT", list);
		  list= agregarList("CAMINO", "CM",list);
		  list=agregarList("PLAZUELA", "PZ", list);
		  list= agregarList("PASEO", "PO",list);
		  list=agregarList("PZ", "PL", list);
		  list= agregarList("PSJ", "PJ",list);
		  list=agregarList("OVALO", "OV", list);
		  list= agregarList("ML", "MA",list);
		  list=agregarList("GAL", "GL", list);
 		  list= agregarList("CALLEJON", "CJ",list);
		  list=agregarList("CAR", "CA", list);
		  list= agregarList("COND", "CDM",list);
		  
		  if (tdpOrder.getAddressCalleAtis() != null) {
			  for(BeanAddress b:list) {
				  if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase(b.getNombre())) {
					  request.setAddressCalleAtis(b.getAbreviatura());
					  break;
				  }else {
					  request.setAddressCalleAtis(tdpOrder.getAddressCalleAtis());
				  }
			  }
		  }
		  //Valida el campo nombre de calle de address si es null.
		  if (tdpOrder.getAddressCalleNombre() != null) {
	            request.setAddressCalleNombre(tdpOrder.getAddressCalleNombre());
	        }
		//Valida el campo numeri de calle de address si es null.
	        if (tdpOrder.getAddressCalleNumero() != null) {
	            request.setAddressCalleNumero(tdpOrder.getAddressCalleNumero());
	        } else {
	            request.setAddressCalleNumero("0");
	        }
		  
	        List<BeanAddress> listAddressCchhTipo= new ArrayList<BeanAddress>();
	        listAddressCchhTipo=agregarList("URB", "UR", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("POBLADO", "PO", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("VILLA MILITAR", "VM", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("URBANIZACION POPULAR", "UP", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("RES", "RS", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("PPJJ", "PJ", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("PARCELA", "PA", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("LOT", "LO", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("GRUPO", "GR", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("FUNDO", "FU", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("COOP", "CV", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("CIUDAD UNIVERSITARIA", "CU", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("CIUDAD SATELITE", "CS", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("CH", "CO", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("COMITE", "CM", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("CIUDADELA", "CD", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("BARRIO", "BA", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("ASOC", "AS", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("AGR", "AG", listAddressCchhTipo);
	        listAddressCchhTipo=agregarList("COND", "CDM", listAddressCchhTipo);
	        
	        if (tdpOrder.getAddressCchhTipo() != null) {
	            if(!tdpOrder.getAddressCchhTipo().isEmpty()){
				  for(BeanAddress b:listAddressCchhTipo) {
					  if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase(b.getNombre())) {
						  request.setAddressCCHHTipo(b.getAbreviatura());
						  break;
					  }else {
						  request.setAddressCCHHTipo(tdpOrder.getAddressCchhTipo());
					  }
				  }
				}else {
					request.setAddressCCHHTipo(".");
				}
			  }else {
				  request.setAddressCCHHTipo(".");
			  }
	        
	        //Valida el campo de chh nombre address si es null.
	        if (tdpOrder.getAddressCchhNombre() != null) {
	            if(!tdpOrder.getAddressCchhNombre().isEmpty()){
	                request.setAddressCCHHNombre(tdpOrder.getAddressCchhNombre());
	            }else{
	                request.setAddressCCHHNombre(".");
	            }
	        }else{
	            request.setAddressCCHHNombre(".");
	        }	
	        
	        
	        //valida que el campo no sea null.
	        if (tdpOrder.getOrderGisCable() != null) {
	            request.setOrderGisTipoSenial(tdpOrder.getOrderGisCable());
	        }

	        if (tdpOrder.getOrderGisGpon() != null) {
	            if(!tdpOrder.getOrderGisGpon().isEmpty()){
	                request.setOrderGisGPON(tdpOrder.getOrderGisGpon());
	            } else {
	                request.setOrderGisGPON("N");
	            }
	        } else {
	            request.setOrderGisGPON("N");
	        }

	        if (tdpOrder.getProductCodigo() != null) {
	            if(!tdpOrder.getProductCodigo().isEmpty()){
	                request.setProductoCodigo(tdpOrder.getProductCodigo());
	            }
	        }
	        if(tdpOrder.getOrderOperationNumber()!=null) {
	        	if(tdpOrder.getOrderOperationNumber()==2) {
		        	request.setProductoCodigo("");
		        }
	        }
	        

	        if (tdpOrder.getOrderId() != null) {
	            request.setOrderId(tdpOrder.getOrderId());
	        } else {
	            return null;
	        }

	        String dateOrderStr = "";
	        Date dateOrder = tdpOrder.getOrderFechaHora();
	        if (dateOrder != null) {
	            dateOrderStr = sdfDatetime.format(dateOrder);
	        }
	        if (dateOrderStr != null) {
	            request.setOrderFecha(dateOrderStr);
	        } else {
	            return null;
	        }

	        if (tdpOrder.getOrderOperationNumber() != null) {
	            if(tdpOrder.getOrderOperationNumber().equals(0)){
	                return null;
	            }
	            request.setOrderOperacionComercial(tdpOrder.getOrderOperationNumber().toString());
	        } else {
	            return null;
	        }

	        if (tdpOrder.getClientCmsServicio() != null) {
	            request.setClienteCMSServicio(tdpOrder.getClientCmsServicio());
	        }
	        
	        TdpUbigeoEquiv ubigeo = tdpUbigeoEquivRepository.getUbigeoAutomatizadorByNames(tdpOrder.getAddressDepartamento(), tdpOrder.getAddressProvincia(), tdpOrder.getAddressDistrito());
	        String departamentoCod = "000", provinciaCod = "000", distritoCod = "000";
	        if (ubigeo != null) {
	            departamentoCod = ubigeo.getDepCod();
	            provinciaCod = ubigeo.getProvCod();
	            distritoCod = ubigeo.getDisCod();
	        }


	        request.setAddressDepartamentoCodigo(departamentoCod);
	        request.setAddressProvinciaCodigo(provinciaCod);
	        request.setAddressDistritoCodigo(distritoCod);

	        return request;
	 }
	 
	 public List<BeanAddress> agregarList(String valor1, String valor2,List<BeanAddress> list) {
		
		  BeanAddress bean= new BeanAddress();
		  bean.setNombre(valor1);
		  bean.setAbreviatura(valor2);
		  list.add(bean);
		  
		  return list;
	 }

	 public AutomatizadorSaleRequestBody setAtisAutomatizador(AutomatizadorSaleRequestBody request, TdpOrder tdpOrder) {
		 
		 
		 if (tdpOrder.getClientRucDigitCtrl() != null) {
	            request.setClienteRucDigitoControl(tdpOrder.getClientRucDigitCtrl());
	        }
	        if (tdpOrder.getClientRucActividad() != null) {
	            request.setClienteRucActividadCodigo(tdpOrder.getClientRucActividad().toString());
	        }
	        if (tdpOrder.getClientRucSubactividad() != null) {
	            request.setClienteRucSubActividadCodigo(tdpOrder.getClientRucSubactividad().toString());
	        }

	        if (tdpOrder.getAddressCchhCodigo() != null) {
	            request.setAddressCCHHCodigo(tdpOrder.getAddressCchhCodigo().toString());

	        }
	        if (tdpOrder.getAddressCchhCompTip1() != null ) {
	            if(!tdpOrder.getAddressCchhCompTip1().isEmpty()){
	                request.setAddressCCHHCompTipo1(tdpOrder.getAddressCchhCompTip1());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompNom1() != null ) {
	            if(!tdpOrder.getAddressCchhCompNom1().isEmpty()){
	                request.setAddressCCHHCompNombre1(tdpOrder.getAddressCchhCompNom1());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompTip2() != null ) {
	            if(!tdpOrder.getAddressCchhCompTip2().isEmpty()){
	                request.setAddressCCHHCompTipo2(tdpOrder.getAddressCchhCompTip2());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompNom2() != null ) {
	            if(!tdpOrder.getAddressCchhCompNom2().isEmpty()){
	                request.setAddressCCHHCompNombre2(tdpOrder.getAddressCchhCompNom2());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompTip3() != null) {
	            if(!tdpOrder.getAddressCchhCompTip3().isEmpty()){
	                request.setAddressCCHHCompTipo3(tdpOrder.getAddressCchhCompTip3());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompNom3() != null ) {
	            if(!tdpOrder.getAddressCchhCompNom3().isEmpty()){
	                request.setAddressCCHHCompNombre3(tdpOrder.getAddressCchhCompNom3());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompTip4() != null ) {
	            if(!tdpOrder.getAddressCchhCompTip4().isEmpty()){
	                request.setAddressCCHHCompTipo4(tdpOrder.getAddressCchhCompTip4());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompNom4() != null) {
	            if(!tdpOrder.getAddressCchhCompNom4().isEmpty()){
	                request.setAddressCCHHCompNombre4(tdpOrder.getAddressCchhCompNom4());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompTip5() != null) {
	            if(!tdpOrder.getAddressCchhCompTip5().isEmpty()){
	                request.setAddressCCHHCompTipo5(tdpOrder.getAddressCchhCompTip5());
	            }
	        }
	        if (tdpOrder.getAddressCchhCompNom5() != null) {
	            if(!tdpOrder.getAddressCchhCompNom5().isEmpty()){
	                request.setAddressCCHHCompNombre5(tdpOrder.getAddressCchhCompNom5());
	            }
	        }
	        
	        //Coreccion de duplicado
	        //Inicio duplicado
	        
	        if (tdpOrder.getAddressViaCompTip1() != null) {
	            if(!tdpOrder.getAddressViaCompTip1().isEmpty()){
	                request.setAddressViaCompTipo1(tdpOrder.getAddressViaCompTip1());
	            }
	        }
	        if (tdpOrder.getAddressViaCompNom1() != null) {
	            if(!tdpOrder.getAddressViaCompNom1().isEmpty()){
	                request.setAddressViaCompNombre1(tdpOrder.getAddressViaCompNom1());
	            }
	        }

	        if(tdpOrder.getAddressViaCompTip1() != null){
	            if (tdpOrder.getAddressViaCompTip2() != null) {
	                if(!tdpOrder.getAddressViaCompTip2().isEmpty()){
	                    if(!tdpOrder.getAddressViaCompTip1().equalsIgnoreCase(tdpOrder.getAddressViaCompTip2())){
	                        request.setAddressViaCompTipo2(tdpOrder.getAddressViaCompTip2());
	                    }
	                }
	            }

	            if (tdpOrder.getAddressViaCompNom2() != null) {
	                if(!tdpOrder.getAddressViaCompNom2().isEmpty()){
	                    if(!tdpOrder.getAddressViaCompNom1().equalsIgnoreCase(tdpOrder.getAddressViaCompNom2())){
	                        request.setAddressViaCompNombre2(tdpOrder.getAddressViaCompNom2());
	                    }
	                    if(tdpOrder.getAddressViaCompNom2().equalsIgnoreCase(".")){
	                        request.setAddressViaCompNombre2(tdpOrder.getAddressViaCompNom2());
	                    }
	                }
	            }
	        }


	        //OCT_TIP_CMP
	        if (tdpOrder.getAddressViaCompTip3() != null) {
	            request.setAddressViaCompTipo3(null);
	        }
	        //OCT_TIP_DIR
	        if (tdpOrder.getAddressViaCompNom3() != null) {
	            request.setAddressViaCompNombre3(null);

	        }
	        //NOV_TIP_CMP
	        if (tdpOrder.getAddressViaCompTip4() != null) {
	            request.setAddressViaCompTipo4(null);
	        }
	        //NOV_TIP_DIR
	        if (tdpOrder.getAddressViaCompNom4() != null) {
	            request.setAddressViaCompNombre4(null);
	        }
	        //DCO_TIP_CMP
	        if (tdpOrder.getAddressViaCompTip5() != null) {
	            request.setAddressViaCompTipo5(null);
	        }
	        //DCO_TIP_DIR
	        if (tdpOrder.getAddressViaCompNom5() != null) {
	            request.setAddressViaCompNombre5(null);
	        }

	        if (tdpOrder.getClientEmail() != null) {
	            request.setClienteEmail(tdpOrder.getClientEmail());
	        }

	        //Fin duplicado
		 
	        
	        
	        if (tdpOrder.getOrderGisXdsl() != null) {
	            if(!tdpOrder.getOrderGisXdsl().equalsIgnoreCase("")){
	                request.setOrderGisXDSL(tdpOrder.getOrderGisXdsl());
	            }else{
	                request.setOrderGisXDSL("N");
	            }
	        } else {
	            request.setOrderGisXDSL("N");
	        }
	        
	        if (tdpOrder.getOrderGisHfc() != null) {
	            if(!tdpOrder.getOrderGisHfc().equalsIgnoreCase("")){
	                request.setOrderGisHFC(tdpOrder.getOrderGisHfc());
	            }else {
	                request.setOrderGisHFC("N");
	            }
	        } else {
	            request.setOrderGisHFC("N");
	        }
	        
	        if (tdpOrder.getUserAtis() != null) {
	            if(!tdpOrder.getUserAtis().equalsIgnoreCase("")){
	                request.setUserAtis(tdpOrder.getUserAtis());
	            }
	        }
	        
	        if (tdpOrder.getUserCanalCodigo() != null) {
	            if(!tdpOrder.getUserCanalCodigo().isEmpty()){
	                request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());
	            }
	        }
	        
	        if (tdpOrder.getProductPsAdmin1() != null ) {
	            request.setProductoPSAdmin1(tdpOrder.getProductPsAdmin1().toString());
	        }
	        if (tdpOrder.getProductPsAdmin2() != null) {
	            request.setProductoPSAdmin2(tdpOrder.getProductPsAdmin2().toString());
	        }
	        if (tdpOrder.getProductPsAdmin3() != null) {
	            request.setProductoPSAdmin3(tdpOrder.getProductPsAdmin3().toString());
	        }
	        if (tdpOrder.getProductPsAdmin4() != null) {
	            request.setProductoPSAdmin4(tdpOrder.getProductPsAdmin4().toString());
	        }

	        if (tdpOrder.getSvaCodigo1() != null) {
	            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
	        }
	        if (tdpOrder.getSvaCodigo2() != null) {
	            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
	        }
	        if (tdpOrder.getSvaCodigo3() != null) {
	            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
	        }
	        if (tdpOrder.getSvaCodigo4() != null) {
	            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
	        }
	        if (tdpOrder.getSvaCodigo5() != null) {
	            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
	        }
	        if (tdpOrder.getSvaCodigo6() != null) {
	            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
	        }
	        if (tdpOrder.getSvaCodigo7() != null) {
	            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
	        }
	        if (tdpOrder.getSvaCodigo8() != null) {
	            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
	        }
	        if (tdpOrder.getSvaCodigo9() != null) {
	            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
	        }
	        if (tdpOrder.getSvaCodigo10() != null) {
	            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());
	        }
	        
	        
	        if (tdpOrder.getOrderParqueTelefono() != null) {
	            if(!tdpOrder.getOrderParqueTelefono().isEmpty()){
	                request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
	            }
	        }
	        if (tdpOrder.getClientCmsCodigo() != null ) {

	            request.setClienteCMSCodigo(tdpOrder.getClientCmsCodigo().toString());
	        }
	        
	        String gpsX = tdpOrder.getOrderGpsX() != null ? tdpOrder.getOrderGpsX().toString() : "";
	        String gpsY = tdpOrder.getOrderGpsY() != null ? tdpOrder.getOrderGpsY().toString() : "";
	        if (gpsX != null && gpsX != "") {
	            request.setOrderGPSX(gpsX.length() > 10 ? gpsX.substring(0, 11) : gpsX);
	        }
	        if (gpsY != null && gpsY != "") {
	            request.setOrderGPSY(gpsY.length() > 10 ? gpsY.substring(0, 11) : gpsY);
	        }

	        if (tdpOrder.getUserFuerzaVenta() != null && String.valueOf(tdpOrder.getUserFuerzaVenta()) != "") {
	            request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
	        }
	        
	        if (tdpOrder.getUserCms() != null && String.valueOf(tdpOrder.getUserCms()) != "") {
	            request.setUserCMS(String.valueOf(tdpOrder.getUserCms()));
	        }

	        if (tdpOrder.getClientSexo() != null && String.valueOf(tdpOrder.getUserCms()) != "") {
	            request.setClienteSexo(tdpOrder.getClientSexo());
	        }
	        
	        if(tdpOrder.getClientCivil()!=null){
	        	request.setClienteCivil(tdpOrder.getClientCivil());
	        }

	        return request;
	 }
	 
	 public AutomatizadorSaleRequestBody setCMSAutomatizador(AutomatizadorSaleRequestBody request, TdpOrder tdpOrder, SimpleDateFormat sdfDate) {
		  
		 if (tdpOrder.getProductCabecera() != null) {
	            if(!tdpOrder.getProductCabecera().isEmpty())
	                request.setProductoCabecera(tdpOrder.getProductCabecera());
	        } else {
	            return null;
	        }
		  
		  
		 	String dateNacimientoStr = "";
	        Date dateNacimiento = tdpOrder.getClientNacimiento();
	        if (dateNacimiento != null) {
	            dateNacimientoStr = sdfDate.format(dateNacimiento);
	        }
	        if (dateNacimientoStr != null) {
	            request.setClienteNacimiento(dateNacimientoStr);
	        } else {
	            return null;
	        }
	        
	        if (tdpOrder.getClientSexo() != null) {
	            request.setClienteSexo(tdpOrder.getClientSexo());
	        }

	        request.setClienteCivil("SOLTERO");

	        if (tdpOrder.getClientEmail() != null) {
	            request.setClienteEmail(tdpOrder.getClientEmail());
	        }

	        String telefonoArea = "0";
	        List<Parameters> telAreaList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "TEL_AREA");
	        for (Parameters obj : telAreaList) {
	            if (toLowerWithouAccent(obj.getElement()).equalsIgnoreCase(toLowerWithouAccent(tdpOrder.getAddressDepartamento()))) {
	                telefonoArea = obj.getStrValue();
	                break;
	            }
	        }
	        
	        if (telefonoArea != null || telefonoArea != "") {
	            request.setClienteTelefono1Area(telefonoArea);
	        }

	        if (tdpOrder.getClientTelefono2Area() != null) {
	            request.setClienteTelefono2Area(tdpOrder.getClientTelefono2Area());
	        }

	        String codigoFacturacion = "1";
	        if (tdpOrder.getProductTecTv() != null && tdpOrder.getProductTecTv().equalsIgnoreCase("CATV"))
	            codigoFacturacion = "1";
	        if (tdpOrder.getProductTecTv() != null && tdpOrder.getProductTecTv().equalsIgnoreCase("DTH"))
	            codigoFacturacion = "5";
	        request.setClienteCMSFactura(codigoFacturacion);

	        if (tdpOrder.getProductTipoReg().equalsIgnoreCase("CMS") && tdpOrder.getOrderGisNtlt() == null) {
	            request.setOrderGisNTLT("VALD361603*MIR1361601*MIR1361607*MIR1361606*MIR1361608");
	        }
	        
	        String gpsX = tdpOrder.getOrderGpsX() != null ? tdpOrder.getOrderGpsX().toString() : "";
	        String gpsY = tdpOrder.getOrderGpsY() != null ? tdpOrder.getOrderGpsY().toString() : "";
	        if (gpsX != null && gpsX != "") {
	            request.setOrderGPSX(gpsX.length() > 10 ? gpsX.substring(0, 11) : gpsX);
	        } else {
	            return null;
	        }
	        if (gpsY != null && gpsY != "") {
	            request.setOrderGPSY(gpsY.length() > 10 ? gpsY.substring(0, 11) : gpsY);
	        } else {
	            return null;
	        }
	        
	        if (tdpOrder.getUserFuerzaVenta() != null) {
	            request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
	        }
	        if (tdpOrder.getUserCms() != null) {
	            request.setUserCMS(String.valueOf(tdpOrder.getUserCms()));
	        }
	        //automatizador agregando validación a cms por datos de gis
	        if (tdpOrder.getOrderGisXdsl() != null) {
	            if(!tdpOrder.getOrderGisXdsl().equalsIgnoreCase("")){
	                request.setOrderGisXDSL(tdpOrder.getOrderGisXdsl());
	            }else{
	                request.setOrderGisXDSL("N");
	            }
	        } else {
	            request.setOrderGisXDSL("N");
	        }
	        if (tdpOrder.getOrderGisHfc() != null) {
	            if(!tdpOrder.getOrderGisHfc().equalsIgnoreCase("")){
	                request.setOrderGisHFC(tdpOrder.getOrderGisHfc());
	            }else {
	                request.setOrderGisHFC("N");
	            }
	        } else {
	            request.setOrderGisHFC("N");
	        }
	        //fin automatizador agregando validación a cms por datos de gis
		 
		 return request;
	 }
	 
	  public AutomatizadorSalesService poblarRequest(AutomatizadorSaleRequestBody request){
	        AutomatizadorSalesService automatizadorSaleService = new AutomatizadorSalesService();

	        automatizadorSaleService.setNom_ds(request.getClienteNombre());
	        automatizadorSaleService.setPri_ape_ds(request.getClienteApellidoPaterno());
	        automatizadorSaleService.setSeg_ape_ds(request.getClienteApellidoMaterno());
	        automatizadorSaleService.setNum_doc_cli_cd(request.getClienteNumeroDoc());
	        automatizadorSaleService.setDig_ctl_doc_cd(request.getClienteRucDigitoControl());
	        automatizadorSaleService.setTip_doc_cd(request.getClienteTipoDoc());
	        automatizadorSaleService.setTel_pri_cot_cd(request.getClienteTelefono1());
	        automatizadorSaleService.setTel_seg_cot_cd(request.getClienteTelefono2());
	        automatizadorSaleService.setCod_act_eco_cd(request.getClienteRucActividadCodigo());
	        automatizadorSaleService.setCod_sac_eco_cd(request.getClienteRucSubActividadCodigo());
	        automatizadorSaleService.setTip_cal_ati_cd(request.getAddressCalleAtis());
	        automatizadorSaleService.setNom_cal_ds(request.getAddressCalleNombre());
	        automatizadorSaleService.setNum_cal_nu(request.getAddressCalleNumero());
	        automatizadorSaleService.setCod_cnj_hbl_cd(request.getAddressCCHHCodigo());
	        automatizadorSaleService.setTip_cnj_hbl_cd(request.getAddressCCHHTipo());
	        automatizadorSaleService.setNom_cnj_hbl_no(request.getAddressCCHHNombre());
	        automatizadorSaleService.setPri_tip_cmp_cd(request.getAddressCCHHCompTipo1());
	        automatizadorSaleService.setPri_cmp_dir_ds(request.getAddressCCHHCompNombre1());
	        automatizadorSaleService.setSeg_tip_cmp_cd(request.getAddressCCHHCompTipo2());
	        automatizadorSaleService.setSeg_cmp_dir_ds(request.getAddressCCHHCompNombre2());
	        automatizadorSaleService.setTrc_tip_cmp_cd(request.getAddressCCHHCompTipo3());
	        automatizadorSaleService.setTrc_cmp_dir_ds(request.getAddressCCHHCompNombre3());
	        automatizadorSaleService.setCao_tip_cmp_cd(request.getAddressCCHHCompTipo4());
	        automatizadorSaleService.setCao_cmp_dir_ds(request.getAddressCCHHCompNombre4());
	        automatizadorSaleService.setQui_tip_cmp_cd(request.getAddressCCHHCompTipo5());
	        automatizadorSaleService.setQui_cmp_dir_ds(request.getAddressCCHHCompNombre5());
	        automatizadorSaleService.setSet_tip_cmp_cd(request.getAddressViaCompTipo1());
	        automatizadorSaleService.setSet_cmp_dir_ds(request.getAddressViaCompNombre1());
	        automatizadorSaleService.setSpt_tip_cmp_cd(request.getAddressViaCompTipo2());
	        automatizadorSaleService.setSpt_cmp_dir_ds(request.getAddressViaCompNombre2());
	        automatizadorSaleService.setOct_tip_cmp_cd(request.getAddressViaCompTipo3());
	        automatizadorSaleService.setOct_cmp_dir_ds(request.getAddressViaCompNombre3());
	        automatizadorSaleService.setNov_tip_cmp_cd(request.getAddressViaCompTipo4());
	        automatizadorSaleService.setNov_cmp_dir_ds(request.getAddressViaCompNombre4());
	        automatizadorSaleService.setDco_tip_cmp_cd(request.getAddressViaCompTipo5());
	        automatizadorSaleService.setDco_cmp_dir_ds(request.getAddressViaCompNombre5());
	        automatizadorSaleService.setMsx_cbr_voi_ges_in(request.getOrderGisXDSL());
	        automatizadorSaleService.setMsx_cbr_voi_gis_in(request.getOrderGisHFC());
	        automatizadorSaleService.setMsx_ind_snl_gis_cd(request.getOrderGisTipoSenial());
	        automatizadorSaleService.setMsx_ind_gpo_gis_cd(request.getOrderGisGPON());
	        automatizadorSaleService.setCod_fza_ven_cd(request.getUserAtis());
	        automatizadorSaleService.setCod_fza_gen_cd(request.getUserFuerzaVenta());
	        automatizadorSaleService.setCod_cnl_ven_cd(request.getUserCanalCodigo());
	        automatizadorSaleService.setCod_ind_sen_cms(request.getProductoSenial());
	        automatizadorSaleService.setCod_cab_cms(request.getProductoCabecera());
	        automatizadorSaleService.setId_cod_pro_cd(request.getProductoCodigo());
	        automatizadorSaleService.setPs_adm_dep_1(request.getProductoPSAdmin1());
	        automatizadorSaleService.setPs_adm_dep_2(request.getProductoPSAdmin2());
	        automatizadorSaleService.setPs_adm_dep_3(request.getProductoPSAdmin3());
	        automatizadorSaleService.setPs_adm_dep_4(request.getProductoPSAdmin4());
	        automatizadorSaleService.setId_cod_sva_cd_1(request.getProductoSVACodigo1());
	        automatizadorSaleService.setId_cod_sva_cd_2(request.getProductoSVACodigo2());
	        automatizadorSaleService.setId_cod_sva_cd_3(request.getProductoSVACodigo3());
	        automatizadorSaleService.setId_cod_sva_cd_4(request.getProductoSVACodigo4());
	        automatizadorSaleService.setId_cod_sva_cd_5(request.getProductoSVACodigo5());
	        automatizadorSaleService.setId_cod_sva_cd_6(request.getProductoSVACodigo6());
	        automatizadorSaleService.setId_cod_sva_cd_7(request.getProductoSVACodigo7());
	        automatizadorSaleService.setId_cod_sva_cd_8(request.getProductoSVACodigo8());
	        automatizadorSaleService.setId_cod_sva_cd_9(request.getProductoSVACodigo9());
	        automatizadorSaleService.setId_cod_sva_cd_10(request.getProductoSVACodigo10());
	        automatizadorSaleService.setCodvtappcd(request.getOrderId());
	        automatizadorSaleService.setFec_vta_ff(request.getOrderFecha());
	        automatizadorSaleService.setNum_ide_tlf(request.getOrderParqueTelefono());
	        automatizadorSaleService.setCod_cli_ext_cd(request.getClienteCMSCodigo());
	        automatizadorSaleService.setTip_trx_cmr_cd(request.getOrderOperacionComercial());
	        automatizadorSaleService.setCod_srv_cms_cd(request.getClienteCMSServicio());
	        automatizadorSaleService.setFec_nac_ff(request.getClienteNacimiento());
	        automatizadorSaleService.setCod_sex_cd(request.getClienteSexo());
	        automatizadorSaleService.setCod_est_civ_cd(request.getClienteCivil());
	        automatizadorSaleService.setDes_mai_cli_ds(request.getClienteEmail());
	        automatizadorSaleService.setCod_are_te1_cd(request.getClienteTelefono1Area());
	        automatizadorSaleService.setCod_are_te2_cd(request.getClienteTelefono2Area());
	        automatizadorSaleService.setCod_cic_fac_cd(request.getClienteCMSFactura());
	        automatizadorSaleService.setCod_dep_cd(request.getAddressDepartamentoCodigo());
	        automatizadorSaleService.setCod_prv_cd(request.getAddressProvinciaCodigo());
	        automatizadorSaleService.setCod_dis_cd(request.getAddressDistritoCodigo());
	        automatizadorSaleService.setCod_fac_tec_cd(request.getOrderGisNTLT());
	        automatizadorSaleService.setNum_cod_x_cd(request.getOrderGPSX());
	        automatizadorSaleService.setNum_cod_y_cd(request.getOrderGPSY());
	        automatizadorSaleService.setCod_ven_cms_cd(request.getUserCSMPuntoVenta());
	        automatizadorSaleService.setCod_sve_cms_cd(request.getUserCMS());
	        automatizadorSaleService.setNom_ven_ds(request.getUserNombre());
	        automatizadorSaleService.setRef_dir_ent_ds(request.getAddressReferencia());
	        automatizadorSaleService.setCod_tip_cal_cd(request.getOrderCallTipo());
	        automatizadorSaleService.setDes_tip_cal_cd(request.getOrderCallDesc());
	        automatizadorSaleService.setTip_mod_equ_cd(request.getOrderModelo());
	        automatizadorSaleService.setNum_doc_ven_cd(request.getUserDNI());
	        automatizadorSaleService.setNum_tel_ven_ds(request.getUserTelefono());
	        automatizadorSaleService.setInd_env_con_cd(request.getOrderContrato());
	        automatizadorSaleService.setInd_id_gra_cd(request.getOrderIDGrabacion());
	        automatizadorSaleService.setMod_ace_ven_cd(request.getOrderModalidadAcep());
	        automatizadorSaleService.setDet_can_ven_ds(request.getOrderEntidad());
	        automatizadorSaleService.setCod_reg_cd(request.getOrderRegion());
	        automatizadorSaleService.setCod_cip_cd(request.getOrderCIP());
	        automatizadorSaleService.setCod_exp_cd(request.getOrderExperto());
	        automatizadorSaleService.setCod_zon_ven_cd(request.getOrderZonal());
	        automatizadorSaleService.setInd_web_par_cd(request.getOrderWebParental());
	        automatizadorSaleService.setCod_stc(request.getOrderCodigoSTC6I());
	        automatizadorSaleService.setDes_nac_ds(request.getClienteNacionalidad());
	        automatizadorSaleService.setCod_mod_ven_cd(request.getOrderModoVenta());
	        automatizadorSaleService.setCan_equ_ven(request.getOrderCantidadEquipos());

	        return automatizadorSaleService;
	    }
	  	
	public List<String> automatizadorExtraValidacion() {

        List<String> response = new ArrayList<>();

        List<Parameters> enableList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "EXTRA");
        for (Parameters i : enableList) {
            response.add(i.getStrValue());
        }
        return response;
    }

    private String toLowerWithouAccent(String text) {
        if (text != null)
            return text.trim().toLowerCase().replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u");
        else return "";
    }
	
	
	
	 public ApiResponseAuto<InsertResponseBodyAuto> sendInsertOrder(AutomatizadorSaleRequestBody insertRequestBody) throws ClientException {
	        ApiResponseAuto<InsertResponseBodyAuto> response;
	        ClientConfig config = new ClientConfig.ClientConfigBuilder()
	                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AUTO)
	                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AUTO).build();

	        ClientResult<ApiResponseAuto<InsertResponseBodyAuto>> result = postInsertOrderAuto(insertRequestBody, config);
	        result.getEvent().setDocNumber(insertRequestBody.getUserDNI());
	        result.getEvent().setUsername(insertRequestBody.getUserNombre());
	        result.getEvent().setOrderId(insertRequestBody.getOrderId());
	        result.getEvent().setServiceCode("AUTOMATIZADOR_MT");

	        serviceCallEventsService.registerEvent(result.getEvent());

	        if (!result.isSuccess()) {
	            throw result.getE();
	        }

	        response = result.getResult();

	        return response;
	    }

	 public ClientResult<ApiResponseAuto<InsertResponseBodyAuto>> postInsertOrderAuto(AutomatizadorSaleRequestBody insertRequestBody, ClientConfig config) {
	        ClientResult<ApiResponseAuto<InsertResponseBodyAuto>> result = null;
	        InsertOrderClientAuto client = new InsertOrderClientAuto(config);
	        result = client.postAuto(insertRequestBody);
	        return result;
	  }
	 
	 
	 public Boolean EliminarTdpVisor(String id) {
		 Boolean verificar=false;
		 tdpVisorRepository.delete(id);
		 
		 return verificar;
	 }
	 
	 //Verifica el estado del servicio Automatizador
	 public String verificarSeviceAutomatizador() {
		 String estado="";
		 try {
			
			 AutomatizadorSaleRequestBody request= new AutomatizadorSaleRequestBody();
			 
			 request.setClienteNombre("Carla");
			 request.setClienteApellidoPaterno("Flores");
			 request.setClienteApellidoMaterno("FUENTES");
			 request.setClienteNumeroDoc("12123445");
			 request.setClienteRucDigitoControl("");
			 request.setClienteTipoDoc("DNI");
			 request.setClienteTelefono1("4888888");
			 request.setClienteTelefono2("999666555");
			 request.setClienteRucActividadCodigo("");
			 request.setClienteRucSubActividadCodigo("");
			 request.setAddressCalleAtis("PR");
			 request.setAddressCalleNombre("EL PACIFICO");
			 request.setAddressCalleNumero("S/N");
			 request.setAddressCCHHCodigo("7654");
			 request.setAddressCCHHTipo("UR");
			 request.setAddressCCHHNombre("1 DE AGOSTO");
			 request.setAddressCCHHCompTipo1("");
			 request.setAddressCCHHCompNombre1("");
			 request.setAddressCCHHCompTipo2("");
			 request.setAddressCCHHCompNombre2("");
			 request.setAddressCCHHCompTipo3("");
			 request.setAddressCCHHCompNombre3("");
			 request.setAddressCCHHCompTipo4("");
			 request.setAddressCCHHCompNombre4("");
			 request.setAddressCCHHCompTipo5("");
			 request.setAddressCCHHCompNombre5("");
			 request.setAddressViaCompTipo1("");
			 request.setAddressViaCompNombre1("");
			 request.setAddressViaCompTipo2("");
			 request.setAddressViaCompNombre2("");
			 request.setAddressViaCompTipo3("");
			 request.setAddressViaCompNombre3("");
			 request.setAddressViaCompTipo4("");
			 request.setAddressViaCompNombre4("");
			 request.setAddressViaCompTipo5("");
			 request.setAddressViaCompNombre5("");
			 request.setOrderGisXDSL("S");
			 request.setOrderGisHFC("S");
			 request.setOrderGisTipoSenial("1");
			 request.setOrderGisGPON("N");
			 request.setUserAtis("15");
			 request.setUserFuerzaVenta("16");
			 request.setUserCanalCodigo("17");
			 request.setProductoSenial("1");
			 request.setProductoCabecera("20");
			 request.setProductoCodigo("");
			 request.setProductoPSAdmin1("16732");
			 request.setProductoPSAdmin2("0");
			 request.setProductoPSAdmin3("0");
			 request.setProductoPSAdmin4("0");
			 request.setProductoSVACodigo1(null);
			 request.setProductoSVACodigo2(null);
			 request.setProductoSVACodigo3(null);
			 request.setProductoSVACodigo4(null);
			 request.setProductoSVACodigo5(null);
			 request.setProductoSVACodigo6(null);
			 request.setProductoSVACodigo7(null);
			 request.setProductoSVACodigo8(null);
			 request.setProductoSVACodigo9(null);
			 request.setProductoSVACodigo10(null);
			 request.setOrderId("MT30393221");
			 request.setOrderFecha("2019-10-01 01:44:12");
			 request.setOrderParqueTelefono("4");
			 request.setClienteCMSCodigo("44");
			 request.setOrderOperacionComercial("6");
			 request.setClienteCMSServicio("45");
			 request.setClienteNacimiento("1989-10-10");
			 request.setClienteSexo("M");
			 request.setClienteCivil("CASADO");
			 request.setClienteEmail("cms@hotmail.com");
			 request.setClienteTelefono1Area("1");
			 request.setClienteTelefono2Area("1");
			 request.setClienteCMSFactura("1");
			 request.setAddressDepartamentoCodigo("001");
			 request.setAddressProvinciaCodigo("001");
			 request.setAddressDistritoCodigo("043");
			 request.setOrderGisNTLT("MI0001110211");
			 request.setOrderGPSX("-78.5103659");
			 request.setOrderGPSY("-71.53479503");
			 request.setUserCSMPuntoVenta("8719");
			 request.setUserCMS("12");
			 request.setUserNombre("FERNANDO GUZMAN");
			 request.setAddressReferencia("SIN REF");
			 request.setOrderCallTipo("1X");
			 request.setOrderCallDesc("TELEFONICO");
			 request.setOrderModelo("GIG");
			 request.setUserDNI("13131111");
			 request.setUserTelefono("8");
			 request.setOrderContrato("F");
			 request.setOrderIDGrabacion("N");
			 request.setOrderModalidadAcep("F");
			 request.setOrderEntidad("DETALLE_DE_CANAL");
			 request.setOrderRegion("32");
			 request.setOrderCIP("");
			 request.setOrderExperto("");
			 request.setOrderZonal("LIMA SUR");
			 request.setOrderWebParental("NO");
			 request.setOrderCodigoSTC6I("");
			 request.setClienteNacionalidad("Peruano");
			 request.setOrderModoVenta("2");
			 request.setOrderCantidadEquipos("0");
			 
			 
			 String[] erroresBuss = errBusParametrizable.split(",");
			 
			 ApiResponseAuto<InsertResponseBodyAuto> response = sendVerificarAuto(request);
           
			 if(response.getBodyOut().getClientException().getExceptionCode()==1050 && 
					 response.getBodyOut().getClientException().getExceptionMsg().equalsIgnoreCase("Business Error")) {
				estado="ok";
			 }
			 
             if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                     response.getHeaderOut().getMsgType().equalsIgnoreCase("RESPONSE")) {
            	 	estado="OK";
                 
            	 	
             }
             
             if (response != null && response.getBodyOut() != null) {
                 if (response.getBodyOut().getServerException() != null) {
                     for (String i : erroresBuss) {
                         int res = Integer.parseInt(i);
                         if (response.getBodyOut().getServerException().getExceptionCode() == res) {
                            
                             estado="ERR_BUS";
                           
                         }

                     }
                 }
             }
             
        
             return estado;
		 }catch (Exception e) {
			 estado="ERR_BUS";
             
             return estado;
		}
		
	}
	 
	 private ApiResponseAuto<InsertResponseBodyAuto> sendVerificarAuto(AutomatizadorSaleRequestBody insertRequestBody) throws ClientException {
	        ApiResponseAuto<InsertResponseBodyAuto> response;
	        ClientConfig config = new ClientConfig.ClientConfigBuilder()
	                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AUTO)
	                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AUTO).build();
	        ClientResult<ApiResponseAuto<InsertResponseBodyAuto>> result = postInsertOrderAuto(insertRequestBody, config);
	        
	        if (!result.isSuccess()) {
	            throw result.getE();
	        }
	        response = result.getResult();
	        return response;
	    }
}
