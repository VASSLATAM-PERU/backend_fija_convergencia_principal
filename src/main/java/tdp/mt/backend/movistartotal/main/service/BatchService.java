package tdp.mt.backend.movistartotal.main.service;

public interface BatchService {

    void procesarExcelCustodiaToAzure() throws  Exception;

    void proccessFilesToAzure() throws Exception;

    void procesarFilesPDFAndPNGToAzure() throws Exception;

    void reintentoTgestiona();
    
    void reintentoAutomatizador();
}
