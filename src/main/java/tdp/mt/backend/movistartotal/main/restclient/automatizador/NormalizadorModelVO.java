package tdp.mt.backend.movistartotal.main.restclient.automatizador;
import java.sql.Timestamp;

public class NormalizadorModelVO {
	private String id_transaccion;
    private Boolean normalizador;
    private String tipo_urbanizacion; 
    private String nombre_urbanizacion;
    private String nombreVia; 
    private String tipoVia;
    private String manzana; 
    private String lote; 
    private String cuadra; 
    private String tipo;
    private String piso;
    private String interior;

    public String getId_transaccion() {
        return id_transaccion;
    }
 
    public void setId_transaccion(String id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public Boolean getNormalizador() {
        return normalizador;
    }

    public void setNormalizador(Boolean normalizador) {
        this.normalizador = normalizador;
    }

    public String getTipo_urbanizacion() {
        return tipo_urbanizacion;
    }

    public void setTipo_urbanizacion(String tipo_urbanizacion) {
        this.tipo_urbanizacion = tipo_urbanizacion;
    }

    public String getNombre_urbanizacion() {
        return nombre_urbanizacion;
    }

    public void setNombre_urbanizacion(String nombre_urbanizacion) {
        this.nombre_urbanizacion = nombre_urbanizacion;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public String getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getCuadra() {
        return cuadra;
    }

    public void setCuadra(String cuadra) {
        this.cuadra = cuadra;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getInterior() {
		return interior;
	}

	public void setInterior(String interior) {
		this.interior = interior;
	}
    
}
