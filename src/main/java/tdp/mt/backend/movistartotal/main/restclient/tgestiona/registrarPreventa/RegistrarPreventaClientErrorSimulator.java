package tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.io.IOException;

/**
 * Cliente Hdec para simular respuestas con error
 *
 */
public class RegistrarPreventaClientErrorSimulator extends RegistrarPreventaClient {
    private String jsonErrorResponse;

    public RegistrarPreventaClientErrorSimulator(ClientConfig config, String jsonErrorResponse) {
        super(config);
        this.jsonErrorResponse = jsonErrorResponse;
    }

    @Override
    protected ApiResponse<PreventaResponseBody> getResponse(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        // usamos "jsonErrorResponse" en vez de "json"
        ApiResponse<PreventaResponseBody> objCms = mapper.readValue(jsonErrorResponse, new TypeReference<ApiResponse<PreventaResponseBody>>() {});
        return objCms;
    }
}
