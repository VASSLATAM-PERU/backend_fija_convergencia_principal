package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tdp.mt.backend.movistartotal.main.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query("select c.id from Customer c where c.docType = ?1 and c.docNumber = ?2")
    Long loadIdByDocTypeAndDocNumber(String docType, String docNumber);

    @Query("select c from Customer c where c.docNumber = ?1")
    Customer searchCustomerByDocNumber(String docNumber);
}
