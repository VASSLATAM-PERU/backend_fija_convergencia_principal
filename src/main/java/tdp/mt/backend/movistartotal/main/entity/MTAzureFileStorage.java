package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mt_azure_file_storage", schema = "ibmx_a07e6d02edaf552") //dochoaro se adiciona schema
public class MTAzureFileStorage {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mtorder")
    private String mtorder;

    @Column(name = "estado_tdpvisor")
    private String estadoTdpVisor;

    @Column(name = "codigo_pedido_tdpvisor")
    private String codigoPedidoTdpVisor;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_register")
    private Date dateRegister;

    @Column(name = "file_date")
    private String fileDate;

    @Column(name = "file_extension")
    private String fileExtension;

    @Column(name= "code_mt")
    private String codeMt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMtorder() {
        return mtorder;
    }

    public void setMtorder(String mtorder) {
        this.mtorder = mtorder;
    }

    public String getEstadoTdpVisor() {
        return estadoTdpVisor;
    }

    public void setEstadoTdpVisor(String estadoTdpVisor) {
        this.estadoTdpVisor = estadoTdpVisor;
    }

    public String getCodigoPedidoTdpVisor() {
        return codigoPedidoTdpVisor;
    }

    public void setCodigoPedidoTdpVisor(String codigoPedidoTdpVisor) {
        this.codigoPedidoTdpVisor = codigoPedidoTdpVisor;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getFileDate() {
        return fileDate;
    }

    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getCodeMt() {
        return codeMt;
    }

    public void setCodeMt(String codeMt) {
        this.codeMt = codeMt;
    }
}
