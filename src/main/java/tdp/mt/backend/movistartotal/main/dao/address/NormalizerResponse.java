package tdp.mt.backend.movistartotal.main.dao.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NormalizerResponse {

    @JsonProperty("countAddress")
    private int countAddress;

    @JsonProperty("countAddressOutUbigeo")
    private int countAddressOutUbigeo;

    @JsonProperty("codeMsmAddressNotFound")
    private String codeMsmAddressNotFound;

    @JsonProperty("messageMsmAddressNotFoung")
    private String messageMsmAddressNotFoung;

    @JsonProperty("addresses")
    private List<Addresses> addresses;

    @JsonProperty("paramNivelConfianza")
    private int paramNivelConfianza;

    @JsonProperty("codeMessageNivelConfianza")
    private String codeMessageNivelConfianza;

    @JsonProperty("messageNivelConfianza")
    private String messageNivelConfianza;

    @JsonProperty("addresses")
    public List<Addresses> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    @JsonProperty("paramNivelConfianza")
    public int getParamNivelConfianza() {
        return paramNivelConfianza;
    }

    @JsonProperty("paramNivelConfianza")
    public void setParamNivelConfianza(int paramNivelConfianza) {
        this.paramNivelConfianza = paramNivelConfianza;
    }

    @JsonProperty("codeMessageNivelConfianza")
    public String getCodeMessageNivelConfianza() {
        return codeMessageNivelConfianza;
    }

    @JsonProperty("codeMessageNivelConfianza")
    public void setCodeMessageNivelConfianza(String codeMessageNivelConfianza) {
        this.codeMessageNivelConfianza = codeMessageNivelConfianza;
    }

    @JsonProperty("messageNivelConfianza")
    public String getMessageNivelConfianza() {
        return messageNivelConfianza;
    }

    @JsonProperty("messageNivelConfianza")
    public void setMessageNivelConfianza(String messageNivelConfianza) {
        this.messageNivelConfianza = messageNivelConfianza;
    }

    @JsonProperty("countAddress")
    public int getCountAddress() {
        return countAddress;
    }

    @JsonProperty("countAddress")
    public void setCountAddress(int countAddress) {
        this.countAddress = countAddress;
    }

    @JsonProperty("countAddressOutUbigeo")
    public int getCountAddressOutUbigeo() {
        return countAddressOutUbigeo;
    }

    @JsonProperty("countAddressOutUbigeo")
    public void setCountAddressOutUbigeo(int countAddressOutUbigeo) {
        this.countAddressOutUbigeo = countAddressOutUbigeo;
    }

    @JsonProperty("codeMsmAddressNotFound")
    public String getCodeMsmAddressNotFound() {
        return codeMsmAddressNotFound;
    }

    @JsonProperty("codeMsmAddressNotFound")
    public void setCodeMsmAddressNotFound(String codeMsmAddressNotFound) {
        this.codeMsmAddressNotFound = codeMsmAddressNotFound;
    }

    @JsonProperty("messageMsmAddressNotFoung")
    public String getMessageMsmAddressNotFoung() {
        return messageMsmAddressNotFoung;
    }

    @JsonProperty("messageMsmAddressNotFoung")
    public void setMessageMsmAddressNotFoung(String messageMsmAddressNotFoung) {
        this.messageMsmAddressNotFoung = messageMsmAddressNotFoung;
    }
}
