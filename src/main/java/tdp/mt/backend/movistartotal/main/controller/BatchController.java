package tdp.mt.backend.movistartotal.main.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tdp.mt.backend.movistartotal.main.service.BatchService;

@RestController
public class BatchController {

    @Autowired
    private BatchService batchService;

    private static final Logger logger = LogManager.getLogger(OrderController.class);

    //@Scheduled(cron = "*/30 * * * * *") /*** Cada 30 segundos ***/
    //@Scheduled(cron = "${mt.batch.procesar.azure}")

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/batch/procesarExcelCustodiaToAzure", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    void procesarExcelCustodiaToAzure() throws Exception {
        logger.info("Inicio procesamiento files hacia Azure");
        batchService.procesarExcelCustodiaToAzure();
        logger.info("Final procesamiento files hacia Azure");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/batch/procesarFilesToAzure", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    void procesarFilesToAzure() throws Exception {
        logger.info("Inicio procesamiento files hacia Azure");
        batchService.proccessFilesToAzure();
        logger.info("Final procesamiento files hacia Azure");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/batch/procesarFilesPDFAndPNGToAzure", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    void procesarFilesPDFAndPNGToAzure() throws Exception {
        logger.info("Inicio procesamiento files hacia Azure");
        batchService.procesarFilesPDFAndPNGToAzure();
        logger.info("Final procesamiento files hacia Azure");
    }

    //@Scheduled(cron = "*/30 * * * * *") /*** Cada 30 segundos ***/
    //@Scheduled(cron = "${mt.batch.tgestiona.reintento}")
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/batch/reintentarTgestiona", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    void reintentarTgestiona(){
        logger.info("Inicio reintento reintento hacia Tgestiona");
        batchService.reintentoTgestiona();
        logger.info("Final reintento reintento hacia Tgestiona");
    }
    
    
    @Scheduled(cron = "${mt.tdp.automatizador.batch}")
//    @Scheduled(cron = "0 0/1 * * * *")
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/batch/reintentarAutomatizador", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    void reintentarAutomatizador(){
        logger.info("Inicio reintento reintento hacia Automatizador");
        batchService.reintentoAutomatizador();
        logger.info("Final reintento reintento hacia Automatizador");
    	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    	 logger.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
    }

}
