package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.cliente.AbstractClientAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.dto.ApiResponseAuto;

public class AutomatizadorSaleClient extends AbstractClientAuto<AutomatizadorSaleRequestBody, AutomatizadorSaleResponseBody> {

	public AutomatizadorSaleClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCodeAuto() {
		return "AUTOMATIZADOR";
	}

	@Override 
	protected ApiResponseAuto<AutomatizadorSaleResponseBody> getResponseAuto(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponseAuto<AutomatizadorSaleResponseBody> objAtis = mapper.readValue(json, new TypeReference<ApiResponse<AutomatizadorSaleResponseBody>>() {});
		return objAtis;
	}

}
