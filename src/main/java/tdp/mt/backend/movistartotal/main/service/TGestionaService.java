package tdp.mt.backend.movistartotal.main.service;


import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.domain.Response;

import java.util.Map;

public interface TGestionaService {

    Response<FijaHDECResponse> saveAndSendTGestiona(FijaHDECRequest request, String codigoAplicacion);

    FijaHDECResponse sendTGestionaRegistrarPreventa(String orderId);

    FijaHDECResponse sendTGestionaInsertOrder(String orderId);

}
