package tdp.mt.backend.movistartotal.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.service.impl.AzureEmailServiceImpl;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;

@RestController
public class AzureController {

    private static final Logger logger = LogManager.getLogger(AzureController.class);

    @Autowired
    private AzureEmailServiceImpl service;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/test/email", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public Response<String> azureEmail(String mtOrder) {
        LogVass.startController(logger, "azureEmail", "");
        Response<String> response = new Response<>();
        try {
            service.sendRetailEmailProcess(mtOrder);
            response.setResponseData("OK");
        } catch (Exception e) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Error en azureEmail: " + e.getMessage());
        }
        LogVass.finishController(logger, "azureEmail", response);
        return response;
    }

}
