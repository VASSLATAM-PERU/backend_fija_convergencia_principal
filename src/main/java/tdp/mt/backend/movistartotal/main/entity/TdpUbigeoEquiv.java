package tdp.mt.backend.movistartotal.main.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_ubigeo_equiv", schema = "ibmx_a07e6d02edaf552")
public class TdpUbigeoEquiv {
	 @Id
	    @Column
	    private Integer id;
	    @Column(name = "dep_cod")
	    private String depCod;
	    @Column(name = "dep_name")
	    private String depName;
	    @Column(name = "dep_cod_inei")
	    private String depCodInei;
	    @Column(name = "dep_name_inei")
	    private String depNameInei;
	    @Column(name = "prov_cod")
	    private String provCod;
	    @Column(name = "prov_name")
	    private String provName;
	    @Column(name = "prov_cod_inei")
	    private String provCodInei;
	    @Column(name = "prov_name_inei")
	    private String provNameInei;
	    @Column(name = "dis_cod")
	    private String disCod;
	    @Column(name = "dis_name")
	    private String disName;
	    @Column(name = "dis_cod_inei")
	    private String disCodInei;
	    @Column(name = "dis_name_inei")
	    private String disNameInei;

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getDepCod() {
	        return depCod;
	    }

	    public void setDepCod(String depCod) {
	        this.depCod = depCod;
	    }

	    public String getDepName() {
	        return depName;
	    }

	    public void setDepName(String depName) {
	        this.depName = depName;
	    }

	    public String getDepCodInei() {
	        return depCodInei;
	    }

	    public void setDepCodInei(String depCodInei) {
	        this.depCodInei = depCodInei;
	    }

	    public String getDepNameInei() {
	        return depNameInei;
	    }

	    public void setDepNameInei(String depNameInei) {
	        this.depNameInei = depNameInei;
	    }

	    public String getProvCod() {
	        return provCod;
	    }

	    public void setProvCod(String provCod) {
	        this.provCod = provCod;
	    }

	    public String getProvName() {
	        return provName;
	    }

	    public void setProvName(String provName) {
	        this.provName = provName;
	    }

	    public String getProvCodInei() {
	        return provCodInei;
	    }

	    public void setProvCodInei(String provCodInei) {
	        this.provCodInei = provCodInei;
	    }

	    public String getProvNameInei() {
	        return provNameInei;
	    }

	    public void setProvNameInei(String provNameInei) {
	        this.provNameInei = provNameInei;
	    }

	    public String getDisCod() {
	        return disCod;
	    }

	    public void setDisCod(String disCod) {
	        this.disCod = disCod;
	    }

	    public String getDisName() {
	        return disName;
	    }

	    public void setDisName(String disName) {
	        this.disName = disName;
	    }

	    public String getDisCodInei() {
	        return disCodInei;
	    }

	    public void setDisCodInei(String disCodInei) {
	        this.disCodInei = disCodInei;
	    }

	    public String getDisNameInei() {
	        return disNameInei;
	    }

	    public void setDisNameInei(String disNameInei) {
	        this.disNameInei = disNameInei;
	    }
}
