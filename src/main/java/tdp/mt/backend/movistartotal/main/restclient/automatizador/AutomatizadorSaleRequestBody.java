package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "NOM_DS", "PRI_APE_DS", "SEG_APE_DS", "NUM_DOC_CLI_CD","DIG_CTL_DOC_CD",
				"TIP_DOC_CD", "TEL_PRI_COT_CD", "TEL_SEG_COT_CD", "COD_ACT_ECO_CD", "COD_SAC_ECO_CD",
				"TIP_CAL_ATI_CD","NOM_CAL_DS","NUM_CAL_NU","COD_CNJ_HBL_CD","TIP_CNJ_HBL_CD", "NOM_CNJ_HBL_NO",
				"PRI_TIP_CMP_CD","PRI_CMP_DIR_DS","SEG_TIP_CMP_CD","SEG_CMP_DIR_DS","TRC_TIP_CMP_CD","TRC_CMP_DIR_DS",
				"CAO_TIP_CMP_CD","CAO_CMP_DIR_DS","QUI_TIP_CMP_CD","QUI_CMP_DIR_DS","SET_TIP_CMP_CD","SET_CMP_DIR_DS",
				"SPT_TIP_CMP_CD","SPT_CMP_DIR_DS","OCT_TIP_CMP_CD","OCT_CMP_DIR_DS","NOV_TIP_CMP_CD","NOV_CMP_DIR_DS",
				"DCO_TIP_CMP_CD","DCO_CMP_DIR_DS",
				"MSX_CBR_VOI_GES_IN","MSX_CBR_VOI_GIS_IN","MSX_IND_SNL_GIS_CD","MSX_IND_GPO_GIS_CD",
				"COD_FZA_VEN_CD","COD_FZA_GEN_CD","COD_CNL_VEN_CD",
				"COD_IND_SEN_CMS","COD_CAB_CMS","ID_COD_PRO_CD","PS_ADM_DEP_1","PS_ADM_DEP_2","PS_ADM_DEP_3","PS_ADM_DEP_4",
				"ID_COD_SVA_CD_1","ID_COD_SVA_CD_2","ID_COD_SVA_CD_3","ID_COD_SVA_CD_4","ID_COD_SVA_CD_5",
				"ID_COD_SVA_CD_6","ID_COD_SVA_CD_7","ID_COD_SVA_CD_8","ID_COD_SVA_CD_9","ID_COD_SVA_CD_10",
		"COD_VTA_APP_CD","FEC_VTA_FF","NUM_IDE_TLF","COD_CLI_EXT_CD","TIP_TRX_CMR_CD",
		"COD_SRV_CMS_CD","FEC_NAC_FF","COD_SEX_CD","COD_EST_CIV_CD","DES_MAI_CLI_DS",
		"COD_ARE_TE1_CD","COD_ARE_TE2_CD","COD_CIC_FAC_CD","COD_DEP_CD","COD_PRV_CD","COD_DIS_CD",
		"COD_FAC_TEC_CD","NUM_COD_X_CD","NUM_COD_Y_CD","COD_VEN_CMS_CD","COD_SVE_CMS_CD","NOM_VEN_DS",
		"REF_DIR_ENT_DS","COD_TIP_CAL_CD","DES_TIP_CAL_CD","TIP_MOD_EQU_CD","NUM_DOC_VEN_CD","NUM_TEL_VEN_DS",
		"IND_ENV_CON_CD","IND_ID_GRA_CD","MOD_ACE_VEN_CD","DET_CAN_VEN_DS","COD_REG_CD",
		"COD_CIP_CD","COD_EXP_CD","COD_ZON_VEN_CD","IND_WEB_PAR_CD","COD_STC",
		"DES_NAC_DS","COD_MOD_VEN_CD","CAN_EQU_VEN"
				}) 
public class AutomatizadorSaleRequestBody {

	private String clienteNombre;
	private String clienteApellidoPaterno;
	private String clienteApellidoMaterno;
	private String clienteNumeroDoc;
	private String clienteRucDigitoControl;

	private String clienteTipoDoc;
	private String clienteTelefono1;
	private String clienteTelefono2;
    private String clienteRucActividadCodigo;
	private String clienteRucSubActividadCodigo;

	private String addressCalleAtis;
	private String addressCalleNombre;
	private String addressCalleNumero;
	private String addressCCHHCodigo;
	private String addressCCHHTipo;
	private String addressCCHHNombre;

	private String addressCCHHCompTipo1;
	private String addressCCHHCompNombre1;
	private String addressCCHHCompTipo2;
	private String addressCCHHCompNombre2;
	private String addressCCHHCompTipo3;
	private String addressCCHHCompNombre3;
	private String addressCCHHCompTipo4;
	private String addressCCHHCompNombre4;
	private String addressCCHHCompTipo5;
	private String addressCCHHCompNombre5;

	private String addressViaCompTipo1;
	private String addressViaCompNombre1;
	private String addressViaCompTipo2;
	private String addressViaCompNombre2;
	private String addressViaCompTipo3;
	private String addressViaCompNombre3;
	private String addressViaCompTipo4;
	private String addressViaCompNombre4;
	private String addressViaCompTipo5;
	private String addressViaCompNombre5;

	private String orderGisXDSL;
	private String orderGisHFC;
	private String orderGisTipoSenial;
	private String orderGisGPON;

	private String userAtis;
	private String userFuerzaVenta;
	private String userCanalCodigo;

	private String productoSenial;
	private String productoCabecera;
	private String productoCodigo;
	private String productoPSAdmin1;
	private String productoPSAdmin2;
	private String productoPSAdmin3;
	private String productoPSAdmin4;

	private String productoSVACodigo1;
	private String productoSVACodigo2;
	private String productoSVACodigo3;
	private String productoSVACodigo4;
	private String productoSVACodigo5;

	private String productoSVACodigo6;
	private String productoSVACodigo7;
	private String productoSVACodigo8;
	private String productoSVACodigo9;
	private String productoSVACodigo10;

	private String orderId;
	private String orderFecha;
	private String orderParqueTelefono;
	private String clienteCMSCodigo;
	private String orderOperacionComercial;

	private String clienteCMSServicio;
	private String clienteNacimiento;
	private String clienteSexo;
	private String clienteCivil;
	private String clienteEmail;

	private String clienteTelefono1Area;
	private String clienteTelefono2Area;
	private String clienteCMSFactura;
	private String addressDepartamentoCodigo;
	private String addressProvinciaCodigo;
	private String addressDistritoCodigo;

	private String orderGisNTLT;
	private String orderGPSX;
	private String orderGPSY;
	private String userCSMPuntoVenta;
	private String userCMS;
	private String userNombre;

	private String addressReferencia;
	private String orderCallTipo;
	private String orderCallDesc;
	private String orderModelo;
	private String userDNI;
	private String userTelefono;

	private String orderContrato;
	private String orderIDGrabacion;
	private String orderModalidadAcep;
	private String orderEntidad;
	private String orderRegion;

	private String orderCIP;
	private String orderExperto;
	private String orderZonal;
	private String orderWebParental;
	private String orderCodigoSTC6I;

	private String clienteNacionalidad; //"DES_NAC_DS"
	private String orderModoVenta;  //"COD_MOD_VEN_CD"
	private String orderCantidadEquipos; //"CAN_EQU_VEN"
			

	@JsonProperty("NOM_DS")
	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	@JsonProperty("PRI_APE_DS")
	public String getClienteApellidoPaterno() {
		return clienteApellidoPaterno;
	}

	

	public void setClienteApellidoPaterno(String clienteApellidoPaterno) {
		this.clienteApellidoPaterno = clienteApellidoPaterno;
	}

	@JsonProperty("SEG_APE_DS")
	public String getClienteApellidoMaterno() { return this.clienteApellidoMaterno; }

	public void setClienteApellidoMaterno(String clienteApellidoMaterno) {
		this.clienteApellidoMaterno = clienteApellidoMaterno;
	}

	@JsonProperty("NUM_DOC_CLI_CD")
	public String getClienteNumeroDoc() {
		return clienteNumeroDoc;
	}

	public void setClienteNumeroDoc(String clienteNumeroDoc) {
		this.clienteNumeroDoc = clienteNumeroDoc;
	}

	@JsonProperty("DIG_CTL_DOC_CD")
	public String getClienteRucDigitoControl() {
		return clienteRucDigitoControl;
	}

	public void setClienteRucDigitoControl(String clienteRucDigitoControl) {
		this.clienteRucDigitoControl = clienteRucDigitoControl;
	}

	@JsonProperty("TIP_DOC_CD")
	public String getClienteTipoDoc() {
		return clienteTipoDoc;
	}

	public void setClienteTipoDoc(String clienteTipoDoc) {
		this.clienteTipoDoc = clienteTipoDoc;
	}

	@JsonProperty("TEL_PRI_COT_CD")
	public String getClienteTelefono1() {
		return clienteTelefono1;
	}

	public void setClienteTelefono1(String clienteTelefono1) {
		this.clienteTelefono1 = clienteTelefono1;
	}

	@JsonProperty("TEL_SEG_COT_CD")
	public String getClienteTelefono2() {
		return clienteTelefono2;
	}

	public void setClienteTelefono2(String clienteTelefono2) {
		this.clienteTelefono2 = clienteTelefono2;
	}

	@JsonProperty("COD_ACT_ECO_CD")
	public String getClienteRucActividadCodigo() {
		return clienteRucActividadCodigo;
	}

	public void setClienteRucActividadCodigo(String clienteRucActividadCodigo) {
		this.clienteRucActividadCodigo = clienteRucActividadCodigo;
	}

	@JsonProperty("COD_SAC_ECO_CD")
	public String getClienteRucSubActividadCodigo() {
		return clienteRucSubActividadCodigo;
	}

	public void setClienteRucSubActividadCodigo(String clienteRucSubActividadCodigo) {
		this.clienteRucSubActividadCodigo = clienteRucSubActividadCodigo;
	}

	@JsonProperty("TIP_CAL_ATI_CD")
	public String getAddressCalleAtis() {
		return addressCalleAtis;
	}

	public void setAddressCalleAtis(String addressCalleAtis) {
		this.addressCalleAtis = addressCalleAtis;
	}

	@JsonProperty("NOM_CAL_DS")
	public String getAddressCalleNombre() {
		return addressCalleNombre;
	}

	public void setAddressCalleNombre(String addressCalleNombre) {
		this.addressCalleNombre = addressCalleNombre;
	}

	@JsonProperty("NUM_CAL_NU")
	public String getAddressCalleNumero() {
		return addressCalleNumero;
	}

	public void setAddressCalleNumero(String addressCalleNumero) {
		this.addressCalleNumero = addressCalleNumero;
	}

	@JsonProperty("COD_CNJ_HBL_CD")
	public String getAddressCCHHCodigo() {
		return addressCCHHCodigo;
	}

	public void setAddressCCHHCodigo(String addressCCHHCodigo) {
		this.addressCCHHCodigo = addressCCHHCodigo;
	}

	@JsonProperty("TIP_CNJ_HBL_CD")
	public String getAddressCCHHTipo() {
		return addressCCHHTipo;
	}

	public void setAddressCCHHTipo(String addressCCHHTipo) {
		this.addressCCHHTipo = addressCCHHTipo;
	}

	@JsonProperty("NOM_CNJ_HBL_NO")
	public String getAddressCCHHNombre() {
		return addressCCHHNombre;
	}

	public void setAddressCCHHNombre(String addressCCHHNombre) {
		this.addressCCHHNombre = addressCCHHNombre;
	}

	@JsonProperty("PRI_TIP_CMP_CD")
	public String getAddressCCHHCompTipo1() {
		return addressCCHHCompTipo1;
	}

	public void setAddressCCHHCompTipo1(String addressCCHHCompTipo1) {
		this.addressCCHHCompTipo1 = addressCCHHCompTipo1;
	}

	@JsonProperty("PRI_CMP_DIR_DS")
	public String getAddressCCHHCompNombre1() {
		return addressCCHHCompNombre1;
	}

	public void setAddressCCHHCompNombre1(String addressCCHHCompNombre1) {
		this.addressCCHHCompNombre1 = addressCCHHCompNombre1;
	}

	@JsonProperty("SEG_TIP_CMP_CD")
	public String getAddressCCHHCompTipo2() {
		return addressCCHHCompTipo2;
	}

	public void setAddressCCHHCompTipo2(String addressCCHHCompTipo2) {
		this.addressCCHHCompTipo2 = addressCCHHCompTipo2;
	}

	@JsonProperty("SEG_CMP_DIR_DS")
	public String getAddressCCHHCompNombre2() {
		return addressCCHHCompNombre2;
	}

	public void setAddressCCHHCompNombre2(String addressCCHHCompNombre2) {
		this.addressCCHHCompNombre2 = addressCCHHCompNombre2;
	}

	@JsonProperty("TRC_TIP_CMP_CD")
	public String getAddressCCHHCompTipo3() {
		return addressCCHHCompTipo3;
	}

	public void setAddressCCHHCompTipo3(String addressCCHHCompTipo3) {
		this.addressCCHHCompTipo3 = addressCCHHCompTipo3;
	}

	@JsonProperty("TRC_CMP_DIR_DS")
	public String getAddressCCHHCompNombre3() {
		return addressCCHHCompNombre3;
	}

	public void setAddressCCHHCompNombre3(String addressCCHHCompNombre3) {
		this.addressCCHHCompNombre3 = addressCCHHCompNombre3;
	}

	@JsonProperty("CAO_TIP_CMP_CD")
	public String getAddressCCHHCompTipo4() {
		return addressCCHHCompTipo4;
	}

	public void setAddressCCHHCompTipo4(String addressCCHHCompTipo4) {
		this.addressCCHHCompTipo4 = addressCCHHCompTipo4;
	}

	@JsonProperty("CAO_CMP_DIR_DS")
	public String getAddressCCHHCompNombre4() {
		return addressCCHHCompNombre4;
	}

	public void setAddressCCHHCompNombre4(String addressCCHHCompNombre4) {
		this.addressCCHHCompNombre4 = addressCCHHCompNombre4;
	}

	@JsonProperty("QUI_TIP_CMP_CD")
	public String getAddressCCHHCompTipo5() {
		return addressCCHHCompTipo5;
	}

	public void setAddressCCHHCompTipo5(String addressCCHHCompTipo5) {
		this.addressCCHHCompTipo5 = addressCCHHCompTipo5;
	}

	@JsonProperty("QUI_CMP_DIR_DS")
	public String getAddressCCHHCompNombre5() {
		return addressCCHHCompNombre5;
	}

	public void setAddressCCHHCompNombre5(String addressCCHHCompNombre5) {
		this.addressCCHHCompNombre5 = addressCCHHCompNombre5;
	}

	@JsonProperty("SET_TIP_CMP_CD")
	public String getAddressViaCompTipo1() {
		return addressViaCompTipo1;
	}

	public void setAddressViaCompTipo1(String addressViaCompTipo1) {
		this.addressViaCompTipo1 = addressViaCompTipo1;
	}

	@JsonProperty("SET_CMP_DIR_DS")
	public String getAddressViaCompNombre1() {
		return addressViaCompNombre1;
	}

	public void setAddressViaCompNombre1(String addressViaCompNombre1) {
		this.addressViaCompNombre1 = addressViaCompNombre1;
	}

	@JsonProperty("SPT_TIP_CMP_CD")
	public String getAddressViaCompTipo2() {
		return addressViaCompTipo2;
	}

	public void setAddressViaCompTipo2(String addressViaCompTipo2) {
		this.addressViaCompTipo2 = addressViaCompTipo2;
	}

	@JsonProperty("SPT_CMP_DIR_DS")
	public String getAddressViaCompNombre2() {
		return addressViaCompNombre2;
	}

	public void setAddressViaCompNombre2(String addressViaCompNombre2) {
		this.addressViaCompNombre2 = addressViaCompNombre2;
	}

	@JsonProperty("OCT_TIP_CMP_CD")
	public String getAddressViaCompTipo3() {
		return addressViaCompTipo3;
	}

	public void setAddressViaCompTipo3(String addressViaCompTipo3) {
		this.addressViaCompTipo3 = addressViaCompTipo3;
	}

	@JsonProperty("OCT_CMP_DIR_DS")
	public String getAddressViaCompNombre3() {
		return addressViaCompNombre3;
	}

	public void setAddressViaCompNombre3(String addressViaCompNombre3) {
		this.addressViaCompNombre3 = addressViaCompNombre3;
	}

	@JsonProperty("NOV_TIP_CMP_CD")
	public String getAddressViaCompTipo4() {
		return addressViaCompTipo4;
	}

	public void setAddressViaCompTipo4(String addressViaCompTipo4) {
		this.addressViaCompTipo4 = addressViaCompTipo4;
	}

	@JsonProperty("NOV_CMP_DIR_DS")
	public String getAddressViaCompNombre4() {
		return addressViaCompNombre4;
	}

	public void setAddressViaCompNombre4(String addressViaCompNombre4) {
		this.addressViaCompNombre4 = addressViaCompNombre4;
	}

	@JsonProperty("DCO_TIP_CMP_CD")
	public String getAddressViaCompTipo5() {
		return addressViaCompTipo5;
	}

	public void setAddressViaCompTipo5(String addressViaCompTipo5) {
		this.addressViaCompTipo5 = addressViaCompTipo5;
	}

	@JsonProperty("DCO_CMP_DIR_DS")
	public String getAddressViaCompNombre5() {
		return addressViaCompNombre5;
	}

	public void setAddressViaCompNombre5(String addressViaCompNombre5) {
		this.addressViaCompNombre5 = addressViaCompNombre5;
	}

	@JsonProperty("MSX_CBR_VOI_GES_IN")
	public String getOrderGisXDSL() {
		return orderGisXDSL;
	}

	public void setOrderGisXDSL(String orderGisXDSL) {
		this.orderGisXDSL = orderGisXDSL;
	}

	@JsonProperty("MSX_CBR_VOI_GIS_IN")
	public String getOrderGisHFC() {
		return orderGisHFC;
	}

	public void setOrderGisHFC(String orderGisHFC) {
		this.orderGisHFC = orderGisHFC;
	}

	@JsonProperty("MSX_IND_SNL_GIS_CD")
	public String getOrderGisTipoSenial() {
		return orderGisTipoSenial;
	}

	public void setOrderGisTipoSenial(String orderGisTipoSenial) {
		this.orderGisTipoSenial = orderGisTipoSenial;
	}

	@JsonProperty("MSX_IND_GPO_GIS_CD")
	public String getOrderGisGPON() {
		return orderGisGPON;
	}

	public void setOrderGisGPON(String orderGisGPON) {
		this.orderGisGPON = orderGisGPON;
	}

	@JsonProperty("COD_FZA_VEN_CD")
	public String getUserAtis() {
		return userAtis;
	}

	public void setUserAtis(String userAtis) {
		this.userAtis = userAtis;
	}

	@JsonProperty("COD_FZA_GEN_CD")
	public String getUserFuerzaVenta() {
		return userFuerzaVenta;
	}

	public void setUserFuerzaVenta(String userFuerzaVenta) {
		this.userFuerzaVenta = userFuerzaVenta;
	}

	@JsonProperty("COD_CNL_VEN_CD")
	public String getUserCanalCodigo() {
		return userCanalCodigo;
	}

	public void setUserCanalCodigo(String userCanalCodigo) {
		this.userCanalCodigo = userCanalCodigo;
	}

	@JsonProperty("COD_IND_SEN_CMS")
	public String getProductoSenial() {
		return productoSenial;
	}

	public void setProductoSenial(String productoSenial) {
		this.productoSenial = productoSenial;
	}

	@JsonProperty("COD_CAB_CMS")
	public String getProductoCabecera() {
		return productoCabecera;
	}

	public void setProductoCabecera(String productoCabecera) {
		this.productoCabecera = productoCabecera;
	}

	@JsonProperty("ID_COD_PRO_CD")
	public String getProductoCodigo() {
		return productoCodigo;
	}

	public void setProductoCodigo(String productoCodigo) {
		this.productoCodigo = productoCodigo;
	}

	@JsonProperty("PS_ADM_DEP_1")
	public String getProductoPSAdmin1() {
		return productoPSAdmin1;
	}

	public void setProductoPSAdmin1(String productoPSAdmin1) {
		this.productoPSAdmin1 = productoPSAdmin1;
	}

	@JsonProperty("PS_ADM_DEP_2")
	public String getProductoPSAdmin2() {
		return productoPSAdmin2;
	}

	public void setProductoPSAdmin2(String productoPSAdmin2) {
		this.productoPSAdmin2 = productoPSAdmin2;
	}

	@JsonProperty("PS_ADM_DEP_3")
	public String getProductoPSAdmin3() {
		return productoPSAdmin3;
	}

	public void setProductoPSAdmin3(String productoPSAdmin3) {
		this.productoPSAdmin3 = productoPSAdmin3;
	}

	@JsonProperty("PS_ADM_DEP_4")
	public String getProductoPSAdmin4() {
		return productoPSAdmin4;
	}

	public void setProductoPSAdmin4(String productoPSAdmin4) {
		this.productoPSAdmin4 = productoPSAdmin4;
	}

	@JsonProperty("ID_COD_SVA_CD_1")
	public String getProductoSVACodigo1() {
		return productoSVACodigo1;
	}

	public void setProductoSVACodigo1(String productoSVACodigo1) {
		this.productoSVACodigo1 = productoSVACodigo1;
	}

	@JsonProperty("ID_COD_SVA_CD_2")
	public String getProductoSVACodigo2() {
		return productoSVACodigo2;
	}

	public void setProductoSVACodigo2(String productoSVACodigo2) {
		this.productoSVACodigo2 = productoSVACodigo2;
	}

	@JsonProperty("ID_COD_SVA_CD_3")
	public String getProductoSVACodigo3() {
		return productoSVACodigo3;
	}

	public void setProductoSVACodigo3(String productoSVACodigo3) {
		this.productoSVACodigo3 = productoSVACodigo3;
	}

	@JsonProperty("ID_COD_SVA_CD_4")
	public String getProductoSVACodigo4() {
		return productoSVACodigo4;
	}

	public void setProductoSVACodigo4(String productoSVACodigo4) {
		this.productoSVACodigo4 = productoSVACodigo4;
	}

	@JsonProperty("ID_COD_SVA_CD_5")
	public String getProductoSVACodigo5() {
		return productoSVACodigo5;
	}

	public void setProductoSVACodigo5(String productoSVACodigo5) {
		this.productoSVACodigo5 = productoSVACodigo5;
	}

	@JsonProperty("ID_COD_SVA_CD_6")
	public String getProductoSVACodigo6() {
		return productoSVACodigo6;
	}

	public void setProductoSVACodigo6(String productoSVACodigo6) {
		this.productoSVACodigo6 = productoSVACodigo6;
	}

	@JsonProperty("ID_COD_SVA_CD_7")
	public String getProductoSVACodigo7() {
		return productoSVACodigo7;
	}

	public void setProductoSVACodigo7(String productoSVACodigo7) {
		this.productoSVACodigo7 = productoSVACodigo7;
	}

	@JsonProperty("ID_COD_SVA_CD_8")
	public String getProductoSVACodigo8() {
		return productoSVACodigo8;
	}

	public void setProductoSVACodigo8(String productoSVACodigo8) {
		this.productoSVACodigo8 = productoSVACodigo8;
	}

	@JsonProperty("ID_COD_SVA_CD_9")
	public String getProductoSVACodigo9() {
		return productoSVACodigo9;
	}

	public void setProductoSVACodigo9(String productoSVACodigo9) {
		this.productoSVACodigo9 = productoSVACodigo9;
	}

	@JsonProperty("ID_COD_SVA_CD_10")
	public String getProductoSVACodigo10() {
		return productoSVACodigo10;
	}

	public void setProductoSVACodigo10(String productoSVACodigo10) {
		this.productoSVACodigo10 = productoSVACodigo10;
	}

	@JsonProperty("COD_VTA_APP_CD")
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@JsonProperty("FEC_VTA_FF")
	public String getOrderFecha() {
		return orderFecha;
	}

	public void setOrderFecha(String orderFecha) {
		this.orderFecha = orderFecha;
	}

	@JsonProperty("NUM_IDE_TLF")
	public String getOrderParqueTelefono() {
		return orderParqueTelefono;
	}

	public void setOrderParqueTelefono(String orderParqueTelefono) {
		this.orderParqueTelefono = orderParqueTelefono;
	}

	@JsonProperty("COD_CLI_EXT_CD")
	public String getClienteCMSCodigo() {
		return clienteCMSCodigo;
	}

	public void setClienteCMSCodigo(String clienteCMSCodigo) {
		this.clienteCMSCodigo = clienteCMSCodigo;
	}

	@JsonProperty("TIP_TRX_CMR_CD")
	public String getOrderOperacionComercial() {
		return orderOperacionComercial;
	}

	public void setOrderOperacionComercial(String orderOperacionComercial) {
		this.orderOperacionComercial = orderOperacionComercial;
	}

	@JsonProperty("COD_SRV_CMS_CD")
	public String getClienteCMSServicio() {
		return clienteCMSServicio;
	}

	public void setClienteCMSServicio(String clienteCMSServicio) {
		this.clienteCMSServicio = clienteCMSServicio;
	}

	@JsonProperty("FEC_NAC_FF")
	public String getClienteNacimiento() {
		return clienteNacimiento;
	}

	public void setClienteNacimiento(String clienteNacimiento) {
		this.clienteNacimiento = clienteNacimiento;
	}

	@JsonProperty("COD_SEX_CD")
	public String getClienteSexo() {
		return clienteSexo;
	}

	public void setClienteSexo(String clienteSexo) {
		this.clienteSexo = clienteSexo;
	}

	@JsonProperty("COD_EST_CIV_CD")
	public String getClienteCivil() {
		return clienteCivil;
	}

	public void setClienteCivil(String clienteCivil) {
		this.clienteCivil = clienteCivil;
	}

	@JsonProperty("DES_MAI_CLI_DS")
	public String getClienteEmail() {
		return clienteEmail;
	}

	public void setClienteEmail(String clienteEmail) {
		this.clienteEmail = clienteEmail;
	}

	@JsonProperty("COD_ARE_TE1_CD")
	public String getClienteTelefono1Area() {
		return clienteTelefono1Area;
	}

	public void setClienteTelefono1Area(String clienteTelefono1Area) {
		this.clienteTelefono1Area = clienteTelefono1Area;
	}

	@JsonProperty("COD_ARE_TE2_CD")
	public String getClienteTelefono2Area() {
		return clienteTelefono2Area;
	}

	public void setClienteTelefono2Area(String clienteTelefono2Area) {
		this.clienteTelefono2Area = clienteTelefono2Area;
	}

	@JsonProperty("COD_CIC_FAC_CD")
	public String getClienteCMSFactura() {
		return clienteCMSFactura;
	}

	public void setClienteCMSFactura(String clienteCMSFactura) {
		this.clienteCMSFactura = clienteCMSFactura;
	}

	@JsonProperty("COD_DEP_CD")
	public String getAddressDepartamentoCodigo() {
		return addressDepartamentoCodigo;
	}

	public void setAddressDepartamentoCodigo(String addressDepartamentoCodigo) {
		this.addressDepartamentoCodigo = addressDepartamentoCodigo;
	}

	@JsonProperty("COD_PRV_CD")
	public String getAddressProvinciaCodigo() {
		return addressProvinciaCodigo;
	}

	public void setAddressProvinciaCodigo(String addressProvinciaCodigo) {
		this.addressProvinciaCodigo = addressProvinciaCodigo;
	}

	@JsonProperty("COD_DIS_CD")
	public String getAddressDistritoCodigo() {
		return addressDistritoCodigo;
	}

	public void setAddressDistritoCodigo(String addressDistritoCodigo) {
		this.addressDistritoCodigo = addressDistritoCodigo;
	}

	@JsonProperty("COD_FAC_TEC_CD")
	public String getOrderGisNTLT() {
		return orderGisNTLT;
	}

	public void setOrderGisNTLT(String orderGisNTLT) {
		this.orderGisNTLT = orderGisNTLT;
	}

	@JsonProperty("NUM_COD_X_CD")
	public String getOrderGPSX() {
		return orderGPSX;
	}

	public void setOrderGPSX(String orderGPSX) {
		this.orderGPSX = orderGPSX;
	}

	@JsonProperty("NUM_COD_Y_CD")
	public String getOrderGPSY() {
		return orderGPSY;
	}

	public void setOrderGPSY(String orderGPSY) {
		this.orderGPSY = orderGPSY;
	}

	@JsonProperty("COD_VEN_CMS_CD")
	public String getUserCSMPuntoVenta() {
		return userCSMPuntoVenta;
	}

	public void setUserCSMPuntoVenta(String userCSMPuntoVenta) {
		this.userCSMPuntoVenta = userCSMPuntoVenta;
	}

	@JsonProperty("COD_SVE_CMS_CD")
	public String getUserCMS() {
		return userCMS;
	}

	public void setUserCMS(String userCMS) {
		this.userCMS = userCMS;
	}

	@JsonProperty("NOM_VEN_DS")
	public String getUserNombre() {
		return userNombre;
	}

	public void setUserNombre(String userNombre) {
		this.userNombre = userNombre;
	}

	@JsonProperty("REF_DIR_ENT_DS")
	public String getAddressReferencia() {
		return addressReferencia;
	}

	public void setAddressReferencia(String addressReferencia) {
		this.addressReferencia = addressReferencia;
	}

	@JsonProperty("COD_TIP_CAL_CD")
	public String getOrderCallTipo() {
		return orderCallTipo;
	}

	public void setOrderCallTipo(String orderCallTipo) {
		this.orderCallTipo = orderCallTipo;
	}

	@JsonProperty("DES_TIP_CAL_CD")
	public String getOrderCallDesc() {
		return orderCallDesc;
	}

	public void setOrderCallDesc(String orderCallDesc) {
		this.orderCallDesc = orderCallDesc;
	}

	@JsonProperty("TIP_MOD_EQU_CD")
	public String getOrderModelo() {
		return orderModelo;
	}

	public void setOrderModelo(String orderModelo) {
		this.orderModelo = orderModelo;
	}

	@JsonProperty("NUM_DOC_VEN_CD")
	public String getUserDNI() {
		return userDNI;
	}

	public void setUserDNI(String userDNI) {
		this.userDNI = userDNI;
	}

	@JsonProperty("NUM_TEL_VEN_DS")
	public String getUserTelefono() {
		return userTelefono;
	}

	public void setUserTelefono(String userTelefono) {
		this.userTelefono = userTelefono;
	}

	@JsonProperty("IND_ENV_CON_CD")
	public String getOrderContrato() {
		return orderContrato;
	}

	public void setOrderContrato(String orderContrato) {
		this.orderContrato = orderContrato;
	}

	@JsonProperty("IND_ID_GRA_CD")
	public String getOrderIDGrabacion() {
		return orderIDGrabacion;
	}

	public void setOrderIDGrabacion(String orderIDGrabacion) {
		this.orderIDGrabacion = orderIDGrabacion;
	}

	@JsonProperty("MOD_ACE_VEN_CD")
	public String getOrderModalidadAcep() {
		return orderModalidadAcep;
	}

	public void setOrderModalidadAcep(String orderModalidadAcep) {
		this.orderModalidadAcep = orderModalidadAcep;
	}

	@JsonProperty("DET_CAN_VEN_DS")
	public String getOrderEntidad() {
		return orderEntidad;
	}

	public void setOrderEntidad(String orderEntidad) {
		this.orderEntidad = orderEntidad;
	}

	@JsonProperty("COD_REG_CD")
	public String getOrderRegion() {
		return orderRegion;
	}

	public void setOrderRegion(String orderRegion) {
		this.orderRegion = orderRegion;
	}

	@JsonProperty("COD_CIP_CD")
	public String getOrderCIP() {
		return orderCIP;
	}

	public void setOrderCIP(String orderCIP) {
		this.orderCIP = orderCIP;
	}

	@JsonProperty("COD_EXP_CD")
	public String getOrderExperto() {
		return orderExperto;
	}

	public void setOrderExperto(String orderExperto) {
		this.orderExperto = orderExperto;
	}

	@JsonProperty("COD_ZON_VEN_CD")
	public String getOrderZonal() {
		return orderZonal;
	}

	public void setOrderZonal(String orderZonal) {
		this.orderZonal = orderZonal;
	}

	@JsonProperty("IND_WEB_PAR_CD")
	public String getOrderWebParental() {
		return orderWebParental;
	}

	public void setOrderWebParental(String orderWebParental) {
		this.orderWebParental = orderWebParental;
	}

	@JsonProperty("COD_STC")
	public String getOrderCodigoSTC6I() {
		return orderCodigoSTC6I;
	}

	public void setOrderCodigoSTC6I(String orderCodigoSTC6I) {
		this.orderCodigoSTC6I = orderCodigoSTC6I;
	}

	@JsonProperty("DES_NAC_DS")
	public String getClienteNacionalidad() {
		return clienteNacionalidad;
	}

	public void setClienteNacionalidad(String clienteNacionalidad) {
		this.clienteNacionalidad = clienteNacionalidad;
	}

	@JsonProperty("COD_MOD_VEN_CD")
	public String getOrderModoVenta() {
		return orderModoVenta;
	}

	public void setOrderModoVenta(String orderModoVenta) {
		this.orderModoVenta = orderModoVenta;
	}

	@JsonProperty("CAN_EQU_VEN")
	public String getOrderCantidadEquipos() {
		return orderCantidadEquipos;
	}

	public void setOrderCantidadEquipos(String orderCantidadEquipos) {
		this.orderCantidadEquipos = orderCantidadEquipos;
	}
}
