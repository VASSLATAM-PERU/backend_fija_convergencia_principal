package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataAutomatizador {
	 
	@JsonProperty("nom_ds")
	private String nom_ds;
	@JsonProperty("pri_ape_ds") 
	private String pri_ape_ds;
	@JsonProperty("seg_ape_ds")
	private String seg_ape_ds; 
	@JsonProperty("num_doc_cli_cd")
	private String num_doc_cli_cd;
	@JsonProperty("dig_ctl_doc_cd")
	private String dig_ctl_doc_cd;
	@JsonProperty("tip_doc_cd")
	private String tip_doc_cd;
	@JsonProperty("tel_pri_cot_cd")
	private String tel_pri_cot_cd;
	@JsonProperty("tel_seg_cot_cd")
	private String tel_seg_cot_cd;
	@JsonProperty("cod_act_eco_cd")
	private String cod_act_eco_cd;
	@JsonProperty("cod_sac_eco_cd")
	private String cod_sac_eco_cd;
	@JsonProperty("tip_cal_ati_cd")
	private String tip_cal_ati_cd;
	@JsonProperty("nom_cal_ds")
	private String nom_cal_ds;
	@JsonProperty("num_cal_nu")
	private String num_cal_nu;
	@JsonProperty("cod_cnj_hbl_cd")
	private String cod_cnj_hbl_cd;
	@JsonProperty("tip_cnj_hbl_cd")
	private String tip_cnj_hbl_cd;
	@JsonProperty("nom_cnj_hbl_no")
	private String nom_cnj_hbl_no;
	@JsonProperty("pri_tip_cmp_cd")
	private String pri_tip_cmp_cd;
	@JsonProperty("pri_cmp_dir_ds")
	private String pri_cmp_dir_ds;
	@JsonProperty("seg_tip_cmp_cd")
	private String seg_tip_cmp_cd;
	@JsonProperty("seg_cmp_dir_ds")
	private String seg_cmp_dir_ds;
	@JsonProperty("trc_tip_cmp_cd")
	private String trc_tip_cmp_cd;
	@JsonProperty("trc_cmp_dir_ds")
	private String trc_cmp_dir_ds;
	@JsonProperty("cao_tip_cmp_cd")
	private String cao_tip_cmp_cd;
	@JsonProperty("cao_cmp_dir_ds")
	private String cao_cmp_dir_ds;
	@JsonProperty("qui_tip_cmp_cd")
	private String qui_tip_cmp_cd;
	@JsonProperty("qui_cmp_dir_ds")
	private String qui_cmp_dir_ds;
	@JsonProperty("set_tip_cmp_cd")
	private String set_tip_cmp_cd;
	@JsonProperty("set_cmp_dir_ds")
	private String set_cmp_dir_ds;
	@JsonProperty("spt_tip_cmp_cd")
	private String spt_tip_cmp_cd;
	@JsonProperty("spt_cmp_dir_ds")
	private String spt_cmp_dir_ds;
	@JsonProperty("oct_tip_cmp_cd")
	private String oct_tip_cmp_cd;
	@JsonProperty("oct_cmp_dir_ds")
	private String oct_cmp_dir_ds;
	@JsonProperty("nov_tip_cmp_cd")
	private String nov_tip_cmp_cd;
	@JsonProperty("nov_cmp_dir_ds")
	private String nov_cmp_dir_ds;
	@JsonProperty("dco_tip_cmp_cd")
	private String dco_tip_cmp_cd;
	@JsonProperty("dco_cmp_dir_ds")
	private String dco_cmp_dir_ds;
	@JsonProperty("msx_cbr_voi_ges_in")
	private String msx_cbr_voi_ges_in;
	@JsonProperty("msx_cbr_voi_gis_in")
	private String msx_cbr_voi_gis_in;
	@JsonProperty("msx_ind_snl_gis_cd")
	private String msx_ind_snl_gis_cd;
	@JsonProperty("msx_ind_gpo_gis_cd")
	private String msx_ind_gpo_gis_cd;
	@JsonProperty("cod_fza_ven_cd")
	private String cod_fza_ven_cd;
	@JsonProperty("cod_fza_gen_cd")
	private String cod_fza_gen_cd;
	@JsonProperty("cod_cnl_ven_cd")
	private String cod_cnl_ven_cd;
	@JsonProperty("cod_ind_sen_cms")
	private String cod_ind_sen_cms;
	@JsonProperty("cod_cab_cms")
	private String cod_cab_cms;
	@JsonProperty("id_cod_pro_cd")
	private String id_cod_pro_cd;
	@JsonProperty("ps_adm_dep_1")
	private String ps_adm_dep_1;
	@JsonProperty("ps_adm_dep_2")
	private String ps_adm_dep_2;
	@JsonProperty("ps_adm_dep_3")
	private String ps_adm_dep_3;
	@JsonProperty("ps_adm_dep_4")
	private String ps_adm_dep_4;
	@JsonProperty("id_cod_sva_cd_1")
	private String id_cod_sva_cd_1;
	@JsonProperty("id_cod_sva_cd_2")
	private String id_cod_sva_cd_2;
	@JsonProperty("id_cod_sva_cd_3")
	private String id_cod_sva_cd_3;
	@JsonProperty("id_cod_sva_cd_4")
	private String id_cod_sva_cd_4;
	@JsonProperty("id_cod_sva_cd_5")
	private String id_cod_sva_cd_5;
	@JsonProperty("id_cod_sva_cd_6")
	private String id_cod_sva_cd_6;
	@JsonProperty("id_cod_sva_cd_7")
	private String id_cod_sva_cd_7;
	@JsonProperty("id_cod_sva_cd_8")
	private String id_cod_sva_cd_8;
	@JsonProperty("id_cod_sva_cd_9")
	private String id_cod_sva_cd_9;
	@JsonProperty("id_cod_sva_cd_10")
	private String id_cod_sva_cd_10;
	@JsonProperty("cod_vta_app_cd")
	private String cod_vta_app_cd;
	@JsonProperty("fec_vta_ff")
	private String fec_vta_ff;
	@JsonProperty("num_ide_tlf")
	private String num_ide_tlf;
	@JsonProperty("cod_cli_ext_cd")
	private String cod_cli_ext_cd;
	@JsonProperty("tip_trx_cmr_cd")
	private String tip_trx_cmr_cd;
	@JsonProperty("cod_srv_cms_cd")
	private String cod_srv_cms_cd;
	@JsonProperty("fec_nac_ff")
	private String fec_nac_ff;
	@JsonProperty("cod_sex_cd")
	private String cod_sex_cd;
	@JsonProperty("cod_est_civ_cd")
	private String cod_est_civ_cd;
	@JsonProperty("des_mai_cli_ds")
	private String des_mai_cli_ds;
	@JsonProperty("cod_are_te1_cd")
	private String cod_are_te1_cd;
	@JsonProperty("cod_are_te2_cd")
	private String cod_are_te2_cd;
	@JsonProperty("cod_cic_fac_cd")
	private String cod_cic_fac_cd;
	@JsonProperty("cod_dep_cd")
	private String cod_dep_cd;
	@JsonProperty("cod_prv_cd")
	private String cod_prv_cd;
	@JsonProperty("cod_dis_cd")
	private String cod_dis_cd;
	@JsonProperty("cod_fac_tec_cd")
	private String cod_fac_tec_cd;
	@JsonProperty("num_cod_x_cd")
	private String num_cod_x_cd;
	@JsonProperty("num_cod_y_cd")
	private String num_cod_y_cd;
	@JsonProperty("cod_ven_cms_cd")
	private String cod_ven_cms_cd;
	@JsonProperty("cod_sve_cms_cd")
	private String cod_sve_cms_cd;
	@JsonProperty("nom_ven_ds")
	private String nom_ven_ds;
	@JsonProperty("ref_dir_ent_ds")
	private String ref_dir_ent_ds;
	@JsonProperty("cod_tip_cal_cd")
	private String cod_tip_cal_cd;
	@JsonProperty("des_tip_cal_cd")
	private String des_tip_cal_cd;
	@JsonProperty("tip_mod_equ_cd")
	private String tip_mod_equ_cd;
	@JsonProperty("num_doc_ven_cd")
	private String num_doc_ven_cd;
	@JsonProperty("num_tel_ven_ds")
	private String num_tel_ven_ds;
	@JsonProperty("ind_env_con_cd")
	private String ind_env_con_cd;
	@JsonProperty("ind_id_gra_cd")
	private String ind_id_gra_cd;
	@JsonProperty("mod_ace_ven_cd")
	private String mod_ace_ven_cd;
	@JsonProperty("det_can_ven_ds")
	private String det_can_ven_ds;
	@JsonProperty("cod_reg_cd")
	private String cod_reg_cd;
	@JsonProperty("cod_cip_cd")
	private String cod_cip_cd;
	@JsonProperty("cod_exp_cd")
	private String cod_exp_cd;
	@JsonProperty("cod_zon_ven_cd")
	private String cod_zon_ven_cd;
	@JsonProperty("ind_web_par_cd")
	private String ind_web_par_cd;
	@JsonProperty("cod_stc")
	private String cod_stc;
	@JsonProperty("des_nac_ds")
	private String des_nac_ds;
	@JsonProperty("cod_mod_ven_cd")
	private String cod_mod_ven_cd;
	@JsonProperty("can_equ_ven")
	private String can_equ_ven;
	
	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonProperty("nom_ds")
	public String getNom_ds() {
		return nom_ds;
	}
	@JsonProperty("nom_ds")
	public void setNom_ds(String nom_ds) {
		this.nom_ds = nom_ds;
	}

	@JsonProperty("pri_ape_ds")
	public String getPri_ape_ds() {
		return pri_ape_ds;
	}
	@JsonProperty("pri_ape_ds")
	public void setPri_ape_ds(String pri_ape_ds) {
		this.pri_ape_ds = pri_ape_ds;
	}
	@JsonProperty("seg_ape_ds")
	public String getSeg_ape_ds() {
		return seg_ape_ds;
	}
	@JsonProperty("seg_ape_ds")
	public void setSeg_ape_ds(String seg_ape_ds) {
		this.seg_ape_ds = seg_ape_ds;
	}
	@JsonProperty("num_doc_cli_cd")
	public String getNum_doc_cli_cd() {
		return num_doc_cli_cd;
	}
	@JsonProperty("num_doc_cli_cd")
	public void setNum_doc_cli_cd(String num_doc_cli_cd) {
		this.num_doc_cli_cd = num_doc_cli_cd;
	}
	@JsonProperty("dig_ctl_doc_cd")
	public String getDig_ctl_doc_cd() {
		return dig_ctl_doc_cd;
	}
	@JsonProperty("dig_ctl_doc_cd")
	public void setDig_ctl_doc_cd(String dig_ctl_doc_cd) {
		this.dig_ctl_doc_cd = dig_ctl_doc_cd;
	}
	@JsonProperty("tip_doc_cd")
	public String getTip_doc_cd() {
		return tip_doc_cd;
	}
	@JsonProperty("tip_doc_cd")
	public void setTip_doc_cd(String tip_doc_cd) {
		this.tip_doc_cd = tip_doc_cd;
	}
	@JsonProperty("tel_pri_cot_cd")
	public String getTel_pri_cot_cd() {
		return tel_pri_cot_cd;
	}
	@JsonProperty("tel_pri_cot_cd")
	public void setTel_pri_cot_cd(String tel_pri_cot_cd) {
		this.tel_pri_cot_cd = tel_pri_cot_cd;
	}
	@JsonProperty("tel_seg_cot_cd")
	public String getTel_seg_cot_cd() {
		return tel_seg_cot_cd;
	}
	@JsonProperty("tel_seg_cot_cd")
	public void setTel_seg_cot_cd(String tel_seg_cot_cd) {
		this.tel_seg_cot_cd = tel_seg_cot_cd;
	}
	@JsonProperty("cod_act_eco_cd")
	public String getCod_act_eco_cd() {
		return cod_act_eco_cd;
	}
	@JsonProperty("cod_act_eco_cd")
	public void setCod_act_eco_cd(String cod_act_eco_cd) {
		this.cod_act_eco_cd = cod_act_eco_cd;
	}
	@JsonProperty("cod_sac_eco_cd")
	public String getCod_sac_eco_cd() {
		return cod_sac_eco_cd;
	}
	@JsonProperty("cod_sac_eco_cd")
	public void setCod_sac_eco_cd(String cod_sac_eco_cd) {
		this.cod_sac_eco_cd = cod_sac_eco_cd;
	}
	@JsonProperty("tip_cal_ati_cd")
	public String getTip_cal_ati_cd() {
		return tip_cal_ati_cd;
	}
	@JsonProperty("tip_cal_ati_cd")
	public void setTip_cal_ati_cd(String tip_cal_ati_cd) {
		this.tip_cal_ati_cd = tip_cal_ati_cd;
	}
	@JsonProperty("nom_cal_ds")
	public String getNom_cal_ds() {
		return nom_cal_ds;
	}
	@JsonProperty("nom_cal_ds")
	public void setNom_cal_ds(String nom_cal_ds) {
		this.nom_cal_ds = nom_cal_ds;
	}
	@JsonProperty("num_cal_nu")
	public String getNum_cal_nu() {
		return num_cal_nu;
	}
	@JsonProperty("num_cal_nu")
	public void setNum_cal_nu(String num_cal_nu) {
		this.num_cal_nu = num_cal_nu;
	}
	@JsonProperty("cod_cnj_hbl_cd")
	public String getCod_cnj_hbl_cd() {
		return cod_cnj_hbl_cd;
	}
	@JsonProperty("cod_cnj_hbl_cd")
	public void setCod_cnj_hbl_cd(String cod_cnj_hbl_cd) {
		this.cod_cnj_hbl_cd = cod_cnj_hbl_cd;
	}
	@JsonProperty("tip_cnj_hbl_cd")
	public String getTip_cnj_hbl_cd() {
		return tip_cnj_hbl_cd;
	}
	@JsonProperty("tip_cnj_hbl_cd")
	public void setTip_cnj_hbl_cd(String tip_cnj_hbl_cd) {
		this.tip_cnj_hbl_cd = tip_cnj_hbl_cd;
	}
	@JsonProperty("nom_cnj_hbl_no")
	public String getNom_cnj_hbl_no() {
		return nom_cnj_hbl_no;
	}
	@JsonProperty("nom_cnj_hbl_no")
	public void setNom_cnj_hbl_no(String nom_cnj_hbl_no) {
		this.nom_cnj_hbl_no = nom_cnj_hbl_no;
	}
	@JsonProperty("pri_tip_cmp_cd")
	public String getPri_tip_cmp_cd() {
		return pri_tip_cmp_cd;
	}
	@JsonProperty("pri_tip_cmp_cd")
	public void setPri_tip_cmp_cd(String pri_tip_cmp_cd) {
		this.pri_tip_cmp_cd = pri_tip_cmp_cd;
	}
	@JsonProperty("pri_cmp_dir_ds")
	public String getPri_cmp_dir_ds() {
		return pri_cmp_dir_ds;
	}
	@JsonProperty("pri_cmp_dir_ds")
	public void setPri_cmp_dir_ds(String pri_cmp_dir_ds) {
		this.pri_cmp_dir_ds = pri_cmp_dir_ds;
	}
	@JsonProperty("seg_tip_cmp_cd")
	public String getSeg_tip_cmp_cd() {
		return seg_tip_cmp_cd;
	}
	@JsonProperty("seg_tip_cmp_cd")
	public void setSeg_tip_cmp_cd(String seg_tip_cmp_cd) {
		this.seg_tip_cmp_cd = seg_tip_cmp_cd;
	}
	@JsonProperty("seg_cmp_dir_ds")
	public String getSeg_cmp_dir_ds() {
		return seg_cmp_dir_ds;
	}
	@JsonProperty("seg_cmp_dir_ds")
	public void setSeg_cmp_dir_ds(String seg_cmp_dir_ds) {
		this.seg_cmp_dir_ds = seg_cmp_dir_ds;
	}
	@JsonProperty("trc_tip_cmp_cd")
	public String getTrc_tip_cmp_cd() {
		return trc_tip_cmp_cd;
	}
	@JsonProperty("trc_tip_cmp_cd")
	public void setTrc_tip_cmp_cd(String trc_tip_cmp_cd) {
		this.trc_tip_cmp_cd = trc_tip_cmp_cd;
	}
	@JsonProperty("trc_cmp_dir_ds")
	public String getTrc_cmp_dir_ds() {
		return trc_cmp_dir_ds;
	}
	@JsonProperty("trc_cmp_dir_ds")
	public void setTrc_cmp_dir_ds(String trc_cmp_dir_ds) {
		this.trc_cmp_dir_ds = trc_cmp_dir_ds;
	}
	@JsonProperty("cao_tip_cmp_cd")
	public String getCao_tip_cmp_cd() {
		return cao_tip_cmp_cd;
	}
	@JsonProperty("cao_tip_cmp_cd")
	public void setCao_tip_cmp_cd(String cao_tip_cmp_cd) {
		this.cao_tip_cmp_cd = cao_tip_cmp_cd;
	}
	@JsonProperty("cao_cmp_dir_ds")
	public String getCao_cmp_dir_ds() {
		return cao_cmp_dir_ds;
	}
	@JsonProperty("cao_cmp_dir_ds")
	public void setCao_cmp_dir_ds(String cao_cmp_dir_ds) {
		this.cao_cmp_dir_ds = cao_cmp_dir_ds;
	}
	@JsonProperty("qui_tip_cmp_cd")
	public String getQui_tip_cmp_cd() {
		return qui_tip_cmp_cd;
	}
	@JsonProperty("qui_tip_cmp_cd")
	public void setQui_tip_cmp_cd(String qui_tip_cmp_cd) {
		this.qui_tip_cmp_cd = qui_tip_cmp_cd;
	}
	@JsonProperty("qui_cmp_dir_ds")
	public String getQui_cmp_dir_ds() {
		return qui_cmp_dir_ds;
	}
	@JsonProperty("qui_cmp_dir_ds")
	public void setQui_cmp_dir_ds(String qui_cmp_dir_ds) {
		this.qui_cmp_dir_ds = qui_cmp_dir_ds;
	}
	@JsonProperty("set_tip_cmp_cd")
	public String getSet_tip_cmp_cd() {
		return set_tip_cmp_cd;
	}
	@JsonProperty("set_tip_cmp_cd")
	public void setSet_tip_cmp_cd(String set_tip_cmp_cd) {
		this.set_tip_cmp_cd = set_tip_cmp_cd;
	}
	@JsonProperty("set_cmp_dir_ds")
	public String getSet_cmp_dir_ds() {
		return set_cmp_dir_ds;
	}
	@JsonProperty("set_cmp_dir_ds")
	public void setSet_cmp_dir_ds(String set_cmp_dir_ds) {
		this.set_cmp_dir_ds = set_cmp_dir_ds;
	}
	@JsonProperty("spt_tip_cmp_cd")
	public String getSpt_tip_cmp_cd() {
		return spt_tip_cmp_cd;
	}
	@JsonProperty("spt_tip_cmp_cd")
	public void setSpt_tip_cmp_cd(String spt_tip_cmp_cd) {
		this.spt_tip_cmp_cd = spt_tip_cmp_cd;
	}
	@JsonProperty("spt_cmp_dir_ds")
	public String getSpt_cmp_dir_ds() {
		return spt_cmp_dir_ds;
	}
	@JsonProperty("spt_cmp_dir_ds")
	public void setSpt_cmp_dir_ds(String spt_cmp_dir_ds) {
		this.spt_cmp_dir_ds = spt_cmp_dir_ds;
	}
	@JsonProperty("oct_tip_cmp_cd")
	public String getOct_tip_cmp_cd() {
		return oct_tip_cmp_cd;
	}
	@JsonProperty("oct_tip_cmp_cd")
	public void setOct_tip_cmp_cd(String oct_tip_cmp_cd) {
		this.oct_tip_cmp_cd = oct_tip_cmp_cd;
	}
	@JsonProperty("oct_cmp_dir_ds")
	public String getOct_cmp_dir_ds() {
		return oct_cmp_dir_ds;
	}
	@JsonProperty("oct_cmp_dir_ds")
	public void setOct_cmp_dir_ds(String oct_cmp_dir_ds) {
		this.oct_cmp_dir_ds = oct_cmp_dir_ds;
	}
	@JsonProperty("nov_tip_cmp_cd")
	public String getNov_tip_cmp_cd() {
		return nov_tip_cmp_cd;
	}
	@JsonProperty("nov_tip_cmp_cd")
	public void setNov_tip_cmp_cd(String nov_tip_cmp_cd) {
		this.nov_tip_cmp_cd = nov_tip_cmp_cd;
	}
	@JsonProperty("nov_cmp_dir_ds")
	public String getNov_cmp_dir_ds() {
		return nov_cmp_dir_ds;
	}
	@JsonProperty("nov_cmp_dir_ds")
	public void setNov_cmp_dir_ds(String nov_cmp_dir_ds) {
		this.nov_cmp_dir_ds = nov_cmp_dir_ds;
	}
	@JsonProperty("dco_tip_cmp_cd")
	public String getDco_tip_cmp_cd() {
		return dco_tip_cmp_cd;
	}
	@JsonProperty("dco_tip_cmp_cd")
	public void setDco_tip_cmp_cd(String dco_tip_cmp_cd) {
		this.dco_tip_cmp_cd = dco_tip_cmp_cd;
	}
	@JsonProperty("dco_cmp_dir_ds")
	public String getDco_cmp_dir_ds() {
		return dco_cmp_dir_ds;
	}
	@JsonProperty("dco_cmp_dir_ds")
	public void setDco_cmp_dir_ds(String dco_cmp_dir_ds) {
		this.dco_cmp_dir_ds = dco_cmp_dir_ds;
	}
	@JsonProperty("msx_cbr_voi_ges_in")
	public String getMsx_cbr_voi_ges_in() {
		return msx_cbr_voi_ges_in;
	}
	@JsonProperty("msx_cbr_voi_ges_in")
	public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
		this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
	}
	@JsonProperty("msx_cbr_voi_gis_in")
	public String getMsx_cbr_voi_gis_in() {
		return msx_cbr_voi_gis_in;
	}
	@JsonProperty("msx_cbr_voi_gis_in")
	public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
		this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
	}
	@JsonProperty("msx_ind_snl_gis_cd")
	public String getMsx_ind_snl_gis_cd() {
		return msx_ind_snl_gis_cd;
	}
	@JsonProperty("msx_ind_snl_gis_cd")
	public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
		this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
	}
	@JsonProperty("msx_ind_gpo_gis_cd")
	public String getMsx_ind_gpo_gis_cd() {
		return msx_ind_gpo_gis_cd;
	}
	@JsonProperty("msx_ind_gpo_gis_cd")
	public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
		this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
	}
	@JsonProperty("cod_fza_ven_cd")
	public String getCod_fza_ven_cd() {
		return cod_fza_ven_cd;
	}
	@JsonProperty("cod_fza_ven_cd")
	public void setCod_fza_ven_cd(String cod_fza_ven_cd) {
		this.cod_fza_ven_cd = cod_fza_ven_cd;
	}
	@JsonProperty("cod_fza_gen_cd")
	public String getCod_fza_gen_cd() {
		return cod_fza_gen_cd;
	}
	@JsonProperty("cod_fza_gen_cd")
	public void setCod_fza_gen_cd(String cod_fza_gen_cd) {
		this.cod_fza_gen_cd = cod_fza_gen_cd;
	}
	@JsonProperty("cod_cnl_ven_cd")
	public String getCod_cnl_ven_cd() {
		return cod_cnl_ven_cd;
	}
	@JsonProperty("cod_cnl_ven_cd")
	public void setCod_cnl_ven_cd(String cod_cnl_ven_cd) {
		this.cod_cnl_ven_cd = cod_cnl_ven_cd;
	}
	@JsonProperty("cod_ind_sen_cms")
	public String getCod_ind_sen_cms() {
		return cod_ind_sen_cms;
	}
	@JsonProperty("cod_ind_sen_cms")
	public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
		this.cod_ind_sen_cms = cod_ind_sen_cms;
	}
	@JsonProperty("cod_cab_cms")
	public String getCod_cab_cms() {
		return cod_cab_cms;
	}
	@JsonProperty("cod_cab_cms")
	public void setCod_cab_cms(String cod_cab_cms) {
		this.cod_cab_cms = cod_cab_cms;
	}
	@JsonProperty("id_cod_pro_cd")
	public String getId_cod_pro_cd() {
		return id_cod_pro_cd;
	}
	@JsonProperty("id_cod_pro_cd")
	public void setId_cod_pro_cd(String id_cod_pro_cd) {
		this.id_cod_pro_cd = id_cod_pro_cd;
	}
	@JsonProperty("ps_adm_dep_1")
	public String getPs_adm_dep_1() {
		return ps_adm_dep_1;
	}
	@JsonProperty("ps_adm_dep_1")
	public void setPs_adm_dep_1(String ps_adm_dep_1) {
		this.ps_adm_dep_1 = ps_adm_dep_1;
	}
	@JsonProperty("ps_adm_dep_2")
	public String getPs_adm_dep_2() {
		return ps_adm_dep_2;
	}
	@JsonProperty("ps_adm_dep_2")
	public void setPs_adm_dep_2(String ps_adm_dep_2) {
		this.ps_adm_dep_2 = ps_adm_dep_2;
	}
	@JsonProperty("ps_adm_dep_3")
	public String getPs_adm_dep_3() {
		return ps_adm_dep_3;
	}
	@JsonProperty("ps_adm_dep_3")
	public void setPs_adm_dep_3(String ps_adm_dep_3) {
		this.ps_adm_dep_3 = ps_adm_dep_3;
	}
	@JsonProperty("ps_adm_dep_4")
	public String getPs_adm_dep_4() {
		return ps_adm_dep_4;
	}
	@JsonProperty("ps_adm_dep_4")
	public void setPs_adm_dep_4(String ps_adm_dep_4) {
		this.ps_adm_dep_4 = ps_adm_dep_4;
	}
	@JsonProperty("id_cod_sva_cd_1")
	public String getId_cod_sva_cd_1() {
		return id_cod_sva_cd_1;
	}
	@JsonProperty("id_cod_sva_cd_1")
	public void setId_cod_sva_cd_1(String id_cod_sva_cd_1) {
		this.id_cod_sva_cd_1 = id_cod_sva_cd_1;
	}
	@JsonProperty("id_cod_sva_cd_2")
	public String getId_cod_sva_cd_2() {
		return id_cod_sva_cd_2;
	}
	@JsonProperty("id_cod_sva_cd_2")
	public void setId_cod_sva_cd_2(String id_cod_sva_cd_2) {
		this.id_cod_sva_cd_2 = id_cod_sva_cd_2;
	}
	@JsonProperty("id_cod_sva_cd_3")
	public String getId_cod_sva_cd_3() {
		return id_cod_sva_cd_3;
	}
	@JsonProperty("id_cod_sva_cd_3")
	public void setId_cod_sva_cd_3(String id_cod_sva_cd_3) {
		this.id_cod_sva_cd_3 = id_cod_sva_cd_3;
	}
	@JsonProperty("id_cod_sva_cd_4")
	public String getId_cod_sva_cd_4() {
		return id_cod_sva_cd_4;
	}
	@JsonProperty("id_cod_sva_cd_4")
	public void setId_cod_sva_cd_4(String id_cod_sva_cd_4) {
		this.id_cod_sva_cd_4 = id_cod_sva_cd_4;
	}
	@JsonProperty("id_cod_sva_cd_5")
	public String getId_cod_sva_cd_5() {
		return id_cod_sva_cd_5;
	}
	@JsonProperty("id_cod_sva_cd_5")
	public void setId_cod_sva_cd_5(String id_cod_sva_cd_5) {
		this.id_cod_sva_cd_5 = id_cod_sva_cd_5;
	}
	@JsonProperty("id_cod_sva_cd_6")
	public String getId_cod_sva_cd_6() {
		return id_cod_sva_cd_6;
	}
	@JsonProperty("id_cod_sva_cd_6")
	public void setId_cod_sva_cd_6(String id_cod_sva_cd_6) {
		this.id_cod_sva_cd_6 = id_cod_sva_cd_6;
	}
	@JsonProperty("id_cod_sva_cd_7")
	public String getId_cod_sva_cd_7() {
		return id_cod_sva_cd_7;
	}
	@JsonProperty("id_cod_sva_cd_7")
	public void setId_cod_sva_cd_7(String id_cod_sva_cd_7) {
		this.id_cod_sva_cd_7 = id_cod_sva_cd_7;
	}
	@JsonProperty("id_cod_sva_cd_8")
	public String getId_cod_sva_cd_8() {
		return id_cod_sva_cd_8;
	}
	@JsonProperty("id_cod_sva_cd_8")
	public void setId_cod_sva_cd_8(String id_cod_sva_cd_8) {
		this.id_cod_sva_cd_8 = id_cod_sva_cd_8;
	}
	@JsonProperty("id_cod_sva_cd_9")
	public String getId_cod_sva_cd_9() {
		return id_cod_sva_cd_9;
	}
	@JsonProperty("id_cod_sva_cd_9")
	public void setId_cod_sva_cd_9(String id_cod_sva_cd_9) {
		this.id_cod_sva_cd_9 = id_cod_sva_cd_9;
	}
	@JsonProperty("id_cod_sva_cd_10")
	public String getId_cod_sva_cd_10() {
		return id_cod_sva_cd_10;
	}
	@JsonProperty("id_cod_sva_cd_10")
	public void setId_cod_sva_cd_10(String id_cod_sva_cd_10) {
		this.id_cod_sva_cd_10 = id_cod_sva_cd_10;
	}
	@JsonProperty("cod_vta_app_cd")
	public String getCod_vta_app_cd() {
		return cod_vta_app_cd;
	}
	@JsonProperty("cod_vta_app_cd")
	public void setCod_vta_app_cd(String cod_vta_app_cd) {
		this.cod_vta_app_cd = cod_vta_app_cd;
	}
	@JsonProperty("fec_vta_ff")
	public String getFec_vta_ff() {
		return fec_vta_ff;
	}
	@JsonProperty("fec_vta_ff")
	public void setFec_vta_ff(String fec_vta_ff) {
		this.fec_vta_ff = fec_vta_ff;
	}
	@JsonProperty("num_ide_tlf")
	public String getNum_ide_tlf() {
		return num_ide_tlf;
	}
	@JsonProperty("num_ide_tlf")
	public void setNum_ide_tlf(String num_ide_tlf) {
		this.num_ide_tlf = num_ide_tlf;
	}
	@JsonProperty("cod_cli_ext_cd")
	public String getCod_cli_ext_cd() {
		return cod_cli_ext_cd;
	}
	@JsonProperty("cod_cli_ext_cd")
	public void setCod_cli_ext_cd(String cod_cli_ext_cd) {
		this.cod_cli_ext_cd = cod_cli_ext_cd;
	}
	@JsonProperty("tip_trx_cmr_cd")
	public String getTip_trx_cmr_cd() {
		return tip_trx_cmr_cd;
	}
	@JsonProperty("tip_trx_cmr_cd")
	public void setTip_trx_cmr_cd(String tip_trx_cmr_cd) {
		this.tip_trx_cmr_cd = tip_trx_cmr_cd;
	}
	@JsonProperty("cod_srv_cms_cd")
	public String getCod_srv_cms_cd() {
		return cod_srv_cms_cd;
	}
	@JsonProperty("cod_srv_cms_cd")
	public void setCod_srv_cms_cd(String cod_srv_cms_cd) {
		this.cod_srv_cms_cd = cod_srv_cms_cd;
	}
	@JsonProperty("fec_nac_ff")
	public String getFec_nac_ff() {
		return fec_nac_ff;
	}
	@JsonProperty("fec_nac_ff")
	public void setFec_nac_ff(String fec_nac_ff) {
		this.fec_nac_ff = fec_nac_ff;
	}
	@JsonProperty("cod_sex_cd")
	public String getCod_sex_cd() {
		return cod_sex_cd;
	}
	@JsonProperty("cod_sex_cd")
	public void setCod_sex_cd(String cod_sex_cd) {
		this.cod_sex_cd = cod_sex_cd;
	}
	@JsonProperty("cod_est_civ_cd")
	public String getCod_est_civ_cd() {
		return cod_est_civ_cd;
	}
	@JsonProperty("cod_est_civ_cd")
	public void setCod_est_civ_cd(String cod_est_civ_cd) {
		this.cod_est_civ_cd = cod_est_civ_cd;
	}
	@JsonProperty("des_mai_cli_ds")
	public String getDes_mai_cli_ds() {
		return des_mai_cli_ds;
	}
	@JsonProperty("des_mai_cli_ds")
	public void setDes_mai_cli_ds(String des_mai_cli_ds) {
		this.des_mai_cli_ds = des_mai_cli_ds;
	}
	@JsonProperty("cod_are_te1_cd")
	public String getCod_are_te1_cd() {
		return cod_are_te1_cd;
	}
	@JsonProperty("cod_are_te1_cd")
	public void setCod_are_te1_cd(String cod_are_te1_cd) {
		this.cod_are_te1_cd = cod_are_te1_cd;
	}
	@JsonProperty("cod_are_te2_cd")
	public String getCod_are_te2_cd() {
		return cod_are_te2_cd;
	}
	@JsonProperty("cod_are_te2_cd")
	public void setCod_are_te2_cd(String cod_are_te2_cd) {
		this.cod_are_te2_cd = cod_are_te2_cd;
	}
	@JsonProperty("cod_cic_fac_cd")
	public String getCod_cic_fac_cd() {
		return cod_cic_fac_cd;
	}
	@JsonProperty("cod_cic_fac_cd")
	public void setCod_cic_fac_cd(String cod_cic_fac_cd) {
		this.cod_cic_fac_cd = cod_cic_fac_cd;
	}
	@JsonProperty("cod_dep_cd")
	public String getCod_dep_cd() {
		return cod_dep_cd;
	}
	@JsonProperty("cod_dep_cd")
	public void setCod_dep_cd(String cod_dep_cd) {
		this.cod_dep_cd = cod_dep_cd;
	}
	@JsonProperty("cod_prv_cd")
	public String getCod_prv_cd() {
		return cod_prv_cd;
	}
	@JsonProperty("cod_prv_cd")
	public void setCod_prv_cd(String cod_prv_cd) {
		this.cod_prv_cd = cod_prv_cd;
	}
	@JsonProperty("cod_dis_cd")
	public String getCod_dis_cd() {
		return cod_dis_cd;
	}
	@JsonProperty("cod_dis_cd")
	public void setCod_dis_cd(String cod_dis_cd) {
		this.cod_dis_cd = cod_dis_cd;
	}
	@JsonProperty("cod_fac_tec_cd")
	public String getCod_fac_tec_cd() {
		return cod_fac_tec_cd;
	}
	@JsonProperty("cod_fac_tec_cd")
	public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
		this.cod_fac_tec_cd = cod_fac_tec_cd;
	}
	@JsonProperty("num_cod_x_cd")
	public String getNum_cod_x_cd() {
		return num_cod_x_cd;
	}
	@JsonProperty("num_cod_x_cd")
	public void setNum_cod_x_cd(String num_cod_x_cd) {
		this.num_cod_x_cd = num_cod_x_cd;
	}
	@JsonProperty("num_cod_y_cd")
	public String getNum_cod_y_cd() {
		return num_cod_y_cd;
	}
	@JsonProperty("num_cod_y_cd")
	public void setNum_cod_y_cd(String num_cod_y_cd) {
		this.num_cod_y_cd = num_cod_y_cd;
	}
	@JsonProperty("cod_ven_cms_cd")
	public String getCod_ven_cms_cd() {
		return cod_ven_cms_cd;
	}
	@JsonProperty("cod_ven_cms_cd")
	public void setCod_ven_cms_cd(String cod_ven_cms_cd) {
		this.cod_ven_cms_cd = cod_ven_cms_cd;
	}
	@JsonProperty("cod_sve_cms_cd")
	public String getCod_sve_cms_cd() {
		return cod_sve_cms_cd;
	}
	@JsonProperty("cod_sve_cms_cd")
	public void setCod_sve_cms_cd(String cod_sve_cms_cd) {
		this.cod_sve_cms_cd = cod_sve_cms_cd;
	}
	@JsonProperty("nom_ven_ds")
	public String getNom_ven_ds() {
		return nom_ven_ds;
	}
	@JsonProperty("nom_ven_ds")
	public void setNom_ven_ds(String nom_ven_ds) {
		this.nom_ven_ds = nom_ven_ds;
	}
	@JsonProperty("ref_dir_ent_ds")
	public String getRef_dir_ent_ds() {
		return ref_dir_ent_ds;
	}
	@JsonProperty("ref_dir_ent_ds")
	public void setRef_dir_ent_ds(String ref_dir_ent_ds) {
		this.ref_dir_ent_ds = ref_dir_ent_ds;
	}
	@JsonProperty("cod_tip_cal_cd")
	public String getCod_tip_cal_cd() {
		return cod_tip_cal_cd;
	}
	@JsonProperty("cod_tip_cal_cd")
	public void setCod_tip_cal_cd(String cod_tip_cal_cd) {
		this.cod_tip_cal_cd = cod_tip_cal_cd;
	}
	@JsonProperty("des_tip_cal_cd")
	public String getDes_tip_cal_cd() {
		return des_tip_cal_cd;
	}
	@JsonProperty("des_tip_cal_cd")
	public void setDes_tip_cal_cd(String des_tip_cal_cd) {
		this.des_tip_cal_cd = des_tip_cal_cd;
	}
	@JsonProperty("tip_mod_equ_cd")
	public String getTip_mod_equ_cd() {
		return tip_mod_equ_cd;
	}
	@JsonProperty("tip_mod_equ_cd")
	public void setTip_mod_equ_cd(String tip_mod_equ_cd) {
		this.tip_mod_equ_cd = tip_mod_equ_cd;
	}
	@JsonProperty("num_doc_ven_cd")
	public String getNum_doc_ven_cd() {
		return num_doc_ven_cd;
	}
	@JsonProperty("num_doc_ven_cd")
	public void setNum_doc_ven_cd(String num_doc_ven_cd) {
		this.num_doc_ven_cd = num_doc_ven_cd;
	}
	@JsonProperty("num_tel_ven_ds")
	public String getNum_tel_ven_ds() {
		return num_tel_ven_ds;
	}
	@JsonProperty("num_tel_ven_ds")
	public void setNum_tel_ven_ds(String num_tel_ven_ds) {
		this.num_tel_ven_ds = num_tel_ven_ds;
	}
	@JsonProperty("ind_env_con_cd")
	public String getInd_env_con_cd() {
		return ind_env_con_cd;
	}
	@JsonProperty("ind_env_con_cd")
	public void setInd_env_con_cd(String ind_env_con_cd) {
		this.ind_env_con_cd = ind_env_con_cd;
	}
	@JsonProperty("ind_id_gra_cd")
	public String getInd_id_gra_cd() {
		return ind_id_gra_cd;
	}
	@JsonProperty("ind_id_gra_cd")
	public void setInd_id_gra_cd(String ind_id_gra_cd) {
		this.ind_id_gra_cd = ind_id_gra_cd;
	}
	@JsonProperty("mod_ace_ven_cd")
	public String getMod_ace_ven_cd() {
		return mod_ace_ven_cd;
	}
	@JsonProperty("mod_ace_ven_cd")
	public void setMod_ace_ven_cd(String mod_ace_ven_cd) {
		this.mod_ace_ven_cd = mod_ace_ven_cd;
	}
	@JsonProperty("det_can_ven_ds")
	public String getDet_can_ven_ds() {
		return det_can_ven_ds;
	}
	@JsonProperty("det_can_ven_ds")
	public void setDet_can_ven_ds(String det_can_ven_ds) {
		this.det_can_ven_ds = det_can_ven_ds;
	}
	@JsonProperty("cod_reg_cd")
	public String getCod_reg_cd() {
		return cod_reg_cd;
	}
	@JsonProperty("cod_reg_cd")
	public void setCod_reg_cd(String cod_reg_cd) {
		this.cod_reg_cd = cod_reg_cd;
	}
	@JsonProperty("cod_cip_cd")
	public String getCod_cip_cd() {
		return cod_cip_cd;
	}
	@JsonProperty("cod_cip_cd")
	public void setCod_cip_cd(String cod_cip_cd) {
		this.cod_cip_cd = cod_cip_cd;
	}
	@JsonProperty("cod_exp_cd")
	public String getCod_exp_cd() {
		return cod_exp_cd;
	}
	@JsonProperty("cod_exp_cd")
	public void setCod_exp_cd(String cod_exp_cd) {
		this.cod_exp_cd = cod_exp_cd;
	}
	@JsonProperty("cod_zon_ven_cd")
	public String getCod_zon_ven_cd() {
		return cod_zon_ven_cd;
	}
	@JsonProperty("cod_zon_ven_cd")
	public void setCod_zon_ven_cd(String cod_zon_ven_cd) {
		this.cod_zon_ven_cd = cod_zon_ven_cd;
	}
	@JsonProperty("ind_web_par_cd")
	public String getInd_web_par_cd() {
		return ind_web_par_cd;
	}
	@JsonProperty("ind_web_par_cd")
	public void setInd_web_par_cd(String ind_web_par_cd) {
		this.ind_web_par_cd = ind_web_par_cd;
	}
	@JsonProperty("cod_stc")
	public String getCod_stc() {
		return cod_stc;
	}
	@JsonProperty("cod_stc")
	public void setCod_stc(String cod_stc) {
		this.cod_stc = cod_stc;
	}
	@JsonProperty("des_nac_ds")
	public String getDes_nac_ds() {
		return des_nac_ds;
	}
	@JsonProperty("des_nac_ds")
	public void setDes_nac_ds(String des_nac_ds) {
		this.des_nac_ds = des_nac_ds;
	}
	@JsonProperty("cod_mod_ven_cd")
	public String getCod_mod_ven_cd() {
		return cod_mod_ven_cd;
	}
	@JsonProperty("cod_mod_ven_cd")
	public void setCod_mod_ven_cd(String cod_mod_ven_cd) {
		this.cod_mod_ven_cd = cod_mod_ven_cd;
	}
	@JsonProperty("can_equ_ven")
	public String getCan_equ_ven() {
		return can_equ_ven;
	}
	@JsonProperty("can_equ_ven")
	public void setCan_equ_ven(String can_equ_ven) {
		this.can_equ_ven = can_equ_ven;
	}
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	@JsonAnySetter
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	
}
