package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tdp.mt.backend.movistartotal.main.entity.Automatizador;

@Repository
public interface AutomatizadorRepository extends JpaRepository<Automatizador, Integer>{

}
