package tdp.mt.backend.movistartotal.main.restclient.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfoClienteWS {

    @JsonProperty("sistema")
    private String sistema;
    @JsonProperty("usuario")
    private String usuario;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The sistema
     */
    @JsonProperty("sistema")
    public String getSistema() {
        return sistema;
    }

    /**
     * 
     * @param sistema
     *     The sistema
     */
    @JsonProperty("sistema")
    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    /**
     * 
     * @return
     *     The usuario
     */
    @JsonProperty("usuario")
    public String getUsuario() {
        return usuario;
    }

    /**
     * 
     * @param usuario
     *     The usuario
     */
    @JsonProperty("usuario")
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
