package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "tdp_automatizador", schema = "ibmx_a07e6d02edaf552")
@SequenceGenerator(name = "tdpautomatizadorseq", sequenceName = "ibmx_a07e6d02edaf552.tdp_automatizador_seq", catalog = "ibmx_a07e6d02edaf552", schema = "ibmx_a07e6d02edaf552", allocationSize = 1)
public class Automatizador {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tdpautomatizadorseq")
	private Integer id;

	@Column(name = "codvtappcd")
	private String codvtappcd;

	@Column(name = "coderrcd")
	private String coderrcd;

	@Column(name = "deserrds")
	private String deserrds;

	@Column(name = "fechorrecwa")
	// @Convert(converter = LocalDateTimeConverter.class)
	private Date fechorrecwa;

	@Column(name = "codestvtaapp")
	private String codestvtaapp;

	@Column(name = "desestvtaapp")
	private String desestvtaapp;

	@Column(name = "codpedaticd")
	private String codpedaticd;

	@Column(name = "codreqeedcd")
	private String codreqeedcd;

	@Column(name = "codestpedcd")
	private String codestpedcd;

	@Column(name = "desestpedcd")
	private String desestpedcd;

	@Column(name = "fechorreg")
	private String fechorreg;

	@Column(name = "fechoremiped")
	private String fechoremiped;

	@Column(name = "codclicd")
	private String codclicd;

	@Column(name = "codctacd")
	private String codctacd;

	@Column(name = "codinslegcd")
	private String codinslegcd;

	@Column(name = "numidtlf")
	private String numidtlf;

	@Column(name = "codclicms")
	private String codclicms;

	@Column(name = "codctacms")
	private String codctacms;

	@Column(name = "desobsvtaapp")
	private String desobsvtaapp;

	@Column(name = "codidwebex1")
	private String codidwebex1;

	@Column(name = "desidwebex1")
	private String desidwebex1;

	@Column(name = "codestwebex1")
	private String codestwebex1;

	@Column(name = "desestwebex1")
	private String desestwebex1;

	@Column(name = "codidwebex2")
	private String codidwebex2;

	@Column(name = "desidwebex2")
	private String desidwebex2;

	@Column(name = "codestwebex2")
	private String codestwebex2;

	@Column(name = "desestwebex2")
	private String desestwebex2;

	@Column(name = "codidwebex3")
	private String codidwebex3;

	@Column(name = "desidwebex3")
	private String desidwebex3;

	@Column(name = "codestwebex3")
	private String codestwebex3;

	@Column(name = "desestwebex3")
	private String desestwebex3;

	@Column(name = "codidwebex4")
	private String codidwebex4;

	@Column(name = "desidwebex4")
	private String desidwebex4;

	@Column(name = "codestwebex4")
	private String codestwebex4;

	@Column(name = "desestwebex4")
	private String desestwebex4;

	@Column(name = "codidwebex5")
	private String codidwebex5;

	@Column(name = "desidwebex5")
	private String desidwebex5;

	@Column(name = "codestwebex5")
	private String codestwebex5;

	@Column(name = "desestwebex5")
	private String desestwebex5;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodvtappcd() {
		return codvtappcd;
	}

	public void setCodvtappcd(String codvtappcd) {
		this.codvtappcd = codvtappcd;
	}

	public String getCoderrcd() {
		return coderrcd;
	}

	public void setCoderrcd(String coderrcd) {
		this.coderrcd = coderrcd;
	}

	public String getDeserrds() {
		return deserrds;
	}

	public void setDeserrds(String deserrds) {
		this.deserrds = deserrds;
	}

	public Date getFechorrecwa() {
		return fechorrecwa;
	}

	public void setFechorrecwa(Date fechorrecwa) {
		this.fechorrecwa = fechorrecwa;
	}

	public String getCodestvtaapp() {
		return codestvtaapp;
	}

	public void setCodestvtaapp(String codestvtaapp) {
		this.codestvtaapp = codestvtaapp;
	}

	public String getDesestvtaapp() {
		return desestvtaapp;
	}

	public void setDesestvtaapp(String desestvtaapp) {
		this.desestvtaapp = desestvtaapp;
	}

	public String getCodpedaticd() {
		return codpedaticd;
	}

	public void setCodpedaticd(String codpedaticd) {
		this.codpedaticd = codpedaticd;
	}

	public String getCodreqeedcd() {
		return codreqeedcd;
	}

	public void setCodreqeedcd(String codreqeedcd) {
		this.codreqeedcd = codreqeedcd;
	}

	public String getCodestpedcd() {
		return codestpedcd;
	}

	public void setCodestpedcd(String codestpedcd) {
		this.codestpedcd = codestpedcd;
	}

	public String getDesestpedcd() {
		return desestpedcd;
	}

	public void setDesestpedcd(String desestpedcd) {
		this.desestpedcd = desestpedcd;
	}

	public String getFechorreg() {
		return fechorreg;
	}

	public void setFechorreg(String fechorreg) {
		this.fechorreg = fechorreg;
	}

	public String getFechoremiped() {
		return fechoremiped;
	}

	public void setFechoremiped(String fechoremiped) {
		this.fechoremiped = fechoremiped;
	}

	public String getCodclicd() {
		return codclicd;
	}

	public void setCodclicd(String codclicd) {
		this.codclicd = codclicd;
	}

	public String getCodctacd() {
		return codctacd;
	}

	public void setCodctacd(String codctacd) {
		this.codctacd = codctacd;
	}

	public String getCodinslegcd() {
		return codinslegcd;
	}

	public void setCodinslegcd(String codinslegcd) {
		this.codinslegcd = codinslegcd;
	}

	public String getNumidtlf() {
		return numidtlf;
	}

	public void setNumidtlf(String numidtlf) {
		this.numidtlf = numidtlf;
	}

	public String getCodclicms() {
		return codclicms;
	}

	public void setCodclicms(String codclicms) {
		this.codclicms = codclicms;
	}

	public String getCodctacms() {
		return codctacms;
	}

	public void setCodctacms(String codctacms) {
		this.codctacms = codctacms;
	}

	public String getDesobsvtaapp() {
		return desobsvtaapp;
	}

	public void setDesobsvtaapp(String desobsvtaapp) {
		this.desobsvtaapp = desobsvtaapp;
	}

	public String getCodidwebex1() {
		return codidwebex1;
	}

	public void setCodidwebex1(String codidwebex1) {
		this.codidwebex1 = codidwebex1;
	}

	public String getDesidwebex1() {
		return desidwebex1;
	}

	public void setDesidwebex1(String desidwebex1) {
		this.desidwebex1 = desidwebex1;
	}

	public String getCodestwebex1() {
		return codestwebex1;
	}

	public void setCodestwebex1(String codestwebex1) {
		this.codestwebex1 = codestwebex1;
	}

	public String getDesestwebex1() {
		return desestwebex1;
	}

	public void setDesestwebex1(String desestwebex1) {
		this.desestwebex1 = desestwebex1;
	}

	public String getCodidwebex2() {
		return codidwebex2;
	}

	public void setCodidwebex2(String codidwebex2) {
		this.codidwebex2 = codidwebex2;
	}

	public String getDesidwebex2() {
		return desidwebex2;
	}

	public void setDesidwebex2(String desidwebex2) {
		this.desidwebex2 = desidwebex2;
	}

	public String getCodestwebex2() {
		return codestwebex2;
	}

	public void setCodestwebex2(String codestwebex2) {
		this.codestwebex2 = codestwebex2;
	}

	public String getDesestwebex2() {
		return desestwebex2;
	}

	public void setDesestwebex2(String desestwebex2) {
		this.desestwebex2 = desestwebex2;
	}

	public String getCodidwebex3() {
		return codidwebex3;
	}

	public void setCodidwebex3(String codidwebex3) {
		this.codidwebex3 = codidwebex3;
	}

	public String getDesidwebex3() {
		return desidwebex3;
	}

	public void setDesidwebex3(String desidwebex3) {
		this.desidwebex3 = desidwebex3;
	}

	public String getCodestwebex3() {
		return codestwebex3;
	}

	public void setCodestwebex3(String codestwebex3) {
		this.codestwebex3 = codestwebex3;
	}

	public String getDesestwebex3() {
		return desestwebex3;
	}

	public void setDesestwebex3(String desestwebex3) {
		this.desestwebex3 = desestwebex3;
	}

	public String getCodidwebex4() {
		return codidwebex4;
	}

	public void setCodidwebex4(String codidwebex4) {
		this.codidwebex4 = codidwebex4;
	}

	public String getDesidwebex4() {
		return desidwebex4;
	}

	public void setDesidwebex4(String desidwebex4) {
		this.desidwebex4 = desidwebex4;
	}

	public String getCodestwebex4() {
		return codestwebex4;
	}

	public void setCodestwebex4(String codestwebex4) {
		this.codestwebex4 = codestwebex4;
	}

	public String getDesestwebex4() {
		return desestwebex4;
	}

	public void setDesestwebex4(String desestwebex4) {
		this.desestwebex4 = desestwebex4;
	}

	public String getCodidwebex5() {
		return codidwebex5;
	}

	public void setCodidwebex5(String codidwebex5) {
		this.codidwebex5 = codidwebex5;
	}

	public String getDesidwebex5() {
		return desidwebex5;
	}

	public void setDesidwebex5(String desidwebex5) {
		this.desidwebex5 = desidwebex5;
	}

	public String getCodestwebex5() {
		return codestwebex5;
	}

	public void setCodestwebex5(String codestwebex5) {
		this.codestwebex5 = codestwebex5;
	}

	public String getDesestwebex5() {
		return desestwebex5;
	}

	public void setDesestwebex5(String desestwebex5) {
		this.desestwebex5 = desestwebex5;
	}
}
