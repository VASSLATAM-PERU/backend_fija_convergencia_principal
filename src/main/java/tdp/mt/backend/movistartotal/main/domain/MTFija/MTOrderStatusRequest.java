package tdp.mt.backend.movistartotal.main.domain.MTFija;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MTOrderStatusRequest {

    @JsonProperty("mtOrder")
    private Long mtOrder;

    public Long getMtOrder() {
        return mtOrder;
    }

    public void setMtOrder(Long mtOrder) {
        this.mtOrder = mtOrder;
    }
}
