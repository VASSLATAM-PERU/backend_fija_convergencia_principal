package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tdp.mt.backend.movistartotal.main.entity.TdpSalesAgent;

public interface TdpSalesAgentRepository extends JpaRepository<TdpSalesAgent, String> {

    TdpSalesAgent findOneByCodigoAtis(String codigoAtis);

}
