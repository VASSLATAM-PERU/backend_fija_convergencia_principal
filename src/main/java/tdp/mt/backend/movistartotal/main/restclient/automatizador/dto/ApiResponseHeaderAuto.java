package tdp.mt.backend.movistartotal.main.restclient.automatizador.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResponseHeaderAuto {
	@JsonProperty("originator")
	private String originator;
	@JsonProperty("destination")
	private String destination;
	@JsonProperty("pid")
	private String pid;
	@JsonProperty("execId")
	private String execId;
	@JsonProperty("msgId")
	private String msgId;
	@JsonProperty("timestamp")
	private String timestamp;
	@JsonProperty("msgType")
	private String msgType;
	
	public String getOriginator() {
		return originator;
	}
	public void setOriginator(String originator) {
		this.originator = originator;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
}
