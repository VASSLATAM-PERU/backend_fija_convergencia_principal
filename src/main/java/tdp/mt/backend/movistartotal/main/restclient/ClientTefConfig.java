package tdp.mt.backend.movistartotal.main.restclient;

public class ClientTefConfig {
	private String url;
	private String apiId;
	private String apiSecret;
	private String userLogin;
	private String serviceChannel;
	private String sessionCode;
	private String application;
	private String idMessage;
	private String ipAddress;
	private String functionalityCode;
	private String transactionTimestamp;
	private String serviceName;
	private String version;

	public ClientTefConfig() {
		super();
	}

	public ClientTefConfig(String userId,String masterService){
		super();
		this.userLogin = userId;
		this.functionalityCode = masterService;
	}

	public ClientTefConfig(String url, String apiId, String apiSecret, String userLogin, String serviceChannel, String sessionCode, String application,
						   String idMessage, String ipAddress, String functionalityCode, String transactionTimestamp, String serviceName, String version) {
		super();
		this.url = url; 
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.userLogin = userLogin;
		this.serviceChannel = serviceChannel;
		this.sessionCode = sessionCode;
		this.application = application;
		this.idMessage = idMessage;
		this.ipAddress = ipAddress;
		this.functionalityCode =functionalityCode;
		this.transactionTimestamp = transactionTimestamp;
		this.serviceName = serviceName;
		this.version = version;
	}

	
	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getApiId() {
		return apiId;
	}


	public void setApiId(String apiId) {
		this.apiId = apiId;
	}


	public String getApiSecret() {
		return apiSecret;
	}


	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}


	public String getUserLogin() {
		return userLogin;
	}


	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}


	public String getServiceChannel() {
		return serviceChannel;
	}


	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}


	public String getSessionCode() {
		return sessionCode;
	}


	public void setSessionCode(String sessionCode) {
		this.sessionCode = sessionCode;
	}


	public String getApplication() {
		return application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getIdMessage() {
		return idMessage;
	}


	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getFunctionalityCode() {
		return functionalityCode;
	}


	public void setFunctionalityCode(String functionalityCode) {
		this.functionalityCode = functionalityCode;
	}


	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}


	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}


	public String getServiceName() {
		return serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public static class ClientTefConfigBuilder {
		private String url;
		private String apiId;
		private String apiSecret;
		private String userLogin;
		private String serviceChannel;
		private String sessionCode;
		private String application;
		private String idMessage;
		private String ipAddress;
		private String functionalityCode;
		private String transactionTimestamp;
		private String serviceName;
		private String version;
		
		public ClientTefConfigBuilder() {
			super();
		}

		public ClientTefConfigBuilder setUrl(String url) {
			this.url = url;
			return this;
		}

		public ClientTefConfigBuilder setApiId(String apiId) {
			this.apiId = apiId;
			return this;
		}

		public ClientTefConfigBuilder setApiSecret(String apiSecret) {
			this.apiSecret = apiSecret;
			return this;
		}

		public ClientTefConfigBuilder setUserLogin(String userLogin) {
			this.userLogin = userLogin;
			return this;
		}

		public ClientTefConfigBuilder setServiceChannel(String serviceChannel) {
			this.serviceChannel = serviceChannel;
			return this;
		}
		
		public ClientTefConfigBuilder setSessionCode(String sessionCode) {
			this.sessionCode = sessionCode;
			return this;
		}
		
		public ClientTefConfigBuilder setApplication(String application) {
			this.application = application;
			return this;
		}
		
		public ClientTefConfigBuilder setIdMessage(String idMessage) {
			this.idMessage = idMessage;
			return this;
		}
		
		public ClientTefConfigBuilder setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
			return this;
		}
		
		public ClientTefConfigBuilder setFunctionalityCode(String functionalityCode) {
			this.functionalityCode = functionalityCode;
			return this;
		}
		
		public ClientTefConfigBuilder setTransactionTimestamp(String transactionTimestamp) {
			this.transactionTimestamp = transactionTimestamp;
			return this;
		}
		
		public ClientTefConfigBuilder setServiceName(String serviceName) {
			this.serviceName = serviceName;
			return this;
		}
		
		public ClientTefConfigBuilder setVersion(String version) {
			this.version = version;
			return this;
		}
		
		public ClientTefConfig build () {
			return new ClientTefConfig(url, apiId, apiSecret, userLogin, serviceChannel, sessionCode, application, idMessage, ipAddress, functionalityCode, transactionTimestamp, serviceName, version);
		}
	}
}
