package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.entity.TdpOrder;

import java.util.Date;

@Repository
@Transactional
public interface TdpOrderRepository extends JpaRepository<TdpOrder, String> {

    TdpOrder findOneByOrderId(String orderId);

    @Modifying
    @Query("update TdpOrder tdpo set tdpo.addressPrincipal = ?1, tdpo.addressReferencia = ?2 where tdpo.orderId = ?3")
    void updateAddressTdpOrder(String direccion, String referencia, String order_id);

    @Modifying
    @Query("update TdpOrder tdpo set tdpo.automatizadorOK = ?1 where tdpo.orderId = ?2")
    void updateAutomatizadorOkTdpOrder(Integer ok, String order_id);

    @Modifying
    @Query("update TdpOrder tdpo set tdpo.fechaPagoCmsAtis = ?1 where tdpo.orderId = ?2")
    void updateTdpOrderFechaPagoCmsAtis(Date fechaPago, String order_id);

    @Modifying
    @Query("update TdpOrder tdpo set tdpo.clientTelefono1 = ?1 where tdpo.orderId = ?2")
    void updateTdpOrderTelefono(String telefono, String order_id);

    @Modifying
    @Query("update TdpOrder tdpo set tdpo.addressPrincipal = ?1 where tdpo.orderId = ?2")
    void updateTdpOrderAddress(String address, String order_id);

}
