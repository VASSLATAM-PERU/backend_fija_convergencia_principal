package tdp.mt.backend.movistartotal.main.dto;

import java.sql.Timestamp;

public class EndpointCallEvent {
    private Long id;
    private String tag;
    private Timestamp timeRequest;
    private Timestamp timeResponse;
    private String uri;
    private String request;
    private String response;
    private String message;
    private String result;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Timestamp getTimeRequest() {
        return timeRequest;
    }

    public void setTimeRequest(Timestamp timeRequest) {
        this.timeRequest = timeRequest;
    }

    public Timestamp getTimeResponse() {
        return timeResponse;
    }

    public void setTimeResponse(Timestamp timeResponse) {
        this.timeResponse = timeResponse;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
