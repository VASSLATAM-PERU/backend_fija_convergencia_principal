package tdp.mt.backend.movistartotal.main.domain.MTFija.mt;

import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.entity.Order;
import tdp.mt.backend.movistartotal.main.util.NumberUtils;

import java.math.BigDecimal;

public class OrderMTParser implements MTParser<Order> {

    public static final String PARQUE_ATIS = "ATIS";
    public static final String PARQUE_CMS = "CMS";


    @Override
    public Order parse(FijaHDECRequest mtOrder) {
        Order order = new Order();
//id,"
//productName cat,"
        if (mtOrder.getMtClientPackageReqs() != null) {

            if (mtOrder.getMtClientPackageReqs().size() > 0) {
//productName,"
                order.setProductName(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getSubProductoHDEC());
//commercialOperation,"
                order.setCommercialOperation(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getOperacionComercial());
//migrationPhone as telefono_migrar,"
                order.setMigrationPhone(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getServiceNumber());
//equipmentDecoType,"
                order.setEquipmentDecoType(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getEquipamientoDeco());
//serviceType as service_type,"
                order.setServiceType(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getFixedSystemType());
//productType,"
                order.setProductType(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getTipoProductoHDEC());//TODO:"TRIO"
//cmsCustomer,"
                order.setCmsCustomer(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getClienteCMS());
//cmsServiceCode,"
                order.setCmsServiceCode(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCodigoServicioCMS());
//coordinatex,"
                order.setCoordinateX(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoordinateX() != null ?
                        new BigDecimal(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoordinateX()) : null);
//coordinatey,"
                order.setCoordinateY(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoordinateY() != null ?
                        new BigDecimal(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoordinateY()) : null);

                order.setPaquetizacion(mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getPaquetizacion());

                if (mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage() != null) {
                    if (mtOrder.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage().equalsIgnoreCase("G")) {
                        order.setFlagFtth(true);
                    } else {
                        order.setFlagFtth(false);
                    }
                }
            }
        }
        //
//paymentMode,"
        order.setPaymentMode(mtOrder.getMetodoPago());
//ID as ID2,"
//campaign,"
        order.setCampaign(mtOrder.getCampaing());
//cashprice,"
        order.setCashPrice(new BigDecimal(mtOrder.getMontoContado() != null ? mtOrder.getMontoContado() : "0"));
//productcategory, "
        //order.setProductCategory();
//whatsapp, "
        order.setWhatsapp(NumberUtils.isNumber(mtOrder.getWhatsapp()) ? Integer.parseInt(mtOrder.getWhatsapp()) : null);//FIXME

        return order;
    }
}
