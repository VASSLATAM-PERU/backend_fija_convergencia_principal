package tdp.mt.backend.movistartotal.main.dao;

import java.util.Date;

public interface UserSessionDao {

    void invalidateOldTokens(String username, Date endDate);
}
