package tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.ClientException;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InsertResponseBody {

    @JsonProperty("transaccion")
    private Integer transaccion;

    @JsonProperty("error")
    private String error;

    @JsonProperty("ClientException")
    private ClientException ClientException;

    @JsonProperty("ServerException")
    private ApiResponseBodyFault serverException;

    @JsonProperty("transaccion")
    public Integer getTransaccion() {
        return transaccion;
    }

    @JsonProperty("transaccion")
    public void setTransaccion(Integer transaccion) {
        this.transaccion = transaccion;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    @JsonProperty("ClientException")
    public ClientException getClientException() {
        return ClientException;
    }

    @JsonProperty("ClientException")
    public void setClientException(ClientException clientException) {
        ClientException = clientException;
    }

    public ApiResponseBodyFault getServerException() {
        return serverException;
    }

    public void setServerException(ApiResponseBodyFault serverException) {
        this.serverException = serverException;
    }
}