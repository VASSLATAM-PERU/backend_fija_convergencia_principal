package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.main.entity.AutomatizadorSalesService;

@Repository
public interface AutomatizadorRequestSaleService extends JpaRepository<AutomatizadorSalesService, Integer> {

    AutomatizadorSalesService findByCodvtappcd(String codvtappcd);

}