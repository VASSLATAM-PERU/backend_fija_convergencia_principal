package tdp.mt.backend.movistartotal.main.restclient.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.fault.SoapError;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseTef extends SoapError {
    
	@JsonProperty("TefHeaderRes")
    private ApiResponseTefHeader TefHeaderRes;

	@JsonProperty("TefHeaderRes")
	public ApiResponseTefHeader getTefHeaderRes() {
		return TefHeaderRes;
	}
	public void setTefHeaderRes(ApiResponseTefHeader tefHeaderRes) {
		TefHeaderRes = tefHeaderRes;
	}
}

