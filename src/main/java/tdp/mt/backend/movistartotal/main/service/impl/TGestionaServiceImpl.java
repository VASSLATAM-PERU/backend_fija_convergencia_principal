package tdp.mt.backend.movistartotal.main.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.commonms.common.util.NumberUtils;
import tdp.mt.backend.movistartotal.commonms.common.util.StringUtils;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.domain.MTFija.mt.*;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.entity.*;
import tdp.mt.backend.movistartotal.main.repository.*;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder.Data;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder.InsertOrderClient;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder.InsertRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder.InsertResponseBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaResponseBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.RegistrarPreventaClient;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.RegistrarPreventaClientErrorSimulator;
import tdp.mt.backend.movistartotal.main.service.ParametersService;
import tdp.mt.backend.movistartotal.main.service.TGestionaService;
import tdp.mt.backend.movistartotal.main.util.MotorConstants;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TGestionaServiceImpl implements TGestionaService {

    private static final Logger logger = LogManager.getLogger();
    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;

    @Autowired
    private TdpOrderRepository tdpOrderRepository;

    @Autowired
    private ParametersService parametersService;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    @Autowired
    private OrdenMTRepository ordenMTRepository;

    @Autowired
    private MTAzureFileStorageRepository mtAzureFileStorageRepository;

    @Autowired
    private TdpVisorRepository tdpVisorRepository;

    @Value("${mt.campania.diamovistarplanta}")
    private String diaMovistarPlanta;
    @Value("${mt.campania.diamovistar}")
    private String diaMovistar;


    private String HDEC_DEFAULT_EMAIL = "nulo@nulo.com";
    private String HDEC_DEFAULT_TELF_CONTACTO_2 = "11111111";
    private String HDEC_DEFAULT_TELF_MIGRAR = "11111111";
    private String HDEC_DEFAULT_CLIENTE_CMS = "1111111";
    private String HDEC_DEFAULT_COD_SRV_CMS = "1111111";

    private String HDEC_DEFAULT_0 = "0";
    private String HDEC_DEFAULT_NO_APLICA = "NO APLICA";
    private String HDEC_DEFAULT_EMPTY_STRING = "";
    private String HDEC_DEFAULT_EXPERTO_CODE_NA = "N/A";

    //lógica para concatenar sva
    private String adicionalDecoSmart="";
    private String decosHd="0";
    private String decosDvr="0";
    private String bloqueTv="NO APLICA";
    //svaInternet ya existe
    private String svaLinea = "NO APLICA";
    private int repetidorSmartWifi=0;

    @Override
    public Response<FijaHDECResponse> saveAndSendTGestiona(FijaHDECRequest request, String codigoAplicacion) {
        logger.info("TGestionaService => entro metodo [saveAndSendTGestiona]");
        Response<FijaHDECResponse> response = new Response<>();
        //Variables
        String orderId;
        try {

            TdpSalesAgent vendedor = tdpSalesAgentRepository.findOneByCodigoAtis(request.getSellerAtisCode());
            if (vendedor == null) {
                response.setResponseCode(ServiceConstants.SERVICE_ERROR);
                response.setResponseMessage("No se encuentra infomacion del vendedor " + request.getSellerAtisCode());
                return response;
            }

            Order o = saveOrderMT(request, codigoAplicacion);
            orderId = o.getId();

            FijaHDECResponse fijaHDECResponse = sendTGestionaInsertOrder(orderId);

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = df.format(new Date());

            boolean isRetail = vendedor.getCanalEquivalenciaCampania().equals(ServiceConstants.USER_CANAL_RETAIL);
            boolean isOrigen = ServiceConstants.USER_CANAL_CALL.contains(vendedor.getCanalEquivalenciaCampania());//request.getOrigen().equals(ServiceConstants.MT_CALL);

            String TGESTIONA_ENVIO = (request.getFlag() == null ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag().equals("") ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag());

            if (TGESTIONA_ENVIO.equals(ServiceConstants.TGESTIONA_1ER_ENVIO)) {
                orderRepository.insertTdpVisor(orderId, fijaHDECResponse.getEstadoTGestiona(), strDate);
                if (isOrigen) {
                    mtAzureFileStorageRepository.insertMTAzureFileStorage(orderId, fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_GSM);
                }

                if (isRetail) {
                    ordenMTRepository.insertMtOrderRetail(request.getMtOrden(), orderId, true, ServiceConstants.MODE_RETAIL);
                    mtAzureFileStorageRepository.insertMTAzureFileStorageRetail(orderId + ServiceConstants.CUSTODIA_PDF, fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_PDF, orderId);
                    mtAzureFileStorageRepository.insertMTAzureFileStorageRetail(orderId + ServiceConstants.CUSTODIA_PNG, fijaHDECResponse.getEstadoTGestiona(), ServiceConstants.NOT_NULL, request.getFileDate(), ServiceConstants.EXTENSION_PNG, orderId);
                } else {
                    ordenMTRepository.insertMtOrder(request.getMtOrden(), orderId, request.getMtAMDOCSMovil1(), request.getMtAMDOCSMovil2(), isOrigen);
                }

                orderRepository.updatesStatusLegacyOrder(fijaHDECResponse.getEstadoTGestiona(), orderId);
            }

            response.setResponseCode(fijaHDECResponse.getCode());
            response.setResponseMessage(fijaHDECResponse.getMessage());
            response.setResponseData(fijaHDECResponse);
        } catch (Exception e) {
            logger.error("Error al guardar saveAndSendTGestiona", e);
        }
        return response;
    }

    public Order saveOrderMT(FijaHDECRequest request, String codigoAplicacion) throws Exception {
        logger.info("OrderMTService => entro metodo [saveOrderMT]");

        DateTime dt = new DateTime(new Date());
        DateTime dtus = dt.withZone(dtZone);

        Customer customer = saveCustomer(request);

        MTParser<Order> parserOrder = new OrderMTParser();
        Order order = parserOrder.parse(request);
        order.setCustomer(customer);
        order.setId(request.getOrderId());
        order.setAppcode(codigoAplicacion);
        order.setRegisteredTime(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        order.setRegistrationDate(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        order.setServiceType("ATIS");
        //Adicionales InsertOrder (new tgestiona)
        //Solo si MIGRA o tiene EQUIPO (moviles)
        order.setMtOperacionComercial1("?");
        order.setMtIncluyeEquipo1("?");
        order.setMtTelefono1("?");
        order.setMtNumeroOrden1("?");
        order.setMtOperacionComercial2("?");
        order.setMtIncluyeEquipo2("?");
        order.setMtTelefono2("?");
        order.setMtNumeroOrden2("?");
        order.setMtEstadoMovil1("?");
        order.setMtEstadoMovil2("?");
        if (UtilMethods.validString(request.getOperacionComercial1()) && UtilMethods.validString(request.getIncluyeEquipo1())) {
            if (!request.getOperacionComercial1().equals(ServiceConstants.MT_SAME_OFFER_ACTION) || !request.getIncluyeEquipo1().equals("SI")) {
                order.setMtOperacionComercial1(request.getOperacionComercial1());
                order.setMtIncluyeEquipo1(request.getIncluyeEquipo1());
                order.setMtTelefono1(request.getTelefono1());
                order.setMtNumeroOrden1((request.getMtAMDOCSMovil1() == null ? "" : request.getMtAMDOCSMovil1()).equals("") ? "?" : request.getMtAMDOCSMovil1().trim());
                order.setMtEstadoMovil1((request.getEstadoMovil1() == null ? "?" : request.getEstadoMovil1().equals("") ? "?" : request.getEstadoMovil1()));
            }
        }
        if (UtilMethods.validString(request.getOperacionComercial2()) && UtilMethods.validString(request.getIncluyeEquipo2())) {
            if (!request.getOperacionComercial2().equals(ServiceConstants.MT_SAME_OFFER_ACTION) || !request.getIncluyeEquipo2().equals("SI")) {
                order.setMtOperacionComercial2(request.getOperacionComercial2());
                order.setMtIncluyeEquipo2(request.getIncluyeEquipo2());
                order.setMtTelefono2(request.getTelefono2());
                order.setMtNumeroOrden2((request.getMtAMDOCSMovil2() == null ? "" : request.getMtAMDOCSMovil2()).equals("") ? "?" : request.getMtAMDOCSMovil2().trim());
                order.setMtEstadoMovil2((request.getEstadoMovil2() == null ? "?" : request.getEstadoMovil2().equals("") ? "?" : request.getEstadoMovil2()));
            }
        }

        order.setMtPlanMovil(UtilMethods.validString(request.getPlanMovil()) ? request.getPlanMovil() : "?");
        order.setMtFlag(request.getFlag() == null ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag().equals("") ? ServiceConstants.TGESTIONA_1ER_ENVIO : request.getFlag());

        User u = new User();
        u.setId(request.getSellerAtisCode());
        order.setUser(u);
        order.setCampaign(request.getCampaing());

        //order.setCampaign(request.getCampania());
        //order.setCommercialOperation(request.getOperacionComercial());

        orderRepository.save(order);

        MTParser<OrderDetail> parserOrderDetail = new OrderDetailMTParser();
        OrderDetail orderDetail = parserOrderDetail.parse(request);

        Long id = orderDetailRepository.findByOrderId(order.getId());
        orderDetail.setId(id);
        orderDetail.setRegisteredTime(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        orderDetail.setOrderId(order.getId());
        orderDetailRepository.save(orderDetail);

        MTParser<TdpSalesAgent> parserVendedor = new VendedorMTParser();
        TdpSalesAgent vendedor = parserVendedor.parse(request);
        if (vendedor != null) {
            if (tdpSalesAgentRepository.findOneByCodigoAtis(vendedor.getCodigoAtis()) != null) {
                logger.info("Vendedor si esta registrado");
            } else {
                logger.info("Vendedor no registrado");
            }
        }

        //PeticionPagoEfectivo ppe = parserPPE.parse(request);
        //generar Orden

        TdpOrder tdpOrder = new TdpOrder();
        tdpOrder.setOrderId(request.getOrderId());

        tdpOrder.setClientNumeroDoc(request.getClientDocNumber());
        tdpOrder.setClientTipoDoc(request.getClientDocType());
        tdpOrder.setClientEmail(request.getClientEmail());
        tdpOrder.setClientApellidoMaterno(customer.getLastName2());
        tdpOrder.setClientApellidoPaterno(customer.getLastName1());
        tdpOrder.setClientNombre(customer.getFirstName());
        tdpOrder.setClientTelefono1(customer.getCustomerPhone());
        tdpOrder.setClientTelefono2(customer.getCustomerPhone2());

        tdpOrder.setOrderOrigen("WEBMT");

        tdpOrder.setAffiliationDataProtection("No");
        tdpOrder.setAffiliationElectronicInvoice("Si");
        tdpOrder.setAffiliationDataProtection("No");
        tdpOrder.setDisaffiliationWhitePagesGuide("No");

        tdpOrder.setOrderOperationCommercial(order.getCommercialOperation());
        tdpOrder.setOrderOrigen(order.getAppcode());

        tdpOrder.setOrderGpsX(order.getCoordinateX());
        tdpOrder.setOrderGpsY(order.getCoordinateY());

        TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(u.getId());
        tdpOrder.setUserAtis(u.getId());
        tdpOrder.setUserCms(Integer.parseInt(tdpSalesAgent.getCodCms()));
        tdpOrder.setUserCanalCodigo(tdpSalesAgent.getUser_canal_codigo());
        tdpOrder.setUserNombre(tdpSalesAgent.getNombre() + " " + tdpSalesAgent.getApePaterno() + " " + tdpSalesAgent.getApeMaterno());
        tdpOrder.setUserDni(tdpSalesAgent.getDni());
        tdpOrder.setUserEntidad(tdpSalesAgent.getEntidad());
        tdpOrder.setUserZonal(tdpSalesAgent.getZonal());
        tdpOrder.setUserFuerzaVenta(tdpSalesAgent.getUserFuerzaVenta());
        tdpOrder.setUserRegion(tdpSalesAgent.getZona());

        tdpOrder.setAuditoriaCreate(UtilMethods.sumarRestarHorasFecha(new Date(), -5));
        tdpOrder.setAuditoriaModify(UtilMethods.sumarRestarHorasFecha(new Date(), -5));

        if(request.getMtOfferLog()!=null){
            if(request.getMtOfferLog().get(0)!=null){
                if(request.getMtOfferLog().get(0).getSvaList()!=null) {
                    if (request.getMtOfferLog().get(0).getSvaList().size() > 0) {
                        tdpOrder.setSvaCode1(request.getMtOfferLog().get(0).getSvaList().get(0).getSvaPsCode());
                        tdpOrder.setSvaNombre1(request.getMtOfferLog().get(0).getSvaList().get(0).getSvaName());
                        tdpOrder.setSvaCantidad1(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(0).getSold()));
                        tdpOrder.setSvaPrecio1(request.getMtOfferLog().get(0).getSvaList().get(0).getSvaPrice());
                        if (request.getMtOfferLog().get(0).getSvaList().size() > 1) {
                            tdpOrder.setSvaCode2(request.getMtOfferLog().get(0).getSvaList().get(1).getSvaPsCode());
                            tdpOrder.setSvaNombre2(request.getMtOfferLog().get(0).getSvaList().get(1).getSvaName());
                            tdpOrder.setSvaCantidad2(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(1).getSold()));
                            tdpOrder.setSvaPrecio2(request.getMtOfferLog().get(0).getSvaList().get(1).getSvaPrice());
                            if (request.getMtOfferLog().get(0).getSvaList().size() > 2) {
                                tdpOrder.setSvaCode3(request.getMtOfferLog().get(0).getSvaList().get(2).getSvaPsCode());
                                tdpOrder.setSvaNombre3(request.getMtOfferLog().get(0).getSvaList().get(2).getSvaName());
                                tdpOrder.setSvaCantidad3(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(2).getSold()));
                                tdpOrder.setSvaPrecio3(request.getMtOfferLog().get(0).getSvaList().get(2).getSvaPrice());
                                if (request.getMtOfferLog().get(0).getSvaList().size() > 3) {
                                    tdpOrder.setSvaCode4(request.getMtOfferLog().get(0).getSvaList().get(3).getSvaPsCode());
                                    tdpOrder.setSvaNombre4(request.getMtOfferLog().get(0).getSvaList().get(3).getSvaName());
                                    tdpOrder.setSvaCantidad4(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(3).getSold()));
                                    tdpOrder.setSvaPrecio4(request.getMtOfferLog().get(0).getSvaList().get(3).getSvaPrice());
                                    if (request.getMtOfferLog().get(0).getSvaList().size() > 4) {
                                        tdpOrder.setSvaCode5(request.getMtOfferLog().get(0).getSvaList().get(4).getSvaPsCode());
                                        tdpOrder.setSvaNombre5(request.getMtOfferLog().get(0).getSvaList().get(4).getSvaName());
                                        tdpOrder.setSvaCantidad5(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(4).getSold()));
                                        tdpOrder.setSvaPrecio5(request.getMtOfferLog().get(0).getSvaList().get(4).getSvaPrice());
                                        if (request.getMtOfferLog().get(0).getSvaList().size() > 5) {
                                            tdpOrder.setSvaCode6(request.getMtOfferLog().get(0).getSvaList().get(5).getSvaPsCode());
                                            tdpOrder.setSvaNombre6(request.getMtOfferLog().get(0).getSvaList().get(5).getSvaName());
                                            tdpOrder.setSvaCantidad6(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(5).getSold()));
                                            tdpOrder.setSvaPrecio6(request.getMtOfferLog().get(0).getSvaList().get(5).getSvaPrice());
                                            if (request.getMtOfferLog().get(0).getSvaList().size() > 6) {
                                                tdpOrder.setSvaCode7(request.getMtOfferLog().get(0).getSvaList().get(6).getSvaPsCode());
                                                tdpOrder.setSvaNombre7(request.getMtOfferLog().get(0).getSvaList().get(6).getSvaName());
                                                tdpOrder.setSvaCantidad7(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(6).getSold()));
                                                tdpOrder.setSvaPrecio7(request.getMtOfferLog().get(0).getSvaList().get(6).getSvaPrice());
                                                if (request.getMtOfferLog().get(0).getSvaList().size() > 7) {
                                                    tdpOrder.setSvaCode8(request.getMtOfferLog().get(0).getSvaList().get(7).getSvaPsCode());
                                                    tdpOrder.setSvaNombre8(request.getMtOfferLog().get(0).getSvaList().get(7).getSvaName());
                                                    tdpOrder.setSvaCantidad8(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(7).getSold()));
                                                    tdpOrder.setSvaPrecio8(request.getMtOfferLog().get(0).getSvaList().get(7).getSvaPrice());
                                                    if (request.getMtOfferLog().get(0).getSvaList().size() > 8) {
                                                        tdpOrder.setSvaCode9(request.getMtOfferLog().get(0).getSvaList().get(8).getSvaPsCode());
                                                        tdpOrder.setSvaNombre9(request.getMtOfferLog().get(0).getSvaList().get(8).getSvaName());
                                                        tdpOrder.setSvaCantidad9(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(8).getSold()));
                                                        tdpOrder.setSvaPrecio9(request.getMtOfferLog().get(0).getSvaList().get(8).getSvaPrice());
                                                        if (request.getMtOfferLog().get(0).getSvaList().size() > 9) {
                                                            tdpOrder.setSvaCode10(request.getMtOfferLog().get(0).getSvaList().get(9).getSvaPsCode());
                                                            tdpOrder.setSvaNombre10(request.getMtOfferLog().get(0).getSvaList().get(9).getSvaName());
                                                            tdpOrder.setSvaCantidad10(Integer.parseInt(request.getMtOfferLog().get(0).getSvaList().get(9).getSold()));
                                                            tdpOrder.setSvaPrecio10(request.getMtOfferLog().get(0).getSvaList().get(9).getSvaPrice());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        tdpOrderRepository.save(tdpOrder);
        return order;

    }

    private Customer saveCustomer(FijaHDECRequest request) throws Exception {

        MTParser<Customer> parserCustomer = new CustomerMTParser();
        Customer cust = parserCustomer.parse(request);

        Long duplicateCustomer = customerRepository.loadIdByDocTypeAndDocNumber(cust.getDocType(), cust.getDocNumber());
        cust.setId(duplicateCustomer);
        Random rand = new SecureRandom();
        cust.setSex(rand.nextInt(1) == 0 ? "M" : "F");

        String estadoCivilList = "SCVD";
        cust.setMaritalstatus(String.valueOf(estadoCivilList.charAt(rand.nextInt(estadoCivilList.length()))));

        customerRepository.save(cust);

        return cust;
    }

    @Override
    public FijaHDECResponse sendTGestionaRegistrarPreventa(String orderId) {
        FijaHDECResponse response = new FijaHDECResponse();
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            logger.info("sendTGestionaRegistrarPreventa => order: null");
            response.setCode(ServiceConstants.SERVICE_ERROR);
            response.setMessage("No existe registro de la venta.");
            return response;
        }
        List<Object[]> data = orderRepository.getDataTGestiona(orderId);
        PreventaRequestBody request = getArrayData(data);
        try {
            ApiResponse<PreventaResponseBody> responseRegistrarPreventa = sendRegistrarPreventa(request);
            //Validar Respuesta
            if (responseRegistrarPreventa.getHttpCode() != null) {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage("Error " + responseRegistrarPreventa.getHttpCode() + ": " + responseRegistrarPreventa.getMoreInformation());
                response.setEstadoTGestiona("ENVIANDO");
                return response;
            } else if (responseRegistrarPreventa.getBodyOut().getClientException() != null) {
                if (responseRegistrarPreventa.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage().equals("Error: CODIGO_UNICO ya existe => CODIGO_UNICO")) {
                    response.setCode(ServiceConstants.SERVICE_SUCCESS);
                    response.setMessage(responseRegistrarPreventa.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage());
                    response.setEstadoTGestiona("PENDIENTE");
                } else {
                    response.setCode(ServiceConstants.SERVICE_ERROR);
                    response.setMessage("Error " + responseRegistrarPreventa.getBodyOut().getClientException().getExceptionCode() + ": " + responseRegistrarPreventa.getBodyOut().getClientException().getExceptionDetail());
                    response.setEstadoTGestiona("CAIDA");
                    return response;
                }
            } else if (responseRegistrarPreventa.getBodyOut().getServerException() != null) {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage("Error " + responseRegistrarPreventa.getBodyOut().getServerException().getExceptionCode() + ": " + responseRegistrarPreventa.getBodyOut().getServerException().getExceptionDetail());
                response.setEstadoTGestiona("ENVIANDO");
                return response;
            } else {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage(responseRegistrarPreventa.getBodyOut().getMensaje());
                response.setEstadoTGestiona("PENDIENTE");
            }
        } catch (ClientException e) {
            logger.error(e.getMessage());
            response.setCode(ServiceConstants.SERVICE_ERROR);
            response.setMessage("ERROR DEL BACK");
            response.setEstadoTGestiona("CAIDA");
            return response;
        }
        return response;
    }

    private PreventaRequestBody getArrayData(List<Object[]> data) {
        PreventaRequestBody request = new PreventaRequestBody();
        for (Object[] row : data) {
            logger.info("tamaño del array " + row.length);
            String campaing = "";
            if (((Boolean) row[70])) {
                campaing = "EDIFICIOS CON FIBRA";
            } else {
                campaing = ((String) row[52]);
            }
            logger.info("SE ESTA OBTENIENDO" + row[70]);

            String orderId = (String) row[0];
            String firstName = row[1] != null ? (String) row[1] : "";
            String lastName1 = row[2] != null ? (String) row[2] : "";
            String lastName2 = row[3] != null ? (String) row[3] : "";

            // Sprint 7... si es que el apellido materno (lastName2) esta vacio entonces enviamos a HDEC un espacio en blanco
            if (lastName2 == null || lastName2.isEmpty()) {
                lastName2 = " ";
            }

            String docType = (String) row[4];
            String docNumber = (String) row[5];
            String email = (String) row[6];
            String sendContracts = (String) row[7];
            String dataProtection = (String) row[8];
            String canal = (String) row[9];
            String entidad = (String) row[10];
            String nombreVendedor = (String) row[11];
            String codATIS = (String) row[12];
            String codCMS = (String) row[13];
            String departamento = (String) row[14];
            String provincia = (String) row[15];
            String productCategory = (String) row[16];
            String productName = (String) row[17];
            String department = (String) row[18];
            String province = (String) row[19];
            String district = (String) row[20];
            String commercialOperation = (String) row[21];
            String telefonoMigrar = (String) row[22];
            String disableWhitepage = (String) row[23];
            String enableDigitalInvoice = (String) row[24];
            String internetTech = (String) row[25];
            String expertoCode = (String) row[26];
            String tieneMigracion = (String) row[27];
            Date registeredTime1 = (Date) row[28];
            // Date registeredTime2 = (Date) row[29]; no use
            String paymentMode = (String) row[30];
            String recordingId = (String) row[31];
            String tvTech = (String) row[32];
            String noResult = (String) row[33]; // no result
            String paquetizacion = (String) row[34];
            String equipmentDecoType = (String) row[35];
            String dniVendedor = (String) row[36];
            String telefonoOrigen = (String) row[37];
            String serviceType = (String) row[38];
            String distritoVendedor = (String) row[39];

            String decosSD = NumberUtils.isNullOrZero(row[40]) ? null : ((Integer) row[40]).toString();
            String decosHD = NumberUtils.isNullOrZero(row[41]) ? null : ((Integer) row[41]).toString();
            String decosDVR = NumberUtils.isNullOrZero(row[42]) ? null : ((Integer) row[42]).toString();

            String bloqueProducto = (String) row[43];
            String svaInternet = (String) row[44];
            String svaLine = (String) row[45];
            String descuentoWinback = (String) row[46];
            String tvBlock = (String) row[47]; // no use so far
            String blockTv = (String) row[48]; // no use so far
            String customerPhone = (String) row[49];
            String customerPhone2 = (String) row[50];
            String address = (String) row[51];

            String productType = (!commercialOperation.equals("SVAS") ? (String) row[53] : (productName.indexOf("PUNTO ADICIONAL") == 0 || productName.indexOf("EQUIPO") == 0 || productName.indexOf("MODEM") == 0 || productName.indexOf("SOLUCIONES") == 0 || productName.indexOf("ULTRA") == 0 ? "EQUIPOS" : "SVAS"));
            String cmsCustomer = (String) row[54];
            String cmsServiceCode = (String) row[55];
            String altasTv = (String) row[56];
            String coordenadasx = row[57] != null ? String.valueOf((BigDecimal) row[57]) : "";
            String coordenadasy = row[58] != null ? String.valueOf((BigDecimal) row[58]) : "";
            String webParental = (String) row[59];
            String montoContado = row[60] != null ? String.valueOf((BigDecimal) row[60]) : "";
            String coidgoPostal = (String) row[61];
            String publicarGuia = (String) row[62];
            String productcategory = (String) row[63];
            String orderOrigen = row[64] != null ? (String) row[64] : "";
            String clientNationality = row[65] != null ? (String) row[65] : "";
            Integer internetRsw = NumberUtils.isNullOrZero(row[66]) ? 0 : ((Integer) row[66]);
            String cod_CIP = row[67] != null ? (String) row[67] : "";
            Integer whatsapp = NumberUtils.isNullOrZero(row[68]) ? 0 : ((Integer) row[68]);

            // Afiliación al débito automático
            String automaticDebit = row[69] != null ? (String) row[69] : "";

            //Sprint 24 RUC
            String tipDocRrll = row[71] != null ? (String) row[71] : "";
            String docNumRrll = row[72] != null ? (String) row[72] : "";
            String nomRrll = row[73] != null ? (String) row[73] : "";

            //Sprint 24 RECUPERO CAIDA
            //String id_transaccion = row[74] != null ? (String) row[74] : "";
            //boolean flag_recupero = (boolean) row[75];

            address = address == null ? null : address.trim().substring(0, Math.min(199, address.trim().length()));


            request.setCodigoUnico(orderId);
            request.setCampana(campaing);
            request.setNombreDeCliente(firstName.trim().equalsIgnoreCase("") ? "-" : firstName);
            request.setApellidoPaterno(lastName1.trim().equalsIgnoreCase("") ? "-" : lastName1);
            request.setApellidoMaterno(lastName2.trim().equalsIgnoreCase("") ? "-" : lastName2);

            // Equivalencias de tipos documento en HDEC
            docType = parametersService.equivalenceDocument("HDEC", docType);
            request.setTipoDeDocumento(docType);
            request.setNumeroDeDocumento(docNumber);
            request.setTelefonoDeContacto1(customerPhone);//customerPhone
            request.setTelefonoDeContacto2(StringUtils.isEmptyString(customerPhone2) ? HDEC_DEFAULT_TELF_CONTACTO_2 : customerPhone2);
            request.setEmail(StringUtils.isEmptyString(email) ? HDEC_DEFAULT_EMAIL : email);
            request.setEnvioDeContratos(sendContracts);
            request.setProteccionDeDatos(dataProtection);
            request.setCanalDeVenta(StringUtils.isEmptyString(canal) ? HDEC_DEFAULT_NO_APLICA : canal);
            request.setDetalleDeCanal(StringUtils.isEmptyString(entidad) ? HDEC_DEFAULT_NO_APLICA : entidad);
            request.setNombreVendedor(StringUtils.isEmptyString(nombreVendedor) ? HDEC_DEFAULT_NO_APLICA : nombreVendedor);
            request.setCodVendedorAtis(codATIS);
            request.setCodVendedorCms(codCMS);
            request.setZonalDepartamentoVendedor(StringUtils.isEmptyString(departamento) ? HDEC_DEFAULT_NO_APLICA : departamento);
            request.setRegionProvinciaDistritoVendedor(StringUtils.isEmptyString(provincia) ? HDEC_DEFAULT_NO_APLICA : provincia);
            request.setTipoDeProducto(productType);
            request.setSubProducto(productName);

            /* Parche para Cerrar Ventas SVAS por HDEC */
            if (commercialOperation.equals("SVAS")) {
                if (productName.indexOf("PUNTO ADICIONAL") != 0) {
                    request.setSubProducto(productcategory);
                }
                if (internetRsw > 0) {
                    request.setSubProducto("REPETIDOR SMART WIFI");
                }
            }
            request.setDepartamento(StringUtils.isEmptyString(department) ? HDEC_DEFAULT_NO_APLICA : department);
            request.setProvincia(StringUtils.isEmptyString(province) ? HDEC_DEFAULT_NO_APLICA : province);
            request.setDistrito(StringUtils.isEmptyString(district) ? HDEC_DEFAULT_NO_APLICA : district);
            request.setDireccion(StringUtils.isEmptyString(address) ? HDEC_DEFAULT_NO_APLICA : address);
            request.setOperacionComercial(commercialOperation);

            request.setTelefonoAMigrar(StringUtils.isEmptyString(telefonoMigrar) ? HDEC_DEFAULT_TELF_MIGRAR : telefonoMigrar);

            //request.setDesafiliacionDePaginasBlancas(disableWhitepage);
            //request.setDesafiliacionDePaginasBlancas((whatsapp == 0 ? "MOTORIZADO" : (whatsapp == 1 ? "WHATSAPP" : "-")));
            request.setDesafiliacionDePaginasBlancas("MOTORIZADO");
            request.setAfiliacionAFacturaDigital(enableDigitalInvoice);
            request.setTecnologiaDeInternet(StringUtils.isEmptyString(internetTech) ? HDEC_DEFAULT_NO_APLICA : internetTech);
            request.setCodigoExperto(expertoCode == null || expertoCode.equalsIgnoreCase("") ? HDEC_DEFAULT_EXPERTO_CODE_NA : expertoCode);

            request.setTieneGrabacion(tieneMigracion);
            request.setFechaRegistro(new SimpleDateFormat("dd-MM-yyyy").format(registeredTime1));
            request.setHoraRegistroWeb(new SimpleDateFormat("HH:mm:ss").format(registeredTime1));
            request.setModalidadDePago(StringUtils.isEmptyString(paymentMode) ? HDEC_DEFAULT_NO_APLICA : paymentMode);
            request.setIdGrabacionNativo(recordingId);
            request.setTecnologiaTelevision(StringUtils.isEmptyString(tvTech) ? HDEC_DEFAULT_NO_APLICA : tvTech);
            request.setPaquetizacion(paquetizacion);
            request.setAltasTv(altasTv);

            request.setTipoEquipamientoDeco(StringUtils.isEmptyString(equipmentDecoType) ? HDEC_DEFAULT_NO_APLICA : equipmentDecoType);
            request.setDniVendedor(dniVendedor);
            request.setTelefonoOrigen(telefonoOrigen == null ? HDEC_DEFAULT_TELF_CONTACTO_2 : telefonoOrigen);

            //request.setTipoServicio(serviceType);
            request.setTipoServicio("ATIS");
            request.setClienteCms(StringUtils.isEmptyString(cmsCustomer) ? HDEC_DEFAULT_CLIENTE_CMS : cmsCustomer);
            request.setDistritoVendedor(StringUtils.isEmptyString(distritoVendedor) ? HDEC_DEFAULT_NO_APLICA : distritoVendedor);
            request.setDecosSd(StringUtils.isEmptyString(decosSD) ? HDEC_DEFAULT_0 : decosSD);
            request.setDecosHd(StringUtils.isEmptyString(decosHD) ? HDEC_DEFAULT_0 : decosHD);
            request.setDecosDvr(StringUtils.isEmptyString(decosDVR) ? HDEC_DEFAULT_0 : decosDVR);
            request.setBloqueTv(StringUtils.isEmptyString(blockTv) ? HDEC_DEFAULT_NO_APLICA : blockTv.substring(0, (blockTv.length() < 100 ? blockTv.length() : 100)));
            request.setSvaInternet(StringUtils.isEmptyString(svaInternet) ? HDEC_DEFAULT_EMPTY_STRING : svaInternet);
            request.setSvaLinea(StringUtils.isEmptyString(svaLine) ? HDEC_DEFAULT_NO_APLICA : svaLine);

            if (request.getModalidadDePago().equalsIgnoreCase("Contado")) {
                request.setDescuentoWinback(cod_CIP);
            } else {
                request.setDescuentoWinback("");
            }

            request.setCodigoDeServicioCms(StringUtils.isEmptyString(cmsServiceCode) ? HDEC_DEFAULT_COD_SRV_CMS : cmsServiceCode);
            request.setBloqueProducto(StringUtils.isEmptyString(tvBlock) ? HDEC_DEFAULT_NO_APLICA : tvBlock);

            request.setCoordenadasX(coordenadasx);
            request.setCoordenadasY(coordenadasy);
            request.setWebParental(webParental);
            request.setMontoContado(montoContado);
            request.setCodigoPostal(coidgoPostal);
            request.setPublicarGuia(automaticDebit);  // request.setPublicarGuia(publicarGuia);
            request.setRepetidorSmartWifi(internetRsw);
            request.setModoVenta(orderOrigen.equalsIgnoreCase("APPVF") ? "APP VENTA" : orderOrigen.equalsIgnoreCase("WEBMT") ? "WEB CONVERGENTE" : "WEB VENTA");
            request.setNacionalidad(clientNationality);

            //Sprint 24 RUC
            if (docType.equals("RUC") && docNumber.substring(0, 2).equals("20")) {
                request.setTipoDocumentoRrll(tipDocRrll);
                request.setNumeroDocumentoRrll(docNumRrll);
                request.setNombreCompletoRrll(nomRrll);
            } else {
                request.setTipoDocumentoRrll("");
                request.setNumeroDocumentoRrll("");
                request.setNombreCompletoRrll("");
            }

            //Sprint 24 RECUPERO CAIDA
            /*if(flag_recupero){
                request.setFlagRecupero(flag_recupero);
                request.setIdTransaccion(id_transaccion);
            }*/
        }
        return request;
    }

    private InsertRequestBody getArrayData2(List<Object[]> data) {
        adicionalDecoSmart="";
        bloqueTv="NO APLICA";
        repetidorSmartWifi=0;

        InsertRequestBody insertRequestBody = new InsertRequestBody();
        for (Object[] row : data) {
            logger.info("tamaño del array " + row.length);

            String campaing = "";

            campaing = (row[52] == null) ? ServiceConstants.TGESTIONA_CAMPANIA_MOVISTAR_TOTAL : ((String) row[52]);

            switch (campaing) {
                case ServiceConstants.TGESTIONA_CAMPANIA_DIAS_MOVISTAR:
                    campaing = diaMovistar;
                    break;
                case ServiceConstants.TGESTIONA_CAMPANIA_DIAS_MOVISTAR_PLANTA:
                    campaing = diaMovistarPlanta;
                    break;
            }

            logger.info("campaña: " + campaing);

            String orderId = (String) row[0];
            String firstName = row[1] != null ? (String) row[1] : "";
            String lastName1 = row[2] != null ? (String) row[2] : "";
            String lastName2 = row[3] != null ? (String) row[3] : "";

            // Sprint 7... si es que el apellido materno (lastName2) esta vacio entonces enviamos a HDEC un espacio en blanco
            if (lastName2 == null || lastName2.isEmpty()) {
                lastName2 = " ";
            }

            String docType = (String) row[4];
            String docNumber = (String) row[5];
            String email = (String) row[6];
            String sendContracts = (String) row[7];
            String dataProtection = (String) row[8];
            String canal = (String) row[9];
            String entidad = (String) row[10];
            String nombreVendedor = (String) row[11];
            String codATIS = (String) row[12];
            String codCMS = (String) row[13];
            String departamento = (String) row[14];
            String provincia = (String) row[15];
            String productCategory = (String) row[16];
            String productName = (String) row[17];
            String department = (String) row[18];
            String province = (String) row[19];
            String district = (String) row[20];
            String commercialOperation = (String) row[21];
            String telefonoMigrar = (String) row[22];
            String disableWhitepage = (String) row[23];
            String enableDigitalInvoice = (String) row[24];
            String internetTech = (String) row[25];
            String expertoCode = (String) row[26];
            String tieneMigracion = (String) row[27];
            Date registeredTime1 = (Date) row[28];
            // Date registeredTime2 = (Date) row[29]; no use
            String paymentMode = (String) row[30];
            String recordingId = (String) row[31];
            String tvTech = (String) row[32];
            String noResult = (String) row[33]; // no result
            String paquetizacion = (String) row[34];
            String equipmentDecoType = (String) row[35];
            String dniVendedor = (String) row[36];
            String telefonoOrigen = (String) row[37];
            String serviceType = (String) row[38];
            String distritoVendedor = (String) row[39];

            String decosSD = NumberUtils.isNullOrZero(row[40]) ? null : ((Integer) row[40]).toString();
            String decosHD = NumberUtils.isNullOrZero(row[41]) ? null : ((Integer) row[41]).toString();
            String decosDVR = NumberUtils.isNullOrZero(row[42]) ? null : ((Integer) row[42]).toString();

            String bloqueProducto = (String) row[43];
            String svaInternet = (String) row[44];
            String svaLine = (String) row[45];
            String descuentoWinback = (String) row[46];
            String tvBlock = (String) row[47]; // no use so far
            String blockTv = (String) row[48]; // no use so far
            String customerPhone = (String) row[49];
            String customerPhone2 = (String) row[50];
            String address = (String) row[51];

            String productType = (!commercialOperation.equals("SVAS") ? (String) row[53] : (productName.indexOf("PUNTO ADICIONAL") == 0 || productName.indexOf("EQUIPO") == 0 || productName.indexOf("MODEM") == 0 || productName.indexOf("SOLUCIONES") == 0 || productName.indexOf("ULTRA") == 0 ? "EQUIPOS" : "SVAS"));
            String cmsCustomer = (String) row[54];
            String cmsServiceCode = (String) row[55];
            String altasTv = (String) row[56];
            String coordenadasx = row[57] != null ? String.valueOf((BigDecimal) row[57]) : "";
            String coordenadasy = row[58] != null ? String.valueOf((BigDecimal) row[58]) : "";
            String webParental = (String) row[59];
            String montoContado = row[60] != null ? String.valueOf((BigDecimal) row[60]) : "";
            String coidgoPostal = (String) row[61];
            String publicarGuia = (String) row[62];
            String productcategory = (String) row[63];
            String orderOrigen = row[64] != null ? (String) row[64] : "";
            String clientNationality = row[65] != null ? (String) row[65] : "";
            Integer internetRsw = NumberUtils.isNullOrZero(row[66]) ? 0 : ((Integer) row[66]);
            String cod_CIP = row[67] != null ? (String) row[67] : "";
            Integer whatsapp = NumberUtils.isNullOrZero(row[68]) ? 0 : ((Integer) row[68]);

            // Afiliación al débito automático
            String automaticDebit = row[69] != null ? (String) row[69] : "";

            //Sprint 24 RUC
            String tipDocRrll = row[71] != null ? (String) row[71] : "";
            String docNumRrll = row[72] != null ? (String) row[72] : "";
            String nomRrll = row[73] != null ? (String) row[73] : "";

            //Sprint 24 RECUPERO CAIDA
            //String id_transaccion = row[74] != null ? (String) row[74] : "";
            //boolean flag_recupero = (boolean) row[75];

            address = address == null ? null : address.trim().substring(0, Math.min(199, address.trim().length()));

            String incluyeEquipo1 = row[76] != null ? (String) row[76] : "?";
            String incluyeEquipo2 = row[77] != null ? (String) row[77] : "?";
            String numeroOrden1 = row[78] != null ? (String) row[78] : "?";
            String numeroOrden2 = row[79] != null ? (String) row[79] : "?";
            String operacionComercial1 = row[80] != null ? (String) row[80] : "?";
            String operacionComercial2 = row[81] != null ? (String) row[81] : "?";
            String planMovil = row[82] != null ? (String) row[82] : "?";
            String telefono1 = row[83] != null ? (String) row[83] : "?";
            String telefono2 = row[84] != null ? (String) row[84] : "?";
            String estadoMovil1 = row[85] != null ? (String) row[85] : "?";
            String estadoMovil2 = row[86] != null ? (String) row[86] : "?";

            Integer flag = row[87] != null ? Integer.parseInt((String) row[87]) : Integer.parseInt(ServiceConstants.TGESTIONA_1ER_ENVIO);

            svaToRequest(row, 88,92);
            svaToRequest(row, 93,97);
            svaToRequest(row, 98,102);
            svaToRequest(row, 103,107);
            svaToRequest(row, 108,112);
            svaToRequest(row, 113,117);
            svaToRequest(row, 118,122);
            svaToRequest(row, 123,127);
            svaToRequest(row, 128,132);

            Data request = new Data();
            request.setCodigoUnico(orderId);
            request.setCampana(campaing);
            request.setNombreDeCliente(firstName.trim().equalsIgnoreCase("") ? "-" : firstName);
            request.setApellidoPaterno(lastName1.trim().equalsIgnoreCase("") ? "-" : lastName1);
            request.setApellidoMaterno(lastName2.trim().equalsIgnoreCase("") ? "-" : lastName2);

            // Equivalencias de tipos documento en HDEC
            docType = parametersService.equivalenceDocument("HDEC", docType);
            request.setTipoDeDocumento(docType);
            request.setNumeroDeDocumento(docNumber);
            request.setTelefonoDeContacto1(customerPhone);//customerPhone
            request.setTelefonoDeContacto2(StringUtils.isEmptyString(customerPhone2) ? HDEC_DEFAULT_TELF_CONTACTO_2 : customerPhone2);
            request.setEmail(StringUtils.isEmptyString(email) ? HDEC_DEFAULT_EMAIL : email);
            request.setEnvioDeContratos(sendContracts);
            request.setProteccionDeDatos(dataProtection);
            request.setCanalDeVenta(StringUtils.isEmptyString(canal) ? HDEC_DEFAULT_NO_APLICA : canal);
            request.setDetalleDeCanal(StringUtils.isEmptyString(entidad) ? HDEC_DEFAULT_NO_APLICA : entidad);
            request.setNombreVendedor(StringUtils.isEmptyString(nombreVendedor) ? HDEC_DEFAULT_NO_APLICA : nombreVendedor);
            request.setCodVendedorAtis(codATIS);
            request.setCodVendedorCms(codCMS);
            request.setZonalDepartamentoVendedor(StringUtils.isEmptyString(departamento) ? HDEC_DEFAULT_NO_APLICA : departamento);
            request.setRegionProvinciaDistritoVendedor(StringUtils.isEmptyString(provincia) ? HDEC_DEFAULT_NO_APLICA : provincia);
            request.setTipoDeProducto(productType);
            request.setSubProducto(productName);

            /* Parche para Cerrar Ventas SVAS por HDEC */
            if (commercialOperation.equals("SVAS")) {
                if (productName.indexOf("PUNTO ADICIONAL") != 0) {
                    request.setSubProducto(productcategory);
                }
                if (internetRsw > 0) {
                    request.setSubProducto("REPETIDOR SMART WIFI");
                }
            }
            request.setDepartamento(StringUtils.isEmptyString(department) ? HDEC_DEFAULT_NO_APLICA : department);
            request.setProvincia(StringUtils.isEmptyString(province) ? HDEC_DEFAULT_NO_APLICA : province);
            request.setDistrito(StringUtils.isEmptyString(district) ? HDEC_DEFAULT_NO_APLICA : district);
            request.setDireccion(StringUtils.isEmptyString(address) ? HDEC_DEFAULT_NO_APLICA : address);
            request.setOperacionComercial(commercialOperation);

            request.setTelefonoAMigrar(StringUtils.isEmptyString(telefonoMigrar) ? HDEC_DEFAULT_TELF_MIGRAR : telefonoMigrar);

            //request.setDesafiliacionDePaginasBlancas(disableWhitepage);
            //request.setDesafiliacionDePaginasBlancas((whatsapp == 0 ? "MOTORIZADO" : (whatsapp == 1 ? "WHATSAPP" : "-")));
            request.setDesafiliacionDePaginasBlancas("MOTORIZADO");
            request.setAfiliacionAFacturaDigital(enableDigitalInvoice);
            request.setTecnologiaDeInternet(StringUtils.isEmptyString(internetTech) ? HDEC_DEFAULT_NO_APLICA : internetTech);
            request.setCodigoExperto(expertoCode == null || expertoCode.equalsIgnoreCase("") ? HDEC_DEFAULT_EXPERTO_CODE_NA : expertoCode);

            request.setTieneGrabacion(tieneMigracion);
            request.setFechaRegistro(new SimpleDateFormat("dd-MM-yyyy").format(registeredTime1));
            request.setHoraRegistroWeb(new SimpleDateFormat("HH:mm:ss").format(registeredTime1));
            request.setModalidadDePago(StringUtils.isEmptyString(paymentMode) ? HDEC_DEFAULT_NO_APLICA : paymentMode);
            request.setIdGrabacionNativo(recordingId);
            request.setTecnologiaTelevision(StringUtils.isEmptyString(tvTech) ? HDEC_DEFAULT_NO_APLICA : tvTech);
            request.setPaquetizacion(paquetizacion);
            request.setAltasTv(altasTv);

            request.setTipoEquipamientoDeco(StringUtils.isEmptyString(equipmentDecoType) ? HDEC_DEFAULT_NO_APLICA : equipmentDecoType);
            request.setDniVendedor(dniVendedor);
            request.setTelefonoOrigen(telefonoOrigen == null ? HDEC_DEFAULT_TELF_CONTACTO_2 : telefonoOrigen);

            request.setTipoServicio(serviceType);
            request.setClienteCms(StringUtils.isEmptyString(cmsCustomer) ? HDEC_DEFAULT_CLIENTE_CMS : cmsCustomer);
            request.setDistritoVendedor(StringUtils.isEmptyString(distritoVendedor) ? HDEC_DEFAULT_NO_APLICA : distritoVendedor);
            //request.setAdicionalDecoSmart(StringUtils.isEmptyString(decosSD) ? HDEC_DEFAULT_0 : decosSD);
            request.setDecosHd(HDEC_DEFAULT_0);
            request.setDecosDvr(HDEC_DEFAULT_0);
            request.setSvaInternet(StringUtils.isEmptyString(svaInternet) ? HDEC_DEFAULT_EMPTY_STRING : svaInternet);
            request.setSvaLinea(StringUtils.isEmptyString(svaLine) ? HDEC_DEFAULT_NO_APLICA : svaLine);

            if (request.getModalidadDePago().equalsIgnoreCase("Contado")) {
                request.setDescuentoWinback(cod_CIP);
            } else {
                request.setDescuentoWinback("");
            }

            request.setCodigoDeServicioCms(StringUtils.isEmptyString(cmsServiceCode) ? HDEC_DEFAULT_COD_SRV_CMS : cmsServiceCode);
            //request.setBloqueProducto(StringUtils.isEmptyString(tvBlock) ? HDEC_DEFAULT_NO_APLICA : tvBlock);

            request.setCoordenadasX(coordenadasx);
            request.setCoordenadasY(coordenadasy);
            request.setWebParental(webParental);
            request.setMontoContado(montoContado);
            request.setCodigoPostal(coidgoPostal);
            request.setPublicarGuia(automaticDebit);  // request.setPublicarGuia(publicarGuia);
            //request.setRepetidorSmartWifi(internetRsw);
            request.setModoVenta(orderOrigen.equalsIgnoreCase("APPVF") ? "APP VENTA" : orderOrigen.equalsIgnoreCase("WEBMT") ? "WEB CONVERGENTE" : "WEB VENTA");
            request.setNacionalidad(clientNationality);

            request.setModalidadContrato("?");
            request.setIdTransaccion("?");
            request.setRecuperoVenta("NO");
            request.setFechaAgenda("?");
            request.setRangoAgenda("?");

            //Sprint 24 RUC
            if (docType.equals("RUC") && docNumber.substring(0, 2).equals("20")) {
                request.setTipoDocumentoRrll(tipDocRrll);
                request.setNumeroDocumentoRrll(docNumRrll);
                request.setNombreCompletoRrll(nomRrll);
            } else {
                request.setTipoDocumentoRrll("?");
                request.setNumeroDocumentoRrll("?");
                request.setNombreCompletoRrll("?");
            }

            //Sprint 24 RECUPERO CAIDA
            /*if(flag_recupero){
                request.setFlagRecupero(flag_recupero);
                request.setIdTransaccion(id_transaccion);
            }*/

            if (request.getModalidadDePago().equalsIgnoreCase("Contado")) {
                request.setCodigoCip(cod_CIP);
            } else {
                request.setCodigoCip("?");
            }

            //Campos Adicionales
            request.setCampoAdicional12(incluyeEquipo1);
            request.setCampoAdicional13(incluyeEquipo2);
            request.setCampoAdicional14(numeroOrden1);
            request.setCampoAdicional15(numeroOrden2);
            request.setCampoAdicional16(operacionComercial1);
            request.setCampoAdicional17(operacionComercial2);
            request.setCampoAdicional18(planMovil);
            request.setCampoAdicional19(telefono1);
            request.setCampoAdicional20(telefono2);
            request.setEstadoMovil1(estadoMovil1);
            request.setEstadoMovil2(estadoMovil2);
            request.setFlag(flag == null ? Integer.parseInt(ServiceConstants.TGESTIONA_1ER_ENVIO) : flag > 0 ? flag : Integer.parseInt(ServiceConstants.TGESTIONA_1ER_ENVIO));

            request.setDecosHd(decosHd);
            request.setDecosDvr(decosDvr);
            request.setSvaLinea(svaLinea);

            request.setBloqueTv(bloqueTv);
            request.setRepetidorSmartWifi(repetidorSmartWifi);
            request.setAdicionalDecoSmart(adicionalDecoSmart);

            insertRequestBody.setData(request);
        }

        return insertRequestBody;
    }

    private void svaToRequest(Object[] row, int psCode, int nomSva){
        String svaPsCode = row[psCode] != null ? (String) row[psCode] : "?";
        if(svaPsCode.equals(MotorConstants.SVA_PS_ADICIONAL_DECO_SMART)){
            adicionalDecoSmart = row[nomSva] != null ? String.valueOf(row[nomSva]) : "?";
        }else{
            if(svaPsCode.equals(MotorConstants.SVA_PS_REPETIDOR_SMART_WIFI)){
                repetidorSmartWifi = row[nomSva] != null ? (int) row[nomSva] : 0;
            }else{
                switch(svaPsCode){
                    case MotorConstants.SVA_PS_BLOQUE_ESTELAR:
                        if(bloqueTv.equals(MotorConstants.SVA_NO_APLICA)){
                            bloqueTv = MotorConstants.SVA_BLOQUE_ESTELAR;
                        }else{
                            bloqueTv += MotorConstants.SVA_SEPARADOR + MotorConstants.SVA_BLOQUE_ESTELAR;
                        }
                        break;
                    case MotorConstants.SVA_PS_BLOQUE_FULL_HD:
                        if(bloqueTv.equals(MotorConstants.SVA_NO_APLICA)){
                            bloqueTv = MotorConstants.SVA_BLOQUE_FULL_HD;
                        }else{
                            bloqueTv += MotorConstants.SVA_SEPARADOR + MotorConstants.SVA_BLOQUE_FULL_HD;
                        }
                        break;
                    case MotorConstants.SVA_PS_BLOQUE_FOX:
                        if(bloqueTv.equals(MotorConstants.SVA_NO_APLICA)){
                            bloqueTv = MotorConstants.SVA_BLOQUE_FOX;
                        }else{
                            bloqueTv += MotorConstants.SVA_SEPARADOR + MotorConstants.SVA_BLOQUE_FOX;
                        }
                        break;
                    case MotorConstants.SVA_PS_BLOQUE_HBO:
                        if(bloqueTv.equals(MotorConstants.SVA_NO_APLICA)){
                            bloqueTv = MotorConstants.SVA_BLOQUE_HBO;
                        }else{
                            bloqueTv += MotorConstants.SVA_SEPARADOR + MotorConstants.SVA_BLOQUE_HBO;
                        }
                        break;
                }
            }
        }
    }

    private ApiResponse<PreventaResponseBody> sendRegistrarPreventa(PreventaRequestBody apiRequestBody) throws ClientException {
        ApiResponse<PreventaResponseBody> response;
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_HDEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_HDEC).build();

        ClientResult<ApiResponse<PreventaResponseBody>> result = postRegistrarPreventa(apiRequestBody, config);
        result.getEvent().setDocNumber(apiRequestBody.getNumeroDeDocumento());
        result.getEvent().setUsername(apiRequestBody.getCodVendedorAtis());
        result.getEvent().setOrderId(apiRequestBody.getIdGrabacionNativo());
        serviceCallEventsService.registerEvent(result.getEvent());
        if (!result.isSuccess()) {
            throw result.getE();
        } /*else {
            ApiResponse<PreventaResponseBody> apiResponse = result.getResult();
            if (!Constants.RESPONSE.equals(apiResponse.getHeaderOut().getMsgType())) {
                String message = "Error TGESTIONA";
                if (apiResponse.getBodyOut() != null) {
                    PreventaResponseBody body = apiResponse.getBodyOut();
                    if (body.getClientException() != null) {
                        if (body.getClientException().getAppDetail() != null) {
                            message = body.getClientException().getAppDetail().getExceptionAppMessage();
                        }
                    }
                }
                throw new ClientException(message);
            }
        }*/
        response = result.getResult();

        return response;

    }

    public ClientResult<ApiResponse<PreventaResponseBody>> postRegistrarPreventa(PreventaRequestBody apiRequestBody, ClientConfig config) {
        String jsonResponse = null;
        Parameters parameter = parametersService.findOneByDomainAndCategoryAndElement("TEST", "PARAMETER", "TESTHDEC");
        ClientResult<ApiResponse<PreventaResponseBody>> result = null;
        if ("NO".equals(parameter.getStrValue())) {
            RegistrarPreventaClient client = new RegistrarPreventaClient(config);
            result = client.post(apiRequestBody);
        } else {
            // Entonces ejecutamos en modo test... procedemos a verificar que error simular (4002 o 4003)
            Parameters parameterErrorCode = parametersService.findOneByDomainAndCategoryAndElement("TEST", "PARAMETER", "HDECERRORCODE");

            Parameters parameterMessageErrorCode = parametersService.findOneByDomainAndCategoryAndElement("CONFIG", "PARAMETER", "HDECERROR.MENSAJE." + parameterErrorCode.getStrValue());
            jsonResponse = parameterMessageErrorCode.getStrValue();
            RegistrarPreventaClient client = new RegistrarPreventaClientErrorSimulator(config, jsonResponse);
            result = client.post(apiRequestBody);
            result.getEvent().setServiceResponse(jsonResponse);
        }
        return result;
    }

    @Override
    public FijaHDECResponse sendTGestionaInsertOrder(String orderId) {
        FijaHDECResponse response = new FijaHDECResponse();
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            logger.info("sendTGestionaInsertOrder => order: null");
            response.setCode(ServiceConstants.SERVICE_ERROR);
            response.setMessage("No existe registro de la venta.");
            return response;
        }
        List<Object[]> data = orderRepository.getDataTGestiona(orderId);
        InsertRequestBody request = getArrayData2(data);
        try {
            ApiResponse<InsertResponseBody> responseInsertOrder = sendInsertOrder(request);
            //Validar Respuesta
            if (responseInsertOrder.getHttpCode() != null) {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage("Error " + responseInsertOrder.getHttpCode() + ": " + responseInsertOrder.getMoreInformation());
                response.setEstadoTGestiona("ENVIANDO");
                return response;
            } else if (responseInsertOrder.getBodyOut().getClientException() != null) {
                if (responseInsertOrder.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage().equals("Error: CODIGO_UNICO ya existe => CODIGO_UNICO") ||
                        responseInsertOrder.getBodyOut().getClientException().getAppDetail().getExceptionAppCode().equals(1)) {
                    response.setCode(ServiceConstants.SERVICE_SUCCESS);
                    response.setMessage(responseInsertOrder.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage());
                    response.setEstadoTGestiona("PENDIENTE");
                } else {
                    response.setCode(ServiceConstants.SERVICE_ERROR);
                    response.setMessage("Error " + responseInsertOrder.getBodyOut().getClientException().getExceptionCode() + ": " + responseInsertOrder.getBodyOut().getClientException().getExceptionDetail());
                    response.setEstadoTGestiona("CAIDA");
                    return response;
                }
            } else if (responseInsertOrder.getBodyOut().getServerException() != null) {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage("Error " + responseInsertOrder.getBodyOut().getServerException().getExceptionCode() + ": " + responseInsertOrder.getBodyOut().getServerException().getExceptionDetail());
                response.setEstadoTGestiona("ENVIANDO");
                return response;
            } else {
                response.setCode(ServiceConstants.SERVICE_SUCCESS);
                response.setMessage(responseInsertOrder.getBodyOut().getTransaccion() + "");
                response.setEstadoTGestiona("PENDIENTE");
            }
        } catch (ClientException e) {
            logger.error(e.getMessage());
            response.setCode(ServiceConstants.SERVICE_ERROR);
            response.setMessage("ERROR DEL BACK");
            response.setEstadoTGestiona("CAIDA");
            return response;
        }
        return response;
    }

    private ApiResponse<InsertResponseBody> sendInsertOrder(InsertRequestBody insertRequestBody) throws ClientException {
        ApiResponse<InsertResponseBody> response;
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_HDEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_HDEC).build();

        ClientResult<ApiResponse<InsertResponseBody>> result = postInsertOrder(insertRequestBody, config);
        result.getEvent().setDocNumber(insertRequestBody.getData().getNumeroDeDocumento());
        result.getEvent().setUsername(insertRequestBody.getData().getCodVendedorAtis());
        result.getEvent().setOrderId(insertRequestBody.getData().getIdGrabacionNativo());
        //result.getEvent().setSourceApp("WEBMT");

        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }

        response = result.getResult();

        return response;
    }

    public ClientResult<ApiResponse<InsertResponseBody>> postInsertOrder(InsertRequestBody insertRequestBody, ClientConfig config) {
        ClientResult<ApiResponse<InsertResponseBody>> result = null;
        InsertOrderClient client = new InsertOrderClient(config);
        result = client.post(insertRequestBody);
        return result;
    }

}
