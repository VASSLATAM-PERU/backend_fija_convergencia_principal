package tdp.mt.backend.movistartotal.main.restclient.domain;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultadoEjecucion {

	@JsonProperty("codigoRespuesta")
	private String codigoRespuesta;
	@JsonProperty("descripcionRespuesta")
	private String descripcionRespuesta;
	@JsonProperty("respuestaWS")
	private String respuestaWS;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("codigoRespuesta")
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	@JsonProperty("codigoRespuesta")
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	@JsonProperty("descripcionRespuesta")
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	@JsonProperty("descripcionRespuesta")
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}

	@JsonProperty("respuestaWS")
	public String getRespuestaWS() {
		return respuestaWS;
	}

	@JsonProperty("respuestaWS")
	public void setRespuestaWS(String respuestaWS) {
		this.respuestaWS = respuestaWS;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
