package tdp.mt.backend.movistartotal.main.service;


import tdp.mt.backend.movistartotal.main.domain.FijaDatesBD.FijaDatesVendedorRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaReporteBBIIRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.domain.MTFija.MTOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaOrderStatus.FijaOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.Response;

import java.util.Map;

public interface OrderService {

    Response<Map<String, String>> getMTOrderStatus(MTOrderStatusRequest request);

    Response<Map<String, String>> getFijaOrder(FijaOrderStatusRequest request);

    Response<Map<String, String>> getDateInFija(FijaDatesVendedorRequest request);

    Response<Map<String, Map<String,String>>> getFijaDataReporteBBII(FijaReporteBBIIRequest request);

    void getFijaDataReporteBBIITest();
}
