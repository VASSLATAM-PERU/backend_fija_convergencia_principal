package tdp.mt.backend.movistartotal.main.restclient.automatizador;
import com.fasterxml.jackson.annotation.JsonProperty;

import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

public class AutomatizadorSaleResponseBody {
	@JsonProperty("COD_ERR_CD")
	private String Codigo;
	@JsonProperty("OBS_RES_DS")
	private String RespuestaMsj;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		Codigo = codigo;
	}

	public String getRespuestaMsj() {
		return RespuestaMsj;
	} 

	public void setRespuestaMsj(String respuestaMsj) {
		RespuestaMsj = respuestaMsj;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
