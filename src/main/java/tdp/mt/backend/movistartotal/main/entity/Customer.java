package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "customer", schema = "ibmx_a07e6d02edaf552")
@SequenceGenerator(name = "myseqcustomer", sequenceName = "ibmx_a07e6d02edaf552.customer_seq", catalog = "ibmx_a07e6d02edaf552", schema = "ibmx_a07e6d02edaf552", allocationSize = 1)
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseqcustomer")
    private Long id;

    @Column(name = "doctype")
    private String docType;

    @Column(name = "docnumber")
    private String docNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "customerphone")
    private String customerPhone;

    @Column(name = "customerphone2")
    private String customerPhone2;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname1")
    private String lastName1;

    @Column(name = "lastname2")
    private String lastName2;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "birthdate")
    private Date birthDate;

    @Column(name = "sex")
    private String sex;

    @Column(name = "maritalstatus")
    private String maritalstatus;

    @Column(name = "doctyperrll")
    private String doctyperrll;

    @Column(name = "docnumberrrll")
    private String docnumberrrll;

    @Column(name = "fullnamerrll")
    private String fullnamerrll;

    public Customer() {
    }

    public Customer(String docType, String docNumber) {
        this.docType = docType.toUpperCase();
        this.docNumber = docNumber.toUpperCase();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone2() {
        return customerPhone2;
    }

    public void setCustomerPhone2(String customerPhone2) {
        this.customerPhone2 = customerPhone2;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName1() {
        return lastName1;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public String getLastName2() {
        return lastName2;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMaritalstatus() {
        return maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        this.maritalstatus = maritalstatus;
    }

    public String getDoctyperrll() {
        return doctyperrll;
    }

    public void setDoctyperrll(String doctyperrll) {
        this.doctyperrll = doctyperrll;
    }

    public String getDocnumberrrll() {
        return docnumberrrll;
    }

    public void setDocnumberrrll(String docnumberrrll) {
        this.docnumberrrll = docnumberrrll;
    }

    public String getFullnamerrll() {
        return fullnamerrll;
    }

    public void setFullnamerrll(String fullnamerrll) {
        this.fullnamerrll = fullnamerrll;
    }
}
