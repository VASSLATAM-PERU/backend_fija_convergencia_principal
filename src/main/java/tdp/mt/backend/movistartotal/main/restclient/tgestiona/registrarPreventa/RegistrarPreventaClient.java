package tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;


import java.io.IOException;


public class RegistrarPreventaClient extends AbstractClient<PreventaRequestBody, PreventaResponseBody> {

	private static final String urlendpoint ="preventa/";
	private static final String urlmethod = "registrarPreventa";

	public RegistrarPreventaClient(ClientConfig config) {
		super(config);
		this.config = UtilMethods.buildConfigHeader(urlendpoint,urlmethod);
	}
	
	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_TGESTIONA;
	}

	@Override
	protected ApiResponse<PreventaResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<PreventaResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<PreventaResponseBody>>() {});
		return objCms;
	}
	
}
