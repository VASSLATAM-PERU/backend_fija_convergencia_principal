package tdp.mt.backend.movistartotal.main.restclient.automatizador;

public class BeanAddress {
	
	private String nombre;
	private String abreviatura;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	
}
 