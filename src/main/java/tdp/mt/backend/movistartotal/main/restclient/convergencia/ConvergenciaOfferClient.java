package tdp.mt.backend.movistartotal.main.restclient.convergencia;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.util.List;

public class ConvergenciaOfferClient extends AbstractClient<OfferDataRequest, MtOfferVO> {

    private static final String urlendpoint ="offer/";
    private static final String urlmethod = "azurecontract";

    public ConvergenciaOfferClient(ClientConfig config) {
        super(config);
        this.config = UtilMethods.buildConfigHeaderFijaNoApi(urlendpoint,urlmethod);
    }

    @Override
    protected String getServiceCode() {
        return "CONVERGENCIA_OFFER";
    }

    @Override
    protected ApiResponse<MtOfferVO> getResponse(String json) throws Exception {
        return null;
    }

    public OfferDataRequest getRequestData(String mtorder){
        OfferDataRequest data = new OfferDataRequest();
        data.setMtorderid(mtorder);
        return data;
    }
}
