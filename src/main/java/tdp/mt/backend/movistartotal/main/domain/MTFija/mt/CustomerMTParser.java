package tdp.mt.backend.movistartotal.main.domain.MTFija.mt;

import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.entity.Customer;

public class CustomerMTParser implements MTParser<Customer> {

    @Override
    public Customer parse(FijaHDECRequest mtSaleLog) {
        Customer customer = new Customer();
//firstName,"
        customer.setFirstName(mtSaleLog.getClientFirstName());
//lastName1,"
        customer.setLastName1(mtSaleLog.getClientLastName1());
//lastName2,"
        customer.setLastName2(mtSaleLog.getClientLastName1());
//docType,"
        customer.setDocType(mtSaleLog.getClientDocType());
//docNumber,"
        customer.setDocNumber(mtSaleLog.getClientDocNumber());
//email,"
        customer.setEmail(mtSaleLog.getClientEmail());
//customerPhone,"
        customer.setCustomerPhone(mtSaleLog.getClientCustomerPhone());
//customerPhone2,"
        customer.setCustomerPhone2(mtSaleLog.getClientCustomerPhone2());
//doctyperrll, "
        //customer.setDocnumberrrll();
//docnumberrrll, "
        //customer.setDocnumberrrll();
//fullnamerrll, "
        //customer.setFullnamerrll();

        return customer;
    }
}
