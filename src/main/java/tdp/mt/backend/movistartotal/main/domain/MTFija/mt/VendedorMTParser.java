package tdp.mt.backend.movistartotal.main.domain.MTFija.mt;

import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.entity.TdpSalesAgent;

public class VendedorMTParser implements MTParser<TdpSalesAgent> {


    @Override
    public TdpSalesAgent parse(FijaHDECRequest mtSaleLog) {

        TdpSalesAgent vendedor = new TdpSalesAgent();
//canal,"
        vendedor.setCanal(mtSaleLog.getSellerAtisChannel());
//entidad,"
        vendedor.setEntidad(mtSaleLog.getSellerAtisEntity());
//nombre,"
        vendedor.setNombre(mtSaleLog.getSellerName());
//codATIS,"
        vendedor.setCodigoAtis(mtSaleLog.getSellerAtisCode());
//codCMS,"
        vendedor.setCodCms(mtSaleLog.getSellerCmsCode());
//departamento,"
//provincia,"
//distrito,"

//dni,"
        vendedor.setDni(mtSaleLog.getSellerDocument());

        return null;
    }
}
