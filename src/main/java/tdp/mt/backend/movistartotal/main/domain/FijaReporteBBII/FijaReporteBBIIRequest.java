package tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII;

import java.util.List;

public class FijaReporteBBIIRequest {

    private List<String> mtOrders;
    private List<FijaVisorSpecs>FijaVisorSpecs;

    public List<String> getMtOrders() {
        return mtOrders;
    }

    public void setMtOrders(List<String> mtOrders) {
        this.mtOrders = mtOrders;
    }

    public List<tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaVisorSpecs> getFijaVisorSpecs() {
        return FijaVisorSpecs;
    }

    public void setFijaVisorSpecs(List<tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaVisorSpecs> fijaVisorSpecs) {
        FijaVisorSpecs = fijaVisorSpecs;
    }
}
