package tdp.mt.backend.movistartotal.main.dao.reniec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.commonservice.connection.Database;
import tdp.mt.backend.movistartotal.commonservice.util.LogVass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ReniecDAO {
    private static final Logger logger = LogManager.getLogger();

    /**
     * Gets count names that are not like notLike for the gender gender
     *
     * @param notLike
     * @param gender
     * @param count
     * @return
     */
    public List<String> getNames(String notLike, String gender, Long count) {
        String query = "select name from public.random_names where name not ilike ? and gender = ? order by random() limit ?";
        List<String> names = null;
        try (Connection con = Database.dataSource().getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, notLike);
                stmt.setString(2, gender);
                stmt.setLong(3, count);

                LogVass.daoQueryDAO(logger, "getNames", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                names = new ArrayList<>();
                while (rs.next()) {
                    names.add(rs.getString("name").toUpperCase());
                }
            }
        } catch (Exception e) {
            logger.error("Error getNames error.", e);

            if (Constants.GENDER_MAS.equals(gender)) {
                names = new ArrayList<String>() {{
                    add("JORGE");
                    add("EDGAR");
                    add("BRYAN");
                    add("LUIS");
                    add("HAROLD");
                }};
            } else if (Constants.GENDER_FEM.equals(gender)) {
                names = new ArrayList<String>() {{
                    add("VANIA");
                    add("OMAYRA");
                    add("DIANA");
                    add("MARIA");
                    add("ROSA");
                }};
            }
        }
        return names;
    }


}

