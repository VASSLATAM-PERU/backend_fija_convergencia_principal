package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.entity.OrdenMT;

@Repository
public interface OrdenMTRepository extends JpaRepository<OrdenMT, Long> {

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.order_mt(id_mt_order, id_fija_order, " +
            "m1_amdocs_movil_order, m2_amdocs_movil_order, register_date, remote)" +
            "VALUES (:idMtOrder, :idFijaOrder, :m1AmdocsMovilOrder, :m2AmdocsMovilOrder, now(), :remote);", nativeQuery = true)
    @Transactional
    void insertMtOrder(@Param("idMtOrder") Long idMtOrder,
                       @Param("idFijaOrder") String idFijaOrder,
                       @Param("m1AmdocsMovilOrder") String m1AmdocsMovilOrder,
                       @Param("m2AmdocsMovilOrder") String m2AmdocsMovilOrder,
                       @Param("remote") boolean remote);

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.order_mt(id_mt_order, id_fija_order, " +
            "register_date, remote, mode_retail)" +
            "VALUES (:idMtOrder, :idFijaOrder, now(), :remote, :mode_retail);", nativeQuery = true)
    @Transactional
    void insertMtOrderRetail(@Param("idMtOrder") Long idMtOrder,
                             @Param("idFijaOrder") String idFijaOrder,
                             @Param("remote") boolean remote,
                             @Param("mode_retail") String mode_retail);

    @Query("SELECT o FROM OrdenMT o WHERE o.idFijaOrder = ?1 AND o.modeRetail = '1'")
    OrdenMT getVentaMT(String idFijaOrder);

}
