package tdp.mt.backend.movistartotal.main.domain.AzureMailing;

import tdp.mt.backend.movistartotal.main.restclient.convergencia.MtOfferVO;

public class EmailOfferObject {

    private String clientName;
    private String clientEmail;
    private MtOfferVO mtOfferVO;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public MtOfferVO getMtOfferVO() {
        return mtOfferVO;
    }

    public void setMtOfferVO(MtOfferVO mtOfferVO) {
        this.mtOfferVO = mtOfferVO;
    }
}
