package tdp.mt.backend.movistartotal.main.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonservice.services.ServiceCallEventsService;
import tdp.mt.backend.movistartotal.main.domain.AzureMailing.EmailOfferObject;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.entity.Customer;
import tdp.mt.backend.movistartotal.main.entity.MTAzureBlobStorage;
import tdp.mt.backend.movistartotal.main.entity.MTAzureFileStorage;
import tdp.mt.backend.movistartotal.main.entity.TdpVisor;
import tdp.mt.backend.movistartotal.main.repository.CustomerRepository;
import tdp.mt.backend.movistartotal.main.repository.MTAzureBlobStorageRepository;
import tdp.mt.backend.movistartotal.main.repository.MTAzureFileStorageRepository;
import tdp.mt.backend.movistartotal.main.repository.TdpVisorRepository;
import tdp.mt.backend.movistartotal.main.restclient.convergencia.ConvergenciaOfferClient;
import tdp.mt.backend.movistartotal.main.restclient.convergencia.MtOfferVO;
import tdp.mt.backend.movistartotal.main.restclient.mailing.EmailsClient;
import tdp.mt.backend.movistartotal.main.restclient.mailing.UserMailingResponse;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Service
public class AzureEmailServiceImpl {

    private static final Log log = LogFactory.getLog(AzureEmailServiceImpl.class);

    @Value("${mt.azure.legados18.protocol}")
    private String protocolAzure;
    @Value("${mt.azure.legados18.name}")
    private String nameAzure;
    @Value("${mt.azure.legados18.key}")
    private String keyAzure;
    @Value("${mt.azure.legados18.suffix}")
    private String suffixAzure;

    @Autowired
    private TdpVisorRepository visorRepository;
    @Autowired
    private MTAzureBlobStorageRepository blobRepository;
    @Autowired
    private MTAzureFileStorageRepository fileRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;


    public void sendRetailEmailProcess(String mtOrder) {
        //mtOrder = "MT6555";
        EmailOfferObject object = new EmailOfferObject();

        //CONSGUIR DATA DE MT; CLIENTE Y REgistro en azure
        //MTAzureBlobStorage azureBlob = blobRepository.getMtOrderBlob(mtOrder);
        MTAzureFileStorage azureFileShare = fileRepository.getMtOrderFileByExtension(mtOrder, ServiceConstants.EXTENSION_PDF);

        String filename = azureFileShare.getMtorder() + azureFileShare.getFileExtension();
        String container = azureFileShare.getFileDate();

        TdpVisor visor = visorRepository.findOneByIdVisor(mtOrder);
        //Customer customer = customerRepository.searchCustomerByDocNumber(visor.getDni());
        String name = "Cliente";
        if (visor.getCliente() != null && !visor.getCliente().equals("")) {
            name = visor.getCliente().substring(0, visor.getCliente().indexOf(" "));
        }
        object.setClientName(name);
        object.setClientEmail(visor.getCorreo());
        // Consultar data de la Oferta en Convergencia
        ConvergenciaOfferClient client = new ConvergenciaOfferClient(new ClientConfig());

        ClientResult<Response<MtOfferVO>> result = client.postNoApi(client.getRequestData(mtOrder));
        serviceCallEventsService.registerEvent(result.getEvent());

        if (result.getResult() != null && result.getResult().getResponseData() != null) {

            Map<String, Object> map = (Map<String, Object>) result.getResult().getResponseData();
            object.setMtOfferVO(mapServiceResponse(map));
            String url = getFileShareSAS(filename, container);
            sendEmail(url, object); //Envio de correo

        }

    }

    private MtOfferVO mapServiceResponse(Map<String, Object> map) {
        MtOfferVO offer = new MtOfferVO();
        int mt = (int) map.get("mtsaleid");
        offer.setMtsaleid(mt);
        offer.setOfferChannel((String) map.get("offerChannel"));
        offer.setOfferDecos((String) map.get("offerDecos"));
        offer.setOfferInternetSpeed((String) map.get("offerInternetSpeed"));
        offer.setOfferMobileData((String) map.get("offerMobileData"));
        offer.setOfferName((String) map.get("offerName"));
        offer.setOfferSVAs((String) map.get("offerSVAs"));

        return offer;
    }

    private String getFileShareSAS(String fileName, String container) {

        String accountName = nameAzure;
        //fileName = "ReporteUsabilidadTEST1120190617.xlsx";
        //container = "reportes";
        //String key = key;
        String resourceUrl = "https://" + accountName + ".file.core.windows.net/" + container + "/" + fileName;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        ;
        String start = now.format(formatter);
        now = now.plusDays(30);
        String expiry = now.format(formatter);
        ;
        String azureApiVersion = "2017-11-09";

        String stringToSign = accountName + "\n" +
                "r\n" +
                "f\n" +
                "o\n" +
                start + "\n" +
                expiry + "\n" +
                "\n" +
                "https\n" +
                azureApiVersion + "\n";

        String signature = getHMAC256(keyAzure, stringToSign);
        String url = "";

        try {

            String sasToken = "sv=" + azureApiVersion +
                    "&ss=f" +
                    "&srt=o" +
                    "&sp=r" +
                    "&se=" + URLEncoder.encode(expiry, "UTF-8") +
                    "&st=" + URLEncoder.encode(start, "UTF-8") +
                    "&spr=https" +
                    "&sig=" + URLEncoder.encode(signature, "UTF-8");

            url = resourceUrl + "?" + sasToken;
            System.out.println(resourceUrl + "?" + sasToken);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

    private String GetBlobFileSAS(String fileName, String container) {

        String accountName = nameAzure;
        //fileName = "JunioTXT.txt"; //FIXME: NOmbre de archivo
        //container = "pruebajk";
        String resourceUrl = "https://" + accountName + ".blob.core.windows.net/" + container + "/" + fileName;
        String canonResource = "/blob/" + accountName + "/" + container + "/" + fileName;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();
        //LocalDateTime before = now.minusDays(1);
        //String start = now.format(formatter);
        now = now.plusDays(30); //FIXME: modificar dias
        String expiry = now.format(formatter);

        String azureApiVersion = "2018-03-28";
        //String contentDisposition = "file; attachment";
        //String contentType = "text/plain";

        String stringToSign = "r\n" +
                "\n" +
                expiry + "\n" +
                canonResource + "\n" +
                "\n" +
                "\n" +
                "https,http\n" +
                azureApiVersion + "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n";


        String signature = getHMAC256(keyAzure, stringToSign);
        String url = "";

        try {

            String sasToken = "sv=" + azureApiVersion +
                    "&sr=b" +
                    "&sp=r" +
                    "&se=" + URLEncoder.encode(expiry, "UTF-8") +
                    //"&st=" + URLEncoder.encode(start, "UTF-8") +
                    "&spr=https,http" +
                    "&sig=" + URLEncoder.encode(signature, "UTF-8");

            url = resourceUrl + "?" + sasToken;
            System.out.println(resourceUrl + "?" + sasToken);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

    private String getHMAC256(String accountKey, String signStr) {
        String signature = null;
        try {
            SecretKeySpec secretKey = new SecretKeySpec(Base64.getDecoder().decode(accountKey), "HmacSHA256");
            Mac sha256HMAC = Mac.getInstance("HmacSHA256");
            sha256HMAC.init(secretKey);
            signature = Base64.getEncoder().encodeToString(sha256HMAC.doFinal(signStr.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return signature;
    }

    public void sendEmail(String uri, EmailOfferObject offerLog) {

        //TODO BUSCAR VALORES EN BD
        String customerName = offerLog.getClientName();
        String mtPlanName = offerLog.getMtOfferVO().getOfferName();
        String clientEmail = offerLog.getClientEmail();//"jorge.krentzien@vasslatam.com"; //

        String mobileData = offerLog.getMtOfferVO().getOfferMobileData();
        long data = (mobileData != null && !mobileData.equals("")) ? Long.parseLong(mobileData) / 1000 : 0; //GB
        String mobileInformation = (data == 0 ? "" : data) + " GB"; //"Datos del Movil: GB";

        String fijaSpeed = offerLog.getMtOfferVO().getOfferInternetSpeed();
        long speed = (fijaSpeed != null && !fijaSpeed.equals("")) ? Long.parseLong(fijaSpeed) / 1000 : 0;
        String internetSpeed = (speed == 0 ? "" : speed) + " Mbps"; //"Velocidad Internet en mbps";
        String svaInfo = "SVAS"; //offerLog.getMtOfferVO().getOfferSVAs();
        String channelQty = offerLog.getMtOfferVO().getOfferChannel(); //"Cantidad de Canales";
        String decoQty = (offerLog.getMtOfferVO().getOfferDecos() != null) ? offerLog.getMtOfferVO().getOfferDecos() : "Decos";//"Cantidad de Decos";

        EmailsClient client = new EmailsClient(new ClientConfig());
        ClientResult<ApiResponse<UserMailingResponse>> clientResult = client.post3(client.getMTContractRequestData(customerName, mtPlanName, mobileInformation,
                internetSpeed, svaInfo, channelQty, decoQty, uri, clientEmail));
        serviceCallEventsService.registerEvent(clientResult.getEvent());

        //VALIDAR RESPONSE?
    }


}
