package tdp.mt.backend.movistartotal.main.restclient.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiRequestTef {

	@JsonProperty("TefHeaderReq")
    private ApiRequestTefHeader TefHeaderReq;


    @JsonProperty("TefHeaderReq")
	public ApiRequestTefHeader getTefHeaderReq() {
		return TefHeaderReq;
	}


	public void setTefHeaderReq(ApiRequestTefHeader tefHeaderReq) {
		TefHeaderReq = tefHeaderReq;
	}
}
