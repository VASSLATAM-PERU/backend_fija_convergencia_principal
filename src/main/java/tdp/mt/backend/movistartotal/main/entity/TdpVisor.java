package tdp.mt.backend.movistartotal.main.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.jdo.annotations.Transactional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "tdp_visor", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class TdpVisor {

    @Id
    @Column(name = "id_visor")
    private String idVisor;
    @Column(name = "back")
    private String back;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_grabacion")
    private Date fechaGrabacion;
    @Column(name = "id_grabacion")
    private String idGrabacion;
    @Column(name = "cliente")
    private String cliente;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "departamento")
    private String departamento;
    @Column(name = "dni")
    private String dni;
    @Column(name = "nombre_producto")
    private String nombreProducto;
    @Column(name = "estado_solicitud")
    private String estadoSolicitud;
    @Column(name = "motivo_estado")
    private String motivoEstado;
    @Column(name = "speech")
    private String speech;
    @Column(name = "accion")
    private String accion;
    @Column(name = "operacion_comercial")
    private String operacionComercial;
    @Column(name = "distrito")
    private String distrito;
    @Column(name = "codigo_llamada")
    private String codigoLlamada;
    @Column(name = "sub_producto")
    private String subProducto;
    @Column(name = "tipo_producto")
    private String tipoProducto;
    @Column(name = "codigo_gestion")
    private Integer codigoGestion;
    @Column(name = "codigo_sub_gestion")
    private Integer codigoSubGestion;
    @Column(name = "flag_dependencia")
    private String flagDependencia;
    @Column(name = "version_fila")
    private String versionFila;
    @Column(name = "tipo_documento")
    private String tipoDocumento;
    @Column(name = "accion_aplicativos_back")
    private String accionAplicativosBack;
    @Column(name = "provincia")
    private String provincia;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_de_llamada")
    private Date fechaDeLlamada;
    @Column(name = "codigo_pedido")
    private String codigoPedido;
    @Column(name = "version_fila_t")
    private String versionFilaT;
    @Column(name = "sub_producto_equiv")
    private String subProductoEquiv;
    @Column(name = "producto")
    private String producto;
    @Column(name = "nombre_producto_fuente")
    private String nombreProductoFuente;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_solicitado")
    private Date fechaSolicitado;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_registrado")
    private Date fechaRegistrado;
    @Column(name = "estado_anterior")
    private String estadoAnterior;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_equipo_tecnico")
    private Date fechaEquipoTecnico;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecha_instalado")
    private Date fechaInstalado;
    @Column(name = "flg_solicitado_email")
    private Integer flgSolicitadoEmail;
    @Column(name = "flg_registrado_email")
    private String flgRegistradoEmail;
    @Column(name = "flg_equipo_tecnico_email")
    private Integer flgEquipoTecnicoEmail;
    @Column(name = "flg_instalado_email")
    private Integer flgInstaladoEmail;
    @Column(name = "cnt_intentos_hdec")
    private Integer cntIntentosHdec;
    @Column(name = "correo")
    private String correo;
    
    @Transactional
    @Column(name = "cnt_intentos_aut")
    private Integer cntIntentosAuto;

    public String getIdVisor() {
        return idVisor;
    }

    public void setIdVisor(String idVisor) {
        this.idVisor = idVisor;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public Date getFechaGrabacion() {
        return fechaGrabacion;
    }

    public void setFechaGrabacion(Date fechaGrabacion) {
        this.fechaGrabacion = fechaGrabacion;
    }

    public String getIdGrabacion() {
        return idGrabacion;
    }

    public void setIdGrabacion(String idGrabacion) {
        this.idGrabacion = idGrabacion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public String getSpeech() {
        return speech;
    }

    public void setSpeech(String speech) {
        this.speech = speech;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getOperacionComercial() {
        return operacionComercial;
    }

    public void setOperacionComercial(String operacionComercial) {
        this.operacionComercial = operacionComercial;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCodigoLlamada() {
        return codigoLlamada;
    }

    public void setCodigoLlamada(String codigoLlamada) {
        this.codigoLlamada = codigoLlamada;
    }

    public String getSubProducto() {
        return subProducto;
    }

    public void setSubProducto(String subProducto) {
        this.subProducto = subProducto;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public Integer getCodigoGestion() {
        return codigoGestion;
    }

    public void setCodigoGestion(Integer codigoGestion) {
        this.codigoGestion = codigoGestion;
    }

    public Integer getCodigoSubGestion() {
        return codigoSubGestion;
    }

    public void setCodigoSubGestion(Integer codigoSubGestion) {
        this.codigoSubGestion = codigoSubGestion;
    }

    public String getFlagDependencia() {
        return flagDependencia;
    }

    public void setFlagDependencia(String flagDependencia) {
        this.flagDependencia = flagDependencia;
    }

    public String getVersionFila() {
        return versionFila;
    }

    public void setVersionFila(String versionFila) {
        this.versionFila = versionFila;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getAccionAplicativosBack() {
        return accionAplicativosBack;
    }

    public void setAccionAplicativosBack(String accionAplicativosBack) {
        this.accionAplicativosBack = accionAplicativosBack;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Date getFechaDeLlamada() {
        return fechaDeLlamada;
    }

    public void setFechaDeLlamada(Date fechaDeLlamada) {
        this.fechaDeLlamada = fechaDeLlamada;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getVersionFilaT() {
        return versionFilaT;
    }

    public void setVersionFilaT(String versionFilaT) {
        this.versionFilaT = versionFilaT;
    }

    public String getSubProductoEquiv() {
        return subProductoEquiv;
    }

    public void setSubProductoEquiv(String subProductoEquiv) {
        this.subProductoEquiv = subProductoEquiv;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getNombreProductoFuente() {
        return nombreProductoFuente;
    }

    public void setNombreProductoFuente(String nombreProductoFuente) {
        this.nombreProductoFuente = nombreProductoFuente;
    }

    public Date getFechaSolicitado() {
        return fechaSolicitado;
    }

    public void setFechaSolicitado(Date fechaSolicitado) {
        this.fechaSolicitado = fechaSolicitado;
    }

    public Date getFechaRegistrado() {
        return fechaRegistrado;
    }

    public void setFechaRegistrado(Date fechaRegistrado) {
        this.fechaRegistrado = fechaRegistrado;
    }

    public String getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public Date getFechaEquipoTecnico() {
        return fechaEquipoTecnico;
    }

    public void setFechaEquipoTecnico(Date fechaEquipoTecnico) {
        this.fechaEquipoTecnico = fechaEquipoTecnico;
    }

    public Date getFechaInstalado() {
        return fechaInstalado;
    }

    public void setFechaInstalado(Date fechaInstalado) {
        this.fechaInstalado = fechaInstalado;
    }

    public Integer getFlgSolicitadoEmail() {
        return flgSolicitadoEmail;
    }

    public void setFlgSolicitadoEmail(Integer flgSolicitadoEmail) {
        this.flgSolicitadoEmail = flgSolicitadoEmail;
    }

    public String getFlgRegistradoEmail() {
        return flgRegistradoEmail;
    }

    public void setFlgRegistradoEmail(String flgRegistradoEmail) {
        this.flgRegistradoEmail = flgRegistradoEmail;
    }

    public Integer getFlgEquipoTecnicoEmail() {
        return flgEquipoTecnicoEmail;
    }

    public void setFlgEquipoTecnicoEmail(Integer flgEquipoTecnicoEmail) {
        this.flgEquipoTecnicoEmail = flgEquipoTecnicoEmail;
    }

    public Integer getFlgInstaladoEmail() {
        return flgInstaladoEmail;
    }

    public void setFlgInstaladoEmail(Integer flgInstaladoEmail) {
        this.flgInstaladoEmail = flgInstaladoEmail;
    }

    public Integer getCntIntentosHdec() {
        return cntIntentosHdec;
    }

    public void setCntIntentosHdec(Integer cntIntentosHdec) {
        this.cntIntentosHdec = cntIntentosHdec;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

	public Integer getCntIntentosAuto() {
		return cntIntentosAuto;
	}

	public void setCntIntentosAuto(Integer cntIntentosAuto) {
		this.cntIntentosAuto = cntIntentosAuto;
	}

    
}
