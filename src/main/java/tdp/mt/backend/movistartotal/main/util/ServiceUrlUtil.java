package tdp.mt.backend.movistartotal.main.util;

public class ServiceUrlUtil {


    //private static String PROP_FILE = (System.getenv("PROPERTIES") != null ? System.getenv("PROPERTIES") : "setup.properties");
    private static String PROP_FILE = "setupPrd.properties";

    //MOBILE
    public static String BASE_SERVICE_URL = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.url");
    public static String BASE_SERVICE_API_KEY = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.key");
    public static String BASE_SERVICE_API_SECRET = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.key");

    //FIJA
    public static String BASE_SERVICE_URL2 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.url");
    public static String BASE_SERVICE_API_KEY2 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.key");
    public static String BASE_SERVICE_API_SECRET2 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.key");

    //PORTA
    public static String BASE_SERVICE_URL3 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.url3");
    public static String BASE_SERVICE_API_KEY3 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.key3");
    public static String BASE_SERVICE_API_SECRET3 = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.key3");

    //MOTOR
    public static String BASE_SERVICE_URLD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.urld");
    public static String BASE_SERVICE_API_KEYD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.keyd");
    public static String BASE_SERVICE_API_SECRETD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.keyd");

    //DEV DEV
    public static String BASE_SERVICE_URLDD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.urldd");
    public static String BASE_SERVICE_API_KEYDD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.keydd");
    public static String BASE_SERVICE_API_SECRETDD = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.keydd");

    //FIJA TESTING
    public static String BASE_SERVICE_URL_F_T = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.url.f.t");
    public static String BASE_SERVICE_API_KEY_F_T = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.key.f.t");
    public static String BASE_SERVICE_API_SECRET_F_T = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.key.f.t");

    public static String REMOTE_CHANNELS = PropertiesUtil.getPropertyFromFile(PROP_FILE, "remote.channels");
    
  //AUTOMATIZADOR
    public static String BASE_SERVICE_URL_AUTO = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.base.url.auto");
    public static String BASE_SERVICE_API_KEY_AUTO = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.key.auto");
    public static String BASE_SERVICE_API_SECRET_AUTO = PropertiesUtil.getPropertyFromFile(PROP_FILE, "api.connect.secret.key.auto");

}

