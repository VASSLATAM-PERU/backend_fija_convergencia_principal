package tdp.mt.backend.movistartotal.main.dao.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoordenadasXYResponse {

    @JsonProperty("coordenadaX")
    private String coordenadaX;
    @JsonProperty("coordenadaY")
    private String coordenadaY;

    @JsonProperty("direccion")
    private String direccion;

    @JsonProperty("coordenadaX")
    public String getCoordenadaX() {
        return coordenadaX;
    }

    @JsonProperty("coordenadaX")
    public void setCoordenadaX(String coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    @JsonProperty("coordenadaY")
    public String getCoordenadaY() {
        return coordenadaY;
    }

    @JsonProperty("coordenadaY")
    public void setCoordenadaY(String coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    @JsonProperty("direccion")
    public String getDireccion() {
        return this.direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
