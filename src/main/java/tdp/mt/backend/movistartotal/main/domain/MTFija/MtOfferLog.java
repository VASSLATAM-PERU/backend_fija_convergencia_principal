package tdp.mt.backend.movistartotal.main.domain.MTFija;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the mt_offer_log database table.
 *
 */
public class MtOfferLog implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idOffer;

	private String offerBillingId;

	private String offerName;

	private BigDecimal offerPrice;

	private String offerProductId;

	//bi-directional many-to-one association to MtOfferSvaReq
	private List<SvaList> svaList;

	public List<SvaList> getSvaList() {
		return svaList;
	}

	public void setSvaList(List<SvaList> svaList) {
		this.svaList = svaList;
	}

	public MtOfferLog() {
	}

	public Long getIdOffer() {
		return this.idOffer;
	}

	public void setIdOffer(Long idOffer) {
		this.idOffer = idOffer;
	}

	public String getOfferBillingId() {
		return this.offerBillingId;
	}

	public void setOfferBillingId(String offerBillingId) {
		this.offerBillingId = offerBillingId;
	}

	public String getOfferName() {
		return this.offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public BigDecimal getOfferPrice() {
		return this.offerPrice;
	}

	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}

	public String getOfferProductId() {
		return this.offerProductId;
	}

	public void setOfferProductId(String offerProductId) {
		this.offerProductId = offerProductId;
	}

	public List<SvaList> getMtOfferSvaReqs() {
		return this.svaList;
	}

	public void setMtOfferSvaReqs(List<SvaList> mtOfferSvaReqs) {
		this.svaList = mtOfferSvaReqs;
	}

	public SvaList addMtOfferSvaReq(SvaList svaList) {
		getMtOfferSvaReqs().add(svaList);

		return svaList;
	}

	public SvaList removeMtOfferSvaReq(SvaList svaList) {
		getMtOfferSvaReqs().remove(svaList);

		return svaList;
	}

}