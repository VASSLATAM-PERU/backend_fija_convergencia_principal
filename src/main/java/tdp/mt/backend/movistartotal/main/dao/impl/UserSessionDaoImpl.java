package tdp.mt.backend.movistartotal.main.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.dao.UserSessionDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;

@Repository
public class UserSessionDaoImpl implements UserSessionDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void invalidateOldTokens(String username, Date endDate) {

        try {
            Query query = em.createQuery("UPDATE UserSessionControl " +
                    "SET isactive = false, " +
                    "endDate = ?1 " +
                    "WHERE isactive = true AND username = ?2");
            query.setParameter(1,endDate);
            query.setParameter(2,username);
            int i = query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }
}
