package tdp.mt.backend.movistartotal.main.domain.MTFija;

import javax.persistence.Transient;

import tdp.mt.backend.movistartotal.main.restclient.automatizador.NormalizadorModelVO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the mt_sale_log database table.
 *
 */
public class FijaHDECRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idMtLog;

	private String clientAmdocsCustomerid;

	private String clientDocNumber;

	private String clientDocType;

	private String clientFirstName;
	private String clientLastName1;
	private String clientLastName2;
	private String clientEmail;
	private String clientCustomerPhone;
	private String clientCustomerPhone2;
	private Boolean hasPosibleOffers;
	private String mtOfferCompleteId;
	private Date offerSaleDate;
	private Date offerSaleInitDate;
	private String saleState;
	private String sellerAtisChannel;
	private String sellerAtisCode;
	private String sellerAtisEntity;
	private String sellerAtisPtoventa;
	private String sellerCmsCode;
	private String sellerDocument;
	private String sellerName;
	private String sellerJdvChannel;
	private String sellerJdvEntity;
	private String sellerJdvPtoventa;

	@Transient
	private String campaing;

	@Transient
	private String envioContrato;

	@Transient
	private String proteccionDatos;

	@Transient
	private String desafiliacionPB;

	@Transient
	private String publicacionPB;

	@Transient
	private String afiliacionFD;

	@Transient
	private String tieneGrabacion;

	@Transient
	private String idGrabacionNativo;

	@Transient
	private String debitoAutomatico;

	@Transient
	private String parentalProtection;

	@Transient
	private String metodoPago;

	@Transient
	private String montoContado;

	@Transient
	private String whatsapp;

	@Transient
	private String origen;
	//bi-directional many-to-one association to MtClientPackageReq
	private List<MtClientPackageReq> mtClientPackageReqs;
	//bi-directional many-to-one association to MtOfferLog
	private List<MtOfferLog> mtOfferLogs;

	private Long mtOrden;
	private String mtAMDOCSMovil1;
	private String mtAMDOCSMovil2;
	private String orderId;
	private String fileDate;

	//Campos adicionales
	private String incluyeEquipo1;
	private String incluyeEquipo2;
	private String numeroOrden1;
	private String numeroOrden2;
	private String operacionComercial1;
	private String operacionComercial2;
	private String planMovil;
	private String telefono1;
	private String telefono2;

	private String estadoMovil1;
	private String estadoMovil2;
	private String flag;

	private String campania;
	private String operacionComercial;
	private NormalizadorModelVO normalizador;
	

	public FijaHDECRequest() {
	}

	public Long getIdMtLog() {
		return this.idMtLog;
	}

	public void setIdMtLog(Long idMtLog) {
		this.idMtLog = idMtLog;
	}

	public String getClientAmdocsCustomerid() {
		return this.clientAmdocsCustomerid;
	}

	public void setClientAmdocsCustomerid(String clientAmdocsCustomerid) {
		this.clientAmdocsCustomerid = clientAmdocsCustomerid;
	}

	public String getClientDocNumber() {
		return this.clientDocNumber;
	}

	public void setClientDocNumber(String clientDocNumber) {
		this.clientDocNumber = clientDocNumber;
	}

	public String getClientDocType() {
		return this.clientDocType;
	}

	public void setClientDocType(String clientDocType) {
		this.clientDocType = clientDocType;
	}

	public Boolean getHasPosibleOffers() {
		return this.hasPosibleOffers;
	}

	public void setHasPosibleOffers(Boolean hasPosibleOffers) {
		this.hasPosibleOffers = hasPosibleOffers;
	}

	public String getMtOfferCompleteId() {
		return this.mtOfferCompleteId;
	}

	public void setMtOfferCompleteId(String mtOfferCompleteId) {
		this.mtOfferCompleteId = mtOfferCompleteId;
	}

	public Date getOfferSaleDate() {
		return this.offerSaleDate;
	}

	public void setOfferSaleDate(Date offerSaleDate) {
		this.offerSaleDate = offerSaleDate;
	}

	public Date getOfferSaleInitDate() {
		return this.offerSaleInitDate;
	}

	public void setOfferSaleInitDate(Date offerSaleInitDate) {
		this.offerSaleInitDate = offerSaleInitDate;
	}

	public String getSaleState() {
		return this.saleState;
	}

	public void setSaleState(String saleState) {
		this.saleState = saleState;
	}

	public String getSellerAtisChannel() {
		return this.sellerAtisChannel;
	}

	public void setSellerAtisChannel(String sellerAtisChannel) {
		this.sellerAtisChannel = sellerAtisChannel;
	}

	public String getSellerAtisCode() {
		return this.sellerAtisCode;
	}

	public void setSellerAtisCode(String sellerAtisCode) {
		this.sellerAtisCode = sellerAtisCode;
	}

	public String getSellerAtisEntity() {
		return this.sellerAtisEntity;
	}

	public void setSellerAtisEntity(String sellerAtisEntity) {
		this.sellerAtisEntity = sellerAtisEntity;
	}

	public String getSellerAtisPtoventa() {
		return this.sellerAtisPtoventa;
	}

	public void setSellerAtisPtoventa(String sellerAtisPtoventa) {
		this.sellerAtisPtoventa = sellerAtisPtoventa;
	}

	public String getSellerCmsCode() {
		return this.sellerCmsCode;
	}

	public void setSellerCmsCode(String sellerCmsCode) {
		this.sellerCmsCode = sellerCmsCode;
	}

	public String getSellerDocument() {
		return this.sellerDocument;
	}

	public void setSellerDocument(String sellerDocument) {
		this.sellerDocument = sellerDocument;
	}

	public String getSellerJdvChannel() {
		return this.sellerJdvChannel;
	}

	public void setSellerJdvChannel(String sellerJdvChannel) {
		this.sellerJdvChannel = sellerJdvChannel;
	}

	public String getSellerJdvEntity() {
		return this.sellerJdvEntity;
	}

	public void setSellerJdvEntity(String sellerJdvEntity) {
		this.sellerJdvEntity = sellerJdvEntity;
	}

	public String getSellerJdvPtoventa() {
		return this.sellerJdvPtoventa;
	}

	public void setSellerJdvPtoventa(String sellerJdvPtoventa) {
		this.sellerJdvPtoventa = sellerJdvPtoventa;
	}

	public List<MtClientPackageReq> getMtClientPackageReqs() {
		return this.mtClientPackageReqs;
	}

	public void setMtClientPackageReqs(List<MtClientPackageReq> mtClientPackageReqs) {
		this.mtClientPackageReqs = mtClientPackageReqs;
	}

	public MtClientPackageReq addMtClientPackageReq(MtClientPackageReq mtClientPackageLog) {
		getMtClientPackageReqs().add(mtClientPackageLog);

		return mtClientPackageLog;
	}

	public MtClientPackageReq removeMtClientPackageReq(MtClientPackageReq mtClientPackageLog) {
		getMtClientPackageReqs().remove(mtClientPackageLog);

		return mtClientPackageLog;
	}

	public List<MtOfferLog> getMtOfferLog() {
		return this.mtOfferLogs;
	}

	public void setMtOfferLogs(List<MtOfferLog> mtOfferLog) {
		this.mtOfferLogs = mtOfferLog;
	}

	public MtOfferLog addMtOfferLog(MtOfferLog mtOfferLog) {
		getMtOfferLog().add(mtOfferLog);

		return mtOfferLog;
	}

	public MtOfferLog removeMtOfferLog(MtOfferLog mtOfferLog) {
		getMtOfferLog().remove(mtOfferLog);

		return mtOfferLog;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getClientFirstName() {
		return clientFirstName;
	}

	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}

	public String getClientLastName1() {
		return clientLastName1;
	}

	public void setClientLastName1(String clientLastName1) {
		this.clientLastName1 = clientLastName1;
	}

	public String getClientLastName2() {
		return clientLastName2;
	}

	public void setClientLastName2(String clientLastName2) {
		this.clientLastName2 = clientLastName2;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientCustomerPhone() {
		return clientCustomerPhone;
	}

	public void setClientCustomerPhone(String clientCustomerPhone) {
		this.clientCustomerPhone = clientCustomerPhone;
	}

	public String getClientCustomerPhone2() {
		return clientCustomerPhone2;
	}

	public void setClientCustomerPhone2(String clientCustomerPhone2) {
		this.clientCustomerPhone2 = clientCustomerPhone2;
	}

	public String getCampaing() {
		return campaing;
	}

	public void setCampaing(String campaing) {
		this.campaing = campaing;
	}

	public String getEnvioContrato() {
		return envioContrato;
	}

	public void setEnvioContrato(String envioContrato) {
		this.envioContrato = envioContrato;
	}

	public String getProteccionDatos() {
		return proteccionDatos;
	}

	public void setProteccionDatos(String proteccionDatos) {
		this.proteccionDatos = proteccionDatos;
	}

	public String getDesafiliacionPB() {
		return desafiliacionPB;
	}

	public void setDesafiliacionPB(String desafiliacionPB) {
		this.desafiliacionPB = desafiliacionPB;
	}

	public String getAfiliacionFD() {
		return afiliacionFD;
	}

	public void setAfiliacionFD(String afiliacionFD) {
		this.afiliacionFD = afiliacionFD;
	}

	public String getTieneGrabacion() {
		return tieneGrabacion;
	}

	public void setTieneGrabacion(String tieneGrabacion) {
		this.tieneGrabacion = tieneGrabacion;
	}

	public String getIdGrabacionNativo() {
		return idGrabacionNativo;
	}

	public void setIdGrabacionNativo(String idGrabacionNativo) {
		this.idGrabacionNativo = idGrabacionNativo;
	}

	public String getDebitoAutomatico() {
		return debitoAutomatico;
	}

	public void setDebitoAutomatico(String debitoAutomatico) {
		this.debitoAutomatico = debitoAutomatico;
	}

	public String getParentalProtection() {
		return parentalProtection;
	}

	public void setParentalProtection(String parentalProtection) {
		this.parentalProtection = parentalProtection;
	}

	public String getPublicacionPB() {
		return publicacionPB;
	}

	public void setPublicacionPB(String publicacionPB) {
		this.publicacionPB = publicacionPB;
	}

	public String getMetodoPago() {
		return metodoPago;
	}

	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getMontoContado() {
		return montoContado;
	}

	public void setMontoContado(String montoContado) {
		this.montoContado = montoContado;
	}

	public Long getMtOrden() {
		return mtOrden;
	}

	public void setMtOrden(Long mtOrden) {
		this.mtOrden = mtOrden;
	}

	public String getMtAMDOCSMovil1() {
		return mtAMDOCSMovil1;
	}

	public void setMtAMDOCSMovil1(String mtAMDOCSMovil1) {
		this.mtAMDOCSMovil1 = mtAMDOCSMovil1;
	}

	public String getMtAMDOCSMovil2() {
		return mtAMDOCSMovil2;
	}

	public void setMtAMDOCSMovil2(String mtAMDOCSMovil2) {
		this.mtAMDOCSMovil2 = mtAMDOCSMovil2;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getFileDate() {
		return fileDate;
	}

	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}

	public String getIncluyeEquipo1() {
		return incluyeEquipo1;
	}

	public void setIncluyeEquipo1(String incluyeEquipo1) {
		this.incluyeEquipo1 = incluyeEquipo1;
	}

	public String getIncluyeEquipo2() {
		return incluyeEquipo2;
	}

	public void setIncluyeEquipo2(String incluyeEquipo2) {
		this.incluyeEquipo2 = incluyeEquipo2;
	}

	public String getNumeroOrden1() {
		return numeroOrden1;
	}

	public void setNumeroOrden1(String numeroOrden1) {
		this.numeroOrden1 = numeroOrden1;
	}

	public String getNumeroOrden2() {
		return numeroOrden2;
	}

	public void setNumeroOrden2(String numeroOrden2) {
		this.numeroOrden2 = numeroOrden2;
	}

	public String getOperacionComercial1() {
		return operacionComercial1;
	}

	public void setOperacionComercial1(String operacionComercial1) {
		this.operacionComercial1 = operacionComercial1;
	}

	public String getOperacionComercial2() {
		return operacionComercial2;
	}

	public void setOperacionComercial2(String operacionComercial2) {
		this.operacionComercial2 = operacionComercial2;
	}

	public String getPlanMovil() {
		return planMovil;
	}

	public void setPlanMovil(String planMovil) {
		this.planMovil = planMovil;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getEstadoMovil1() {
		return estadoMovil1;
	}

	public void setEstadoMovil1(String estadoMovil1) {
		this.estadoMovil1 = estadoMovil1;
	}

	public String getEstadoMovil2() {
		return estadoMovil2;
	}

	public void setEstadoMovil2(String estadoMovil2) {
		this.estadoMovil2 = estadoMovil2;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCampania() {
		return campania;
	}

	public void setCampania(String campania) {
		this.campania = campania;
	}

	public String getOperacionComercial() {
		return operacionComercial;
	}

	public void setOperacionComercial(String operacionComercial) {
		this.operacionComercial = operacionComercial;
	}

	public NormalizadorModelVO getNormalizador() {
		return normalizador;
	}

	public void setNormalizador(NormalizadorModelVO normalizador) {
		this.normalizador = normalizador;
	}
	
	
}