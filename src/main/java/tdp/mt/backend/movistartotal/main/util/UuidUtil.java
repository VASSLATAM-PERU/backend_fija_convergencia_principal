package tdp.mt.backend.movistartotal.main.util;


import java.util.GregorianCalendar;
import java.util.Random;
import java.util.UUID;

public class UuidUtil {
	
	/**
	 * Obtiene una cadena aleatoria alfanumerica de la longitud indicada
	 * @param longitud
	 * @return Cadena Aleatoria
	 * 
	 * @author TDP
	 */
	public static String getCadenaAlfanumAleatoria(int longitud) {
		String cadenaAleatoria = "";
		long milis=new GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		
		while (i < longitud) {
			char c = (char)r.nextInt(255);
			if ((c >= '0' && c <='9') || (c >='a' && c <='f')) {
				cadenaAleatoria += c;
				i++;
			}
		}
		
		return cadenaAleatoria;
	}
	
	/**
	 * Genera la cadena identificadora del campo execId.
	 * @return Cadena alfanumerica basado en formato 8-4-4-4-12
	 */
	public static String generarExecId() {
		String execid = "";
		execid = getCadenaAlfanumAleatoria(8) + "-" + getCadenaAlfanumAleatoria(4) + "-"+getCadenaAlfanumAleatoria(4)+"-" +
				getCadenaAlfanumAleatoria(4) + "-" + getCadenaAlfanumAleatoria(12);
		return execid;
	}
	
	/**
	 * Genera una cadena aleatoria de Uuid
	 * @return cadena alfanumerica
	 */
	public static String getUUIDasString() {
		UUID idOne = UUID.randomUUID();
		return idOne.toString();
	}
	
	public static String getUUIDfromLong(long value){
		byte[] by = Long.toString(value).getBytes();		
		UUID uid = UUID.nameUUIDFromBytes(by);
		return (uid.toString()).substring(0, 8);
	}




}
