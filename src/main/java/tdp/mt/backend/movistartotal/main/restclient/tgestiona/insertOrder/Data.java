package tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {

    @JsonProperty("codigoUnico")
    private String codigoUnico;
    @JsonProperty("campana")
    private String campana;
    @JsonProperty("nombreDeCliente")
    private String nombreDeCliente;
    @JsonProperty("apellidoPaterno")
    private String apellidoPaterno;
    @JsonProperty("apellidoMaterno")
    private String apellidoMaterno;
    @JsonProperty("tipoDeDocumento")
    private String tipoDeDocumento;
    @JsonProperty("numeroDeDocumento")
    private String numeroDeDocumento;
    @JsonProperty("telefonoDeContacto1")
    private String telefonoDeContacto1;
    @JsonProperty("telefonoDeContacto2")
    private String telefonoDeContacto2;
    @JsonProperty("email")
    private String email;
    @JsonProperty("envioDeContratos")
    private String envioDeContratos;
    @JsonProperty("proteccionDeDatos")
    private String proteccionDeDatos;
    @JsonProperty("canalDeVenta")
    private String canalDeVenta;
    @JsonProperty("detalleDeCanal")
    private String detalleDeCanal;
    @JsonProperty("nombreVendedor")
    private String nombreVendedor;
    @JsonProperty("codVendedorAtis")
    private String codVendedorAtis;
    @JsonProperty("codVendedorCms")
    private String codVendedorCms;
    @JsonProperty("zonalDepartamentoVendedor")
    private String zonalDepartamentoVendedor;
    @JsonProperty("regionProvinciaDistritoVendedor")
    private String regionProvinciaDistritoVendedor;
    @JsonProperty("tipoDeProducto")
    private String tipoDeProducto;
    @JsonProperty("subProducto")
    private String subProducto;
    @JsonProperty("departamento")
    private String departamento;
    @JsonProperty("provincia")
    private String provincia;
    @JsonProperty("distrito")
    private String distrito;
    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("operacionComercial")
    private String operacionComercial;
    @JsonProperty("telefonoAMigrar")
    private String telefonoAMigrar;
    @JsonProperty("desafiliacionDePaginasBlancas")
    private String desafiliacionDePaginasBlancas;
    @JsonProperty("afiliacionAFacturaDigital")
    private String afiliacionAFacturaDigital;
    @JsonProperty("tecnologiaDeInternet")
    private String tecnologiaDeInternet;
    @JsonProperty("codigoExperto")
    private String codigoExperto;
    @JsonProperty("tieneGrabacion")
    private String tieneGrabacion;
    @JsonProperty("fechaRegistro")
    private String fechaRegistro;
    @JsonProperty("horaRegistroWeb")
    private String horaRegistroWeb;
    @JsonProperty("modalidadDePago")
    private String modalidadDePago;
    @JsonProperty("idGrabacionNativo")
    private String idGrabacionNativo;
    @JsonProperty("tecnologiaTelevision")
    private String tecnologiaTelevision;
    @JsonProperty("paquetizacion")
    private String paquetizacion;
    @JsonProperty("altasTv")
    private String altasTv;
    @JsonProperty("tipoEquipamientoDeco")
    private String tipoEquipamientoDeco;
    @JsonProperty("dniVendedor")
    private String dniVendedor;
    @JsonProperty("telefonoOrigen")
    private String telefonoOrigen;
    @JsonProperty("tipoServicio")
    private String tipoServicio;
    @JsonProperty("clienteCms")
    private String clienteCms;
    @JsonProperty("distritoVendedor")
    private String distritoVendedor;
    @JsonProperty("adicionalDecoSmart")
    private String adicionalDecoSmart;
    @JsonProperty("decosHd")
    private String decosHd;
    @JsonProperty("decosDvr")
    private String decosDvr;
    @JsonProperty("bloqueTv")
    private String bloqueTv;
    @JsonProperty("svaInternet")
    private String svaInternet;
    @JsonProperty("svaLinea")
    private String svaLinea;
    @JsonProperty("descuentoWinback")
    private String descuentoWinback;
    @JsonProperty("codigoDeServicioCms")
    private String codigoDeServicioCms;
    @JsonProperty("coordenadasX")
    private String coordenadasX;
    @JsonProperty("coordenadasY")
    private String coordenadasY;
    @JsonProperty("webParental")
    private String webParental;
    @JsonProperty("montoContado")
    private String montoContado;
    @JsonProperty("codigoPostal")
    private String codigoPostal;
    @JsonProperty("publicarGuia")
    private String publicarGuia;
    @JsonProperty("repetidorSmartWifi")
    private Integer repetidorSmartWifi;
    @JsonProperty("modoVenta")
    private String modoVenta;
    @JsonProperty("nacionalidad")
    private String nacionalidad;
    @JsonProperty("modalidadContrato")
    private String modalidadContrato;
    @JsonProperty("idTransaccion")
    private String idTransaccion;
    @JsonProperty("recuperoVenta")
    private String recuperoVenta;
    @JsonProperty("fechaAgenda")
    private String fechaAgenda;
    @JsonProperty("rangoAgenda")
    private String rangoAgenda;
    @JsonProperty("tipoDocumentoRrll")
    private String tipoDocumentoRrll;
    @JsonProperty("numeroDocumentoRrll")
    private String numeroDocumentoRrll;
    @JsonProperty("nombreCompletoRrll")
    private String nombreCompletoRrll;
    @JsonProperty("codigoCip")
    private String codigoCip;
    @JsonProperty("campoAdicional12")
    private String campoAdicional12;
    @JsonProperty("campoAdicional13")
    private String campoAdicional13;
    @JsonProperty("campoAdicional14")
    private String campoAdicional14;
    @JsonProperty("campoAdicional15")
    private String campoAdicional15;
    @JsonProperty("campoAdicional16")
    private String campoAdicional16;
    @JsonProperty("campoAdicional17")
    private String campoAdicional17;
    @JsonProperty("campoAdicional18")
    private String campoAdicional18;
    @JsonProperty("campoAdicional19")
    private String campoAdicional19;
    @JsonProperty("campoAdicional20")
    private String campoAdicional20;
    @JsonProperty("estadoMovil1")
    private String estadoMovil1;
    @JsonProperty("estadoMovil2")
    private String estadoMovil2;
    @JsonProperty("flag")
    private Integer flag;




    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("codigoUnico")
    public String getCodigoUnico() {
        return codigoUnico;
    }

    @JsonProperty("codigoUnico")
    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    @JsonProperty("campana")
    public String getCampana() {
        return campana;
    }

    @JsonProperty("campana")
    public void setCampana(String campana) {
        this.campana = campana;
    }

    @JsonProperty("nombreDeCliente")
    public String getNombreDeCliente() {
        return nombreDeCliente;
    }

    @JsonProperty("nombreDeCliente")
    public void setNombreDeCliente(String nombreDeCliente) {
        this.nombreDeCliente = nombreDeCliente;
    }

    @JsonProperty("apellidoPaterno")
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    @JsonProperty("apellidoPaterno")
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    @JsonProperty("apellidoMaterno")
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    @JsonProperty("apellidoMaterno")
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @JsonProperty("tipoDeDocumento")
    public String getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    @JsonProperty("tipoDeDocumento")
    public void setTipoDeDocumento(String tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    @JsonProperty("numeroDeDocumento")
    public String getNumeroDeDocumento() {
        return numeroDeDocumento;
    }

    @JsonProperty("numeroDeDocumento")
    public void setNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
    }

    @JsonProperty("telefonoDeContacto1")
    public String getTelefonoDeContacto1() {
        return telefonoDeContacto1;
    }

    @JsonProperty("telefonoDeContacto1")
    public void setTelefonoDeContacto1(String telefonoDeContacto1) {
        this.telefonoDeContacto1 = telefonoDeContacto1;
    }

    @JsonProperty("telefonoDeContacto2")
    public String getTelefonoDeContacto2() {
        return telefonoDeContacto2;
    }

    @JsonProperty("telefonoDeContacto2")
    public void setTelefonoDeContacto2(String telefonoDeContacto2) {
        this.telefonoDeContacto2 = telefonoDeContacto2;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("envioDeContratos")
    public String getEnvioDeContratos() {
        return envioDeContratos;
    }

    @JsonProperty("envioDeContratos")
    public void setEnvioDeContratos(String envioDeContratos) {
        this.envioDeContratos = envioDeContratos;
    }

    @JsonProperty("proteccionDeDatos")
    public String getProteccionDeDatos() {
        return proteccionDeDatos;
    }

    @JsonProperty("proteccionDeDatos")
    public void setProteccionDeDatos(String proteccionDeDatos) {
        this.proteccionDeDatos = proteccionDeDatos;
    }

    @JsonProperty("canalDeVenta")
    public String getCanalDeVenta() {
        return canalDeVenta;
    }

    @JsonProperty("canalDeVenta")
    public void setCanalDeVenta(String canalDeVenta) {
        this.canalDeVenta = canalDeVenta;
    }

    @JsonProperty("detalleDeCanal")
    public String getDetalleDeCanal() {
        return detalleDeCanal;
    }

    @JsonProperty("detalleDeCanal")
    public void setDetalleDeCanal(String detalleDeCanal) {
        this.detalleDeCanal = detalleDeCanal;
    }

    @JsonProperty("nombreVendedor")
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    @JsonProperty("nombreVendedor")
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    @JsonProperty("codVendedorAtis")
    public String getCodVendedorAtis() {
        return codVendedorAtis;
    }

    @JsonProperty("codVendedorAtis")
    public void setCodVendedorAtis(String codVendedorAtis) {
        this.codVendedorAtis = codVendedorAtis;
    }

    @JsonProperty("codVendedorCms")
    public String getCodVendedorCms() {
        return codVendedorCms;
    }

    @JsonProperty("codVendedorCms")
    public void setCodVendedorCms(String codVendedorCms) {
        this.codVendedorCms = codVendedorCms;
    }

    @JsonProperty("zonalDepartamentoVendedor")
    public String getZonalDepartamentoVendedor() {
        return zonalDepartamentoVendedor;
    }

    @JsonProperty("zonalDepartamentoVendedor")
    public void setZonalDepartamentoVendedor(String zonalDepartamentoVendedor) {
        this.zonalDepartamentoVendedor = zonalDepartamentoVendedor;
    }

    @JsonProperty("regionProvinciaDistritoVendedor")
    public String getRegionProvinciaDistritoVendedor() {
        return regionProvinciaDistritoVendedor;
    }

    @JsonProperty("regionProvinciaDistritoVendedor")
    public void setRegionProvinciaDistritoVendedor(String regionProvinciaDistritoVendedor) {
        this.regionProvinciaDistritoVendedor = regionProvinciaDistritoVendedor;
    }

    @JsonProperty("tipoDeProducto")
    public String getTipoDeProducto() {
        return tipoDeProducto;
    }

    @JsonProperty("tipoDeProducto")
    public void setTipoDeProducto(String tipoDeProducto) {
        this.tipoDeProducto = tipoDeProducto;
    }

    @JsonProperty("subProducto")
    public String getSubProducto() {
        return subProducto;
    }

    @JsonProperty("subProducto")
    public void setSubProducto(String subProducto) {
        this.subProducto = subProducto;
    }

    @JsonProperty("departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("provincia")
    public String getProvincia() {
        return provincia;
    }

    @JsonProperty("provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @JsonProperty("distrito")
    public String getDistrito() {
        return distrito;
    }

    @JsonProperty("distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @JsonProperty("direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("operacionComercial")
    public String getOperacionComercial() {
        return operacionComercial;
    }

    @JsonProperty("operacionComercial")
    public void setOperacionComercial(String operacionComercial) {
        this.operacionComercial = operacionComercial;
    }

    @JsonProperty("telefonoAMigrar")
    public String getTelefonoAMigrar() {
        return telefonoAMigrar;
    }

    @JsonProperty("telefonoAMigrar")
    public void setTelefonoAMigrar(String telefonoAMigrar) {
        this.telefonoAMigrar = telefonoAMigrar;
    }

    @JsonProperty("desafiliacionDePaginasBlancas")
    public String getDesafiliacionDePaginasBlancas() {
        return desafiliacionDePaginasBlancas;
    }

    @JsonProperty("desafiliacionDePaginasBlancas")
    public void setDesafiliacionDePaginasBlancas(String desafiliacionDePaginasBlancas) {
        this.desafiliacionDePaginasBlancas = desafiliacionDePaginasBlancas;
    }

    @JsonProperty("afiliacionAFacturaDigital")
    public String getAfiliacionAFacturaDigital() {
        return afiliacionAFacturaDigital;
    }

    @JsonProperty("afiliacionAFacturaDigital")
    public void setAfiliacionAFacturaDigital(String afiliacionAFacturaDigital) {
        this.afiliacionAFacturaDigital = afiliacionAFacturaDigital;
    }

    @JsonProperty("tecnologiaDeInternet")
    public String getTecnologiaDeInternet() {
        return tecnologiaDeInternet;
    }

    @JsonProperty("tecnologiaDeInternet")
    public void setTecnologiaDeInternet(String tecnologiaDeInternet) {
        this.tecnologiaDeInternet = tecnologiaDeInternet;
    }

    @JsonProperty("codigoExperto")
    public String getCodigoExperto() {
        return codigoExperto;
    }

    @JsonProperty("codigoExperto")
    public void setCodigoExperto(String codigoExperto) {
        this.codigoExperto = codigoExperto;
    }

    @JsonProperty("tieneGrabacion")
    public String getTieneGrabacion() {
        return tieneGrabacion;
    }

    @JsonProperty("tieneGrabacion")
    public void setTieneGrabacion(String tieneGrabacion) {
        this.tieneGrabacion = tieneGrabacion;
    }

    @JsonProperty("fechaRegistro")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    @JsonProperty("fechaRegistro")
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @JsonProperty("horaRegistroWeb")
    public String getHoraRegistroWeb() {
        return horaRegistroWeb;
    }

    @JsonProperty("horaRegistroWeb")
    public void setHoraRegistroWeb(String horaRegistroWeb) {
        this.horaRegistroWeb = horaRegistroWeb;
    }

    @JsonProperty("modalidadDePago")
    public String getModalidadDePago() {
        return modalidadDePago;
    }

    @JsonProperty("modalidadDePago")
    public void setModalidadDePago(String modalidadDePago) {
        this.modalidadDePago = modalidadDePago;
    }

    @JsonProperty("idGrabacionNativo")
    public String getIdGrabacionNativo() {
        return idGrabacionNativo;
    }

    @JsonProperty("idGrabacionNativo")
    public void setIdGrabacionNativo(String idGrabacionNativo) {
        this.idGrabacionNativo = idGrabacionNativo;
    }

    @JsonProperty("tecnologiaTelevision")
    public String getTecnologiaTelevision() {
        return tecnologiaTelevision;
    }

    @JsonProperty("tecnologiaTelevision")
    public void setTecnologiaTelevision(String tecnologiaTelevision) {
        this.tecnologiaTelevision = tecnologiaTelevision;
    }

    @JsonProperty("paquetizacion")
    public String getPaquetizacion() {
        return paquetizacion;
    }

    @JsonProperty("paquetizacion")
    public void setPaquetizacion(String paquetizacion) {
        this.paquetizacion = paquetizacion;
    }

    @JsonProperty("altasTv")
    public String getAltasTv() {
        return altasTv;
    }

    @JsonProperty("altasTv")
    public void setAltasTv(String altasTv) {
        this.altasTv = altasTv;
    }

    @JsonProperty("tipoEquipamientoDeco")
    public String getTipoEquipamientoDeco() {
        return tipoEquipamientoDeco;
    }

    @JsonProperty("tipoEquipamientoDeco")
    public void setTipoEquipamientoDeco(String tipoEquipamientoDeco) {
        this.tipoEquipamientoDeco = tipoEquipamientoDeco;
    }

    @JsonProperty("dniVendedor")
    public String getDniVendedor() {
        return dniVendedor;
    }

    @JsonProperty("dniVendedor")
    public void setDniVendedor(String dniVendedor) {
        this.dniVendedor = dniVendedor;
    }

    @JsonProperty("telefonoOrigen")
    public String getTelefonoOrigen() {
        return telefonoOrigen;
    }

    @JsonProperty("telefonoOrigen")
    public void setTelefonoOrigen(String telefonoOrigen) {
        this.telefonoOrigen = telefonoOrigen;
    }

    @JsonProperty("tipoServicio")
    public String getTipoServicio() {
        return tipoServicio;
    }

    @JsonProperty("tipoServicio")
    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    @JsonProperty("clienteCms")
    public String getClienteCms() {
        return clienteCms;
    }

    @JsonProperty("clienteCms")
    public void setClienteCms(String clienteCms) {
        this.clienteCms = clienteCms;
    }

    @JsonProperty("distritoVendedor")
    public String getDistritoVendedor() {
        return distritoVendedor;
    }

    @JsonProperty("distritoVendedor")
    public void setDistritoVendedor(String distritoVendedor) {
        this.distritoVendedor = distritoVendedor;
    }

    @JsonProperty("adicionalDecoSmart")
    public String getAdicionalDecoSmart() {
        return adicionalDecoSmart;
    }

    @JsonProperty("adicionalDecoSmart")
    public void setAdicionalDecoSmart(String adicionalDecoSmart) {
        this.adicionalDecoSmart = adicionalDecoSmart;
    }

    @JsonProperty("decosHd")
    public String getDecosHd() {
        return decosHd;
    }

    @JsonProperty("decosHd")
    public void setDecosHd(String decosHd) {
        this.decosHd = decosHd;
    }

    @JsonProperty("decosDvr")
    public String getDecosDvr() {
        return decosDvr;
    }

    @JsonProperty("decosDvr")
    public void setDecosDvr(String decosDvr) {
        this.decosDvr = decosDvr;
    }

    @JsonProperty("bloqueTv")
    public String getBloqueTv() {
        return bloqueTv;
    }

    @JsonProperty("bloqueTv")
    public void setBloqueTv(String bloqueTv) {
        this.bloqueTv = bloqueTv;
    }

    @JsonProperty("svaInternet")
    public String getSvaInternet() {
        return svaInternet;
    }

    @JsonProperty("svaInternet")
    public void setSvaInternet(String svaInternet) {
        this.svaInternet = svaInternet;
    }

    @JsonProperty("svaLinea")
    public String getSvaLinea() {
        return svaLinea;
    }

    @JsonProperty("svaLinea")
    public void setSvaLinea(String svaLinea) {
        this.svaLinea = svaLinea;
    }

    @JsonProperty("descuentoWinback")
    public String getDescuentoWinback() {
        return descuentoWinback;
    }

    @JsonProperty("descuentoWinback")
    public void setDescuentoWinback(String descuentoWinback) {
        this.descuentoWinback = descuentoWinback;
    }

    @JsonProperty("codigoDeServicioCms")
    public String getCodigoDeServicioCms() {
        return codigoDeServicioCms;
    }

    @JsonProperty("codigoDeServicioCms")
    public void setCodigoDeServicioCms(String codigoDeServicioCms) {
        this.codigoDeServicioCms = codigoDeServicioCms;
    }

    @JsonProperty("coordenadasX")
    public String getCoordenadasX() {
        return coordenadasX;
    }

    @JsonProperty("coordenadasX")
    public void setCoordenadasX(String coordenadasX) {
        this.coordenadasX = coordenadasX;
    }

    @JsonProperty("coordenadasY")
    public String getCoordenadasY() {
        return coordenadasY;
    }

    @JsonProperty("coordenadasY")
    public void setCoordenadasY(String coordenadasY) {
        this.coordenadasY = coordenadasY;
    }

    @JsonProperty("webParental")
    public String getWebParental() {
        return webParental;
    }

    @JsonProperty("webParental")
    public void setWebParental(String webParental) {
        this.webParental = webParental;
    }

    @JsonProperty("montoContado")
    public String getMontoContado() {
        return montoContado;
    }

    @JsonProperty("montoContado")
    public void setMontoContado(String montoContado) {
        this.montoContado = montoContado;
    }

    @JsonProperty("codigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    @JsonProperty("codigoPostal")
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @JsonProperty("publicarGuia")
    public String getPublicarGuia() {
        return publicarGuia;
    }

    @JsonProperty("publicarGuia")
    public void setPublicarGuia(String publicarGuia) {
        this.publicarGuia = publicarGuia;
    }

    @JsonProperty("repetidorSmartWifi")
    public Integer getRepetidorSmartWifi() {
        return repetidorSmartWifi;
    }

    @JsonProperty("repetidorSmartWifi")
    public void setRepetidorSmartWifi(Integer repetidorSmartWifi) {
        this.repetidorSmartWifi = repetidorSmartWifi;
    }

    @JsonProperty("modoVenta")
    public String getModoVenta() {
        return modoVenta;
    }

    @JsonProperty("modoVenta")
    public void setModoVenta(String modoVenta) {
        this.modoVenta = modoVenta;
    }

    @JsonProperty("nacionalidad")
    public String getNacionalidad() {
        return nacionalidad;
    }

    @JsonProperty("nacionalidad")
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @JsonProperty("modalidadContrato")
    public String getModalidadContrato() {
        return modalidadContrato;
    }

    @JsonProperty("modalidadContrato")
    public void setModalidadContrato(String modalidadContrato) {
        this.modalidadContrato = modalidadContrato;
    }

    @JsonProperty("idTransaccion")
    public String getIdTransaccion() {
        return idTransaccion;
    }

    @JsonProperty("idTransaccion")
    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    @JsonProperty("recuperoVenta")
    public String getRecuperoVenta() {
        return recuperoVenta;
    }

    @JsonProperty("recuperoVenta")
    public void setRecuperoVenta(String recuperoVenta) {
        this.recuperoVenta = recuperoVenta;
    }

    @JsonProperty("fechaAgenda")
    public String getFechaAgenda() {
        return fechaAgenda;
    }

    @JsonProperty("fechaAgenda")
    public void setFechaAgenda(String fechaAgenda) {
        this.fechaAgenda = fechaAgenda;
    }

    @JsonProperty("rangoAgenda")
    public String getRangoAgenda() {
        return rangoAgenda;
    }

    @JsonProperty("rangoAgenda")
    public void setRangoAgenda(String rangoAgenda) {
        this.rangoAgenda = rangoAgenda;
    }

    @JsonProperty("tipoDocumentoRrll")
    public String getTipoDocumentoRrll() {
        return tipoDocumentoRrll;
    }

    @JsonProperty("tipoDocumentoRrll")
    public void setTipoDocumentoRrll(String tipoDocumentoRrll) {
        this.tipoDocumentoRrll = tipoDocumentoRrll;
    }

    @JsonProperty("numeroDocumentoRrll")
    public String getNumeroDocumentoRrll() {
        return numeroDocumentoRrll;
    }

    @JsonProperty("numeroDocumentoRrll")
    public void setNumeroDocumentoRrll(String numeroDocumentoRrll) {
        this.numeroDocumentoRrll = numeroDocumentoRrll;
    }

    @JsonProperty("nombreCompletoRrll")
    public String getNombreCompletoRrll() {
        return nombreCompletoRrll;
    }

    @JsonProperty("nombreCompletoRrll")
    public void setNombreCompletoRrll(String nombreCompletoRrll) {
        this.nombreCompletoRrll = nombreCompletoRrll;
    }

    @JsonProperty("codigoCip")
    public String getCodigoCip() {
        return codigoCip;
    }

    @JsonProperty("codigoCip")
    public void setCodigoCip(String codigoCip) {
        this.codigoCip = codigoCip;
    }

    @JsonProperty("campoAdicional12")
    public String getCampoAdicional12() {
        return campoAdicional12;
    }

    @JsonProperty("campoAdicional12")
    public void setCampoAdicional12(String campoAdicional12) {
        this.campoAdicional12 = campoAdicional12;
    }

    @JsonProperty("campoAdicional13")
    public String getCampoAdicional13() {
        return campoAdicional13;
    }

    @JsonProperty("campoAdicional13")
    public void setCampoAdicional13(String campoAdicional13) {
        this.campoAdicional13 = campoAdicional13;
    }

    @JsonProperty("campoAdicional14")
    public String getCampoAdicional14() {
        return campoAdicional14;
    }

    @JsonProperty("campoAdicional14")
    public void setCampoAdicional14(String campoAdicional14) {
        this.campoAdicional14 = campoAdicional14;
    }

    @JsonProperty("campoAdicional15")
    public String getCampoAdicional15() {
        return campoAdicional15;
    }

    @JsonProperty("campoAdicional15")
    public void setCampoAdicional15(String campoAdicional15) {
        this.campoAdicional15 = campoAdicional15;
    }

    @JsonProperty("campoAdicional16")
    public String getCampoAdicional16() {
        return campoAdicional16;
    }

    @JsonProperty("campoAdicional16")
    public void setCampoAdicional16(String campoAdicional16) {
        this.campoAdicional16 = campoAdicional16;
    }

    @JsonProperty("campoAdicional17")
    public String getCampoAdicional17() {
        return campoAdicional17;
    }

    @JsonProperty("campoAdicional17")
    public void setCampoAdicional17(String campoAdicional17) {
        this.campoAdicional17 = campoAdicional17;
    }

    @JsonProperty("campoAdicional18")
    public String getCampoAdicional18() {
        return campoAdicional18;
    }

    @JsonProperty("campoAdicional18")
    public void setCampoAdicional18(String campoAdicional18) {
        this.campoAdicional18 = campoAdicional18;
    }

    @JsonProperty("campoAdicional19")
    public String getCampoAdicional19() {
        return campoAdicional19;
    }

    @JsonProperty("campoAdicional19")
    public void setCampoAdicional19(String campoAdicional19) {
        this.campoAdicional19 = campoAdicional19;
    }

    @JsonProperty("campoAdicional20")
    public String getCampoAdicional20() {
        return campoAdicional20;
    }

    @JsonProperty("campoAdicional20")
    public void setCampoAdicional20(String campoAdicional20) {
        this.campoAdicional20 = campoAdicional20;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("estadoMovil1")
    public String getEstadoMovil1() {
        return estadoMovil1;
    }

    @JsonProperty("estadoMovil1")
    public void setEstadoMovil1(String estadoMovil1) {
        this.estadoMovil1 = estadoMovil1;
    }

    @JsonProperty("estadoMovil2")
    public String getEstadoMovil2() {
        return estadoMovil2;
    }

    @JsonProperty("estadoMovil2")
    public void setEstadoMovil2(String estadoMovil2) {
        this.estadoMovil2 = estadoMovil2;
    }

    @JsonProperty("flag")
    public Integer getFlag() {
        return flag;
    }

    @JsonProperty("flag")
    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}