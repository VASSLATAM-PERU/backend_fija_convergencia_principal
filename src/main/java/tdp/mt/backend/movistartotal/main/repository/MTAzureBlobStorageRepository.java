package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.entity.MTAzureBlobStorage;

import java.util.List;

@Repository
public interface MTAzureBlobStorageRepository extends JpaRepository<MTAzureBlobStorage, String> {
    @Transactional
    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_blob_storage(mtorder, estado_tdpvisor, codigo_pedido_tdpvisor, date_register, blob_date, file_storage) " +
            "SELECT b.mtorder, b.estado_tdpvisor, b.codigo_pedido_tdpvisor, now(), :blob_date, b.file_date FROM ibmx_a07e6d02edaf552.mt_azure_file_storage b " +
            "WHERE b.mtorder = :mtorder;", nativeQuery = true)
    void insertMTAzureBlobStorage(@Param("mtorder") String mtorder, @Param("blob_date") String blob_date);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update MTAzureBlobStorage m set m.estadoTdpVisor = ?1, m.codigoPedidoTdpVisor = ?2 where m.mtorder = ?3")
    void updateMTAzureBlobStorage(String estadoTdpVisor, String codigoPedidoTdpVisor, String mtorder);

    @Query("select m from MTAzureBlobStorage m where m.mtorder = ?1")
    MTAzureBlobStorage getMtOrderBlob(String mtorder);

    @Query(value = "select mtorder from ibmx_a07e6d02edaf552.mt_azure_blob_storage where blob_date = ?1", nativeQuery = true)
    List<String> getListMTContainer(String container);

    @Query(value = "SELECT " +
            "CONCAT(b.mtorder,b.file_extension) as ID_GRABACION," +
            "v.codigo_pedido as IDENTIFICADOR_UNICO," +
            "sa.canal as CANAL_VENTA_PROV," +
            //"cus.customerphone as NUMERO_CELULAR_PROV," +
            "v.telefono as NUMERO_CELULAR_PROV," +
            "(CASE WHEN o.migrationphone is null THEN 'VACIO' ELSE o.migrationphone END) as NUMERO_LINEA_PROV," +
            "to_char(v.fecha_grabacion,'DD/MM/YYYY') as FECHA_DOCUMENTO_PROV," +
            "o.campaign as NOMBRE_CAMPAÑA_PROV," +
            //"cus.docnumber as NUMERO_DOCUMENTO_PROV," +
            "v.dni as NUMERO_DOCUMENTO_PROV," +
            //"CONCAT(cus.firstname,' ',cus.lastname1) as NOMBRE_CLIENTE_PROV," +
            "v.cliente as NOMBRE_CLIENTE_PROV," +
            "upper(od.disablewhitepage) as DESAFILIACION_DE_PAGINAS_BLANCAS_PROV, " +
            "upper(od.enabledigitalinvoice) as AFILIACION_FACTURA_DIGITAL_PROV, " +
            "upper(od.dataprotection) as PROTECCION_DE_DATOS_PROV " +
            "FROM ibmx_a07e6d02edaf552.tdp_visor v " +
            "INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor " +
            "INNER JOIN ibmx_a07e6d02edaf552.tdp_sales_agent sa ON sa.codatis = o.userid " +
            //"INNER JOIN ibmx_a07e6d02edaf552.customer cus on cus.docnumber = v.dni " +
            "INNER JOIN ibmx_a07e6d02edaf552.order_detail od on od.orderid = o.id " +
            "INNER JOIN ibmx_a07e6d02edaf552.mt_azure_blob_storage b on b.mtorder = v.id_visor " +
            "WHERE b.blob_date= ?1 " +
            "ORDER BY v.fecha_grabacion asc", nativeQuery = true)
    List<Object[]> getFilesProccessAzureBlob(String container);

    @Query(value = "SELECT " +
            "CONCAT(b.mtorder,b.file_extension) as ID_GRABACION," +
            "v.codigo_pedido as IDENTIFICADOR_UNICO," +
            "sa.canal as CANAL_VENTA_PROV," +
            //"cus.customerphone as NUMERO_CELULAR_PROV," +
            "v.telefono as NUMERO_CELULAR_PROV," +
            "(CASE WHEN o.migrationphone is null THEN 'VACIO' ELSE o.migrationphone END) as NUMERO_LINEA_PROV," +
            "to_char(v.fecha_grabacion,'DD/MM/YYYY') as FECHA_DOCUMENTO_PROV," +
            "o.campaign as NOMBRE_CAMPAÑA_PROV," +
            //"cus.docnumber as NUMERO_DOCUMENTO_PROV," +
            "v.dni as NUMERO_DOCUMENTO_PROV," +
            //"CONCAT(cus.firstname,' ',cus.lastname1) as NOMBRE_CLIENTE_PROV," +
            "v.cliente as NOMBRE_CLIENTE_PROV," +
            "upper(od.disablewhitepage) as DESAFILIACION_DE_PAGINAS_BLANCAS_PROV, " +
            "upper(od.enabledigitalinvoice) as AFILIACION_FACTURA_DIGITAL_PROV, " +
            "upper(od.dataprotection) as PROTECCION_DE_DATOS_PROV " +
            "FROM ibmx_a07e6d02edaf552.tdp_visor v " +
            "INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor " +
            "INNER JOIN ibmx_a07e6d02edaf552.tdp_sales_agent sa ON sa.codatis = o.userid " +
            //"INNER JOIN ibmx_a07e6d02edaf552.customer cus on cus.docnumber = v.dni " +
            "INNER JOIN ibmx_a07e6d02edaf552.order_detail od on od.orderid = o.id " +
            "INNER JOIN ibmx_a07e6d02edaf552.mt_azure_blob_storage b on b.code_mt = v.id_visor " +
            "WHERE b.blob_date= ?1 and b.code_mt is not null and b.file_extension is not null " +
            "ORDER BY b.mtorder ", nativeQuery = true)
    List<Object[]> getPdfAndPngProccessAzureBlob(String container);
}
