package tdp.mt.backend.movistartotal.main.util;

public class ServiceConstants {

    public static final String SERVICE_SUCCESS = "0";
    public static final String DUPLICATE_SERVICE_SUCCESS = "00";
    public static final String SERVICE_ERROR = "1";
    public static final String SERVICE_WARNING = "2";
    public static final String SERVICE_INFO = "3";
    public static final String SERVICE_REJECTED = "4";

    public static final String API_REQUEST_HEADER_SESSION_CODE = "550e8400-e29b-41d4-a716-446655440003";
    public static final String API_REQUEST_ADDRESS = "169.54.245.69";

    //PORTABILITY STATE
    public static final String APPROVED = "APROBADO";
    public static final String REJECTED = "RECHAZADO";
    public static final String PENDING = "PENDIENTE";
    public static final String INVALID = "INVALIDO";

    //OFFER CONSTANTS

    public static final String PRODUCT_TYPE="mt";
    public static final String NEGOCIATION_TYPE="ventas";
    public static final String PRODUCT_FIJO="fijo";
    public static final String PRODUCT_FIJO_MT="mtf";
    public static final String COD_OPE_ALTA_PURA="ALTA PURA";
    public static final String PRODUCT_MOVIL="movil";
    public static final String PRODUCT_MOVIL_MT="mtm";
    public static final String PRODUCT_MOVIL_SALE_MODE="alta combo";

    public static final String OPERACION_NEW = "provide";
    public static final String OPERACION_CHANGE = "replaceOffer";
    public static final String OPERACION_SIM_CHANGE = "simChange";
    public static final String OPERACION_PORTABILITY = "portability";

    public static final String PRODUCT_MOVIL_ALTA_NUEVA_DEVICE_OPE="LineactivationProvide";
    public static final String PRODUCT_MOVIL_LINEA_EXISTENTE_DEVICE_OPE="DevicechangeCAEQ";
    public static final String PRODUCT_MOVIL_PORTABILIDAD_DEVICE_OPE="Portability";

    public static final String CREDIT_DATA_COM_OPER_NEW = "provide";
    public static final String CREDIT_DATA_COM_OPER_PORTA="portability";
    public static final String CREDIT_DATA_COM_OPER_REPLACE = "replaceOffer";

    public static final String QUERY_TYPE_BOTH="planesEquipos";
    public static final String QUERY_TYPE_OFFER="soloPlanes";
    public static final String QUERY_TYPE_DEVICE="soloEquipos";

    public static final String PRODUCT_TYPE_TRIO="Trío";
    public static final String PRODUCT_TYPE_DUO_INTERNET="Línea+Internet";
    public static final String PRODUCT_TYPE_DUO_TV="Línea+TV";
    public static final String PRODUCT_TYPE_MONO_INTERNET="Solo Internet";
    public static final String PRODUCT_TYPE_MONO_TV="Solo TV";
    public static final String PRODUCT_TYPE_MONO_FIJA="Solo Fijo";

    public static final String PUNTO=".";
    public static final String COMA=",";

    public static final String PRODUCT_TYPE_SUBSCRIPTION_TRIO="trio";
    public static final String PRODUCT_TYPE_SUBSCRIPTION_DUO_INTERNET="duoInternet";
    public static final String PRODUCT_TYPE_SUBSCRIPTION_DUO_TV="duoCable";
    public static final String PRODUCT_TYPE_SUBSCRIPTION_MONO_INTERNET="internet";
    public static final String PRODUCT_TYPE_SUBSCRIPTION_MONO_TV="cable";
    public static final String PRODUCT_TYPE_SUBSCRIPTION_MONO_TLF="vozFija";


    public static final String SI="SI";
    public static final String NO="NO";

    //VALIDACION ESTADOS
    public static final String DEUDA="deuda";
    public static final String ACTIVO="activo";
    public static final String OTROTITULAR="otroTitular";

    //SALELOG

    public static final String MT_SALE_STATE_ENVENTA="EN VENTA";
    public static final String MT_SALE_STATE_COMPLETO="COMPLETED";

    //COMMERCIAL OPERATION - CONVERGENCIA / FIJAHDEC

    //GESTION DE PLANTA ACTIONS
    public static final String MT_SAME_OFFER_ACTION="MANTIENE";
    public static final String MT_OTHER_OFFER_ACTION="MIGRAMT";

    //Fija Actions
    public static final String FIXED_ACTION_ALTA="ALTA";
    public static final String FIXED_ACTION_MIGRA="MIGRA";

    public static final String HDEC_ALTA="Alta Pura";
    public static final String HDEC_MIGRA="Migracion";

    //Movil Actions
    public static final String MOVIL_ACTION_ALTA="ALTA";
    public static final String MOVIL_ACTION_CAPL="CAPL";
    public static final String MOVIL_ACTION_PORTA="PORTA";
    public static final String MOVIL_ACTION_CAEQ="CAEQ";

    //Validacion MT en vuelo

    public static final String SIN_MT="00";
    public static final String FIJO_MT_CANCELADO="01";
    public static final String FIJO_MT_EN_VUELO="02";
    public static final String FIJO_MT="03";
    public static final String MT_PENDIENTE="04";


    //Estado TGESTIONA
    public static final String TGESTIONA_MT_PENDIENTE = "PENDIENTE";
    public static final String TGESTIONA_MT_ENVIANDO = "ENVIANDO";
    public static final String TGESTIONA_MT_CAIDA = "CAIDA";

    //CALENDAR

    public static final String PARAMETERS_VALUE_DAY = "DAY";
    public static final String PARAMETERS_VALUE_MONTH = "MONTH";

    //Status MOVIL
    public static final String MO_STATUS_REGISTRADO= "REGISTRADO";
    public static final String MO_STATUS_PENDIENTE= "PENDIENTE";
    public static final String MO_STATUS_RECHAZADO= "RECHAZADO";

    //Flag TGESTIONA
    public static final String TGESTIONA_1ER_ENVIO = "1";
    public static final String TGESTIONA_2DO_ENVIO = "2";

    //CAMPANIA TGESTIONA
    public static final String TGESTIONA_CAMPANIA_DIAS_MOVISTAR = "DIAS MOVISTAR";
    public static final String TGESTIONA_CAMPANIA_DIAS_MOVISTAR_PLANTA = "DIAS MOVISTAR PLANTA";
    public static final String TGESTIONA_CAMPANIA_MOVISTAR_TOTAL = "MOVISTAR TOTAL";

    //CANAL EQUIV CAMPANIA USUARIOS
    public static final String USER_CANAL_RETAIL = "RETAIL";
    public static final String MODE_RETAIL = "1";

    //Extensiones Files Azure
    public static final String EXTENSION_GSM = ".gsm";
    public static final String EXTENSION_PNG = ".png";
    public static final String EXTENSION_PDF = ".pdf";

    public static final String CUSTODIA_PDF = "_2";
    public static final String CUSTODIA_PNG = "_3";

    public static final String NOT_NULL = "";

    public static final int COUNT_FILES_RETAIL = 2;

    //Tipo Venta
    public static final String MT_CALL = "CALL";
    public static final String USER_CANAL_CALL = "OUT,IN,CROSS,ONLINE";
    
    
    public static final String INDICADOR_BATCH_REINTENTOS="ENVIANDO_AUT_MT";
    public static final String INDICADOR_NUM_REINTENTO="AUTO_MT";
    public static final String INDICADOR_LIMIT="CONFIG_AUT";
}
