package tdp.mt.backend.movistartotal.main.dao;

public interface MTAzureBlobStorageDao {

    void registerAzureBlobStorage(String mtorden, String container);

    void registerAzureBlobStorageRetail(String mtorden, String container);
}
