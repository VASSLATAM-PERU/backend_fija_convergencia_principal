package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FijaHDECResponseAuto {
	 @JsonProperty("code")
	    private String code;
	    @JsonProperty("message")
	    private String message;
	    @JsonProperty("estadoAutomatizador")
	    private String estadoAutomatizador;
 
	    @JsonIgnore
	    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	    public String getCode() {
	        return code;
	    }

	    public void setCode(String code) {
	        this.code = code;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }
	    public String getEstadoAutomatizador() {
			return estadoAutomatizador;
		}

		public void setEstadoAutomatizador(String estadoAutomatizador) {
			this.estadoAutomatizador = estadoAutomatizador;
		}

		public Map<String, Object> getAdditionalProperties() {
	        return additionalProperties;
	    }

	    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
	        this.additionalProperties = additionalProperties;
	    }
	}
