package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mt_azure_blob_storage", schema = "ibmx_a07e6d02edaf552") //dochoaro se adiciona schema
public class MTAzureBlobStorage {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "mtorder")
    private String mtorder;

    @Column(name = "estado_tdpvisor")
    private String estadoTdpVisor;

    @Column(name = "codigo_pedido_tdpvisor")
    private String codigoPedidoTdpVisor;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_register")
    private Date dateRegister;

    @Column(name = "blob_date")
    private String blobDate;

    @Column(name = "file_storage")
    private String fileStorage;

    @Column(name = "file_extension")
    private String fileExtension;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMtorder() {
        return mtorder;
    }

    public void setMtorder(String mtorder) {
        this.mtorder = mtorder;
    }

    public String getEstadoTdpVisor() {
        return estadoTdpVisor;
    }

    public void setEstadoTdpVisor(String estadoTdpVisor) {
        this.estadoTdpVisor = estadoTdpVisor;
    }

    public String getCodigoPedidoTdpVisor() {
        return codigoPedidoTdpVisor;
    }

    public void setCodigoPedidoTdpVisor(String codigoPedidoTdpVisor) {
        this.codigoPedidoTdpVisor = codigoPedidoTdpVisor;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getBlobDate() {
        return blobDate;
    }

    public void setBlobDate(String blobDate) {
        this.blobDate = blobDate;
    }

    public String getFileStorage() {
        return fileStorage;
    }

    public void setFileStorage(String fileStorage) {
        this.fileStorage = fileStorage;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }
}
