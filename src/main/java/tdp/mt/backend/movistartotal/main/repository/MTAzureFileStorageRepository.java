package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tdp.mt.backend.movistartotal.main.entity.MTAzureFileStorage;

import java.util.List;

@Repository
public interface MTAzureFileStorageRepository extends JpaRepository<MTAzureFileStorage, Long> {

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_file_storage(mtorder, " +
            "estado_tdpvisor, codigo_pedido_tdpvisor, date_register, file_date, file_extension, code_mt)" +
            "VALUES (:mtOrder, :estado_tdpvisor, :codigo_pedido_tdpvisor, now(), :file_date, :file_extension, :code_mt);", nativeQuery = true)
    @Transactional
    void insertMTAzureFileStorageRetail(@Param("mtOrder") String mtOrder,
                                  @Param("estado_tdpvisor") String estado_tdpvisor,
                                  @Param("codigo_pedido_tdpvisor") String codigo_pedido_tdpvisor,
                                  @Param("file_date") String file_date,
                                  @Param("file_extension") String file_extension,
                                  @Param("code_mt") String code_mt);

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_file_storage(mtorder, " +
            "estado_tdpvisor, codigo_pedido_tdpvisor, date_register, file_date, file_extension)" +
            "VALUES (:mtOrder, :estado_tdpvisor, :codigo_pedido_tdpvisor, now(), :file_date, :file_extension);", nativeQuery = true)
    @Transactional
    void insertMTAzureFileStorage(@Param("mtOrder") String mtOrder,
                                        @Param("estado_tdpvisor") String estado_tdpvisor,
                                        @Param("codigo_pedido_tdpvisor") String codigo_pedido_tdpvisor,
                                        @Param("file_date") String file_date,
                                        @Param("file_extension") String file_extension);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update MTAzureFileStorage m set m.estadoTdpVisor = ?1, m.codigoPedidoTdpVisor = ?2 where m.mtorder = ?3")
    void updateMTAzureFileStorage(String estadoTdpVisor, String codigoPedidoTdpVisor, String mtorder);

    @Query("select m from MTAzureFileStorage m where m.mtorder = ?1")
    MTAzureFileStorage getMtOrderFile(String mtorder);

    @Query("select f from MTAzureFileStorage f where f.codigoPedidoTdpVisor <> '' and f.fileExtension = '.gsm'")
    List<MTAzureFileStorage> getListFileStorageAudio();

    @Query("select f from MTAzureFileStorage f where f.codigoPedidoTdpVisor <> '' and f.fileExtension <> '.gsm' order by f.codeMt, f.mtorder")
    List<MTAzureFileStorage> getListFileStorageRetail();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from MTAzureFileStorage m where m.mtorder = ?1")
    void deleteMTAzureFileStorage(String mtorder);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from MTAzureFileStorage m where m.mtorder = ?1")
    void deleteMTAzureFileStorageRetail(String mtorder);
	
	@Query("select m from MTAzureFileStorage m where m.codeMt = ?1 AND m.fileExtension = ?2")
    MTAzureFileStorage getMtOrderFileByExtension(String mtorder, String extension);
}
