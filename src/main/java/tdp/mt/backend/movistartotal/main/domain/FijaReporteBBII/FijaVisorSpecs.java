package tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII;

public class FijaVisorSpecs {

    private String codigoPedido;
    private String estadoSolicitud;
    private String motivoEstado;
    private String visorUpdate;
    private String estadoAnterior;
    private String estadoAtis;
    private String idVisor;

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public FijaVisorSpecs(String codigoPedido, String estadoSolicitud, String motivoEstado, String visorUpdate, String estadoAnterior, String estadoAtis, String idVisor) {
        this.codigoPedido = codigoPedido;
        this.estadoSolicitud = estadoSolicitud;
        this.motivoEstado = motivoEstado;
        this.visorUpdate = visorUpdate;
        this.estadoAnterior = estadoAnterior;
        this.estadoAtis = estadoAtis;
        this.idVisor = idVisor;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getIdVisor() {
        return idVisor;
    }

    public void setIdVisor(String idVisor) {
        this.idVisor = idVisor;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public String getVisorUpdate() {
        return visorUpdate;
    }

    public void setVisorUpdate(String visorUpdate) {
        this.visorUpdate = visorUpdate;
    }

    public String getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public String getEstadoAtis() { return estadoAtis; }

    public void setEstadoAtis(String estadoAtis) { this.estadoAtis = estadoAtis; }
}
