package tdp.mt.backend.movistartotal.main.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Response<E> {
	@JsonInclude(Include.NON_NULL)
	private String responseCode;
	@JsonInclude(Include.NON_NULL)
	private String responseMessage;
	@JsonInclude(Include.NON_NULL)
	private E responseData;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public E getResponseData() {
		return responseData;
	}

	public void setResponseData(E responseData) {
		this.responseData = responseData;
	}

	@Override
	public String toString() {
		return "Response{" +
				"responseCode='" + responseCode + '\'' +
				", responseMessage='" + responseMessage + '\'' +
				", responseData=" + responseData +
				'}';
	}
}
