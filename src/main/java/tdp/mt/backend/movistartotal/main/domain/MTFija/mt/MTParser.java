package tdp.mt.backend.movistartotal.main.domain.MTFija.mt;

import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;

public interface MTParser<T> {
    T parse(FijaHDECRequest mtSaleLog);
}
