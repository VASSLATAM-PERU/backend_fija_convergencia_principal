package tdp.mt.backend.movistartotal.main.dao.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NormalizerRequest {
    @JsonProperty("ubigeo")
    private String ubigeo;
    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("distrito")
    private String distrito;
    @JsonProperty("provincia")
    private String provincia;
    @JsonProperty("id")
    private String id;

    @JsonProperty("ubigeo")
    public String getUbigeo() {
        return ubigeo;
    }

    @JsonProperty("ubigeo")
    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @JsonProperty("direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("distrito")
    public String getDistrito() {
        return distrito;
    }

    @JsonProperty("distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @JsonProperty("provincia")
    public String getProvincia() { return provincia; }

    @JsonProperty("provincia")
    public void setProvincia(String provincia) { this.provincia = provincia; }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }
}
