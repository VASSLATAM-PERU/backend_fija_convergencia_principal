package tdp.mt.backend.movistartotal.main.service;

import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.domain.Automatizador.EstadoSolicitudRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;

public interface AutomatizadorService {
	Response<FijaHDECResponse> saveAndSendAutomatizador(FijaHDECRequest request, String codigoAplicacion);
	public Boolean EliminarTdpVisor(String id);
	public String verificarSeviceAutomatizador();
}
