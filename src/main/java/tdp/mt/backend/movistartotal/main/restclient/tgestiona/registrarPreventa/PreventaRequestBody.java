package tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PreventaRequestBody {
    @JsonProperty("CodigoUnico")
    private String codigoUnico;
    @JsonProperty("Campana")
    private String campana;
    @JsonProperty("NombreDeCliente")
    private String nombreDeCliente;
    @JsonProperty("ApellidoPaterno")
    private String apellidoPaterno;
    @JsonProperty("ApellidoMaterno")
    private String apellidoMaterno;
    @JsonProperty("TipoDeDocumento")
    private String tipoDeDocumento;
    @JsonProperty("NumeroDeDocumento")
    private String numeroDeDocumento;
    @JsonProperty("TelefonoDeContacto1")
    private String telefonoDeContacto1;
    @JsonProperty("TelefonoDeContacto2")
    private String telefonoDeContacto2;
    @JsonProperty("Email")
    private String email;
    @JsonProperty("EnvioDeContratos")
    private String envioDeContratos;
    @JsonProperty("ProteccionDeDatos")
    private String proteccionDeDatos;
    @JsonProperty("CanalDeVenta")
    private String canalDeVenta;
    @JsonProperty("DetalleDeCanal")
    private String detalleDeCanal;
    @JsonProperty("NombreVendedor")
    private String nombreVendedor;
    @JsonProperty("CodVendedorAtis")
    private String codVendedorAtis;
    @JsonProperty("CodVendedorCms")
    private String codVendedorCms;
    @JsonProperty("ZonalDepartamentoVendedor")
    private String zonalDepartamentoVendedor;
    @JsonProperty("RegionProvinciaDistritoVendedor")
    private String regionProvinciaDistritoVendedor;
    @JsonProperty("TipoDeProducto")
    private String tipoDeProducto;
    @JsonProperty("SubProducto")
    private String subProducto;
    @JsonProperty("Departamento")
    private String departamento;
    @JsonProperty("Provincia")
    private String provincia;
    @JsonProperty("Distrito")
    private String distrito;
    @JsonProperty("Direccion")
    private String direccion;
    @JsonProperty("OperacionComercial")
    private String operacionComercial;
    @JsonProperty("TelefonoAMigrar")
    private String telefonoAMigrar;
    @JsonProperty("DesafiliacionDePaginasBlancas")
    private String desafiliacionDePaginasBlancas;
    @JsonProperty("AfiliacionAFacturaDigital")
    private String afiliacionAFacturaDigital;
    @JsonProperty("TecnologiaDeInternet")
    private String tecnologiaDeInternet;
    @JsonProperty("CodigoExperto")
    private String codigoExperto;
    @JsonProperty("TieneGrabacion")
    private String tieneGrabacion;
    @JsonProperty("FechaRegistro")
    private String fechaRegistro;
    @JsonProperty("HoraRegistroWeb")
    private String horaRegistroWeb;
    @JsonProperty("ModalidadDePago")
    private String modalidadDePago;
    @JsonProperty("IdGrabacionNativo")
    private String idGrabacionNativo;
    @JsonProperty("TecnologiaTelevision")
    private String tecnologiaTelevision;
    @JsonProperty("Paquetizacion")
    private String paquetizacion;
    @JsonProperty("AltasTv")
    private String altasTv;
    @JsonProperty("TipoEquipamientoDeco")
    private String tipoEquipamientoDeco;
    @JsonProperty("DniVendedor")
    private String dniVendedor;
    @JsonProperty("TelefonoOrigen")
    private String telefonoOrigen;
    @JsonProperty("TipoServicio")
    private String tipoServicio;
    @JsonProperty("ClienteCms")
    private String clienteCms;
    @JsonProperty("DistritoVendedor")
    private String distritoVendedor;
    @JsonProperty("DecosSd")
    private String decosSd;
    @JsonProperty("DecosHd")
    private String decosHd;
    @JsonProperty("DecosDvr")
    private String decosDvr;
    @JsonProperty("BloqueTv")
    private String bloqueTv;
    @JsonProperty("SvaInternet")
    private String svaInternet;
    @JsonProperty("SvaLinea")
    private String svaLinea;
    @JsonProperty("DescuentoWinback")
    private String descuentoWinback;
    @JsonProperty("CodigoDeServicioCms")
    private String codigoDeServicioCms;
    @JsonProperty("BloqueProducto")
    private String bloqueProducto;
    @JsonProperty("CoordenadasX")
    private String coordenadasX;
    @JsonProperty("CoordenadasY")
    private String coordenadasY;
    @JsonProperty("WebParental")
    private String webParental;
    @JsonProperty("MontoContado")
    private String montoContado;
    @JsonProperty("CodigoPostal")
    private String codigoPostal;
    @JsonProperty("PublicarGuia")
    private String publicarGuia;
    @JsonProperty("RepetidorSmartWifi")
    private Integer repetidorSmartWifi;
    @JsonProperty("ModoVenta")
    private String modoVenta;
    @JsonProperty("Nacionalidad")
    private String nacionalidad;
    @JsonProperty("TipoDocumentoRrll")
    private String tipoDocumentoRrll;
    @JsonProperty("NumeroDocumentoRrll")
    private String numeroDocumentoRrll;
    @JsonProperty("NombreCompletoRrll")
    private String nombreCompletoRrll;

    @JsonProperty("CodigoUnico")
    public String getCodigoUnico() {
        return codigoUnico;
    }

    @JsonProperty("CodigoUnico")
    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    @JsonProperty("Campana")
    public String getCampana() {
        return campana;
    }

    @JsonProperty("Campana")
    public void setCampana(String campana) {
        this.campana = campana;
    }

    @JsonProperty("NombreDeCliente")
    public String getNombreDeCliente() {
        return nombreDeCliente;
    }

    @JsonProperty("NombreDeCliente")
    public void setNombreDeCliente(String nombreDeCliente) {
        this.nombreDeCliente = nombreDeCliente;
    }

    @JsonProperty("ApellidoPaterno")
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    @JsonProperty("ApellidoPaterno")
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    @JsonProperty("ApellidoMaterno")
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    @JsonProperty("ApellidoMaterno")
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @JsonProperty("TipoDeDocumento")
    public String getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    @JsonProperty("TipoDeDocumento")
    public void setTipoDeDocumento(String tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    @JsonProperty("NumeroDeDocumento")
    public String getNumeroDeDocumento() {
        return numeroDeDocumento;
    }

    @JsonProperty("NumeroDeDocumento")
    public void setNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
    }

    @JsonProperty("TelefonoDeContacto1")
    public String getTelefonoDeContacto1() {
        return telefonoDeContacto1;
    }

    @JsonProperty("TelefonoDeContacto1")
    public void setTelefonoDeContacto1(String telefonoDeContacto1) {
        this.telefonoDeContacto1 = telefonoDeContacto1;
    }

    @JsonProperty("TelefonoDeContacto2")
    public String getTelefonoDeContacto2() {
        return telefonoDeContacto2;
    }

    @JsonProperty("TelefonoDeContacto2")
    public void setTelefonoDeContacto2(String telefonoDeContacto2) {
        this.telefonoDeContacto2 = telefonoDeContacto2;
    }

    @JsonProperty("Email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("Email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("EnvioDeContratos")
    public String getEnvioDeContratos() {
        return envioDeContratos;
    }

    @JsonProperty("EnvioDeContratos")
    public void setEnvioDeContratos(String envioDeContratos) {
        this.envioDeContratos = envioDeContratos;
    }

    @JsonProperty("ProteccionDeDatos")
    public String getProteccionDeDatos() {
        return proteccionDeDatos;
    }

    @JsonProperty("ProteccionDeDatos")
    public void setProteccionDeDatos(String proteccionDeDatos) {
        this.proteccionDeDatos = proteccionDeDatos;
    }

    @JsonProperty("CanalDeVenta")
    public String getCanalDeVenta() {
        return canalDeVenta;
    }

    @JsonProperty("CanalDeVenta")
    public void setCanalDeVenta(String canalDeVenta) {
        this.canalDeVenta = canalDeVenta;
    }

    @JsonProperty("DetalleDeCanal")
    public String getDetalleDeCanal() {
        return detalleDeCanal;
    }

    @JsonProperty("DetalleDeCanal")
    public void setDetalleDeCanal(String detalleDeCanal) {
        this.detalleDeCanal = detalleDeCanal;
    }

    @JsonProperty("NombreVendedor")
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    @JsonProperty("NombreVendedor")
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    @JsonProperty("CodVendedorAtis")
    public String getCodVendedorAtis() {
        return codVendedorAtis;
    }

    @JsonProperty("CodVendedorAtis")
    public void setCodVendedorAtis(String codVendedorAtis) {
        this.codVendedorAtis = codVendedorAtis;
    }

    @JsonProperty("CodVendedorCms")
    public String getCodVendedorCms() {
        return codVendedorCms;
    }

    @JsonProperty("CodVendedorCms")
    public void setCodVendedorCms(String codVendedorCms) {
        this.codVendedorCms = codVendedorCms;
    }

    @JsonProperty("ZonalDepartamentoVendedor")
    public String getZonalDepartamentoVendedor() {
        return zonalDepartamentoVendedor;
    }

    @JsonProperty("ZonalDepartamentoVendedor")
    public void setZonalDepartamentoVendedor(String zonalDepartamentoVendedor) {
        this.zonalDepartamentoVendedor = zonalDepartamentoVendedor;
    }

    @JsonProperty("RegionProvinciaDistritoVendedor")
    public String getRegionProvinciaDistritoVendedor() {
        return regionProvinciaDistritoVendedor;
    }

    @JsonProperty("RegionProvinciaDistritoVendedor")
    public void setRegionProvinciaDistritoVendedor(String regionProvinciaDistritoVendedor) {
        this.regionProvinciaDistritoVendedor = regionProvinciaDistritoVendedor;
    }

    @JsonProperty("TipoDeProducto")
    public String getTipoDeProducto() {
        return tipoDeProducto;
    }

    @JsonProperty("TipoDeProducto")
    public void setTipoDeProducto(String tipoDeProducto) {
        this.tipoDeProducto = tipoDeProducto;
    }

    @JsonProperty("SubProducto")
    public String getSubProducto() {
        return subProducto;
    }

    @JsonProperty("SubProducto")
    public void setSubProducto(String subProducto) {
        this.subProducto = subProducto;
    }

    @JsonProperty("Departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("Departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("Provincia")
    public String getProvincia() {
        return provincia;
    }

    @JsonProperty("Provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @JsonProperty("Distrito")
    public String getDistrito() {
        return distrito;
    }

    @JsonProperty("Distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @JsonProperty("Direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("Direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("OperacionComercial")
    public String getOperacionComercial() {
        return operacionComercial;
    }

    @JsonProperty("OperacionComercial")
    public void setOperacionComercial(String operacionComercial) {
        this.operacionComercial = operacionComercial;
    }

    @JsonProperty("TelefonoAMigrar")
    public String getTelefonoAMigrar() {
        return telefonoAMigrar;
    }

    @JsonProperty("TelefonoAMigrar")
    public void setTelefonoAMigrar(String telefonoAMigrar) {
        this.telefonoAMigrar = telefonoAMigrar;
    }

    @JsonProperty("DesafiliacionDePaginasBlancas")
    public String getDesafiliacionDePaginasBlancas() {
        return desafiliacionDePaginasBlancas;
    }

    @JsonProperty("DesafiliacionDePaginasBlancas")
    public void setDesafiliacionDePaginasBlancas(String desafiliacionDePaginasBlancas) {
        this.desafiliacionDePaginasBlancas = desafiliacionDePaginasBlancas;
    }

    @JsonProperty("AfiliacionAFacturaDigital")
    public String getAfiliacionAFacturaDigital() {
        return afiliacionAFacturaDigital;
    }

    @JsonProperty("AfiliacionAFacturaDigital")
    public void setAfiliacionAFacturaDigital(String afiliacionAFacturaDigital) {
        this.afiliacionAFacturaDigital = afiliacionAFacturaDigital;
    }

    @JsonProperty("TecnologiaDeInternet")
    public String getTecnologiaDeInternet() {
        return tecnologiaDeInternet;
    }

    @JsonProperty("TecnologiaDeInternet")
    public void setTecnologiaDeInternet(String tecnologiaDeInternet) {
        this.tecnologiaDeInternet = tecnologiaDeInternet;
    }

    @JsonProperty("CodigoExperto")
    public String getCodigoExperto() {
        return codigoExperto;
    }

    @JsonProperty("CodigoExperto")
    public void setCodigoExperto(String codigoExperto) {
        this.codigoExperto = codigoExperto;
    }

    @JsonProperty("TieneGrabacion")
    public String getTieneGrabacion() {
        return tieneGrabacion;
    }

    @JsonProperty("TieneGrabacion")
    public void setTieneGrabacion(String tieneGrabacion) {
        this.tieneGrabacion = tieneGrabacion;
    }

    @JsonProperty("FechaRegistro")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    @JsonProperty("FechaRegistro")
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @JsonProperty("HoraRegistroWeb")
    public String getHoraRegistroWeb() {
        return horaRegistroWeb;
    }

    @JsonProperty("HoraRegistroWeb")
    public void setHoraRegistroWeb(String horaRegistroWeb) {
        this.horaRegistroWeb = horaRegistroWeb;
    }

    @JsonProperty("ModalidadDePago")
    public String getModalidadDePago() {
        return modalidadDePago;
    }

    @JsonProperty("ModalidadDePago")
    public void setModalidadDePago(String modalidadDePago) {
        this.modalidadDePago = modalidadDePago;
    }

    @JsonProperty("IdGrabacionNativo")
    public String getIdGrabacionNativo() {
        return idGrabacionNativo;
    }

    @JsonProperty("IdGrabacionNativo")
    public void setIdGrabacionNativo(String idGrabacionNativo) {
        this.idGrabacionNativo = idGrabacionNativo;
    }

    @JsonProperty("TecnologiaTelevision")
    public String getTecnologiaTelevision() {
        return tecnologiaTelevision;
    }

    @JsonProperty("TecnologiaTelevision")
    public void setTecnologiaTelevision(String tecnologiaTelevision) {
        this.tecnologiaTelevision = tecnologiaTelevision;
    }

    @JsonProperty("Paquetizacion")
    public String getPaquetizacion() {
        return paquetizacion;
    }

    @JsonProperty("Paquetizacion")
    public void setPaquetizacion(String paquetizacion) {
        this.paquetizacion = paquetizacion;
    }

    @JsonProperty("AltasTv")
    public String getAltasTv() {
        return altasTv;
    }

    @JsonProperty("AltasTv")
    public void setAltasTv(String altasTv) {
        this.altasTv = altasTv;
    }

    @JsonProperty("TipoEquipamientoDeco")
    public String getTipoEquipamientoDeco() {
        return tipoEquipamientoDeco;
    }

    @JsonProperty("TipoEquipamientoDeco")
    public void setTipoEquipamientoDeco(String tipoEquipamientoDeco) {
        this.tipoEquipamientoDeco = tipoEquipamientoDeco;
    }

    @JsonProperty("DniVendedor")
    public String getDniVendedor() {
        return dniVendedor;
    }

    @JsonProperty("DniVendedor")
    public void setDniVendedor(String dniVendedor) {
        this.dniVendedor = dniVendedor;
    }

    @JsonProperty("TelefonoOrigen")
    public String getTelefonoOrigen() {
        return telefonoOrigen;
    }

    @JsonProperty("TelefonoOrigen")
    public void setTelefonoOrigen(String telefonoOrigen) {
        this.telefonoOrigen = telefonoOrigen;
    }

    @JsonProperty("TipoServicio")
    public String getTipoServicio() {
        return tipoServicio;
    }

    @JsonProperty("TipoServicio")
    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    @JsonProperty("ClienteCms")
    public String getClienteCms() {
        return clienteCms;
    }

    @JsonProperty("ClienteCms")
    public void setClienteCms(String clienteCms) {
        this.clienteCms = clienteCms;
    }

    @JsonProperty("DistritoVendedor")
    public String getDistritoVendedor() {
        return distritoVendedor;
    }

    @JsonProperty("DistritoVendedor")
    public void setDistritoVendedor(String distritoVendedor) {
        this.distritoVendedor = distritoVendedor;
    }

    @JsonProperty("DecosSd")
    public String getDecosSd() {
        return decosSd;
    }

    @JsonProperty("DecosSd")
    public void setDecosSd(String decosSd) {
        this.decosSd = decosSd;
    }

    @JsonProperty("DecosHd")
    public String getDecosHd() {
        return decosHd;
    }

    @JsonProperty("DecosHd")
    public void setDecosHd(String decosHd) {
        this.decosHd = decosHd;
    }

    @JsonProperty("DecosDvr")
    public String getDecosDvr() {
        return decosDvr;
    }

    @JsonProperty("DecosDvr")
    public void setDecosDvr(String decosDvr) {
        this.decosDvr = decosDvr;
    }

    @JsonProperty("BloqueTv")
    public String getBloqueTv() {
        return bloqueTv;
    }

    @JsonProperty("BloqueTv")
    public void setBloqueTv(String bloqueTv) {
        this.bloqueTv = bloqueTv;
    }

    @JsonProperty("SvaInternet")
    public String getSvaInternet() {
        return svaInternet;
    }

    @JsonProperty("SvaInternet")
    public void setSvaInternet(String svaInternet) {
        this.svaInternet = svaInternet;
    }

    @JsonProperty("SvaLinea")
    public String getSvaLinea() {
        return svaLinea;
    }

    @JsonProperty("SvaLinea")
    public void setSvaLinea(String svaLinea) {
        this.svaLinea = svaLinea;
    }

    @JsonProperty("DescuentoWinback")
    public String getDescuentoWinback() {
        return descuentoWinback;
    }

    @JsonProperty("DescuentoWinback")
    public void setDescuentoWinback(String descuentoWinback) {
        this.descuentoWinback = descuentoWinback;
    }

    @JsonProperty("CodigoDeServicioCms")
    public String getCodigoDeServicioCms() {
        return codigoDeServicioCms;
    }

    @JsonProperty("CodigoDeServicioCms")
    public void setCodigoDeServicioCms(String codigoDeServicioCms) {
        this.codigoDeServicioCms = codigoDeServicioCms;
    }

    @JsonProperty("BloqueProducto")
    public String getBloqueProducto() {
        return bloqueProducto;
    }

    @JsonProperty("BloqueProducto")
    public void setBloqueProducto(String bloqueProducto) {
        this.bloqueProducto = bloqueProducto;
    }

    @JsonProperty("CoordenadasX")
    public String getCoordenadasX() {
        return coordenadasX;
    }

    @JsonProperty("CoordenadasX")
    public void setCoordenadasX(String coordenadasX) {
        this.coordenadasX = coordenadasX;
    }

    @JsonProperty("CoordenadasY")
    public String getCoordenadasY() {
        return coordenadasY;
    }

    @JsonProperty("CoordenadasY")
    public void setCoordenadasY(String coordenadasY) {
        this.coordenadasY = coordenadasY;
    }

    @JsonProperty("WebParental")
    public String getWebParental() {
        return webParental;
    }

    @JsonProperty("WebParental")
    public void setWebParental(String webParental) {
        this.webParental = webParental;
    }

    @JsonProperty("MontoContado")
    public String getMontoContado() {
        return montoContado;
    }

    @JsonProperty("MontoContado")
    public void setMontoContado(String montoContado) {
        this.montoContado = montoContado;
    }

    @JsonProperty("CodigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    @JsonProperty("CodigoPostal")
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @JsonProperty("PublicarGuia")
    public String getPublicarGuia() {
        return publicarGuia;
    }

    @JsonProperty("PublicarGuia")
    public void setPublicarGuia(String publicarGuia) {
        this.publicarGuia = publicarGuia;
    }

    @JsonProperty("RepetidorSmartWifi")
    public Integer getRepetidorSmartWifi() {
        return repetidorSmartWifi;
    }

    @JsonProperty("RepetidorSmartWifi")
    public void setRepetidorSmartWifi(Integer repetidorSmartWifi) {
        this.repetidorSmartWifi = repetidorSmartWifi;
    }

    @JsonProperty("ModoVenta")
    public String getModoVenta() {
        return modoVenta;
    }

    @JsonProperty("ModoVenta")
    public void setModoVenta(String modoVenta) {
        this.modoVenta = modoVenta;
    }

    @JsonProperty("Nacionalidad")
    public String getNacionalidad() {
        return nacionalidad;
    }

    @JsonProperty("Nacionalidad")
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @JsonProperty("TipoDocumentoRrll")
    public String getTipoDocumentoRrll() {
        return tipoDocumentoRrll;
    }

    @JsonProperty("TipoDocumentoRrll")
    public void setTipoDocumentoRrll(String tipoDocumentoRrll) {
        this.tipoDocumentoRrll = tipoDocumentoRrll;
    }

    @JsonProperty("NumeroDocumentoRrll")
    public String getNumeroDocumentoRrll() {
        return numeroDocumentoRrll;
    }

    @JsonProperty("NumeroDocumentoRrll")
    public void setNumeroDocumentoRrll(String numeroDocumentoRrll) {
        this.numeroDocumentoRrll = numeroDocumentoRrll;
    }

    @JsonProperty("NombreCompletoRrll")
    public String getNombreCompletoRrll() {
        return nombreCompletoRrll;
    }

    @JsonProperty("NombreCompletoRrll")
    public void setNombreCompletoRrll(String nombreCompletoRrll) {
        this.nombreCompletoRrll = nombreCompletoRrll;
    }
}