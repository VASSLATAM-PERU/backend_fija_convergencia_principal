/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdp.mt.backend.movistartotal.main.dao;

import tdp.mt.backend.movistartotal.main.dto.Setting;

/**
 *
 * @author Jose Cordova
 */
public interface SettingDao {
    Setting byKey(String key);
}
