package tdp.mt.backend.movistartotal.main.util;

public class ProductConstants {

  public static final String SVA_RESPONSE_CODE_OK = "0";
  public static final String SVA_RESPONSE_CODE_ERROR = "-1";
  public static final String SVA_RESPONSE_MESSAGE = "No se encontraron registros";
  
  public static final String OFFERS_TIPO_SENAL_SIN_FFTT = "Sin FFTT";
 // public static final String OFFERS_API_AVVE_REQUEST_FLAG ="NO";
  public static final String OFFERS_API_AVVE_REQUEST_MAX_SPEED_FIX ="MB";
  public static final String OFFERS_API_EXPERTO_REQUEST_DOCUMENT_TYPE_DNI ="DNI";
  public static final String OFFERS_RESPONSE_CODE_OK = "0";
  public static final String OFFERS_RESPONSE_CODE_ERROR = "-1";

  public static String OFFERS_TIPO_SENAL(String tipoSenal) {
    if (tipoSenal.equals(OFFERS_TIPO_SENAL_SIN_FFTT)) {
      return null;
    } else {
      return tipoSenal;
    }
  }

    //GIS
    public static final String PARAMETRO_VENTA_FIJA_WEB="WEBVF";
    public static final String PARAMETRO_VENTA_FIJA_APP="APPVF";
    public static final String PARAMETRO_GIS_OFFER="GIS_PRODUCTO";
    public static final String PARAMETRO_GEOCODIFICAR_DIRECCION="GEOCODIFICAR_DIRECCION";
    public static final String PARAMETRO_GIS_TI="1";
    public static final String PARAMETRO_GIS_TTV="2";
    public static final String PARAMETRO_GIS_FLAG="F";
    public static final String PARAMETRO_GIS_VELOCIDAD="SPEED";
    public static final String PARAMETRO_GIS_SEÑAL="SIGNAL";
    public static final String PARAMETRO_GIS_SEÑAL_DIGITAL="DIGITAL";
    public static final String PARAMETRO_GIS_SEÑAL_ANALOGICO="ANALOGICO";
    public static final String PARAMETRO_GIS_OPERACION_ALTA_PURA="Alta Pura";
    public static final String PARAMETRO_GIS_OPERACION_MIGRACION="Migracion";
    public static final String PARAMETRO_GIS_NO_APLICA="NA";
    public static final String PARAMETRO_GIS_NINGUN_PRODUCTO="NN";

    public static final String PARAMETRO_SEGMENTO_RESIDENCIAL="RESIDENCIAL";
    public static final String PARAMETRO_SEGMENTO_NEGOCIOS="NEGOCIOS";
    public static final String PARAMETRO_FORMAPAGO_FINANCIADO="Financiado";
    public static final String PARAMETRO_FORMAPAGO_CONTADO="Contado";

    public static final String PARAMETRO_CANAL_PROACTIVO="Proactivo";
    public static final String PARAMETRO_CANAL_ONLINE="Online";
    public static final String PARAMETRO_CANAL_TIENDAS="Tienda";
    public static final String PARAMETRO_CANAL_IN="In";
    public static final String PARAMETRO_CANAL_CROSS="Cross";
    public static final String PARAMETRO_CANAL_OUT="out";

    public static final String PARAMETRO_CAMPANIA_EMBAJADOR="Embajador";
    public static final String PARAMETRO_CAMPANIA_CRECIMIENTO_VERTICAL="Crecimiento Vertical";
    public static final String PARAMETRO_CAMPANIA_DECOS="Decos Propiedad de Cliente";
    public static final String PARAMETRO_CAMPANIA_PLAZA="Plaza Vea";

    public static final String PARAMETRO_SPEED_1="120";
    public static final String PARAMETRO_SPEED_2="200";

    public static final String PARAMETRO_IT_HFC="HFC";
    public static final String PARAMETRO_IT_FTTH="FTTH";
    public static final String PARAMETRO_IT_ADSL="ADSL";

    public static final String PARAMETRO_TV_DIGITAL="DIGITAL";
    public static final String PARAMETRO_TV_SATELITAL="SATELITAL";

    public static final String PARAMETRO_PERIODO_1="3";

    public static final String PARAMETRO_PRODUCTO_MONOLINEA="Mono Linea";
    public static final String PARAMETRO_PRODUCTO_DUOS="Duo";
    public static final String PARAMETRO_PRODUCTO_TRIOS="Trio";
    public static final String PARAMETRO_PRODUCTO_MONO_TV="Mono TV";
    public static final String PARAMETRO_PRODUCTO_INTERNET_NAKED="Internet Naked";

    public static final String PARAMETRO_PRODUCTO_TIPO_ATIS="Atis";
    public static final String PARAMETRO_PRODUCTO_TIPO_CMS="Cms";

    public static final String PARAMETRO_BLOQUE_FULL_HD="Full HD";
    public static final String PARAMETRO_BLOQUE_HBO="Bloque HBO";
    public static final String PARAMETRO_BLOQUE_FOX="Bloque FOX";
    public static final String PARAMETRO_BLOQUE_DECO="Deco Smart HD";

    public static final String PARAMETRO_PROVINCIA_CONST_CALLAO="Prov. Const. del Callao";
    public static final String PARAMETRO_PROVINCIA_CALLAO="Callao";

    public static final String PARAMETRO_MONO_HOGAR_DIGITAL = "MHD";
    public static final String PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL="MTE";
    public static final String CODE_BLOQUE_TV = "BTV";
  public static final String CODE_BLOQUE_PRODUCTO = "BP";

}
