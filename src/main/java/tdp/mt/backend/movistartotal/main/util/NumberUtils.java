package tdp.mt.backend.movistartotal.main.util;

/**
 * Util functions for operating with numbers
 * @author jvilcayp
 *
 */
public class NumberUtils {

	/**
	 * function to validate if certain object is null or integer zero
	 * @param o object to be tested
	 * @return true when value is null or 0, false otherwise
	 */
	public static boolean isNullOrZero (Object o) {
		boolean result = false;
		if (o == null) 
			result = true;
		if (o instanceof Integer) {
			Integer i = (Integer) o;
			if (i == 0) {
				result = true;
			}
		}
		return result;
	}
	
	public static boolean isNumber (String number) {
		boolean result = false;
		if (number != null && number.matches("^\\s*-?[0-9]+\\s*$")) {
			result = true;
		}
		return result;
	}
}
