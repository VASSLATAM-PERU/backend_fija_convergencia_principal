package tdp.mt.backend.movistartotal.main.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.commonms.common.constant.ParametersConstants;
import tdp.mt.backend.movistartotal.commonms.common.util.*;
import tdp.mt.backend.movistartotal.commonservice.dao.ParametersRepository;
import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;
import tdp.mt.backend.movistartotal.commonservice.services.LegadoService;
import tdp.mt.backend.movistartotal.main.domain.FijaDatesBD.FijaDatesVendedorRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaReporteBBIIRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaReporteBBII.FijaVisorSpecs;
import tdp.mt.backend.movistartotal.main.domain.MTFija.MTOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.FijaOrderStatus.FijaOrderStatusRequest;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.entity.*;
import tdp.mt.backend.movistartotal.main.repository.*;
import tdp.mt.backend.movistartotal.main.service.OrderService;
import tdp.mt.backend.movistartotal.main.util.ServiceConstants;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    @Autowired
    private ParametersRepository parametersRepository;

    @Autowired
    private OrdenMTRepository ordenMTRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TdpVisorRepository tdpVisorRepository;

    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;

    @Autowired
    private LegadoService legadoService;


    @Override
    public Response<Map<String, String>> getMTOrderStatus(MTOrderStatusRequest request) {
        Response<Map<String, String>> response = new Response<>();

        Map<String, String> responseData = new HashMap<>();

        OrdenMT ordenMT = ordenMTRepository.findOne(request.getMtOrder());
        if (ordenMT != null) {
            Order orderFija = orderRepository.findOne(ordenMT.getIdFijaOrder());
            if (orderFija != null) {
                TdpVisor tdpVisor = tdpVisorRepository.findOne(ordenMT.getIdFijaOrder());
                if (tdpVisor != null) {
                    if (StringUtils.validString(tdpVisor.getEstadoSolicitud())) {
                        if (tdpVisor.getEstadoSolicitud().equalsIgnoreCase(CustomerConstants.ESTADO_SOLICITUD_CAIDA)) {
                            responseData.put("fijaCodeStatus", CustomerConstants.FIJO_MT_CANCELADO);
                        } else if (tdpVisor.getEstadoSolicitud().equalsIgnoreCase(CustomerConstants.ESTADO_SOLICITUD_INGRESADO)) {
                            responseData.put("fijaCodeStatus", CustomerConstants.FIJO_MT);
                        } else {
                            responseData.put("fijaCodeStatus", CustomerConstants.FIJO_MT_EN_VUELO);
                            responseData.put("fijaRegisterDate", DateUtils.format("yyyy-MM-dd HH:mm:ss", orderFija.getRegistrationDate()));
                        }
                    } else {
                        responseData.put("fijaCodeStatus", CustomerConstants.FIJO_MT_CANCELADO);
                    }
                } else {
                    responseData.put("fijaCodeStatus", CustomerConstants.FIJO_MT_CANCELADO);
                }
            } else {
                responseData.put("fijaCodeStatus", CustomerConstants.SIN_MT);
            }
        }

        response.setResponseData(responseData);
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        return response;
    }

    @Override
    public Response<Map<String, String>> getFijaOrder(FijaOrderStatusRequest request) {
        logger.info("Inicia service getFijaOrder");

        Response<Map<String, String>> response = new Response<>();
        Map<String, String> responseData = null;
        Date limitDateToValidate = null;

        List<Parameters> limitTypeData = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(
                ParametersConstants.PARAMETERS_DOMAIN_CHECK_IN_FLIGHT_ORDER_LIMIT,
                ParametersConstants.PARAMETERS_CATEGORY_CHECK_IN_FLIGHT_ORDER_LIMIT);

        if (UtilMethods.validList(limitTypeData)) {
            Parameters limitType = limitTypeData.get(0);
            Parameters limitDays = limitTypeData.get(1);
            Parameters limitMonths = limitTypeData.get(2);
            if (UtilMethods.validString(limitType.getStrValue())) {
                switch (limitType.getStrValue()) {
                    case ServiceConstants.PARAMETERS_VALUE_DAY:
                        limitDateToValidate = UtilMethods.sumRestCalendar(new Date(), 1, -Integer.parseInt(limitDays.getStrValue()));
                        break;
                    case ServiceConstants.PARAMETERS_VALUE_MONTH:
                        limitDateToValidate = UtilMethods.sumRestCalendar(new Date(), 2, -Integer.parseInt(limitMonths.getStrValue()));
                        break;
                    default:
                        limitDateToValidate = UtilMethods.sumRestCalendar(new Date(), 0, 30);
                }
            }
        }


        List<Object[]> data = tdpVisorRepository.findAllFijaOrders(request.getDocType(), request.getDocNumber(), limitDateToValidate);

        String idVisor = null;
        String estadoSolicitud = null;
        String nombreProducto = null;
        String subProducto = null;
        String fechaRegistro = null;
        String idOrder = null;
        String appCode = null;
        String motivoEstado = null;
        String codigoPedido = null;
        if (UtilMethods.validList(data)) {
            for (Object[] row : data) {
                idVisor = row[0] != null ? (String) row[0] : null;
                estadoSolicitud = row[1] != null ? (String) row[1] : null;
                nombreProducto = row[2] != null ? (String) row[2] : null;
                subProducto = row[3] != null ? (String) row[3] : null;
                fechaRegistro = row[4] != null ? (String) row[4] : null;
                idOrder = row[5] != null ? (String) row[5] : null;
                appCode = row[6] != null ? (String) row[6] : null;
                motivoEstado = row[7] != null ? (String) row[7] : null;
                codigoPedido = row[8] != null ? (String) row[8] : null;

                boolean skip = false, goLegados = false;
                if (UtilMethods.validString(estadoSolicitud)) {
                    switch (estadoSolicitud.trim().toUpperCase()) {
                        case "CAIDA":
                            skip = true;
                            break;
                        case "NO EFECTIVO":
                            skip = true;
                            break;
                        case "INGRESADO":
                            goLegados = true;
                            break;
                        default:
                            skip = false;
                    }
                } else if (UtilMethods.validString(motivoEstado)) {
                    if (!motivoEstado.trim().toUpperCase().equalsIgnoreCase("INGRESADO")) {
                        skip = true;
                    } else {
                        goLegados = true;
                    }
                }
                if (goLegados) {
                    Order order = orderRepository.findOne(idVisor);
                    String status = null;

                    if (order != null) {
                        if (order.getServiceType().equals("ATIS")) {
                            status = legadoService.getEstadoAtisCodeWithTry(codigoPedido);
                        } else {
                            status = legadoService.getEstadoCmsCodeWithTry(codigoPedido);
                        }
                    } else {
                        //FIXME
                        status = legadoService.getEstadoAtisCodeWithTry(codigoPedido);
                        if (UtilMethods.validString(status)) {
                            status = legadoService.getEstadoCmsCodeWithTry(codigoPedido);
                        }
                    }

                    logger.info("SE OBTUVO EL ESTADO: " + status);

                    if (UtilMethods.validString(status)) {
                        List<String> l_status = new ArrayList<>();
                        l_status.add("TE");
                        l_status.add("CG");
                        l_status.add("05");
                        l_status.add("04");
                        if (l_status.contains(status)) {
                            skip = true;//No bloquear
                        }
                    } else {
                        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                        response.setResponseMessage("No se pudo consultar el estado del Producto Fijo.");
                        skip = true;//No bloquear
                    }
                }
                if (skip) {
                    continue;
                } else {
                    if (idVisor != null) {
                        responseData = new HashMap<>();
                        responseData.put("idVisor", idVisor);
                        responseData.put("estadoSolicitud", estadoSolicitud);
                        responseData.put("nombreProducto", nombreProducto);
                        responseData.put("subProducto", subProducto);
                        responseData.put("fechaRegistro", fechaRegistro);
                        responseData.put("idOrder", idOrder);
                        responseData.put("appCode", appCode);
                        responseData.put("motivoEstado", motivoEstado);
                        break;
                    }
                }
            }
            if (responseData != null) {
                response.setResponseData(responseData);
                response.setResponseMessage("El cliente tiene ordenes fijas pendientes");
            } else {
                response.setResponseMessage("El cliente no tiene ordenes fijas pendientes");
            }
        } else {
            response.setResponseMessage("El cliente no tiene ordenes fijas registradas");
        }

        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        return response;
    }

    @Override
    public Response<Map<String, String>> getDateInFija(FijaDatesVendedorRequest request) {
        logger.info("Inicia service getDateInFija");
        Response<Map<String, String>> response = new Response<>();
        Map<String, String> responseData = new HashMap<>();
        TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(request.getCodatis());
        if (tdpSalesAgent == null) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("No se encontro usuario Atis");
            return response;
        }

        if (tdpSalesAgent.getCanalEquivalenciaCampania().equals("-")) {
            response.setResponseMessage(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Usuario no tiene un canal de equivalencia asignado.");
            return response;
        }

        boolean remoto = false;

        switch (tdpSalesAgent.getCanalEquivalenciaCampania()) {
            case "OUT":
                remoto = true;
                break;
            case "IN":
                remoto = true;
                break;
            case "CROSS":
                remoto = true;
                break;
            case "ONLINE":
                remoto = true;
                break;
        }

        response.setResponseCode(ServiceConstants.SERVICE_SUCCESS);
        responseData.put("remote", remoto ? "SI" : "NO");
        responseData.put("codigoPuntoVenta", tdpSalesAgent.getCodPuntoVenta());
        response.setResponseData(responseData);
        return response;
    }


    @Override
    public Response<Map<String, Map<String, String>>> getFijaDataReporteBBII(FijaReporteBBIIRequest request) {
       Response<Map<String, Map<String, String>>> response = null;
       if (UtilMethods.validList(request.getMtOrders())) {
            String status = null;
            String codigoPedido = null;
            String estado_solicitud = null;
            List<String> estado_Atis = new ArrayList<>();
            response = new Response<>();
            Map<String, Map<String, String>> responseData = new HashMap<>();
            for (String mtOrder : request.getMtOrders()) {
                List<Object[]> infoData = tdpVisorRepository.getVisorInfoReporteBBII(mtOrder);
                if (UtilMethods.validList(infoData)) {
                    for (Object[] row : infoData) {
                        Map<String, String> subResponseData = new HashMap<>();
                        subResponseData.put("codigoPedido", row[1] != null ? (String) row[1] : null);
                        subResponseData.put("estadoSolicitud", row[2] != null ? (String) row[2] : null);
                        subResponseData.put("motivoEstado", row[3] != null ? (String) row[3] : null);
                        subResponseData.put("visorUpdate", row[4] != null ? UtilMethods.getFormattedDate("yyyy-MM-dd", (Date) row[4]) : null);
                        subResponseData.put("estadoAnterior", row[5] != null ? (String) row[5] : null);

                        codigoPedido = row[1] != null ? (String) row[1] : null;
                        estado_solicitud = row[2] != null ? (String) row[2] : null;
                        if (codigoPedido!=null) {
                            if (estado_solicitud != null && estado_solicitud.equals("INGRESADO")) {
                               // status = legadoService.getEstadoAtisCodeWithTry(codigoPedido);
                                status = legadoService.getEstadoAtisCodePedido(codigoPedido);
                                subResponseData.put("estadoAtis", status);
                                estado_Atis.add(status);
                            }
                        }
                        responseData.put(row[0] != null ? (String) row[0] : null, subResponseData);
                    }

                }
            }

            response.setResponseCode(ServiceConstants.SERVICE_SUCCESS);
            response.setResponseData(responseData);
        } else {
            response.setResponseCode(ServiceConstants.SERVICE_ERROR);
            response.setResponseMessage("Error en lista de ordenes. Vacia o nula.");
        }



        return response;
    }

    @Override
    public void getFijaDataReporteBBIITest() {
        String status = null;
        String codigoPedido = null;
        String estadoSolicitud = null;
        String motivoEstado = null;
        String visorUpdate = null;
        String estadoAnterior = null;
        String idAtis = null;

        String estado_solicitud = null;
        List<String> estado_Atis = new ArrayList<>();
        List<String> mtOrde= new ArrayList<>();

        mtOrde.add("MT675814");
        mtOrde.add("MT675831");
        /*mtOrde.add("MT675945");
        mtOrde.add("MT675968");
        mtOrde.add("MT676013");
        mtOrde.add("MT676032");
        mtOrde.add("MT676035");
        mtOrde.add("MT676074");
        mtOrde.add("MT676088");
        mtOrde.add("MT676102");
        mtOrde.add("MT676164");
        mtOrde.add("MT676176");
        mtOrde.add("MT676220");
        mtOrde.add("MT676438");
        mtOrde.add("MT676496");
        mtOrde.add("MT676504");
        mtOrde.add("MT676592");
        mtOrde.add("MT676675");
        mtOrde.add("MT676713");
        mtOrde.add("MT676754");
        mtOrde.add("MT676868");
        mtOrde.add("MT677036");
        mtOrde.add("MT677056");
        mtOrde.add("MT677080");
        mtOrde.add("MT677132");
        mtOrde.add("MT677192");
        mtOrde.add("MT677351");
        mtOrde.add("MT677489");
        mtOrde.add("MT677620");
        mtOrde.add("MT677685");
        mtOrde.add("MT677720");
        mtOrde.add("MT677831");
        mtOrde.add("MT677926");
        mtOrde.add("MT677987");
        mtOrde.add("MT678014");
        mtOrde.add("MT678056");
        mtOrde.add("MT678060");
        mtOrde.add("MT678239");
        mtOrde.add("MT678242");
        mtOrde.add("MT678266");
        mtOrde.add("MT678293");
        mtOrde.add("MT678331");
        mtOrde.add("MT678472");
        mtOrde.add("MT678477");
        mtOrde.add("MT678737");
        mtOrde.add("MT678746");
        mtOrde.add("MT678770");
        mtOrde.add("MT679035");
        mtOrde.add("MT679117");
        mtOrde.add("MT679187");
        mtOrde.add("MT679234");
        mtOrde.add("MT679248");
        mtOrde.add("MT679305");
        mtOrde.add("MT679410");
        mtOrde.add("MT679524");
        mtOrde.add("MT679840");
        mtOrde.add("MT679879");
        mtOrde.add("MT679885");
        mtOrde.add("MT680013");
        mtOrde.add("MT680082");
        mtOrde.add("MT680104");
        mtOrde.add("MT680201");
        mtOrde.add("MT680291");
        mtOrde.add("MT680382");
        mtOrde.add("MT680536");
        mtOrde.add("MT680544");
        mtOrde.add("MT680668");
        mtOrde.add("MT680677");
        mtOrde.add("MT680735");
        mtOrde.add("MT680749");
        mtOrde.add("MT680769");
        mtOrde.add("MT680899");
        mtOrde.add("MT681007");
        mtOrde.add("MT681063");
        mtOrde.add("MT681068");
        mtOrde.add("MT681095");
        mtOrde.add("MT681109");
        mtOrde.add("MT681188");
        mtOrde.add("MT681201");
        mtOrde.add("MT681213");
        mtOrde.add("MT681222");
        mtOrde.add("MT681280");
        mtOrde.add("MT681286");
        mtOrde.add("MT681347");
        mtOrde.add("MT681355");
        mtOrde.add("MT681356");
        mtOrde.add("MT681380");
        mtOrde.add("MT681408");
        mtOrde.add("MT681411");
        mtOrde.add("MT681428");
        mtOrde.add("MT681440");
        mtOrde.add("MT681442");
        mtOrde.add("MT681586");
        mtOrde.add("MT681598");
        mtOrde.add("MT681619");
        mtOrde.add("MT681627");
        mtOrde.add("MT681631");
        mtOrde.add("MT681646");
        mtOrde.add("MT681647");
        mtOrde.add("MT681658");
        mtOrde.add("MT681696");
        mtOrde.add("MT681707");
        mtOrde.add("MT681722");
        mtOrde.add("MT681731");
        mtOrde.add("MT681759");
        mtOrde.add("MT681763");
        mtOrde.add("MT681770");
        mtOrde.add("MT681806");
        mtOrde.add("MT681809");
        mtOrde.add("MT681824");
        mtOrde.add("MT681834");
        mtOrde.add("MT681843");
        mtOrde.add("MT681866");
        mtOrde.add("MT681868");
        mtOrde.add("MT681875");
        mtOrde.add("MT681917");
        mtOrde.add("MT681929");
        mtOrde.add("MT681931");
        mtOrde.add("MT681958");
        mtOrde.add("MT681972");
        mtOrde.add("MT681978");
        mtOrde.add("MT681994");
        mtOrde.add("MT681995");
        mtOrde.add("MT682002");
        mtOrde.add("MT682100");
        mtOrde.add("MT682104");
        mtOrde.add("MT682178");
        mtOrde.add("MT682180");
        mtOrde.add("MT682204");
        mtOrde.add("MT682222");
        mtOrde.add("MT682229");
        mtOrde.add("MT682248");
        mtOrde.add("MT682255");
        mtOrde.add("MT682257");
        mtOrde.add("MT682260");
        mtOrde.add("MT682280");
        mtOrde.add("MT682295");
        mtOrde.add("MT682305");
        mtOrde.add("MT682306");
        mtOrde.add("MT682321");
        mtOrde.add("MT682335");
        mtOrde.add("MT682349");
        mtOrde.add("MT682356");
        mtOrde.add("MT682358");
        mtOrde.add("MT682360");
        mtOrde.add("MT682363");
        mtOrde.add("MT682373");
        mtOrde.add("MT682383");
        mtOrde.add("MT682384");
        mtOrde.add("MT682389");
        mtOrde.add("MT682430");
        mtOrde.add("MT682458");
        mtOrde.add("MT682487");
        mtOrde.add("MT682491");
        mtOrde.add("MT682511");
        mtOrde.add("MT682527");
        mtOrde.add("MT682558");
        mtOrde.add("MT682564");
        mtOrde.add("MT682565");
        mtOrde.add("MT682592");
        mtOrde.add("MT682602");
        mtOrde.add("MT682611");
        mtOrde.add("MT682636");
        mtOrde.add("MT682654");
        mtOrde.add("MT682661");
        mtOrde.add("MT682676");
        mtOrde.add("MT682683");
        mtOrde.add("MT682700");
        mtOrde.add("MT682758");
        mtOrde.add("MT682766");
        mtOrde.add("MT682801");
        mtOrde.add("MT682810");
        mtOrde.add("MT682826");
        mtOrde.add("MT682830");
        mtOrde.add("MT682836");
        mtOrde.add("MT682847");
        mtOrde.add("MT682858");
        mtOrde.add("MT682883");
        mtOrde.add("MT682887");
        mtOrde.add("MT682907");
        mtOrde.add("MT682909");
        mtOrde.add("MT682923");
        mtOrde.add("MT682938");
        mtOrde.add("MT682940");
        mtOrde.add("MT682949");
        mtOrde.add("MT682952");
        mtOrde.add("MT682965");
        mtOrde.add("MT682979");
        mtOrde.add("MT682994");
        mtOrde.add("MT683019");
        mtOrde.add("MT683032");
        mtOrde.add("MT683067");
        mtOrde.add("MT683080");
        mtOrde.add("MT683082");
        mtOrde.add("MT683090");
        mtOrde.add("MT683093");
        mtOrde.add("MT683147");
        mtOrde.add("MT683169");
        mtOrde.add("MT683174");
        mtOrde.add("MT683199");
        mtOrde.add("MT683209");
        mtOrde.add("MT683216");
        mtOrde.add("MT683225");
        mtOrde.add("MT683228");
        mtOrde.add("MT683258");
        mtOrde.add("MT683264");
        mtOrde.add("MT683267");
        mtOrde.add("MT683270");
        mtOrde.add("MT683280");
        mtOrde.add("MT683291");
        mtOrde.add("MT683311");
        mtOrde.add("MT683329");
        mtOrde.add("MT683372");
        mtOrde.add("MT683386");
        mtOrde.add("MT683390");
        mtOrde.add("MT683391");
        mtOrde.add("MT683395");
        mtOrde.add("MT683403");
        mtOrde.add("MT683429");
        mtOrde.add("MT683430");
        mtOrde.add("MT683446");
        mtOrde.add("MT683448");
        mtOrde.add("MT683452");
        mtOrde.add("MT683454");
        mtOrde.add("MT683456");
        mtOrde.add("MT683472");
        mtOrde.add("MT683499");
        mtOrde.add("MT683500");
        mtOrde.add("MT683545");
        mtOrde.add("MT683556");
        mtOrde.add("MT683575");
        mtOrde.add("MT683583");
        mtOrde.add("MT683609");
        mtOrde.add("MT683647");
        mtOrde.add("MT683664");
        mtOrde.add("MT683732");
        mtOrde.add("MT683758");
        mtOrde.add("MT683763");
        mtOrde.add("MT683765");
        mtOrde.add("MT683776");
        mtOrde.add("MT683780");
        mtOrde.add("MT683790");
        mtOrde.add("MT683791");
        mtOrde.add("MT683813");
        mtOrde.add("MT683821");
        mtOrde.add("MT683835");
        mtOrde.add("MT683846");
        mtOrde.add("MT683856");
        mtOrde.add("MT683871");
        mtOrde.add("MT683885");
        mtOrde.add("MT683908");
        mtOrde.add("MT683912");
        mtOrde.add("MT683947");
        mtOrde.add("MT684012");
        mtOrde.add("MT684014");
        mtOrde.add("MT684037");
        mtOrde.add("MT684042");
        mtOrde.add("MT684053");
        mtOrde.add("MT684101");
        mtOrde.add("MT684119");
        mtOrde.add("MT684152");
        mtOrde.add("MT684187");
        mtOrde.add("MT684210");
        mtOrde.add("MT684271");
        mtOrde.add("MT684276");
        mtOrde.add("MT684323");
        mtOrde.add("MT684367");
        mtOrde.add("MT684378");
        mtOrde.add("MT684382");
        mtOrde.add("MT684410");
        mtOrde.add("MT684411");
        mtOrde.add("MT684438");
        mtOrde.add("MT684452");
        mtOrde.add("MT684457");
        mtOrde.add("MT684467");
        mtOrde.add("MT684498");
        mtOrde.add("MT684516");
        mtOrde.add("MT684524");
        mtOrde.add("MT684549");
        mtOrde.add("MT684559");
        mtOrde.add("MT684576");
        mtOrde.add("MT684580");
        mtOrde.add("MT684626");
        mtOrde.add("MT684650");
        mtOrde.add("MT684656");
        mtOrde.add("MT684660");
        mtOrde.add("MT684681");
        mtOrde.add("MT684682");
        mtOrde.add("MT684732");
        mtOrde.add("MT684759");
        mtOrde.add("MT684764");
        mtOrde.add("MT684784");
        mtOrde.add("MT684788");
        mtOrde.add("MT684796");
        mtOrde.add("MT684820");
        mtOrde.add("MT684824");
        mtOrde.add("MT684832");
        mtOrde.add("MT684868");
        mtOrde.add("MT684890");
        mtOrde.add("MT684915");
        mtOrde.add("MT684918");
        mtOrde.add("MT684931");
        mtOrde.add("MT684942");
        mtOrde.add("MT684946");
        mtOrde.add("MT684977");
        mtOrde.add("MT684984");
        mtOrde.add("MT685000");
        mtOrde.add("MT685002");
        mtOrde.add("MT685013");
        mtOrde.add("MT685025");
        mtOrde.add("MT685050");
        mtOrde.add("MT685051");
        mtOrde.add("MT685052");
        mtOrde.add("MT685067");
        mtOrde.add("MT685087");
        mtOrde.add("MT685095");
        mtOrde.add("MT685099");
        mtOrde.add("MT685105");
        mtOrde.add("MT685155");
        mtOrde.add("MT685205");
        mtOrde.add("MT685231");
        mtOrde.add("MT685258");
        mtOrde.add("MT685263");
        mtOrde.add("MT685311");
        mtOrde.add("MT685372");
        mtOrde.add("MT685390");
        mtOrde.add("MT685395");
        mtOrde.add("MT685412");
        mtOrde.add("MT685417");
        mtOrde.add("MT685418");
        mtOrde.add("MT685432");
        mtOrde.add("MT685449");
        mtOrde.add("MT685456");
        mtOrde.add("MT685464");
        mtOrde.add("MT685472");
        mtOrde.add("MT685494");
        mtOrde.add("MT685507");
        mtOrde.add("MT685511");
        mtOrde.add("MT685518");
        mtOrde.add("MT685519");
        mtOrde.add("MT685524");
        mtOrde.add("MT685525");
        mtOrde.add("MT685543");
        mtOrde.add("MT685547");
        mtOrde.add("MT685572");
        mtOrde.add("MT685588");
        mtOrde.add("MT685603");
        mtOrde.add("MT685633");
        mtOrde.add("MT685645");
        mtOrde.add("MT685648");
        mtOrde.add("MT685672");
        mtOrde.add("MT685676");
        mtOrde.add("MT685705");
        mtOrde.add("MT685744");
        mtOrde.add("MT685769");
        mtOrde.add("MT685773");
        mtOrde.add("MT685776");
        mtOrde.add("MT685787");
        mtOrde.add("MT685799");
        mtOrde.add("MT685800");
        mtOrde.add("MT685801");
        mtOrde.add("MT685842");
        mtOrde.add("MT685917");
        mtOrde.add("MT685927");
        mtOrde.add("MT685945");
        mtOrde.add("MT685999");
        mtOrde.add("MT686007");
        mtOrde.add("MT686059");
        mtOrde.add("MT686071");
        mtOrde.add("MT686105");
        mtOrde.add("MT686119");
        mtOrde.add("MT686129");
        mtOrde.add("MT686145");
        mtOrde.add("MT686158");
        mtOrde.add("MT686176");
        mtOrde.add("MT686217");
        mtOrde.add("MT686222");
        mtOrde.add("MT686244");
        mtOrde.add("MT686283");
        mtOrde.add("MT686352");
        mtOrde.add("MT686397");
        mtOrde.add("MT686408");
        mtOrde.add("MT686411");
        mtOrde.add("MT686419");
        mtOrde.add("MT686575");
        mtOrde.add("MT686598");
        mtOrde.add("MT686646");
        mtOrde.add("MT652961");
        mtOrde.add("MT653133");
        mtOrde.add("MT653134");
        mtOrde.add("MT653239");
        mtOrde.add("MT653257");
        mtOrde.add("MT653680");
        mtOrde.add("MT653707");
        mtOrde.add("MT653760");
        mtOrde.add("MT653935");
        mtOrde.add("MT654039");
        mtOrde.add("MT654222");
        mtOrde.add("MT654329");
        mtOrde.add("MT654403");
        mtOrde.add("MT654694");
        mtOrde.add("MT655032");
        mtOrde.add("MT655130");
        mtOrde.add("MT655184");
        mtOrde.add("MT655251");
        mtOrde.add("MT655465");
        mtOrde.add("MT655628");
        mtOrde.add("MT655773");
        mtOrde.add("MT655878");
        mtOrde.add("MT655920");
        mtOrde.add("MT655945");
        mtOrde.add("MT656167");
        mtOrde.add("MT656275");
        mtOrde.add("MT656408");
        mtOrde.add("MT656425");
        mtOrde.add("MT656590");
        mtOrde.add("MT656880");
        mtOrde.add("MT656922");
        mtOrde.add("MT656926");
        mtOrde.add("MT657083");
        mtOrde.add("MT657148");
        mtOrde.add("MT657208");
        mtOrde.add("MT657359");
        mtOrde.add("MT657399");
        mtOrde.add("MT657548");
        mtOrde.add("MT657553");
        mtOrde.add("MT657766");
        mtOrde.add("MT657901");
        mtOrde.add("MT657902");
        mtOrde.add("MT658102");
        mtOrde.add("MT658180");
        mtOrde.add("MT658192");
        mtOrde.add("MT658412");
        mtOrde.add("MT658707");
        mtOrde.add("MT658804");
        mtOrde.add("MT659056");
        mtOrde.add("MT659237");
        mtOrde.add("MT659296");
        mtOrde.add("MT659337");
        mtOrde.add("MT659516");
        mtOrde.add("MT659594");
        mtOrde.add("MT659697");
        mtOrde.add("MT659714");
        mtOrde.add("MT659722");
        mtOrde.add("MT659725");
        mtOrde.add("MT659741");
        mtOrde.add("MT660001");
        mtOrde.add("MT660198");
        mtOrde.add("MT660272");
        mtOrde.add("MT660282");
        mtOrde.add("MT660422");
        mtOrde.add("MT660436");
        mtOrde.add("MT660752");
        mtOrde.add("MT660787");
        mtOrde.add("MT660838");
        mtOrde.add("MT660894");
        mtOrde.add("MT661120");
        mtOrde.add("MT661226");
        mtOrde.add("MT661265");
        mtOrde.add("MT661495");
        mtOrde.add("MT661544");
        mtOrde.add("MT661718");
        mtOrde.add("MT661791");
        mtOrde.add("MT661987");
        mtOrde.add("MT662153");
        mtOrde.add("MT662246");
        mtOrde.add("MT662338");
        mtOrde.add("MT662366");
        mtOrde.add("MT662396");
        mtOrde.add("MT662418");
        mtOrde.add("MT662450");
        mtOrde.add("MT662630");
        mtOrde.add("MT662661");
        mtOrde.add("MT662704");
        mtOrde.add("MT663024");
        mtOrde.add("MT663049");
        mtOrde.add("MT663100");
        mtOrde.add("MT663276");
        mtOrde.add("MT663320");
        mtOrde.add("MT663349");
        mtOrde.add("MT663459");
        mtOrde.add("MT663565");
        mtOrde.add("MT663670");
        mtOrde.add("MT663933");
        mtOrde.add("MT664081");
        mtOrde.add("MT664191");
        mtOrde.add("MT664559");
        mtOrde.add("MT664578");
        mtOrde.add("MT664881");
        mtOrde.add("MT665190");
        mtOrde.add("MT665225");
        mtOrde.add("MT665298");
        mtOrde.add("MT665487");
        mtOrde.add("MT665493");
        mtOrde.add("MT665570");
        mtOrde.add("MT665666");
        mtOrde.add("MT665728");
        mtOrde.add("MT665762");
        mtOrde.add("MT666029");
        mtOrde.add("MT666038");
        mtOrde.add("MT666066");
        mtOrde.add("MT666217");
        mtOrde.add("MT666244");
        mtOrde.add("MT666339");
        mtOrde.add("MT666350");
        mtOrde.add("MT666412");
        mtOrde.add("MT666433");
        mtOrde.add("MT666633");
        mtOrde.add("MT666646");
        mtOrde.add("MT666702");
        mtOrde.add("MT666776");
        mtOrde.add("MT666879");
        mtOrde.add("MT666924");
        mtOrde.add("MT666933");
        mtOrde.add("MT666998");
        mtOrde.add("MT667031");
        mtOrde.add("MT667288");
        mtOrde.add("MT667416");
        mtOrde.add("MT667466");
        mtOrde.add("MT667788");
        mtOrde.add("MT667797");
        mtOrde.add("MT667987");
        mtOrde.add("MT668121");
        mtOrde.add("MT668233");
        mtOrde.add("MT668260");
        mtOrde.add("MT668299");
        mtOrde.add("MT668531");
        mtOrde.add("MT668612");
        mtOrde.add("MT668634");
        mtOrde.add("MT668650");
        mtOrde.add("MT668685");
        mtOrde.add("MT668782");
        mtOrde.add("MT668969");
        mtOrde.add("MT669040");
        mtOrde.add("MT669294");
        mtOrde.add("MT669392");
        mtOrde.add("MT669484");
        mtOrde.add("MT670080");
        mtOrde.add("MT670248");
        mtOrde.add("MT670420");
        mtOrde.add("MT670479");
        mtOrde.add("MT670492");
        mtOrde.add("MT670792");
        mtOrde.add("MT670829");
        mtOrde.add("MT670851");
        mtOrde.add("MT670939");
        mtOrde.add("MT670942");
        mtOrde.add("MT670969");
        mtOrde.add("MT671115");
        mtOrde.add("MT671318");
        mtOrde.add("MT671319");
        mtOrde.add("MT671614");
        mtOrde.add("MT671831");
        mtOrde.add("MT671845");
        mtOrde.add("MT671997");
        mtOrde.add("MT672040");
        mtOrde.add("MT672053");
        mtOrde.add("MT672095");
        mtOrde.add("MT672229");
        mtOrde.add("MT672254");
        mtOrde.add("MT672260");
        mtOrde.add("MT672567");
        mtOrde.add("MT672598");
        mtOrde.add("MT672662");
        mtOrde.add("MT672682");
        mtOrde.add("MT672693");
        mtOrde.add("MT672758");
        mtOrde.add("MT672887");
        mtOrde.add("MT672915");
        mtOrde.add("MT672920");
        mtOrde.add("MT673038");
        mtOrde.add("MT673079");
        mtOrde.add("MT673152");
        mtOrde.add("MT673218");
        mtOrde.add("MT673420");
        mtOrde.add("MT673424");
        mtOrde.add("MT673603");
        mtOrde.add("MT673605");
        mtOrde.add("MT673670");
        mtOrde.add("MT673697");
        mtOrde.add("MT673730");
        mtOrde.add("MT673752");
        mtOrde.add("MT673762");
        mtOrde.add("MT673810");
        mtOrde.add("MT673830");
        mtOrde.add("MT673845");
        mtOrde.add("MT674120");
        mtOrde.add("MT674153");
        mtOrde.add("MT674217");
        mtOrde.add("MT674238");
        mtOrde.add("MT674289");
        mtOrde.add("MT674295");
        mtOrde.add("MT674386");
        mtOrde.add("MT674435");
        mtOrde.add("MT674449");
        mtOrde.add("MT674450");
        mtOrde.add("MT674459");
        mtOrde.add("MT674503");
        mtOrde.add("MT674508");
        mtOrde.add("MT674514");
        mtOrde.add("MT674532");
        mtOrde.add("MT674598");
        mtOrde.add("MT674624");
        mtOrde.add("MT674641");
        mtOrde.add("MT674703");
        mtOrde.add("MT674777");
        mtOrde.add("MT674825");
        mtOrde.add("MT674895");
        mtOrde.add("MT674906");
        mtOrde.add("MT674955");
        mtOrde.add("MT675011");
        mtOrde.add("MT675027");
        mtOrde.add("MT675036");
        mtOrde.add("MT675058");
        mtOrde.add("MT675095");
        mtOrde.add("MT675099");
        mtOrde.add("MT675142");
        mtOrde.add("MT675145");
        mtOrde.add("MT675187");
        mtOrde.add("MT675219");
        mtOrde.add("MT675264");
        mtOrde.add("MT675289");
        mtOrde.add("MT675318");
        mtOrde.add("MT675352");
        mtOrde.add("MT675368");
        mtOrde.add("MT675375");
        mtOrde.add("MT675387");
        mtOrde.add("MT675432");
        mtOrde.add("MT675492");
        mtOrde.add("MT675505");
        mtOrde.add("MT675518");
        mtOrde.add("MT675546");
        mtOrde.add("MT675559");
        mtOrde.add("MT675577");
        mtOrde.add("MT675587");
        mtOrde.add("MT675590");
        mtOrde.add("MT675612");
        mtOrde.add("MT675653");
        mtOrde.add("MT675663");
        mtOrde.add("MT628802");
        mtOrde.add("MT628946");
        mtOrde.add("MT628950");
        mtOrde.add("MT628955");
        mtOrde.add("MT628983");
        mtOrde.add("MT629015");
        mtOrde.add("MT629074");
        mtOrde.add("MT629212");
        mtOrde.add("MT629324");
        mtOrde.add("MT629355");
        mtOrde.add("MT629423");
        mtOrde.add("MT629556");
        mtOrde.add("MT629734");
        mtOrde.add("MT629851");
        mtOrde.add("MT629858");
        mtOrde.add("MT630086");
        mtOrde.add("MT630218");
        mtOrde.add("MT630240");
        mtOrde.add("MT630391");
        mtOrde.add("MT630469");
        mtOrde.add("MT630682");
        mtOrde.add("MT630719");
        mtOrde.add("MT631314");
        mtOrde.add("MT631332");
        mtOrde.add("MT631436");
        mtOrde.add("MT631441");
        mtOrde.add("MT631719");
        mtOrde.add("MT631728");
        mtOrde.add("MT631899");
        mtOrde.add("MT632099");
        mtOrde.add("MT632184");
        mtOrde.add("MT632224");
        mtOrde.add("MT632586");
        mtOrde.add("MT632737");
        mtOrde.add("MT632889");
        mtOrde.add("MT632971");
        mtOrde.add("MT633081");
        mtOrde.add("MT633584");
        mtOrde.add("MT633618");
        mtOrde.add("MT633643");
        mtOrde.add("MT633738");
        mtOrde.add("MT634051");
        mtOrde.add("MT634557");
        mtOrde.add("MT634581");
        mtOrde.add("MT634750");
        mtOrde.add("MT634789");
        mtOrde.add("MT635316");
        mtOrde.add("MT635555");
        mtOrde.add("MT635580");
        mtOrde.add("MT635727");
        mtOrde.add("MT635780");
        mtOrde.add("MT635849");
        mtOrde.add("MT636091");
        mtOrde.add("MT636335");
        mtOrde.add("MT636487");
        mtOrde.add("MT636502");
        mtOrde.add("MT636749");
        mtOrde.add("MT636806");
        mtOrde.add("MT637316");
        mtOrde.add("MT637340");
        mtOrde.add("MT637481");
        mtOrde.add("MT637784");
        mtOrde.add("MT637796");
        mtOrde.add("MT637864");
        mtOrde.add("MT637874");
        mtOrde.add("MT638042");
        mtOrde.add("MT638143");
        mtOrde.add("MT638387");
        mtOrde.add("MT638587");
        mtOrde.add("MT638628");
        mtOrde.add("MT638758");
        mtOrde.add("MT638984");
        mtOrde.add("MT639041");
        mtOrde.add("MT639079");
        mtOrde.add("MT639155");
        mtOrde.add("MT639197");
        mtOrde.add("MT639270");
        mtOrde.add("MT639317");
        mtOrde.add("MT639431");
        mtOrde.add("MT639845");
        mtOrde.add("MT639851");
        mtOrde.add("MT640262");
        mtOrde.add("MT640272");
        mtOrde.add("MT640292");
        mtOrde.add("MT640385");
        mtOrde.add("MT640511");
        mtOrde.add("MT640602");
        mtOrde.add("MT640735");
        mtOrde.add("MT640825");
        mtOrde.add("MT640874");
        mtOrde.add("MT641241");
        mtOrde.add("MT641446");
        mtOrde.add("MT641648");
        mtOrde.add("MT641719");
        mtOrde.add("MT641866");
        mtOrde.add("MT641988");
        mtOrde.add("MT642352");
        mtOrde.add("MT642462");
        mtOrde.add("MT642867");
        mtOrde.add("MT643049");
        mtOrde.add("MT643053");
        mtOrde.add("MT643213");
        mtOrde.add("MT643233");
        mtOrde.add("MT643421");
        mtOrde.add("MT643457");
        mtOrde.add("MT643926");
        mtOrde.add("MT644163");
        mtOrde.add("MT644173");
        mtOrde.add("MT644372");
        mtOrde.add("MT644420");
        mtOrde.add("MT644434");
        mtOrde.add("MT644452");
        mtOrde.add("MT644472");
        mtOrde.add("MT644613");
        mtOrde.add("MT644746");
        mtOrde.add("MT644814");
        mtOrde.add("MT644911");
        mtOrde.add("MT645233");
        mtOrde.add("MT645260");
        mtOrde.add("MT645306");
        mtOrde.add("MT645371");
        mtOrde.add("MT645380");
        mtOrde.add("MT645383");
        mtOrde.add("MT645511");
        mtOrde.add("MT645893");
        mtOrde.add("MT646050");
        mtOrde.add("MT646481");
        mtOrde.add("MT646583");
        mtOrde.add("MT646590");
        mtOrde.add("MT646699");
        mtOrde.add("MT646829");
        mtOrde.add("MT646860");
        mtOrde.add("MT647076");
        mtOrde.add("MT647397");
        mtOrde.add("MT647405");
        mtOrde.add("MT647841");
        mtOrde.add("MT647970");
        mtOrde.add("MT648157");
        mtOrde.add("MT648294");
        mtOrde.add("MT648336");
        mtOrde.add("MT648493");
        mtOrde.add("MT648533");
        mtOrde.add("MT648638");
        mtOrde.add("MT648674");
        mtOrde.add("MT648724");
        mtOrde.add("MT648759");
        mtOrde.add("MT648761");
        mtOrde.add("MT648768");
        mtOrde.add("MT648770");
        mtOrde.add("MT648949");
        mtOrde.add("MT648982");
        mtOrde.add("MT649332");
        mtOrde.add("MT649399");
        mtOrde.add("MT649550");
        mtOrde.add("MT649568");
        mtOrde.add("MT649825");
        mtOrde.add("MT649937");
        mtOrde.add("MT650021");
        mtOrde.add("MT650030");
        mtOrde.add("MT650033");
        mtOrde.add("MT650400");
        mtOrde.add("MT650423");
        mtOrde.add("MT650440");
        mtOrde.add("MT650511");
        mtOrde.add("MT650544");
        mtOrde.add("MT650578");
        mtOrde.add("MT650838");
        mtOrde.add("MT650869");
        mtOrde.add("MT650939");
        mtOrde.add("MT650941");
        mtOrde.add("MT651015");
        mtOrde.add("MT651069");
        mtOrde.add("MT651071");
        mtOrde.add("MT651304");
        mtOrde.add("MT651328");
        mtOrde.add("MT651448");
        mtOrde.add("MT651456");
        mtOrde.add("MT651473");
        mtOrde.add("MT651562");
        mtOrde.add("MT651675");
        mtOrde.add("MT651730");
        mtOrde.add("MT651779");
        mtOrde.add("MT651787");
        mtOrde.add("MT651805");
        mtOrde.add("MT651809");
        mtOrde.add("MT651881");
        mtOrde.add("MT651888");
        mtOrde.add("MT652020");
        mtOrde.add("MT652047");
        mtOrde.add("MT652191");
        mtOrde.add("MT652249");
        mtOrde.add("MT652250");
        mtOrde.add("MT652260");
        mtOrde.add("MT652309");
        mtOrde.add("MT652374");
        mtOrde.add("MT652548");
        mtOrde.add("MT652567");
        mtOrde.add("MT652602");
        mtOrde.add("MT652609");
        mtOrde.add("MT652621");
        mtOrde.add("MT652690");
        mtOrde.add("MT652695");
        mtOrde.add("MT652701");
        mtOrde.add("MT652715");
        mtOrde.add("MT652763");
        mtOrde.add("MT652837");
        mtOrde.add("MT652899");
        mtOrde.add("MT652905");
        mtOrde.add("MT652926");
        mtOrde.add("MT605852");
        mtOrde.add("MT605877");
        mtOrde.add("MT606014");
        mtOrde.add("MT606095");
        mtOrde.add("MT606099");
        mtOrde.add("MT606353");
        mtOrde.add("MT606394");
        mtOrde.add("MT606499");
        mtOrde.add("MT607523");
        mtOrde.add("MT607624");
        mtOrde.add("MT607833");
        mtOrde.add("MT607988");
        mtOrde.add("MT608058");
        mtOrde.add("MT608662");
        mtOrde.add("MT608909");
        mtOrde.add("MT609187");
        mtOrde.add("MT609229");
        mtOrde.add("MT609350");
        mtOrde.add("MT610030");
        mtOrde.add("MT610242");
        mtOrde.add("MT610538");
        mtOrde.add("MT610690");
        mtOrde.add("MT610789");
        mtOrde.add("MT610794");
        mtOrde.add("MT610837");
        mtOrde.add("MT610850");
        mtOrde.add("MT610871");
        mtOrde.add("MT610893");
        mtOrde.add("MT611072");
        mtOrde.add("MT611116");
        mtOrde.add("MT611140");
        mtOrde.add("MT611226");
        mtOrde.add("MT611309");
        mtOrde.add("MT611323");
        mtOrde.add("MT611420");
        mtOrde.add("MT611638");
        mtOrde.add("MT611790");
        mtOrde.add("MT611897");
        mtOrde.add("MT611990");
        mtOrde.add("MT612273");
        mtOrde.add("MT612286");
        mtOrde.add("MT612331");
        mtOrde.add("MT612340");
        mtOrde.add("MT612370");
        mtOrde.add("MT612619");
        mtOrde.add("MT612710");
        mtOrde.add("MT612943");
        mtOrde.add("MT612976");
        mtOrde.add("MT613049");
        mtOrde.add("MT613120");
        mtOrde.add("MT613151");
        mtOrde.add("MT613206");
        mtOrde.add("MT613262");
        mtOrde.add("MT613578");
        mtOrde.add("MT613607");
        mtOrde.add("MT613642");
        mtOrde.add("MT613730");
        mtOrde.add("MT613774");
        mtOrde.add("MT614044");
        mtOrde.add("MT614391");
        mtOrde.add("MT614443");
        mtOrde.add("MT614462");
        mtOrde.add("MT614512");
        mtOrde.add("MT614522");
        mtOrde.add("MT614617");
        mtOrde.add("MT614926");
        mtOrde.add("MT615091");
        mtOrde.add("MT615290");
        mtOrde.add("MT615380");
        mtOrde.add("MT615590");
        mtOrde.add("MT616175");
        mtOrde.add("MT616338");
        mtOrde.add("MT616355");
        mtOrde.add("MT616384");
        mtOrde.add("MT616471");
        mtOrde.add("MT616609");
        mtOrde.add("MT616734");
        mtOrde.add("MT617020");
        mtOrde.add("MT617567");
        mtOrde.add("MT617906");
        mtOrde.add("MT617998");
        mtOrde.add("MT618185");
        mtOrde.add("MT618435");
        mtOrde.add("MT618490");
        mtOrde.add("MT618646");
        mtOrde.add("MT618908");
        mtOrde.add("MT618977");
        mtOrde.add("MT619229");
        mtOrde.add("MT619476");
        mtOrde.add("MT619525");
        mtOrde.add("MT619640");
        mtOrde.add("MT619649");
        mtOrde.add("MT619765");
        mtOrde.add("MT619930");
        mtOrde.add("MT620040");
        mtOrde.add("MT620351");
        mtOrde.add("MT620421");
        mtOrde.add("MT620472");
        mtOrde.add("MT620863");
        mtOrde.add("MT621044");
        mtOrde.add("MT621089");
        mtOrde.add("MT621392");
        mtOrde.add("MT621806");
        mtOrde.add("MT621896");
        mtOrde.add("MT622004");
        mtOrde.add("MT622119");
        mtOrde.add("MT622230");
        mtOrde.add("MT622528");
        mtOrde.add("MT622544");
        mtOrde.add("MT623046");
        mtOrde.add("MT623192");
        mtOrde.add("MT623216");
        mtOrde.add("MT623619");
        mtOrde.add("MT623634");
        mtOrde.add("MT623820");
        mtOrde.add("MT623931");
        mtOrde.add("MT623997");
        mtOrde.add("MT624174");
        mtOrde.add("MT624287");
        mtOrde.add("MT624321");
        mtOrde.add("MT624470");
        mtOrde.add("MT624490");
        mtOrde.add("MT624517");
        mtOrde.add("MT624650");
        mtOrde.add("MT624682");
        mtOrde.add("MT625028");
        mtOrde.add("MT625086");
        mtOrde.add("MT625213");
        mtOrde.add("MT625283");
        mtOrde.add("MT625311");
        mtOrde.add("MT625346");
        mtOrde.add("MT625664");
        mtOrde.add("MT625671");
        mtOrde.add("MT625797");
        mtOrde.add("MT625798");
        mtOrde.add("MT626005");
        mtOrde.add("MT626182");
        mtOrde.add("MT626233");
        mtOrde.add("MT626246");
        mtOrde.add("MT626811");
        mtOrde.add("MT626889");
        mtOrde.add("MT627074");
        mtOrde.add("MT627167");
        mtOrde.add("MT627170");
        mtOrde.add("MT627222");
        mtOrde.add("MT627461");
        mtOrde.add("MT627550");
        mtOrde.add("MT627592");
        mtOrde.add("MT627701");
        mtOrde.add("MT627736");
        mtOrde.add("MT627777");
        mtOrde.add("MT627796");*/



        List<FijaVisorSpecs> fijaVisorSpecsList=new ArrayList<>();

        for (String mtOrder : mtOrde) {
            List<Object[]> infoData = tdpVisorRepository.getVisorInfoReporteBBII(mtOrder);
            if (UtilMethods.validList(infoData)) {
                for (Object[] row : infoData) {
                    // estado_solicitud = row[2] != null ? (String) row[2] : null;
                    idAtis = row[0] != null ? (String) row[0] : null;
                    codigoPedido = row[1] != null ? (String) row[1] : null;
                    estadoSolicitud=row[2] != null ? (String) row[2] : null;
                    if (estadoSolicitud != null && estadoSolicitud.equals("INGRESADO")) {
                        status = legadoService.getEstadoAtisCodePedido(codigoPedido);
                    }else
                    {
                        status = "";
                    }
                    // codigoPedido = row[1] != null ? (String) row[1] : null;
                    motivoEstado=row[3] != null ? (String) row[3] : null;
                    visorUpdate=row[4] != null ? UtilMethods.getFormattedDate("yyyy-MM-dd HH24:mm:ss", (Date) row[4]) : null;
                    estadoAnterior= row[5] != null ? (String) row[5] : null;

                    FijaVisorSpecs fijaVisorSpecs=new FijaVisorSpecs(codigoPedido,estadoSolicitud,motivoEstado,visorUpdate,estadoAnterior,status,idAtis);

                    fijaVisorSpecsList.add(fijaVisorSpecs);

                }

            }
        }
        String[] columns = {"codigoPedido", "estadoSolicitud", "motivoEstado", "visorUpdate", "estadoAnterior", "status", "estadoAtis"};
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Employee");
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        // Create a Row
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for (FijaVisorSpecs employee : fijaVisorSpecsList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(employee.getCodigoPedido());

            row.createCell(1)
                    .setCellValue(employee.getEstadoSolicitud());

            row.createCell(2)
                    .setCellValue(employee.getMotivoEstado());
            row.createCell(3)
                    .setCellValue(employee.getVisorUpdate());
            row.createCell(4)
                    .setCellValue(employee.getEstadoAnterior());
            row.createCell(5)
                    .setCellValue(employee.getEstadoAtis());
            row.createCell(6)
                    .setCellValue(employee.getIdVisor());

        }
        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream("D:\\fileReporteBiATis13Nuevo.xlsx");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            workbook.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Closing the workbook
        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}