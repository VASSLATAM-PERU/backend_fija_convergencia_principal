package tdp.mt.backend.movistartotal.main.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tdp.mt.backend.movistartotal.main.entity.TdpUbigeoEquiv;

import java.util.List;

@Repository
public interface TdpUbigeoEquivRepository extends JpaRepository<TdpUbigeoEquiv, Integer> {


	@Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_ubigeo_equiv" +
			" WHERE translate(lower(dep_name_inei), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(:departamento), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC')" +
			" AND translate(lower(prov_name_inei), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(:provincia), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC')" +
			" AND translate(lower(dis_name_inei), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(:distrito), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC')", nativeQuery = true)
	TdpUbigeoEquiv getUbigeoAutomatizadorByNames(@Param("departamento") String departamento, @Param("provincia") String provincia, @Param("distrito") String distrito);
}