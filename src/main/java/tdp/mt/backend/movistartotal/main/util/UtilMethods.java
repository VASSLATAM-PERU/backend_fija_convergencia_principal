package tdp.mt.backend.movistartotal.main.util;

import com.auth0.jwt.internal.com.fasterxml.jackson.core.JsonProcessingException;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.main.restclient.ClientTefConfig;
import tdp.mt.backend.movistartotal.main.dto.EndpointCallEvent;

import javax.swing.text.NumberFormatter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UtilMethods {

    public static String getFormattedDate(final String strFormat, final Date d){
        String format = null;
        if (d!=null && validString(strFormat)){
            SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
            format = sdf.format(d);
        }
        return format;
    }

    public static String amdocsDoctTypeValidation(String strDocType) {
        if (strDocType.equals("CEX")) {
            return "C";
        } else if (strDocType.equals("PAS")) {
            return "P";
        } else { //DNI, RUC
            return strDocType;
        }
    }

    //AMDOCS
    public static ClientTefConfig buildConfig(String userid, String masterService, String serviceEndpoint, String serviceName) {
        ClientTefConfig config = new ClientTefConfig();
        config.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY);
        config.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET);
        config.setUrl(ServiceUrlUtil.BASE_SERVICE_URL+serviceEndpoint+serviceName);

        config.setUserLogin(userid);
        config.setServiceChannel("MS");
        config.setSessionCode(ServiceConstants.API_REQUEST_HEADER_SESSION_CODE);
        config.setApplication("WebConv");
        String execId = UuidUtil.generarExecId();
        config.setIdMessage(execId);
        config.setIpAddress(ServiceConstants.API_REQUEST_ADDRESS);
        config.setFunctionalityCode(masterService);
        String somedate = UtilMethods.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss.SSS", new Date());
        config.setTransactionTimestamp(somedate);
        config.setServiceName(serviceName);
        config.setVersion("1.0");

        return config;
    }

    //AMDOCS-PROD
    public static ClientTefConfig buildConfigP(String userid, String masterService, String serviceEndpoint, String serviceName) {
        ClientTefConfig config = new ClientTefConfig();
        config.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY2);
        config.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET2);
        config.setUrl(ServiceUrlUtil.BASE_SERVICE_URL2+serviceEndpoint+serviceName);

        config.setUserLogin(userid);
        config.setServiceChannel("MS");
        config.setSessionCode(ServiceConstants.API_REQUEST_HEADER_SESSION_CODE);
        config.setApplication("WebConv");
        String execId = UuidUtil.generarExecId();
        config.setIdMessage(execId);
        config.setIpAddress(ServiceConstants.API_REQUEST_ADDRESS);
        config.setFunctionalityCode(masterService);
        String somedate = UtilMethods.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss.SSS", new Date());
        config.setTransactionTimestamp(somedate);
        config.setServiceName(serviceName);
        config.setVersion("1.0");

        return config;
    }

    //AMDOCS
    public static ClientTefConfig buildConfigD(String userid, String masterService, String serviceEndpoint, String serviceName) {
        ClientTefConfig config = new ClientTefConfig();
        config.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEYD);
        config.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRETD);
        config.setUrl(ServiceUrlUtil.BASE_SERVICE_URLD+serviceEndpoint+serviceName);

        config.setUserLogin(userid);
        config.setServiceChannel("MS");
        config.setSessionCode(ServiceConstants.API_REQUEST_HEADER_SESSION_CODE);
        config.setApplication("WebConv");
        String execId = UuidUtil.generarExecId();
        config.setIdMessage(execId);
        config.setIpAddress(ServiceConstants.API_REQUEST_ADDRESS);
        config.setFunctionalityCode(masterService);
        String somedate = UtilMethods.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss.SSS", new Date());
        config.setTransactionTimestamp(somedate);
        config.setServiceName(serviceName);
        config.setVersion("1.0");

        return config;
    }

    //US-DEVDEV
    public static ClientTefConfig buildConfigDD(String userid, String masterService, String serviceEndpoint, String serviceName) {
        ClientTefConfig config = new ClientTefConfig();
        config.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEYDD);
        config.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRETDD);
        config.setUrl(ServiceUrlUtil.BASE_SERVICE_URLDD+serviceEndpoint+serviceName);

        config.setUserLogin(userid);
        config.setServiceChannel("MS");
        config.setSessionCode(ServiceConstants.API_REQUEST_HEADER_SESSION_CODE);
        config.setApplication("WebConv");
        String execId = UuidUtil.generarExecId();
        config.setIdMessage(execId);
        config.setIpAddress(ServiceConstants.API_REQUEST_ADDRESS);
        config.setFunctionalityCode(masterService);
        String somedate = UtilMethods.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss.SSS", new Date());
        config.setTransactionTimestamp(somedate);
        config.setServiceName(serviceName);
        config.setVersion("1.0");

        return config;
    }

    //US-PROD
    public static ClientTefConfig buildConfigUSP(String userid, String masterService, String serviceEndpoint, String serviceName) {
        ClientTefConfig config = new ClientTefConfig();
        config.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY3);
        config.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET3);
        config.setUrl(ServiceUrlUtil.BASE_SERVICE_URL3+serviceEndpoint+serviceName);

        config.setUserLogin(userid);
        config.setServiceChannel("MS");
        config.setSessionCode(ServiceConstants.API_REQUEST_HEADER_SESSION_CODE);
        config.setApplication("WebConv");
        String execId = UuidUtil.generarExecId();
        config.setIdMessage(execId);
        config.setIpAddress(ServiceConstants.API_REQUEST_ADDRESS);
        config.setFunctionalityCode(masterService);
        String somedate = UtilMethods.getFormattedDate("yyyy-MM-dd'T'HH:mm:ss.SSS", new Date());
        config.setTransactionTimestamp(somedate);
        config.setServiceName(serviceName);
        config.setVersion("1.0");

        return config;
    }

    //STC/ATIS
    public static ClientConfig buildConfigHeader(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URL+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEY;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRET;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }

    //US- PROD
    public static ClientConfig buildConfigHeaderUSP(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URL3+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEY3;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRET3;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }

    public static ClientConfig buildConfigHeaderP(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URL2+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEY2;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRET2;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }


    //STC/ATIS
    public static ClientConfig buildConfigHeaderD(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URLD+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEYD;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRETD;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }

    public static ClientConfig buildConfigHeaderDD(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URLDD+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEYDD;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRETDD;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }

    public static ClientConfig buildConfigHeaderFijaNoApi(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URL_F_T+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEY_F_T;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRET_F_T;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,"WEBMT", "xxx", serviceName, null);
        return config;
    }

    public static String scoringDocTypeValidation(String strDocType) {
        if (strDocType.equals("DNI")) {
            return "1";
        } else if (strDocType.equals("CEX")) {
            return "2";
        } else if (strDocType.equals("PAS")) {
            return "3";
        } else { //RUC
            return "4";
        }
    }

    public static String getPortDocumentType(String documentType) {
        String type = "";
        if (documentType.equals("DNI")) {
            type = "01";
        } else if (documentType.equals("CEX")) {
            type = "02";
        } else if (documentType.equals("PAS")) {
            type = "04";
        } else { //RUC
            type = "03";
        }
        return type;
    }

    public static String mapFrontDocumentType(String docType) {
        String type = "";
        if (docType.equals("CE")) {
            type = "CEX";
        } else if (docType.equals("P")) { //AVERIGUAR TIPO DE DATO PASAPORTE
            type = "PAS";
        } else { //RUC,DNI
            type = docType;
        }
        return type;
    }

    public  static Timestamp getFechaActual(){
        Date date = new Date();
        Timestamp fecha = new Timestamp(date.getTime());
        return fecha;
    }

    /**
     * Serializa un objeto java a JSON
     * @return objeto
     */

    public static EndpointCallEvent JavaToJson(Object objectRequest, Object objectResponse, String uri, Timestamp t,
                                               String message, String result) {
        EndpointCallEvent endpoint = new EndpointCallEvent();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            endpoint.setTimeRequest(t);
            endpoint.setUri(uri);
            endpoint.setRequest(objectMapper.writeValueAsString(objectRequest));
            endpoint.setResponse(objectMapper.writeValueAsString(objectResponse));
            endpoint.setMessage(message);
            endpoint.setResult(result);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return endpoint;
    }

    public static List<Date> getDates(){
        List<Date> listDates = new ArrayList<>();

        Date d = new Date();
        Calendar c = Calendar.getInstance(); // timezone
        c.setTime(d);
        c.add(Calendar.HOUR_OF_DAY, -5);
        int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);


        String strdate = year+"-"+month+"-"+dayOfMonth+" 5:00:00";
        String strdate1 = year+"-"+month+"-"+(dayOfMonth+1)+" 4:59:59";
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateFrom = null;
        Date dateTo = null;
        try {
            dateFrom = dateformat.parse(strdate);
            dateTo = dateformat.parse(strdate1);
            listDates.add(dateFrom);
            listDates.add(dateTo);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return listDates;
    }

    //"2019-01-23 17:57:06.88"
    public static List<Date> getDatesPreview(){
        List<Date> listDates = new ArrayList<>();

        Date d = new Date();
        Calendar c = Calendar.getInstance(); // timezone
        c.setTime(d);
        c.add(Calendar.MINUTE,-5);

        Date dateFrom = c.getTime();

        listDates.add(dateFrom);
        listDates.add(d);

        return listDates;
    }

    /**
     * Método que una cantidad en Double a String con un patrón como formato de salida.
     * @param cantidad a convertir
     * @return monto como String
     * @throws ParseException
     */

    public static String setMonto(Double cantidad) throws ParseException {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("0.00", dfs);
        NumberFormatter nf = new NumberFormatter(decimalFormat);
        String monto = nf.valueToString(cantidad);
        return monto;
    }

    public static Integer validInteger(String cadena) {
        if (!validString(cadena)) {
            return null;
        } else {
            int punto = cadena.indexOf(ServiceConstants.PUNTO);
            int coma = cadena.indexOf(ServiceConstants.COMA);
            if (punto != -1) {
                return Integer.parseInt(cadena.substring(0, punto));
            } else if (coma != -1) {
                return Integer.parseInt(cadena.substring(0, coma));
            } else {
                return Integer.parseInt(cadena);
            }
        }
    }

    public static boolean validString(String cadena) {
        if (cadena!=null){
            if (cadena.trim().length()>0){
                return true;
            } else return false;
        } else return false;
    }

    public static boolean validList(List lista){
        if(lista!=null) {
            if(lista.size()>0) return true;
            else return false;
        } else{
            return false;
        }
    }

    public static Date getDateFromString(final String strFormat, final String strDate) throws Exception{
        if (validString(strFormat) && validString(strDate)){
            SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
            Date fecha = sdf.parse(strDate);
            return fecha;
        }
        else return null;
    }

    public static String getDateFromStringNor(final String strFormat, final Date d){
        String format = null;
        SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
        if (d!=null && validString(strFormat)){
            SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
            format = formateador.format(d);
        }
        return format;
    }

    public static Date sumRestCalendar(final Date actualDate, final int type, final int number){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(actualDate);
        switch (type){
            case 1:
                calendar.add(Calendar.DAY_OF_MONTH, number); break;
            case 2:
                calendar.add(Calendar.MONTH, number); break;
            default:
                calendar.add(Calendar.DAY_OF_MONTH, number); break;
        }
        return calendar.getTime();
    }

    public static Date sumarRestarHorasFecha(Date fecha, int horas) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.HOUR, horas);  // numero de horas a añadir, o restar en caso de horas<0

        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas
    }
    
    //Automatizador
    public static ClientConfig buildConfigHeaderAuto(String serviceEndpoint, String serviceName) {

        String serviceUrl = ServiceUrlUtil.BASE_SERVICE_URL_AUTO+serviceEndpoint+serviceName;
        String apikey = ServiceUrlUtil.BASE_SERVICE_API_KEY_AUTO;
        String apiSecret = ServiceUrlUtil.BASE_SERVICE_API_SECRET_AUTO;
        ClientConfig config = new ClientConfig(serviceUrl,apikey,apiSecret,serviceName,null);
        return config;
    }
    //fin
}