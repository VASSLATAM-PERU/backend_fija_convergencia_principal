package tdp.mt.backend.movistartotal.main.restclient.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiRequestTefHeader {

    @JsonProperty("userLogin")
    private String userLogin;
    @JsonProperty("serviceChannel")
    private String serviceChannel;
    @JsonProperty("sessionCode")
    private String sessionCode;
    @JsonProperty("application")
    private String application;
    @JsonProperty("idMessage")
    private String idMessage;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("functionalityCode")
    private String functionalityCode;
    @JsonProperty("transactionTimestamp")
    private String transactionTimestamp;
    @JsonProperty("serviceName")
    private String serviceName;
    @JsonProperty("version")
    private String version;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getServiceChannel() {
        return serviceChannel;
    }

    public void setServiceChannel(String serviceChannel) {
        this.serviceChannel = serviceChannel;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getFunctionalityCode() {
        return functionalityCode;
    }

    public void setFunctionalityCode(String functionalityCode) {
        this.functionalityCode = functionalityCode;
    }

    public String getTransactionTimestamp() {
        return transactionTimestamp;
    }

    public void setTransactionTimestamp(String transactionTimestamp) {
        this.transactionTimestamp = transactionTimestamp;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


}
