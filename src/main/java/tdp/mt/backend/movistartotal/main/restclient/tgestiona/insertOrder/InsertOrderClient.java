package tdp.mt.backend.movistartotal.main.restclient.tgestiona.insertOrder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaResponseBody;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.io.IOException;


public class InsertOrderClient extends AbstractClient<InsertRequestBody, InsertResponseBody> {

	private static final String urlendpoint ="ws-orderMT/";
	private static final String urlmethod = "insertOrder";

	public InsertOrderClient(ClientConfig config) {
		super(config);
		this.config = UtilMethods.buildConfigHeader(urlendpoint,urlmethod);
	}
	
	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_TGESTIONA;
	}

	@Override
	protected ApiResponse<InsertResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<InsertResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<InsertResponseBody>>() {});
		return objCms;
	}
	
}
