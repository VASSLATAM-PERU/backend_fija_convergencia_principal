package tdp.mt.backend.movistartotal.main.service;

import tdp.mt.backend.movistartotal.main.dto.EndpointCallEvent;

public interface EndpointCallEventsService {

    void registerEventEndpoint(EndpointCallEvent event);

}
