package tdp.mt.backend.movistartotal.main.domain.MTFija;

import javax.persistence.Transient;
import java.io.Serializable;


/**
 * The persistent class for the mt_fixed_product_log database table.
 *
 */
public class MtFixedProductReq implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idFixedLog;

	private String address;

	private String addressComplement;

	private String fixedProductType;

	private String fixedScore;

	private String fixedSuscriber;

	private String fixedSystemType;

	private String lineStatus;

	private String productdescription;

	private String serviceNumber;

	private String ubigeo;

	private String coordinateX;

	private String coordinateY;

	private String coverage;

	private String department;

	private String province;

	private String district;

	private String tipoProductoHDEC;

	private String subProductoHDEC;

	private String operacionComercial;

	@Transient
	private String tecnologiaTV;
	@Transient
	private String paquetizacion;
	@Transient
	private String altasTV;
	@Transient
	private String equipamientoDeco;
	@Transient
	private String telefonoOrigen;
	@Transient
	private String clienteCMS;
	@Transient
	private String decosSD;
	@Transient
	private String decosHD;
	@Transient
	private String decosDVR;
	@Transient
	private String bloqueTV;
	@Transient
	private String svaInternet;
	@Transient
	private String svaLinea;
	@Transient
	private String codigoServicioCMS;
	@Transient
	private String bloqueProducto;
	@Transient
	private String internetRSW;
	
	private String productCode;

	//bi-directional many-to-one association to MtClientPackageLog

	public MtFixedProductReq() {
	}

	public Long getIdFixedLog() {
		return this.idFixedLog;
	}

	public void setIdFixedLog(Long idFixedLog) {
		this.idFixedLog = idFixedLog;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFixedProductType() {
		return this.fixedProductType;
	}

	public void setFixedProductType(String fixedProductType) {
		this.fixedProductType = fixedProductType;
	}

	public String getFixedScore() {
		return this.fixedScore;
	}

	public void setFixedScore(String fixedScore) {
		this.fixedScore = fixedScore;
	}

	public String getFixedSuscriber() {
		return this.fixedSuscriber;
	}

	public void setFixedSuscriber(String fixedSuscriber) {
		this.fixedSuscriber = fixedSuscriber;
	}

	public String getFixedSystemType() {
		return this.fixedSystemType;
	}

	public void setFixedSystemType(String fixedSystemType) {
		this.fixedSystemType = fixedSystemType;
	}

	public String getLineStatus() {
		return this.lineStatus;
	}

	public void setLineStatus(String lineStatus) {
		this.lineStatus = lineStatus;
	}

	public String getProductdescription() {
		return this.productdescription;
	}

	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}

	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getUbigeo() {
		return this.ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	public String getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTipoProductoHDEC() {
		return tipoProductoHDEC;
	}

	public void setTipoProductoHDEC(String tipoProductoHDEC) {
		this.tipoProductoHDEC = tipoProductoHDEC;
	}

	public String getSubProductoHDEC() {
		return subProductoHDEC;
	}

	public void setSubProductoHDEC(String subProductoHDEC) {
		this.subProductoHDEC = subProductoHDEC;
	}

	public String getOperacionComercial() {
		return operacionComercial;
	}

	public void setOperacionComercial(String operacionComercial) {
		this.operacionComercial = operacionComercial;
	}

	public String getTecnologiaTV() {
		return tecnologiaTV;
	}

	public void setTecnologiaTV(String tecnologiaTV) {
		this.tecnologiaTV = tecnologiaTV;
	}

	public String getPaquetizacion() {
		return paquetizacion;
	}

	public void setPaquetizacion(String paquetizacion) {
		this.paquetizacion = paquetizacion;
	}

	public String getAltasTV() {
		return altasTV;
	}

	public void setAltasTV(String altasTV) {
		this.altasTV = altasTV;
	}

	public String getEquipamientoDeco() {
		return equipamientoDeco;
	}

	public void setEquipamientoDeco(String equipamientoDeco) {
		this.equipamientoDeco = equipamientoDeco;
	}

	public String getTelefonoOrigen() {
		return telefonoOrigen;
	}

	public void setTelefonoOrigen(String telefonoOrigen) {
		this.telefonoOrigen = telefonoOrigen;
	}

	public String getClienteCMS() {
		return clienteCMS;
	}

	public void setClienteCMS(String clienteCMS) {
		this.clienteCMS = clienteCMS;
	}

	public String getDecosSD() {
		return decosSD;
	}

	public void setDecosSD(String decosSD) {
		this.decosSD = decosSD;
	}

	public String getDecosHD() {
		return decosHD;
	}

	public void setDecosHD(String decosHD) {
		this.decosHD = decosHD;
	}

	public String getDecosDVR() {
		return decosDVR;
	}

	public void setDecosDVR(String decosDVR) {
		this.decosDVR = decosDVR;
	}

	public String getBloqueTV() {
		return bloqueTV;
	}

	public void setBloqueTV(String bloqueTV) {
		this.bloqueTV = bloqueTV;
	}

	public String getSvaInternet() {
		return svaInternet;
	}

	public void setSvaInternet(String svaInternet) {
		this.svaInternet = svaInternet;
	}

	public String getSvaLinea() {
		return svaLinea;
	}

	public void setSvaLinea(String svaLinea) {
		this.svaLinea = svaLinea;
	}

	public String getCodigoServicioCMS() {
		return codigoServicioCMS;
	}

	public void setCodigoServicioCMS(String codigoServicioCMS) {
		this.codigoServicioCMS = codigoServicioCMS;
	}

	public String getBloqueProducto() {
		return bloqueProducto;
	}

	public void setBloqueProducto(String bloqueProducto) {
		this.bloqueProducto = bloqueProducto;
	}

	public String getAddressComplement() {
		return addressComplement;
	}

	public void setAddressComplement(String addressComplement) {
		this.addressComplement = addressComplement;
	}

	public String getInternetRSW() {
		return internetRSW;
	}

	public void setInternetRSW(String internetRSW) {
		this.internetRSW = internetRSW;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
}