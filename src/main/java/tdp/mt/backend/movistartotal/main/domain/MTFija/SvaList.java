package tdp.mt.backend.movistartotal.main.domain.MTFija;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The persistent class for the mt_offer_sva_log database table.
 *
 */
public class SvaList implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean isSVA;
	private String maxSale;
	private String svaName;
	private BigDecimal svaPrice;
	private String svaPsCode;
	private String sold;
	@JsonProperty("codeProduct")
	private String productCode;
	public SvaList() {
	}
	public boolean getIsSVA() {
		return isSVA;
	}
	public void setIsSVA(boolean isSVA) {
		this.isSVA = isSVA;
	}
	public String getMaxSale() {
		return maxSale;
	}
	public void setMaxSale(String maxSale) {
		this.maxSale = maxSale;
	}
	public String getSvaName() {
		return svaName;
	}
	public void setSvaName(String svaName) {
		this.svaName = svaName;
	}
	public BigDecimal getSvaPrice() {
		return svaPrice;
	}
	public void setSvaPrice(BigDecimal svaPrice) {
		this.svaPrice = svaPrice;
	}
	public String getSvaPsCode() {
		return svaPsCode;
	}
	public void setSvaPsCode(String svaPsCode) {
		this.svaPsCode = svaPsCode;
	}
	public String getSold() {
		return sold;
	}
	public void setSold(String sold) {
		this.sold = sold;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
}