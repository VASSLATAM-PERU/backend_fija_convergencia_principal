package tdp.mt.backend.movistartotal.main.util;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileUtil {

    static String filename ="EQUIPOS_SPS_20181113001145496v2.txt";

    public static void readFile() {

        //File file = new File("EQUIPOS_SPS_20181113001145496.txt");
        int i = 0;
        try (Stream<String> lines = Files.lines (Paths.get(filename), StandardCharsets.UTF_8))
        {
            for (String line : (Iterable<String>) lines::iterator)
            {
                i++;
                System.out.println("LINE"+i+": "+line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
