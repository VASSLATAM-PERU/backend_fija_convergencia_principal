package tdp.mt.backend.movistartotal.main.domain.Automatizador;

public class EstadoSolicitudResponse {

    private String codErrCd;
    private String ObsErrDs;

    public String getCodErrCd() {
        return codErrCd;
    }

    public void setCodErrCd(String codErrCd) {
        this.codErrCd = codErrCd;
    }

    public String getObsErrDs() {
        return ObsErrDs;
    }

    public void setObsErrDs(String obsErrDs) {
        ObsErrDs = obsErrDs;
    }
}
