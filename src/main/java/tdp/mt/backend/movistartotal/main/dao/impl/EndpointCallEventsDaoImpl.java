package tdp.mt.backend.movistartotal.main.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.main.dao.EndpointCallEventsDao;
import tdp.mt.backend.movistartotal.main.dto.EndpointCallEvent;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;

@Repository
public class EndpointCallEventsDaoImpl implements EndpointCallEventsDao {

    private Logger LOGGER = LogManager.getLogger(EndpointCallEventsDaoImpl.class);

    private DataSource datasource;

    @Autowired
    public EndpointCallEventsDaoImpl(DataSource datasource) {
        this.datasource = datasource;
    }


    @Override
    public void registerEventEndpoint(EndpointCallEvent event) {

        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO public.endpoint_call_events (tag, datetime_request, datetime_response, uriendpoint, "
                    + "request, response, msg, result) "
                    + "VALUES (?,?,CURRENT_TIMESTAMP,?,?,?,?,?)";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "ENDPOINT_CALL");
            ps.setTimestamp(2, event.getTimeRequest());
            ps.setString(3, event.getUri());
            ps.setString(4, event.getRequest());
            ps.setString(5, event.getResponse());
            ps.setString(6, event.getMessage());
            ps.setString(7, event.getResult());

            ps.executeUpdate();

        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada al Endpoint", e);
        }

    }


}
