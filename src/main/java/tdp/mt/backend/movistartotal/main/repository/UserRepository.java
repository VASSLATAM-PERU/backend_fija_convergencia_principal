package tdp.mt.backend.movistartotal.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tdp.mt.backend.movistartotal.main.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {

}
