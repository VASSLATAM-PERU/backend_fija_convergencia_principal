package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.cliente.AbstractClientAuto;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.dto.ApiResponseAuto;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaRequestBody;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa.PreventaResponseBody;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.io.IOException;


public class InsertOrderClientAuto extends AbstractClientAuto<InsertRequestBodyAuto, InsertResponseBodyAuto> {

	private static final String urlendpoint ="/ws-octopus/";
	private static final String urlmethod = "requestSalesRegistration";

	public InsertOrderClientAuto(ClientConfig config) {
		super(config);
		this.config = UtilMethods.buildConfigHeaderAuto(urlendpoint,urlmethod);
		this.config.setDestination(config.getDestination());
	}
	 
	@Override
	protected String getServiceCodeAuto() {
		return SERVICE_CODE_TGESTIONA;
	}

	@Override
	protected ApiResponseAuto<InsertResponseBodyAuto> getResponseAuto(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponseAuto<InsertResponseBodyAuto> objCms = mapper.readValue(json, new TypeReference<ApiResponseAuto<InsertResponseBodyAuto>>() {});
		return objCms;
	}
	
	
	
}
