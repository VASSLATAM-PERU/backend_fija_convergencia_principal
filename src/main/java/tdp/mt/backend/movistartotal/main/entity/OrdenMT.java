package tdp.mt.backend.movistartotal.main.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "order_mt", schema = "ibmx_a07e6d02edaf552") //dochoaro se adiciona schema
public class OrdenMT {
    @Id
    @Column(name = "id_mt_order")
    private Long idMtOrder;

    @Column(name = "id_fija_order")
    private String idFijaOrder;

    @Column(name = "m1_amdocs_movil_order")
    private String m1AmdocsMovilOrder;

    @Column(name = "m2_amdocs_movil_order")
    private String m2AmdocsMovilOrder;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "register_date")
    private Date registerDate;

    @Column(name = "remote")
    private boolean remote;

    @Column(name = "mode_retail")
    private String modeRetail;

    public Long getIdMtOrder() {
        return idMtOrder;
    }

    public void setIdMtOrder(Long idMtOrder) {
        this.idMtOrder = idMtOrder;
    }

    public String getIdFijaOrder() {
        return idFijaOrder;
    }

    public void setIdFijaOrder(String idFijaOrder) {
        this.idFijaOrder = idFijaOrder;
    }

    public String getM1AmdocsMovilOrder() {
        return m1AmdocsMovilOrder;
    }

    public void setM1AmdocsMovilOrder(String m1AmdocsMovilOrder) {
        this.m1AmdocsMovilOrder = m1AmdocsMovilOrder;
    }

    public String getM2AmdocsMovilOrder() {
        return m2AmdocsMovilOrder;
    }

    public void setM2AmdocsMovilOrder(String m2AmdocsMovilOrder) {
        this.m2AmdocsMovilOrder = m2AmdocsMovilOrder;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public boolean isRemote() {
        return remote;
    }

    public void setRemote(boolean remote) {
        this.remote = remote;
    }

    public String getModeRetail() {
        return modeRetail;
    }

    public void setModeRetail(String modeRetail) {
        this.modeRetail = modeRetail;
    }
}
