package tdp.mt.backend.movistartotal.main.entity;

import tdp.mt.backend.movistartotal.main.domain.MTFija.SvaList;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "order_detail", schema = "ibmx_a07e6d02edaf552")
@SequenceGenerator(name = "myseq", sequenceName = "ibmx_a07e6d02edaf552.order_detail_seq", catalog = "ibmx_a07e6d02edaf552", schema = "ibmx_a07e6d02edaf552", allocationSize = 1)

public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")//glazaror
    private Long id;
    @Column(name = "initcoordinatex")
    private BigDecimal initCoordinateX;
    @Column(name = "initcoordinatey")
    private BigDecimal initCoordinateY;
    @Column(name = "decossd")
    private Integer decosSD;
    @Column(name = "decoshd")
    private Integer decoshd;
    @Column(name = "decosdvr")
    private Integer decosDVR;
    @Column(name = "tvblock")
    private String tvBlock;
    @Column(name = "svainternet")
    private String svaInternet;
    @Column(name = "svaline")
    private String svaLine;
    @Column(name = "packingtype")
    private String packingType;
    @Column(name = "winbackdiscount")
    private String winBackDiscount;
    @Column(name = "blocktv")
    private String blockTV;
    @Column(name = "altastv")
    private String altasTV;
    @Column(name = "internettech")
    private String internetTech;
    @Column(name = "tvtech")
    private String tvTech;
    @Column(name = "hasrecording")
    private String hasRecording;
    @Column(name = "expertocode")
    private String expertoCode;
    @Column(name = "recordingid")
    private String recordingID;
    @Column(name = "department")
    private String department;
    @Column(name = "province")
    private String province;
    @Column(name = "district")
    private String district;
    @Column(name = "address")
    private String address;
    @Column(name = "addresscomplement")
    private String addressComplement;
    @Column(name = "registeredtime")
    private Date registeredTime;
    @Column(name = "inputdocmode")
    private String inputDocMode;
    @Column(name = "speechtxt")
    private String speechText;
    @Column(name = "sendcontracts")
    private String sendContracts;
    @Column(name = "enabledigitalinvoice")
    private String enableDigitalInvoice;
    @Column(name = "disablewhitepage")
    private String disableWhitePage;
    @Column(name = "publicationwhitepages")
    private String publicationWhitePages;
    @Column(name = "dataprotection")
    private String dataProtection;
    @Column(name = "parentalprotection")
    private String parentalProtection;
    @Column(name = "automaticdebit")
    private String automaticDebit;
    @Column(name = "conditionsfecha")
    private String conditionsFecha;

    @Column(name = "originorder")
    private String originOrder;

    @Column(name = "orderid")
    private String orderId;//glazaror prueba

    // Automatizador GIS
    @Column(name = "msx_cbr_voi_ges_in")
    private String msx_cbr_voi_ges_in;
    @Column(name = "msx_cbr_voi_gis_in")
    private String msx_cbr_voi_gis_in;
    @Column(name = "msx_ind_snl_gis_cd")
    private String msx_ind_snl_gis_cd;
    @Column(name = "msx_ind_gpo_gis_cd")
    private String msx_ind_gpo_gis_cd;
    @Column(name = "cod_ind_sen_cms")
    private String cod_ind_sen_cms;
    @Column(name = "cod_cab_cms")
    private String cod_cab_cms;
    @Column(name = "cod_fac_tec_cd")
    private String cod_fac_tec_cd;
    @Column(name = "ps_adm_dep_1")
    private String ps_adm_dep_1;
    @Column(name = "cod_stc")
    private String cod_stc;

    @Column(name = "cobre_blq_vta")
    private String cobre_blq_vta;
    @Column(name = "cobre_blq_trm")
    private String cobre_blq_trm;


    // Automatizador NORMALIZADOR
    @Column(name = "tip_cal_ati_cd")
    private String tip_cal_ati_cd;
    @Column(name = "nom_cal_ds")
    private String nom_cal_ds;
    @Column(name = "num_cal_nu")
    private String num_cal_nu;
    @Column(name = "cod_cnj_hbl_cd")
    private String cod_cnj_hbl_cd;
    @Column(name = "tip_cnj_hbl_cd")
    private String tip_cnj_hbl_cd;
    @Column(name = "nom_cnj_hbl_no")
    private String nom_cnj_hbl_no;
    @Column(name = "pri_tip_cmp_cd")
    private String pri_tip_cmp_cd;
    @Column(name = "pri_cmp_dir_ds")
    private String pri_cmp_dir_ds;
    @Column(name = "seg_tip_cmp_cd")
    private String seg_tip_cmp_cd;
    @Column(name = "seg_cmp_dir_ds")
    private String seg_cmp_dir_ds;
    @Column(name = "trc_tip_cmp_cd")
    private String trc_tip_cmp_cd;
    @Column(name = "trc_cmp_dir_ds")
    private String trc_cmp_dir_ds;
    @Column(name = "cao_tip_cmp_cd")
    private String cao_tip_cmp_cd;
    @Column(name = "cao_cmp_dir_ds")
    private String cao_cmp_dir_ds;
    @Column(name = "qui_tip_cmp_cd")
    private String qui_tip_cmp_cd;
    @Column(name = "qui_cmp_dir_ds")
    private String qui_cmp_dir_ds;
    @Column(name = "set_tip_cmp_cd")
    private String set_tip_cmp_cd;
    @Column(name = "set_cmp_dir_ds")
    private String set_cmp_dir_ds;
    @Column(name = "spt_tip_cmp_cd")
    private String spt_tip_cmp_cd;
    @Column(name = "spt_cmp_dir_ds")
    private String spt_cmp_dir_ds;
    @Column(name = "oct_tip_cmp_cd")
    private String oct_tip_cmp_cd;
    @Column(name = "oct_cmp_dir_ds")
    private String oct_cmp_dir_ds;
    @Column(name = "nov_tip_cmp_cd")
    private String nov_tip_cmp_cd;
    @Column(name = "nov_cmp_dir_ds")
    private String nov_cmp_dir_ds;
    @Column(name = "dco_tip_cmp_cd")
    private String dco_tip_cmp_cd;
    @Column(name = "dco_cmp_dir_ds")
    private String dco_cmp_dir_ds;

    @Column(name = "internetrsw")
    private Integer internetRsw;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getInitCoordinateX() {
        return initCoordinateX;
    }

    public void setInitCoordinateX(BigDecimal initCoordinateX) {
        this.initCoordinateX = initCoordinateX;
    }

    public BigDecimal getInitCoordinateY() {
        return initCoordinateY;
    }

    public void setInitCoordinateY(BigDecimal initCoordinateY) {
        this.initCoordinateY = initCoordinateY;
    }

    public Integer getDecosSD() {
        return decosSD;
    }

    public void setDecosSD(Integer decosSD) {
        this.decosSD = decosSD;
    }

    public Integer getDecoshd() {
        return decoshd;
    }

    public void setDecoshd(Integer decoshd) {
        this.decoshd = decoshd;
    }

    public Integer getDecosDVR() {
        return decosDVR;
    }

    public void setDecosDVR(Integer decosDVR) {
        this.decosDVR = decosDVR;
    }

    public String getTvBlock() {
        return tvBlock;
    }

    public void setTvBlock(String tvBlock) {
        this.tvBlock = tvBlock;
    }

    public String getSvaInternet() {
        return svaInternet;
    }

    public void setSvaInternet(String svaInternet) {
        this.svaInternet = svaInternet;
    }

    public String getSvaLine() {
        return svaLine;
    }

    public void setSvaLine(String svaLine) {
        this.svaLine = svaLine;
    }

    public String getPackingType() {
        return packingType;
    }

    public void setPackingType(String packingType) {
        this.packingType = packingType;
    }

    public String getWinBackDiscount() {
        return winBackDiscount;
    }

    public void setWinBackDiscount(String winBackDiscount) {
        this.winBackDiscount = winBackDiscount;
    }

    public String getBlockTV() {
        return blockTV;
    }

    public void setBlockTV(String blockTV) {
        this.blockTV = blockTV;
    }

    public String getAltasTV() {
        return altasTV;
    }

    public void setAltasTV(String altasTV) {
        this.altasTV = altasTV;
    }

    public String getInternetTech() {
        return internetTech;
    }

    public void setInternetTech(String internetTech) {
        this.internetTech = internetTech;
    }

    public String getTvTech() {
        return tvTech;
    }

    public void setTvTech(String tvTech) {
        this.tvTech = tvTech;
    }

    public String getHasRecording() {
        return hasRecording;
    }

    public void setHasRecording(String hasRecording) {
        this.hasRecording = hasRecording;
    }

    public String getExpertoCode() {
        return expertoCode;
    }

    public void setExpertoCode(String expertoCode) {
        this.expertoCode = expertoCode;
    }

    public String getDisableWhitePage() {
        return disableWhitePage;
    }

    public void setDisableWhitePage(String disableWhitePage) {
        this.disableWhitePage = disableWhitePage;
    }

    public String getEnableDigitalInvoice() {
        return enableDigitalInvoice;
    }

    public void setEnableDigitalInvoice(String enableDigitalInvoice) {
        this.enableDigitalInvoice = enableDigitalInvoice;
    }

    public String getSendContracts() {
        return sendContracts;
    }

    public void setSendContracts(String sendContracts) {
        this.sendContracts = sendContracts;
    }

    public String getDataProtection() {
        return dataProtection;
    }

    public void setDataProtection(String dataProtection) {
        this.dataProtection = dataProtection;
    }

    public String getRecordingID() {
        return recordingID;
    }

    public void setRecordingID(String recordingID) {
        this.recordingID = recordingID;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressComplement() {
        return addressComplement;
    }

    public void setAddressComplement(String addressComplement) {
        this.addressComplement = addressComplement;
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public String getInputDocMode() {
        return inputDocMode;
    }

    public void setInputDocMode(String inputDocMode) {
        this.inputDocMode = inputDocMode;
    }

    public String getSpeechText() {
        return speechText;
    }

    public void setSpeechText(String speechText) {
        this.speechText = speechText;
    }

    public String getPublicationWhitePages() {
        return publicationWhitePages;
    }

    public void setPublicationWhitePages(String publicationWhitePages) {
        this.publicationWhitePages = publicationWhitePages;
    }

    //Agregado get/set

    public String getParentalProtection() {
        return parentalProtection;
    }

    public void setParentalProtection(String parentalProtection) {
        this.parentalProtection = parentalProtection;
    }

    public String getOriginOrder() {
        return originOrder;
    }

    public void setOriginOrder(String originOrder) {
        this.originOrder = originOrder;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMsx_cbr_voi_ges_in() {
        return msx_cbr_voi_ges_in;
    }

    public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
        this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
    }

    public String getMsx_cbr_voi_gis_in() {
        return msx_cbr_voi_gis_in;
    }

    public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
        this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
    }

    public String getMsx_ind_snl_gis_cd() {
        return msx_ind_snl_gis_cd;
    }

    public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
        this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
    }

    public String getMsx_ind_gpo_gis_cd() {
        return msx_ind_gpo_gis_cd;
    }

    public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
        this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
    }

    public String getCod_ind_sen_cms() {
        return cod_ind_sen_cms;
    }

    public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
        this.cod_ind_sen_cms = cod_ind_sen_cms;
    }

    public String getCod_cab_cms() {
        return cod_cab_cms;
    }

    public void setCod_cab_cms(String cod_cab_cms) {
        this.cod_cab_cms = cod_cab_cms;
    }

    public String getCod_fac_tec_cd() {
        return cod_fac_tec_cd;
    }

    public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
        this.cod_fac_tec_cd = cod_fac_tec_cd;
    }

    public String getCobre_blq_vta() {
        return cobre_blq_vta;
    }

    public void setCobre_blq_vta(String cobre_blq_vta) {
        this.cobre_blq_vta = cobre_blq_vta;
    }

    public String getCobre_blq_trm() {
        return cobre_blq_trm;
    }

    public void setCobre_blq_trm(String cobre_blq_trm) {
        this.cobre_blq_trm = cobre_blq_trm;
    }

    public String getPs_adm_dep_1() {
        return ps_adm_dep_1;
    }

    public void setPs_adm_dep_1(String ps_adm_dep_1) {
        this.ps_adm_dep_1 = ps_adm_dep_1;
    }

    public String getCod_stc() {
        return cod_stc;
    }

    public void setCod_stc(String cod_stc) {
        this.cod_stc = cod_stc;
    }

    public String getTip_cal_ati_cd() {
        return tip_cal_ati_cd;
    }

    public void setTip_cal_ati_cd(String tip_cal_ati_cd) {
        this.tip_cal_ati_cd = tip_cal_ati_cd;
    }

    public String getNom_cal_ds() {
        return nom_cal_ds;
    }

    public void setNom_cal_ds(String nom_cal_ds) {
        this.nom_cal_ds = nom_cal_ds;
    }

    public String getNum_cal_nu() {
        return num_cal_nu;
    }

    public void setNum_cal_nu(String num_cal_nu) {
        this.num_cal_nu = num_cal_nu;
    }

    public String getCod_cnj_hbl_cd() {
        return cod_cnj_hbl_cd;
    }

    public void setCod_cnj_hbl_cd(String cod_cnj_hbl_cd) {
        this.cod_cnj_hbl_cd = cod_cnj_hbl_cd;
    }

    public String getTip_cnj_hbl_cd() {
        return tip_cnj_hbl_cd;
    }

    public void setTip_cnj_hbl_cd(String tip_cnj_hbl_cd) {
        this.tip_cnj_hbl_cd = tip_cnj_hbl_cd;
    }

    public String getNom_cnj_hbl_no() {
        return nom_cnj_hbl_no;
    }

    public void setNom_cnj_hbl_no(String nom_cnj_hbl_no) {
        this.nom_cnj_hbl_no = nom_cnj_hbl_no;
    }

    public String getPri_tip_cmp_cd() {
        return pri_tip_cmp_cd;
    }

    public void setPri_tip_cmp_cd(String pri_tip_cmp_cd) {
        this.pri_tip_cmp_cd = pri_tip_cmp_cd;
    }

    public String getPri_cmp_dir_ds() {
        return pri_cmp_dir_ds;
    }

    public void setPri_cmp_dir_ds(String pri_cmp_dir_ds) {
        this.pri_cmp_dir_ds = pri_cmp_dir_ds;
    }

    public String getSeg_tip_cmp_cd() {
        return seg_tip_cmp_cd;
    }

    public void setSeg_tip_cmp_cd(String seg_tip_cmp_cd) {
        this.seg_tip_cmp_cd = seg_tip_cmp_cd;
    }

    public String getSeg_cmp_dir_ds() {
        return seg_cmp_dir_ds;
    }

    public void setSeg_cmp_dir_ds(String seg_cmp_dir_ds) {
        this.seg_cmp_dir_ds = seg_cmp_dir_ds;
    }

    public String getTrc_tip_cmp_cd() {
        return trc_tip_cmp_cd;
    }

    public void setTrc_tip_cmp_cd(String trc_tip_cmp_cd) {
        this.trc_tip_cmp_cd = trc_tip_cmp_cd;
    }

    public String getTrc_cmp_dir_ds() {
        return trc_cmp_dir_ds;
    }

    public void setTrc_cmp_dir_ds(String trc_cmp_dir_ds) {
        this.trc_cmp_dir_ds = trc_cmp_dir_ds;
    }

    public String getCao_tip_cmp_cd() {
        return cao_tip_cmp_cd;
    }

    public void setCao_tip_cmp_cd(String cao_tip_cmp_cd) {
        this.cao_tip_cmp_cd = cao_tip_cmp_cd;
    }

    public String getCao_cmp_dir_ds() {
        return cao_cmp_dir_ds;
    }

    public void setCao_cmp_dir_ds(String cao_cmp_dir_ds) {
        this.cao_cmp_dir_ds = cao_cmp_dir_ds;
    }

    public String getQui_tip_cmp_cd() {
        return qui_tip_cmp_cd;
    }

    public void setQui_tip_cmp_cd(String qui_tip_cmp_cd) {
        this.qui_tip_cmp_cd = qui_tip_cmp_cd;
    }

    public String getQui_cmp_dir_ds() {
        return qui_cmp_dir_ds;
    }

    public void setQui_cmp_dir_ds(String qui_cmp_dir_ds) {
        this.qui_cmp_dir_ds = qui_cmp_dir_ds;
    }

    public String getSet_tip_cmp_cd() {
        return set_tip_cmp_cd;
    }

    public void setSet_tip_cmp_cd(String set_tip_cmp_cd) {
        this.set_tip_cmp_cd = set_tip_cmp_cd;
    }

    public String getSet_cmp_dir_ds() {
        return set_cmp_dir_ds;
    }

    public void setSet_cmp_dir_ds(String set_cmp_dir_ds) {
        this.set_cmp_dir_ds = set_cmp_dir_ds;
    }

    public String getSpt_tip_cmp_cd() {
        return spt_tip_cmp_cd;
    }

    public void setSpt_tip_cmp_cd(String spt_tip_cmp_cd) {
        this.spt_tip_cmp_cd = spt_tip_cmp_cd;
    }

    public String getSpt_cmp_dir_ds() {
        return spt_cmp_dir_ds;
    }

    public void setSpt_cmp_dir_ds(String spt_cmp_dir_ds) {
        this.spt_cmp_dir_ds = spt_cmp_dir_ds;
    }

    public String getOct_tip_cmp_cd() {
        return oct_tip_cmp_cd;
    }

    public void setOct_tip_cmp_cd(String oct_tip_cmp_cd) {
        this.oct_tip_cmp_cd = oct_tip_cmp_cd;
    }

    public String getOct_cmp_dir_ds() {
        return oct_cmp_dir_ds;
    }

    public void setOct_cmp_dir_ds(String oct_cmp_dir_ds) {
        this.oct_cmp_dir_ds = oct_cmp_dir_ds;
    }

    public String getNov_tip_cmp_cd() {
        return nov_tip_cmp_cd;
    }

    public void setNov_tip_cmp_cd(String nov_tip_cmp_cd) {
        this.nov_tip_cmp_cd = nov_tip_cmp_cd;
    }

    public String getNov_cmp_dir_ds() {
        return nov_cmp_dir_ds;
    }

    public void setNov_cmp_dir_ds(String nov_cmp_dir_ds) {
        this.nov_cmp_dir_ds = nov_cmp_dir_ds;
    }

    public String getDco_tip_cmp_cd() {
        return dco_tip_cmp_cd;
    }

    public void setDco_tip_cmp_cd(String dco_tip_cmp_cd) {
        this.dco_tip_cmp_cd = dco_tip_cmp_cd;
    }

    public String getDco_cmp_dir_ds() {
        return dco_cmp_dir_ds;
    }

    public void setDco_cmp_dir_ds(String dco_cmp_dir_ds) {
        this.dco_cmp_dir_ds = dco_cmp_dir_ds;
    }

    public Integer getInternetRsw() {
        return internetRsw;
    }

    public void setInternetRsw(Integer internetRsw) {
        this.internetRsw = internetRsw;
    }

    public String getConditionsFecha() {
        return conditionsFecha;
    }

    public void setConditionsFecha(String conditionsFecha) {
        this.conditionsFecha = conditionsFecha;
    }

    public String getAutomaticDebit() { return automaticDebit; }

    public void setAutomaticDebit(String automaticDebit) { this.automaticDebit = automaticDebit; }
}
