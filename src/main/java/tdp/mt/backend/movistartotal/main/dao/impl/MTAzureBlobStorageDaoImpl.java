package tdp.mt.backend.movistartotal.main.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tdp.mt.backend.movistartotal.main.dao.MTAzureBlobStorageDao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;

@Repository
public class MTAzureBlobStorageDaoImpl implements MTAzureBlobStorageDao {

    private Logger LOGGER = LogManager.getLogger(MTAzureBlobStorageDaoImpl.class);

    private DataSource datasource;

    @Autowired
    public MTAzureBlobStorageDaoImpl(DataSource datasource) {
        this.datasource = datasource;
    }


    @Override
    public void registerAzureBlobStorage(String mtorder, String container) {

        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_blob_storage(mtorder, estado_tdpvisor, codigo_pedido_tdpvisor, date_register, blob_date, file_storage, file_extension) "
                    + "SELECT b.mtorder, b.estado_tdpvisor, b.codigo_pedido_tdpvisor,CURRENT_TIMESTAMP - interval '5' hour,?, b.file_date, b.file_extension FROM ibmx_a07e6d02edaf552.mt_azure_file_storage b "
                    + "WHERE b.mtorder = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, container);
            ps.setString(2, mtorder);

            ps.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada al Endpoint", e);
        }

    }

    @Override
    public void registerAzureBlobStorageRetail(String mtorder, String container) {

        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_blob_storage(mtorder, estado_tdpvisor, codigo_pedido_tdpvisor, date_register, blob_date, file_storage, file_extension, code_mt) "
                    + "SELECT b.mtorder, b.estado_tdpvisor, b.codigo_pedido_tdpvisor,CURRENT_TIMESTAMP - interval '5' hour,?, b.file_date, b.file_extension, b.code_mt FROM ibmx_a07e6d02edaf552.mt_azure_file_storage b "
                    + "WHERE b.mtorder = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, container);
            ps.setString(2, mtorder);

            ps.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada al Endpoint", e);
        }

    }


}
