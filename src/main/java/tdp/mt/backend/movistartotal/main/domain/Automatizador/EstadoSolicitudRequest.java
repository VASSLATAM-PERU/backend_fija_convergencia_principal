package tdp.mt.backend.movistartotal.main.domain.Automatizador;

public class EstadoSolicitudRequest {
	

    private String codVtAppCd;
    private String codErrCd;
    private String desErrDs;
    private String fecHorRecWa;
    private String codEstVtaApp;
    private String desEstVtaApp;
    private String codPedAtiCd;
    private String codReqEedCd;
    private String codEstPedCd;
    private String desEstPedCd;
    private String fecHorReg;
    private String fecHorEmiPed;
    private String codCliCd;
    private String codCtaCd;
    private String codInsLegCd;
    private String numIdTlf;
    private String codCliCms;
    private String codCtaCms;
    private String desObsVtaApp;
    private String codIdWebEx1;
    private String desIdWebEx1;
    private String codEstWebEx1;
    private String desEstWebEx1;
    private String codIdWebEx2;
    private String desIdWebEx2;
    private String codEstWebEx2;
    private String desEstWebEx2;
    private String codIdWebEx3;
    private String desIdWebEx3;
    private String codEstWebEx3;
    private String desEstWebEx3;
    private String codIdWebEx4;
    private String desIdWebEx4;
    private String codEstWebEx4;
    private String desEstWebEx4;
    private String codIdWebEx5;
    private String desIdWebEx5;
    private String codEstWebEx5;
    private String desEstWebEx5;

    public String getCodVtAppCd() {
        return codVtAppCd;
    }

    public void setCodVtAppCd(String codVtAppCd) {
        this.codVtAppCd = codVtAppCd;
    }

    public String getCodErrCd() {
        return codErrCd;
    }

    public void setCodErrCd(String codErrCd) {
        this.codErrCd = codErrCd;
    }

    public String getDesErrDs() {
        return desErrDs;
    }

    public void setDesErrDs(String desErrDs) {
        this.desErrDs = desErrDs;
    }

    public String getFecHorRecWa() {
        return fecHorRecWa;
    }

    public void setFecHorRecWa(String fecHorRecWa) {
        this.fecHorRecWa = fecHorRecWa;
    }

    public String getCodEstVtaApp() {
        return codEstVtaApp;
    }

    public void setCodEstVtaApp(String codEstVtaApp) {
        this.codEstVtaApp = codEstVtaApp;
    }

    public String getDesEstVtaApp() {
        return desEstVtaApp;
    }

    public void setDesEstVtaApp(String desEstVtaApp) {
        this.desEstVtaApp = desEstVtaApp;
    }

    public String getCodPedAtiCd() {
        return codPedAtiCd;
    }

    public void setCodPedAtiCd(String codPedAtiCd) {
        this.codPedAtiCd = codPedAtiCd;
    }

    public String getCodReqEedCd() {
        return codReqEedCd;
    }

    public void setCodReqEedCd(String codReqEedCd) {
        this.codReqEedCd = codReqEedCd;
    }

    public String getCodEstPedCd() {
        return codEstPedCd;
    }

    public void setCodEstPedCd(String codEstPedCd) {
        this.codEstPedCd = codEstPedCd;
    }

    public String getDesEstPedCd() {
        return desEstPedCd;
    }

    public void setDesEstPedCd(String desEstPedCd) {
        this.desEstPedCd = desEstPedCd;
    }

    public String getFecHorReg() {
        return fecHorReg;
    }

    public void setFecHorReg(String fecHorReg) {
        this.fecHorReg = fecHorReg;
    }

    public String getFecHorEmiPed() {
        return fecHorEmiPed;
    }

    public void setFecHorEmiPed(String fecHorEmiPed) {
        this.fecHorEmiPed = fecHorEmiPed;
    }

    public String getCodCliCd() {
        return codCliCd;
    }

    public void setCodCliCd(String codCliCd) {
        this.codCliCd = codCliCd;
    }

    public String getCodCtaCd() {
        return codCtaCd;
    }

    public void setCodCtaCd(String codCtaCd) {
        this.codCtaCd = codCtaCd;
    }

    public String getCodInsLegCd() {
        return codInsLegCd;
    }

    public void setCodInsLegCd(String codInsLegCd) {
        this.codInsLegCd = codInsLegCd;
    }

    public String getNumIdTlf() {
        return numIdTlf;
    }

    public void setNumIdTlf(String numIdTlf) {
        this.numIdTlf = numIdTlf;
    }

    public String getCodCliCms() {
        return codCliCms;
    }

    public void setCodCliCms(String codCliCms) {
        this.codCliCms = codCliCms;
    }

    public String getCodCtaCms() {
        return codCtaCms;
    }

    public void setCodCtaCms(String codCtaCms) {
        this.codCtaCms = codCtaCms;
    }

    public String getDesObsVtaApp() {
        return desObsVtaApp;
    }

    public void setDesObsVtaApp(String desObsVtaApp) {
        this.desObsVtaApp = desObsVtaApp;
    }

    public String getCodIdWebEx1() {
        return codIdWebEx1;
    }

    public void setCodIdWebEx1(String codIdWebEx1) {
        this.codIdWebEx1 = codIdWebEx1;
    }

    public String getDesIdWebEx1() {
        return desIdWebEx1;
    }

    public void setDesIdWebEx1(String desIdWebEx1) {
        this.desIdWebEx1 = desIdWebEx1;
    }

    public String getCodEstWebEx1() {
        return codEstWebEx1;
    }

    public void setCodEstWebEx1(String codEstWebEx1) {
        this.codEstWebEx1 = codEstWebEx1;
    }

    public String getDesEstWebEx1() {
        return desEstWebEx1;
    }

    public void setDesEstWebEx1(String desEstWebEx1) {
        this.desEstWebEx1 = desEstWebEx1;
    }

    public String getCodIdWebEx2() {
        return codIdWebEx2;
    }

    public void setCodIdWebEx2(String codIdWebEx2) {
        this.codIdWebEx2 = codIdWebEx2;
    }

    public String getDesIdWebEx2() {
        return desIdWebEx2;
    }

    public void setDesIdWebEx2(String desIdWebEx2) {
        this.desIdWebEx2 = desIdWebEx2;
    }

    public String getCodEstWebEx2() {
        return codEstWebEx2;
    }

    public void setCodEstWebEx2(String codEstWebEx2) {
        this.codEstWebEx2 = codEstWebEx2;
    }

    public String getDesEstWebEx2() {
        return desEstWebEx2;
    }

    public void setDesEstWebEx2(String desEstWebEx2) {
        this.desEstWebEx2 = desEstWebEx2;
    }

    public String getCodIdWebEx3() {
        return codIdWebEx3;
    }

    public void setCodIdWebEx3(String codIdWebEx3) {
        this.codIdWebEx3 = codIdWebEx3;
    }

    public String getDesIdWebEx3() {
        return desIdWebEx3;
    }

    public void setDesIdWebEx3(String desIdWebEx3) {
        this.desIdWebEx3 = desIdWebEx3;
    }

    public String getCodEstWebEx3() {
        return codEstWebEx3;
    }

    public void setCodEstWebEx3(String codEstWebEx3) {
        this.codEstWebEx3 = codEstWebEx3;
    }

    public String getDesEstWebEx3() {
        return desEstWebEx3;
    }

    public void setDesEstWebEx3(String desEstWebEx3) {
        this.desEstWebEx3 = desEstWebEx3;
    }

    public String getCodIdWebEx4() {
        return codIdWebEx4;
    }

    public void setCodIdWebEx4(String codIdWebEx4) {
        this.codIdWebEx4 = codIdWebEx4;
    }

    public String getDesIdWebEx4() {
        return desIdWebEx4;
    }

    public void setDesIdWebEx4(String desIdWebEx4) {
        this.desIdWebEx4 = desIdWebEx4;
    }

    public String getCodEstWebEx4() {
        return codEstWebEx4;
    }

    public void setCodEstWebEx4(String codEstWebEx4) {
        this.codEstWebEx4 = codEstWebEx4;
    }

    public String getDesEstWebEx4() {
        return desEstWebEx4;
    }

    public void setDesEstWebEx4(String desEstWebEx4) {
        this.desEstWebEx4 = desEstWebEx4;
    }

    public String getCodIdWebEx5() {
        return codIdWebEx5;
    }

    public void setCodIdWebEx5(String codIdWebEx5) {
        this.codIdWebEx5 = codIdWebEx5;
    }

    public String getDesIdWebEx5() {
        return desIdWebEx5;
    }

    public void setDesIdWebEx5(String desIdWebEx5) {
        this.desIdWebEx5 = desIdWebEx5;
    }

    public String getCodEstWebEx5() {
        return codEstWebEx5;
    }

    public void setCodEstWebEx5(String codEstWebEx5) {
        this.codEstWebEx5 = codEstWebEx5;
    }

    public String getDesEstWebEx5() {
        return desEstWebEx5;
    }

    public void setDesEstWebEx5(String desEstWebEx5) {
        this.desEstWebEx5 = desEstWebEx5;
    }
}
