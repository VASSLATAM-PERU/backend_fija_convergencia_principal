package tdp.mt.backend.movistartotal.main.repository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import tdp.mt.backend.movistartotal.main.restclient.automatizador.NormalizadorModelVO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;


@Repository
public class AddressDao {
	private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;
    
    @Autowired
    private DataSource datasource;
    
    

    public Boolean goToNormalizador(String id_transaccion){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.contingencia_normalizador " +
                    "where id_transaccion = " + "'" + id_transaccion + "'" + " and normalizador = 'true' limit 1";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ContingenciaNormalizador obj = new ContingenciaNormalizador();
                    obj.setId_transaccion(rs.getString("id_transaccion"));
                    obj.setNormalizador(rs.getBoolean("normalizador"));

                    response = obj.getNormalizador();

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error leer contingencia_normalizador ", e);
        }

        return response;

    }
    
    public Boolean _exitsVentaVisor(String id_transaccion){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.automatizador_order " +
                    "where order_id = " + "'" + id_transaccion + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ContingenciaNormalizador obj = new ContingenciaNormalizador();
                    obj.setId_transaccion(rs.getString("order_id"));
                    response = true;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error _exitsVentaVisor", e);
        }

        return response;

    }
    
    
    public String autoDepaCmsPunto(String codAtis, String departamento){
        String response = "0";

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT "+departamento+" from ibmx_a07e6d02edaf552.tdp_sales_agent " +
                    "where codatis = " + "'" + codAtis + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    response = rs.getString(departamento);
                    if(response == null){
                        response = "0";
                    }
                    if(response != null ){
                       if(response.isEmpty()){
                           response = "0";
                       }
                    }
                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error autoDepaCmsPunto", e);
        }

        return response;

    }
    
    public NormalizadorModelVO showNormalizador(String id_transaccion){
        NormalizadorModelVO response = new NormalizadorModelVO();

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.contingencia_normalizador " +
                    "where id_transaccion = " + "'" + id_transaccion + "'" + " and normalizador = 'true' ORDER BY id desc LIMIT 1";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    NormalizadorModelVO obj = new NormalizadorModelVO();

                    obj.setId_transaccion(rs.getString("id_transaccion"));
                    obj.setNormalizador(rs.getBoolean("normalizador"));

                    obj.setTipo_urbanizacion(rs.getString("tipo_urbanizacion"));
                    obj.setNombre_urbanizacion(rs.getString("nombre_urbanizacion"));
                    obj.setNombreVia(rs.getString("nombreVia"));
                    obj.setTipoVia(rs.getString("tipoVia"));
                    obj.setManzana(rs.getString("manzana"));
                    obj.setLote(rs.getString("lote"));
                    obj.setCuadra(rs.getString("cuadra"));
                    obj.setTipo(rs.getString("tipo"));

                    response = obj;

                }

            }

        } catch (Exception e) {
            logger.error("Error leer contingencia_normalizador ", e);
        }

        return response;

    }
    
    public void insertAuto(String order_id){
        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.automatizador_order " +
                    "(order_id,duplicado_auto, flag_exits) "
                    + "VALUES (?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, order_id);
            ps.setString(2, "");
            ps.setInt(3, 0);

            //ps.setString(6, sdf.format(new Date()) + "-05:00");

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insertAuto", e);
        }

    }
    
    public void _registerDuplicado( String id_visor){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.tdp_order " +
                    "set duplicado_auto = 'ERR-0004' where order_id = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, id_visor);

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error update _registerDuplicado", e);
        }
    }
    
    public void _registerVentaVisor( String id_visor,String mensaje_duplicado){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.automatizador_order " +
                    "set flag_exits = '1', duplicado_auto = ? where order_id = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, mensaje_duplicado);
            ps.setString(2, id_visor);

            ps.executeUpdate();



        }   catch (Exception e) {
            e.printStackTrace();
            logger.error("Error _registerVentaVisor", e);
        }
    }
    
    
    public int _consultarDuplicado(String id_transaccion){
        Integer response = 0;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.automatizador_order " +
                    "where order_id = " + "'" + id_transaccion + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String text = rs.getString("duplicado_auto");
                    if(text.equalsIgnoreCase("ERR-0004")){
                        response = 1;
                    }
                    if(text.equalsIgnoreCase("ERR_BUS")){
                        response = 2;
                    }
                }
            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error _consultarDuplicado", e);
        }

        return response;

    }
}
