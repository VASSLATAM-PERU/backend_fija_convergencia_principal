package tdp.mt.backend.movistartotal.main.restclient.convergencia;

public class MtOfferVO {

    private long mtsaleid;
    private String offerName;
    private String offerMobileData;
    private String offerInternetSpeed;
    private String offerChannel;
    private String offerSVAs; //FIXME: completar o cambiar por data de SVA.
    private String offerDecos;

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferMobileData() {
        return offerMobileData;
    }

    public void setOfferMobileData(String offerMobileData) {
        this.offerMobileData = offerMobileData;
    }

    public String getOfferInternetSpeed() {
        return offerInternetSpeed;
    }

    public void setOfferInternetSpeed(String offerInternetSpeed) {
        this.offerInternetSpeed = offerInternetSpeed;
    }

    public String getOfferSVAs() {
        return offerSVAs;
    }

    public void setOfferSVAs(String offerSVAs) {
        this.offerSVAs = offerSVAs;
    }

    public String getOfferDecos() {
        return offerDecos;
    }

    public void setOfferDecos(String offerDecos) {
        this.offerDecos = offerDecos;
    }

    public long getMtsaleid() {
        return mtsaleid;
    }

    public void setMtsaleid(long mtsaleid) {
        this.mtsaleid = mtsaleid;
    }

    public String getOfferChannel() {
        return offerChannel;
    }

    public void setOfferChannel(String offerChannel) {
        this.offerChannel = offerChannel;
    }
}
