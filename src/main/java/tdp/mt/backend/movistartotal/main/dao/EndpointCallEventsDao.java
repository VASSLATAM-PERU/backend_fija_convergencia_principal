package tdp.mt.backend.movistartotal.main.dao;

import tdp.mt.backend.movistartotal.main.dto.EndpointCallEvent;

public interface EndpointCallEventsDao {

    void registerEventEndpoint(EndpointCallEvent event);
}
