package tdp.mt.backend.movistartotal.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "automatizador_order", schema = "ibmx_a07e6d02edaf552")
public class AutomatizadorOrder implements Serializable{
	@Id
    @Column
    private String order_id;

    @Column(name = "duplicado_auto")
    private String duplicado_auto;

    @Column(name = "flag_exits")
    private Integer flag_exits;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDuplicado_auto() {
        return duplicado_auto;
    }

    public void setDuplicado_auto(String duplicado_auto) {
        this.duplicado_auto = duplicado_auto;
    }

    public Integer getFlag_exits() {
        return flag_exits;
    }

    public void setFlag_exits(Integer flag_exits) {
        this.flag_exits = flag_exits;
    }
}
