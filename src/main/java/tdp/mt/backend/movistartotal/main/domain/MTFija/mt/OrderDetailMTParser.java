package tdp.mt.backend.movistartotal.main.domain.MTFija.mt;

import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.entity.OrderDetail;

public class OrderDetailMTParser implements MTParser<OrderDetail> {

    @Override
    public OrderDetail parse(FijaHDECRequest mtSaleReq) {

        OrderDetail orderD = new OrderDetail();

        if (mtSaleReq.getMtClientPackageReqs() != null) {
            if (mtSaleReq.getMtClientPackageReqs().size() > 0) {
//department,"
                orderD.setDepartment(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDepartment());
//province,"
                orderD.setProvince(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getProvince());
//district,"
                orderD.setDistrict(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDistrict());

                orderD.setInternetTech(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getCoverage());
//expertoCode,"
                orderD.setExpertoCode(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getFixedScore());
//tvTech,"
                orderD.setTvTech(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getTecnologiaTV());
//decosSD,"
                orderD.setDecosSD(extractInteger(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDecosSD()));
//decosHD,"
                orderD.setDecoshd(extractInteger(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDecosHD()));
//decosDVR,"
                orderD.setDecosDVR(extractInteger(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getDecosDVR()));
//blockTV as bloque_producto,"
                orderD.setBlockTV(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getBloqueTV());
//svaInternet,"
                orderD.setSvaInternet(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getSvaInternet());
//svaLine,"
                orderD.setSvaLine(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getSvaLinea());
//tvBlock,"
                orderD.setTvBlock(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getBloqueTV());
//blockTV,"
//altasTV,"
                orderD.setAltasTV(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getAltasTV());
//internetrsw, "
                orderD.setInternetRsw(extractInteger(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getInternetRSW()));
                orderD.setAddress(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getAddress());
                orderD.setAddressComplement(mtSaleReq.getMtClientPackageReqs().get(0).getMtFixedProductReqs().get(0).getAddressComplement());
            }
        }
//sendContracts,"
        orderD.setSendContracts(mtSaleReq.getEnvioContrato());
//dataProtection,"
        orderD.setDataProtection(mtSaleReq.getProteccionDatos());
//disableWhitePage,"
        orderD.setDisableWhitePage(mtSaleReq.getDesafiliacionPB());
//enableDigitalInvoice,"
        orderD.setEnableDigitalInvoice(mtSaleReq.getAfiliacionFD());
//internetTech,"
//parentalprotection,"
        orderD.setParentalProtection(mtSaleReq.getParentalProtection());
//publicationwhitepages, "
        orderD.setPublicationWhitePages(mtSaleReq.getPublicacionPB());
//automaticdebit, "
        orderD.setAutomaticDebit(mtSaleReq.getDebitoAutomatico());

        return orderD;
    }

    public Integer extractInteger(String cadena) {
        if (cadena == null) return null;
        else return Integer.parseInt(cadena);
    }
}
