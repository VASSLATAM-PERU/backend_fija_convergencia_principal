package tdp.mt.backend.movistartotal.main.util;

public class MotorConstants {

    //SVA PS CODES
    public static final String SVA_PS_ADICIONAL_DECO_SMART = "23269";
    public static final String SVA_PS_REPETIDOR_SMART_WIFI = "23026";

    public static final String SVA_PS_BLOQUE_ESTELAR = "23262";
    public static final String SVA_PS_BLOQUE_FULL_HD = "23249";
    public static final String SVA_PS_BLOQUE_FOX = "23264";
    public static final String SVA_PS_BLOQUE_HBO = "23263";

    public static final String SVA_SEPARADOR = " + ";
    public static final String SVA_NO_APLICA = "NO APLICA";
    public static final String SVA_BLOQUE_ESTELAR = "Bloque Estelar";
    public static final String SVA_BLOQUE_FULL_HD = "Bloque Full HD";
    public static final String SVA_BLOQUE_FOX = "Bloque Fox+ New";
    public static final String SVA_BLOQUE_HBO = "Bloque HBO New";

}
