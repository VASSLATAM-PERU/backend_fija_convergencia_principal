package tdp.mt.backend.movistartotal.main.restclient.domain;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.main.restclient.orderservice.ErrorStatus;

@JsonInclude(Include.NON_NULL)
public class Response {

	@JsonProperty("isSuccess")
    private Boolean isSuccess;
    @JsonProperty("errorStatus")
    private ErrorStatus errorStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The isSuccess
     */
    @JsonProperty("isSuccess")
    public Boolean getIsSuccess() {
        return isSuccess;
    }

    /**
     * 
     * @param isSuccess
     *     The isSuccess
     */
    @JsonProperty("isSuccess")
    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Response withIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
        return this;
    }

    /**
     * 
     * @return
     *     The errorStatus
     */
    @JsonProperty("errorStatus")
    public ErrorStatus getErrorStatus() {
        return errorStatus;
    }

    /**
     * 
     * @param errorStatus
     *     The errorStatus
     */
    @JsonProperty("errorStatus")
    public void setErrorStatus(ErrorStatus errorStatus) {
        this.errorStatus = errorStatus;
    }

    public Response withErrorStatus(ErrorStatus errorStatus) {
        this.errorStatus = errorStatus;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Response withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
