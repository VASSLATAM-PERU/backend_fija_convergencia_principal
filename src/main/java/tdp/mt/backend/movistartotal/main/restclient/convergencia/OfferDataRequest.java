package tdp.mt.backend.movistartotal.main.restclient.convergencia;

public class OfferDataRequest {

    private String mtorderid;

    public String getMtorderid() {
        return mtorderid;
    }

    public void setMtorderid(String mtorderid) {
        this.mtorderid = mtorderid;
    }
}
