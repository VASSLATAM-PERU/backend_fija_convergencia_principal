package tdp.mt.backend.movistartotal.main.restclient.mailing;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.main.util.UtilMethods;

import java.util.ArrayList;
import java.util.List;

public class EmailsClient extends AbstractClient<UserMailRequest,UserMailingResponse> {

    private static final String urlendpoint ="qallarix/v1/users/";
    private static final String urlmethod = "emails";
    public static final int PLANTILLA_CONTRATO_RETAIL = 173941; //PUEDE CAMBIAR
    public static final String CLIENT_NAME = "BASE_MT_WS_PEDIDO_NOMBRE";
    public static final String OFFER_NAME = "BASE_MT_WS_NOMBRE_PLAN";
    public static final String MOBILE_INFORMATION = "BASE_MT_WS_CANTIDADGB";
    public static final String INTERNET_SPEED = "BASE_MT_WS_CANTIDADMB";
    public static final String SVA = "BASE_MT_WS_PEDIDO_NOMBRESVA";
    public static final String CHANNEL_QTY = "BASE_MT_WS_PEDIDO_QCANALES";
    public static final String DECO_QTY = "BASE_MT_WS_PEDIDO_QDECOS";
    public static final String CONTRACT_LINK = "BASE_MT_WS_PEDIDO_VARIABLE";
    public static final String CLIENT_EMAIL = "BASE_MT_WS_PEDIDO_CORREO";

    public EmailsClient(ClientConfig config) {
        super(config);
        this.config = UtilMethods.buildConfigHeaderP(urlendpoint,urlmethod);
    }

    @Override
    protected String getServiceCode() {
        return "EMAIL_MT";
    }

    @Override
    protected ApiResponse<UserMailingResponse> getResponse(String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ApiResponse<UserMailingResponse> objExperto = mapper.readValue(json, new TypeReference<ApiResponse<UserMailingResponse>>() {
        });
        return objExperto;
    }

    public UserMailRequest getRequestDataTest(String name, String email, String fecha, String detail){
        UserMailRequest data = new UserMailRequest();
        data.setIdConversation(169243);

        List<Entry> entryList = new ArrayList<>();

        Entry entry1 = new Entry();
        entry1.setKey("BASE_HOGAR_NOMBRE");
        entry1.setValue(name);
        entryList.add(entry1);

        Entry entry2 = new Entry();
        entry2.setKey("BASE_HOGAR_CORREO");
        entry2.setValue(email);
        entryList.add(entry2);

        Entry entry3 = new Entry();
        entry3.setKey("BASE_HOGAR_FECHA_PEDIDO");
        entry3.setValue(fecha);
        entryList.add(entry3);

        Entry entry4 = new Entry();
        entry4.setKey("BASE_HOGAR_PRODUCTO");
        entry4.setValue("Prueba envio");
        entryList.add(entry4);

        Entry entry5 = new Entry();
        entry5.setKey("BASE_HOGAR_IMPORTE");
        entry5.setValue("jaja");
        entryList.add(entry5);

        Entry entry6 = new Entry();
        entry6.setKey("BASE_HOGAR_DETALLE");
        entry6.setValue(detail);
        entryList.add(entry6);

        Values valuesList = new Values();
        valuesList.setEntry(entryList);

        Values_ values = new Values_();
        values.setValues(valuesList);
        data.setValues(values);

        return data;
    }

    public UserMailRequest getMTContractRequestData(String customerName, String mtPlanName, String mobileInformation, String internetSpeed,
                                                    String svaInfo, String channelQty, String decoQty, String contractLink, String clientEmail) {
        UserMailRequest data = new UserMailRequest();
        data.setIdConversation(PLANTILLA_CONTRATO_RETAIL);

        List<Entry> entryList = new ArrayList<>();

        Entry entry1 = new Entry();
        entry1.setKey(CLIENT_NAME);
        entry1.setValue(customerName);
        entryList.add(entry1);

        Entry entry2 = new Entry();
        entry2.setKey(OFFER_NAME);
        entry2.setValue(mtPlanName);
        entryList.add(entry2);

        Entry entry3 = new Entry();
        entry3.setKey(MOBILE_INFORMATION);
        entry3.setValue(mobileInformation);
        entryList.add(entry3);

        Entry entry4 = new Entry();
        entry4.setKey(INTERNET_SPEED);
        entry4.setValue(internetSpeed);
        entryList.add(entry4);

        Entry entry5 = new Entry();
        entry5.setKey(SVA);
        entry5.setValue(svaInfo);
        entryList.add(entry5);

        Entry entry6 = new Entry();
        entry6.setKey(CHANNEL_QTY);
        entry6.setValue(channelQty);
        entryList.add(entry6);

        Entry entry7 = new Entry();
        entry7.setKey(DECO_QTY);
        entry7.setValue(decoQty);
        entryList.add(entry7);

        Entry entry8 = new Entry();
        entry8.setKey(CONTRACT_LINK);
        entry8.setValue(contractLink);
        entryList.add(entry8);

        Entry entry9 = new Entry();
        entry9.setKey(CLIENT_EMAIL);
        entry9.setValue(clientEmail);
        entryList.add(entry9);

        Values valuesList = new Values();
        valuesList.setEntry(entryList);

        Values_ values = new Values_();
        values.setValues(valuesList);
        data.setValues(values);

        return data;

    }

    /*
@nombre cliente
@nombre de plan
 @informacion de producto movil @velocidad de internet @sva  @ q de canales  @ q de decos
 @linkdecontratoquepasaráback
     */

}
