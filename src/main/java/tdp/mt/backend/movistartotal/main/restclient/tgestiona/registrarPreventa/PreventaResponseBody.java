package tdp.mt.backend.movistartotal.main.restclient.tgestiona.registrarPreventa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;
import tdp.mt.backend.movistartotal.main.restclient.tgestiona.ClientException;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PreventaResponseBody {

    @JsonProperty("Mensaje")
    private String mensaje;

    @JsonProperty("ServerException")
    private ApiResponseBodyFault serverException;

    @JsonProperty("ClientException")
    private ClientException ClientException;

    @JsonProperty("Mensaje")
    public String getMensaje() {
        return mensaje;
    }

    @JsonProperty("Mensaje")
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @JsonProperty("ClientException")
    public ClientException getClientException() {
        return ClientException;
    }

    @JsonProperty("ClientException")
    public void setClientException(ClientException clientException) {
        ClientException = clientException;
    }

    @JsonProperty("ServerException")
    public ApiResponseBodyFault getServerException() {
        return serverException;
    }

    @JsonProperty("ServerException")
    public void setServerException(ApiResponseBodyFault apiResponseBodyFault) {
        this.serverException = apiResponseBodyFault;
    }
}
