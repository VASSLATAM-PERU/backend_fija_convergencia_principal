package tdp.mt.backend.movistartotal.main.service;


import tdp.mt.backend.movistartotal.commonservice.domain.entity.Parameters;

public interface ParametersService {

    Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

    String equivalenceDocument(String appiConnect, String documentType);

}