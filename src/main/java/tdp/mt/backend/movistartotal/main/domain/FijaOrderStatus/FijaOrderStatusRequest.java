package tdp.mt.backend.movistartotal.main.domain.FijaOrderStatus;

public class FijaOrderStatusRequest {

    private String docType;
    private String docNumber;

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }
}
