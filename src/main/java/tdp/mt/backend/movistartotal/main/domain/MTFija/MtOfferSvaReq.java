package tdp.mt.backend.movistartotal.main.domain.MTFija;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the mt_offer_sva_log database table.
 *
 */
public class MtOfferSvaReq implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idSva;

	private String svaBillingId;

	private String svaName;

	private BigDecimal svaPrice;

	private String svaProductId;

	public MtOfferSvaReq() {
	}

	public Long getIdSva() {
		return this.idSva;
	}

	public void setIdSva(Long idSva) {
		this.idSva = idSva;
	}

	public String getSvaBillingId() {
		return this.svaBillingId;
	}

	public void setSvaBillingId(String svaBillingId) {
		this.svaBillingId = svaBillingId;
	}

	public String getSvaName() {
		return this.svaName;
	}

	public void setSvaName(String svaName) {
		this.svaName = svaName;
	}

	public BigDecimal getSvaPrice() {
		return this.svaPrice;
	}

	public void setSvaPrice(BigDecimal svaPrice) {
		this.svaPrice = svaPrice;
	}

	public String getSvaProductId() {
		return this.svaProductId;
	}

	public void setSvaProductId(String svaProductId) {
		this.svaProductId = svaProductId;
	}

}