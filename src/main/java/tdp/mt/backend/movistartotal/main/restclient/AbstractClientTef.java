package tdp.mt.backend.movistartotal.main.restclient;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientException;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientResult;
import tdp.mt.backend.movistartotal.commonms.common.connection.Api;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextHolder;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.main.restclient.domain.ApiRequestTef;
import tdp.mt.backend.movistartotal.main.restclient.domain.ApiRequestTefHeader;
import tdp.mt.backend.movistartotal.main.restclient.domain.ApiResponseTef;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public abstract class AbstractClientTef<T, R> {

    protected ClientTefConfig config;
    protected ServiceCallEvent event;

    protected AbstractClientTef(ClientTefConfig config) {
        super();
        this.config = config;
    }

    public ClientResult<ApiResponseTef> get(T body) {
        ClientResult<ApiResponseTef> result = new ClientResult<>();
        try {
            String json = doGetRequest(body);
            ApiResponseTef apiResponse = getResponse(json);
            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {
            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    public ClientResult<ApiResponseTef> post(T body) {
        ClientResult<ApiResponseTef> result = new ClientResult<>();

        try {
            String json = doRequest(body);
            ApiResponseTef apiResponse = getResponse(json);
            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {
            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    protected String doRequest(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    private String doGetRequest(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonResponse = Api.jerseyGET(uri,config.getApiId(),config.getApiSecret(),getApiGetRequest(body));
        getEvent().setServiceResponse(jsonResponse);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonResponse;
    }

    protected abstract ApiRequestTef getApiRequest(T body);

    protected abstract Map<String, String> getApiGetRequest(T body);

    protected ApiRequestTefHeader getHeader() {
        ApiRequestTefHeader apiRequestTefHeader = new ApiRequestTefHeader();
        apiRequestTefHeader.setUserLogin(config.getUserLogin());
        apiRequestTefHeader.setServiceChannel(config.getServiceChannel());
        apiRequestTefHeader.setSessionCode(config.getSessionCode());
        apiRequestTefHeader.setApplication(config.getApplication());
        apiRequestTefHeader.setIdMessage(config.getIdMessage());
        apiRequestTefHeader.setIpAddress(config.getIpAddress());
        apiRequestTefHeader.setFunctionalityCode(config.getFunctionalityCode());
        //java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestTefHeader.setTransactionTimestamp(config.getTransactionTimestamp());
        apiRequestTefHeader.setServiceName(config.getServiceName());
        apiRequestTefHeader.setVersion(config.getVersion());

        return apiRequestTefHeader;
    }

    protected ServiceCallEvent getEvent() {
        if (event == null) {
            if (VentaFijaContextHolder.getContext().getServiceCallEvent()==null) {
                event = new ServiceCallEvent();
            } else {
                event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
            }
            event.setServiceUrl(config.getUrl());
            event.setServiceCode(getServiceCode());
        }
        return event;
    }

    protected abstract String getServiceCode();

    //protected abstract ApiResponseTef getResponse(String json) throws Exception;

    //@Override
    protected ApiResponseTef getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        ApiResponseTef objBiometric= mapper.readValue(json, new TypeReference<ApiResponseTef>() {});
        return objBiometric;
    }


}
