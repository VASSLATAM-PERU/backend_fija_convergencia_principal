package tdp.mt.backend.movistartotal.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tdp.mt.backend.movistartotal.commonservice.util.LogVass;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.domain.Automatizador.EstadoSolicitudRequest;
import tdp.mt.backend.movistartotal.main.domain.Automatizador.EstadoSolicitudResponse;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECRequest;
import tdp.mt.backend.movistartotal.main.domain.MTFija.FijaHDECResponse;
import tdp.mt.backend.movistartotal.main.restclient.automatizador.FijaHDECResponseAuto;
import tdp.mt.backend.movistartotal.main.service.AutomatizadorService;

@RestController
public class AutomatizadorController {
	 private static final Logger logger = LogManager.getLogger(AutomatizadorController.class);

	    @Autowired
	    private AutomatizadorService automatizadorService;

	    @CrossOrigin(origins = "*")
	    @RequestMapping(value = "/automatizador/insertOrder", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	    public Response<FijaHDECResponseAuto> insertOrder (@RequestBody FijaHDECRequest request, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
	        logger.info("info insertOrder", request);

	        Response<FijaHDECResponseAuto> responseAuto = new Response<>();
	        Response<FijaHDECResponse> response = new Response<>();
	        response = automatizadorService.saveAndSendAutomatizador(request, codigoAplicacion);
	        
	        responseAuto.setResponseCode(response.getResponseCode());
	        responseAuto.setResponseMessage(response.getResponseMessage());
	        
	        responseAuto.setResponseData(new FijaHDECResponseAuto());
	        responseAuto.getResponseData().setCode(response.getResponseData().getCode());
	        responseAuto.getResponseData().setMessage(response.getResponseData().getMessage());
	        responseAuto.getResponseData().setEstadoAutomatizador(response.getResponseData().getEstadoTGestiona());
	        
	        return responseAuto;
	    }
	    
	    
	    @CrossOrigin(origins = "*")
	    @RequestMapping(value = "/estadoServicioAutomatizador", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	    public ResponseEntity<EstadoSolicitudResponse> EstadoServicioautomatizador() {
	        LogVass.startController(logger, "EstadoServicioautomatizador","INICIO");

	        ResponseEntity<EstadoSolicitudResponse> response = null;

	        EstadoSolicitudResponse estadoSolicitudResponse = new EstadoSolicitudResponse();
	        String resultado=automatizadorService.verificarSeviceAutomatizador();
	        if(resultado.equalsIgnoreCase("OK")) {
	        	estadoSolicitudResponse.setCodErrCd("OK");
	 	        estadoSolicitudResponse.setObsErrDs("SERVICIO FUNCIONANDO");
	 	        response = new ResponseEntity<EstadoSolicitudResponse>(estadoSolicitudResponse, HttpStatus.OK);
	 	        LogVass.finishController(logger, "EstadoServicioautomatizador", response);
	        }else {
	        	 estadoSolicitudResponse.setCodErrCd("ERROR");
	 	        estadoSolicitudResponse.setObsErrDs("ERROR BUS");
	 	        response = new ResponseEntity<EstadoSolicitudResponse>(estadoSolicitudResponse, HttpStatus.NOT_FOUND);
	 	        LogVass.finishController(logger, "EstadoServicioautomatizador", response);
	        }
	       
	        return response;
	    }
}
