package tdp.mt.backend.movistartotal.main.restclient.automatizador;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InsertRequestBodyAuto {

//    @JsonProperty("dataAutomatizador")
//    private DataAutomatizador dataAutomatizador;
	@JsonProperty("dataAutomatizador")
	private AutomatizadorSaleRequestBody dataAutomatizador;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("dataAutomatizador")
	public AutomatizadorSaleRequestBody getDataAutomatizador() {
		return dataAutomatizador;
	}
 
    @JsonProperty("dataAutomatizador")
	public void setDataAutomatizador(AutomatizadorSaleRequestBody dataAutomatizador) {
		this.dataAutomatizador = dataAutomatizador;
	}
    

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


	@JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
