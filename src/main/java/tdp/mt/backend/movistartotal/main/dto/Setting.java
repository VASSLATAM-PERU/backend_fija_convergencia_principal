/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdp.mt.backend.movistartotal.main.dto;

import javax.persistence.*;

/**
 *
 * @author Jose Cordova
 */
@Entity
@Table(name = "mt_setting", schema = "public")

@NamedQueries({
        @NamedQuery(name="mt_setting.findAll", query="SELECT s FROM Setting s")
})
public class Setting {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name="key")
    private String key;
    @Column(name="value")
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
}
