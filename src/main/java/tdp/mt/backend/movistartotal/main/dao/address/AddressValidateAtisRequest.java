package tdp.mt.backend.movistartotal.main.dao.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressValidateAtisRequest {

    @JsonProperty("provincia")
    private String provincia;
    @JsonProperty("tipoVia")
    private String tipoVia;
    @JsonProperty("nombreVia")
    private String nombreVia;
    @JsonProperty("numeroPuerta1")
    private String numeroPuerta1;
    @JsonProperty("numeroPuerta2")
    private String numeroPuerta2;
    @JsonProperty("cuadra")
    private String cuadra;
    @JsonProperty("tipoInterior")
    private String tipoInterior;
    @JsonProperty("numeroInterior")
    private String numeroInterior;
    @JsonProperty("piso")
    private String piso;
    @JsonProperty("tipoVivienda")
    private String tipoVivienda;
    @JsonProperty("nombreVivienda")
    private String nombreVivienda;
    @JsonProperty("tipoUrbanizacion")
    private String tipoUrbanizacion;
    @JsonProperty("nombreUrbanizacion")
    private String nombreUrbanizacion;
    @JsonProperty("manzana")
    private String manzana;
    @JsonProperty("lote")
    private String lote;
    @JsonProperty("kilometro")
    private String kilometro;

    @JsonProperty("provincia")
    public String getProvincia() {
        return provincia;
    }

    @JsonProperty("provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @JsonProperty("tipoVia")
    public String getTipoVia() {
        return tipoVia;
    }

    @JsonProperty("tipoVia")
    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    @JsonProperty("nombreVia")
    public String getNombreVia() {
        return nombreVia;
    }

    @JsonProperty("nombreVia")
    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    @JsonProperty("numeroPuerta1")
    public String getNumeroPuerta1() {
        return numeroPuerta1;
    }

    @JsonProperty("numeroPuerta1")
    public void setNumeroPuerta1(String numeroPuerta1) {
        this.numeroPuerta1 = numeroPuerta1;
    }

    @JsonProperty("numeroPuerta2")
    public String getNumeroPuerta2() {
        return numeroPuerta2;
    }

    @JsonProperty("numeroPuerta2")
    public void setNumeroPuerta2(String numeroPuerta2) {
        this.numeroPuerta2 = numeroPuerta2;
    }

    @JsonProperty("cuadra")
    public String getCuadra() {
        return cuadra;
    }

    @JsonProperty("cuadra")
    public void setCuadra(String cuadra) {
        this.cuadra = cuadra;
    }

    @JsonProperty("tipoInterior")
    public String getTipoInterior() {
        return tipoInterior;
    }

    @JsonProperty("tipoInterior")
    public void setTipoInterior(String tipoInterior) {
        this.tipoInterior = tipoInterior;
    }

    @JsonProperty("numeroInterior")
    public String getNumeroInterior() {
        return numeroInterior;
    }

    @JsonProperty("numeroInterior")
    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    @JsonProperty("piso")
    public String getPiso() {
        return piso;
    }

    @JsonProperty("piso")
    public void setPiso(String piso) {
        this.piso = piso;
    }

    @JsonProperty("tipoVivienda")
    public String getTipoVivienda() {
        return tipoVivienda;
    }

    @JsonProperty("tipoVivienda")
    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    @JsonProperty("nombreVivienda")
    public String getNombreVivienda() {
        return nombreVivienda;
    }

    @JsonProperty("nombreVivienda")
    public void setNombreVivienda(String nombreVivienda) {
        this.nombreVivienda = nombreVivienda;
    }

    @JsonProperty("tipoUrbanizacion")
    public String getTipoUrbanizacion() {
        return tipoUrbanizacion;
    }

    @JsonProperty("tipoUrbanizacion")
    public void setTipoUrbanizacion(String tipoUrbanizacion) {
        this.tipoUrbanizacion = tipoUrbanizacion;
    }

    @JsonProperty("nombreUrbanizacion")
    public String getNombreUrbanizacion() {
        return nombreUrbanizacion;
    }

    @JsonProperty("nombreUrbanizacion")
    public void setNombreUrbanizacion(String nombreUrbanizacion) {
        this.nombreUrbanizacion = nombreUrbanizacion;
    }

    @JsonProperty("manzana")
    public String getManzana() {
        return manzana;
    }

    @JsonProperty("manzana")
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @JsonProperty("lote")
    public String getLote() {
        return lote;
    }

    @JsonProperty("lote")
    public void setLote(String lote) {
        this.lote = lote;
    }

    @JsonProperty("kilometro")
    public String getKilometro() {
        return kilometro;
    }

    @JsonProperty("kilometro")
    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }
}
