package tdp.mt.backend.movistartotal.main.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tdp.mt.backend.movistartotal.main.dao.EndpointCallEventsDao;
import tdp.mt.backend.movistartotal.main.dto.EndpointCallEvent;
import tdp.mt.backend.movistartotal.main.service.EndpointCallEventsService;

@Service
public class EndpointCallEventsServiceImpl implements EndpointCallEventsService {

    @Autowired
    private EndpointCallEventsDao dao;


    @Override
    public void registerEventEndpoint(EndpointCallEvent event) {
       dao.registerEventEndpoint(event);
    }
}
