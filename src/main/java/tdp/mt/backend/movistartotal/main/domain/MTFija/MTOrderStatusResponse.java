package tdp.mt.backend.movistartotal.main.domain.MTFija;

public class MTOrderStatusResponse {

    private String fijaCodeStatus;
    private String fijaRegisterDate;

    public String getFijaCodeStatus() {
        return fijaCodeStatus;
    }

    public void setFijaCodeStatus(String fijaCodeStatus) {
        this.fijaCodeStatus = fijaCodeStatus;
    }

    public String getFijaRegisterDate() {
        return fijaRegisterDate;
    }

    public void setFijaRegisterDate(String fijaRegisterDate) {
        this.fijaRegisterDate = fijaRegisterDate;
    }
}
