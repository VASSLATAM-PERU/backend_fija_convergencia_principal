package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyIn {
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("password")
    private String password;
    @JsonProperty("idPaquete")
    private Integer idPaquete;
    @JsonProperty("CodigoUnico")
    private String codigoUnico;
    @JsonProperty("ubigeo")
    private String ubigeo;
    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("usarReferencia")
    private Boolean usarReferencia;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("idPaquete")
    public Integer getIdPaquete() {
        return idPaquete;
    }

    @JsonProperty("idPaquete")
    public void setIdPaquete(Integer idPaquete) {
        this.idPaquete = idPaquete;
    }

    @JsonProperty("CodigoUnico")
    public String getCodigoUnico() {
        return codigoUnico;
    }

    @JsonProperty("CodigoUnico")
    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    @JsonProperty("ubigeo")
    public String getUbigeo() {
        return ubigeo;
    }

    @JsonProperty("ubigeo")
    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @JsonProperty("direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("usarReferencia")
    public Boolean getUsarReferencia() {
        return usarReferencia;
    }

    @JsonProperty("usarReferencia")
    public void setUsarReferencia(Boolean usarReferencia) {
        this.usarReferencia = usarReferencia;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
