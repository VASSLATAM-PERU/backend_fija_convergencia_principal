package tdp.mt.backend.movistartotal.commonms.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class for common date operations
 * @author jvilcayp
 *
 */
public class DateUtils {
	private static Logger logger = LogManager.getLogger(DateUtils.class);
	
	private DateUtils() {
		super();
	}

	/**
	 * Remove time from date
	 * @param date to process
	 * @return date without time, null when error
	 */
	public static final Date removeTime (Date date) {
		Date d = null;
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	    try {
			d = sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			logger.error("Error when parsing date "+date, e);
		}
	    return d;
	}
	
	public static final String convertDateString (Date date) {
		String d = null;
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	    try {
			d = sdf.format(date);
		} catch (Exception e) {
			logger.error("Error when parsing date "+date, e);
		}
	    return d;
	}

	
	/**
	 * Remove a number of months to a date
	 * @param date to remove from
	 * @param numMonths to be removed
	 * @return the computed date
	 */
	public static final Date removeMonths (Date date, int numMonths) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, Math.abs(numMonths)*-1);
		return c.getTime();
	}
	
	/**
	 * Removes the date from a given date
	 * @param date to remove from
	 * @param numDays number of days to remove from date
	 * @return the computed date 
	 */
	public static final Date removeDays (Date date, int numDays) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		
		c.add(Calendar.DAY_OF_MONTH, Math.abs(numDays)*-1);
		
		return c.getTime();
	}
	
	

	public static final Date addDays (Date date, int numDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, Math.abs(numDays));
		return c.getTime();
	}
	
	public static final Date parse (String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		return sdf.parse(date);
	}

	public static final Date parse (String date, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(date);
	}
	
	
	public static final String format (String dateFormat, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(date);
	}
}
