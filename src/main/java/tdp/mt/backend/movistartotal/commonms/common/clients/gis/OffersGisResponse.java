package tdp.mt.backend.movistartotal.commonms.common.clients.gis;

public class OffersGisResponse {


    private String[] tecTV;

    private String[] tecInternet;

    private String[] operacion;

    private String[] velocidadMax;

    private String[] tipoSenal;

    public String[] getTecTV() {
        return tecTV;
    }

    public void setTecTV(String[] tecTV) {
        this.tecTV = tecTV;
    }

    public String[] getTecInternet() {
        return tecInternet;
    }

    public void setTecInternet(String[] tecInternet) {
        this.tecInternet = tecInternet;
    }

    public String[] getOperacion() {
        return operacion;
    }

    public void setOperacion(String[] operacion) {
        this.operacion = operacion;
    }

    public String[] getVelocidadMax() {
        return velocidadMax;
    }

    public void setVelocidadMax(String[] velocidadMax) {
        this.velocidadMax = velocidadMax;
    }

    public String[] getTipoSenal() {
        return tipoSenal;
    }

    public void setTipoSenal(String[] tipoSenal) {
        this.tipoSenal = tipoSenal;
    }
}
