package tdp.mt.backend.movistartotal.commonms.common.constant;

public class LegadosConstants {

    private LegadosConstants() {
    }

	public static final String PARQUE_1050 = "1050NW";

	public static final String PARQUE_CMS = "CMS";
	public static final String API_REQUEST_HEADER_DESTINATION_CMS = "ES:BUS:CMS:CMS";
	public static final String API_REQUEST_HEADER_OPERATION_STATUS_CMS = "Consulta Estado en CMS";
	public static final String API_REQUEST_HEADER_OPERATION_CMS = "Consulta Parque en CMS";

	public static final String PARQUE_ATIS = "ATIS";
	public static final String API_REQUEST_HEADER_DESTINATION_ATIS = "ES:BUS:ATIS:ATIS";
	public static final String API_REQUEST_HEADER_OPERATION_STATUS_ATIS = "Consulta Estado en ATIS";
	public static final String API_REQUEST_HEADER_OPERATION_ATIS = "Consulta Parque en ATIS";

}
