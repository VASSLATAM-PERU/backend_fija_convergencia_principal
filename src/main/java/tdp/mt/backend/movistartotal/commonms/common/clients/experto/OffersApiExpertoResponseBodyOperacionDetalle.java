package tdp.mt.backend.movistartotal.commonms.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OffersApiExpertoResponseBodyOperacionDetalle {
	@JsonProperty("CodProd")
	private String codProd;
	@JsonProperty("DesProd")
	private String desProd;
	@JsonProperty("CuotaFin")
	private float cuotaFin;
	@JsonProperty("MesesFin")
	private float mesesFin;
	@JsonProperty("PagoAdelantado")
	private float pagoAdelantado;
	@JsonProperty("NroDecosAdic")
	private float nroDecosAdic;

	public String getCodProd() {
		return codProd;
	}

	public void setCodProd(String codProd) {
		this.codProd = codProd;
	}

	public String getDesProd() {
		return desProd;
	}

	public void setDesProd(String desProd) {
		this.desProd = desProd;
	}

	public float getCuotaFin() {
		return cuotaFin;
	}

	public void setCuotaFin(float cuotaFin) {
		this.cuotaFin = cuotaFin;
	}

	public float getMesesFin() {
		return mesesFin;
	}

	public void setMesesFin(float mesesFin) {
		this.mesesFin = mesesFin;
	}

	public float getPagoAdelantado() {
		return pagoAdelantado;
	}

	public void setPagoAdelantado(float pagoAdelantado) {
		this.pagoAdelantado = pagoAdelantado;
	}

	public float getNroDecosAdic() {
		return nroDecosAdic;
	}

	public void setNroDecosAdic(float nroDecosAdic) {
		this.nroDecosAdic = nroDecosAdic;
	}
}
