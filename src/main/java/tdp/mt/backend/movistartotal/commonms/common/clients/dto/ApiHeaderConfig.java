package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiHeaderConfig {
	public static final String MSG_TYPE_REQUEST = "REQUEST";
	public static final String MSG_TYPE_RESPONSE = "RESPONSE";
	public static final String MSG_TYPE_ERROR = "ERROR";

	// header data
	@Value("${tdp.api.hdec.country}")
	private String country;
	@Value("${tdp.api.hdec.lang}")
	private String lang;
	@Value("${tdp.api.hdec.entity}")
	private String entity;
	@Value("${tdp.api.hdec.system}")
	private String system;
	@Value("${tdp.api.hdec.subsystem}")
	private String subsystem;
	@Value("${tdp.api.hdec.originator}")
	private String originator;
	@Value("${tdp.api.hdec.sender}")
	private String sender;
	@Value("${tdp.api.hdec.userid}")
	private String userId;
	@Value("${tdp.api.hdec.wsid}")
	private String wsId;
	@Value("${tdp.api.hdec.wsip}")
	private String wsIp;
	@Value("${tdp.api.hdec.operation}")
	private String operation;
	@Value("${tdp.api.hdec.destination}")
	private String destination;
	@Value("${tdp.api.hdec.execid}")
	private String execId;
	//@Value("${tdp.api.reniec.consultacliente.uri}")
	private String reniecConsultaClienteUri= "https://api.us.apiconnect.ibmcloud.com/telefonica-del-peru-production/ter/consulta-reniec/consultarPersona";
	//@Value("${tdp.api.reniec.consultacliente.apiid}")
	private String reniecConsultarClienteApiId= "486c5aa3-d9a7-47e1-b39e-bbbff1c1069c";
	//@Value("${tdp.api.reniec.consultacliente.apisecret}")
	private String reniecConsultarClienteApiSecret= "hW8gL3cJ4pG4cA3nJ6cX2cW6yV7lB5aG8eW5oU5vN6hL5gL5wN";

	@Value("${tdp.api.cms.consultaestado.uri}")
	private String cmsConsultaEstadoUri;
	@Value("${tdp.api.atis.consultaestado.uri}")
	private String atisConsultaEstadoUri;

	@Value("${tdp.api.cms.consultaestado.apiid}")
	private String cmsConsultarEstadoApiId;
	@Value("${tdp.api.cms.consultaestado.apisecret}")
	private String cmsConsultarEstadoApiSecret;

	@Value("${tdp.api.atis.consultaestado.apiid}")
	private String atisConsultarEstadoApiId;
	@Value("${tdp.api.atis.consultaestado.apisecret}")
	private String atisConsultarEstadoApiSecret;



	/* Sprint 8 - Backend de Servicio biometrico */

	public String getCountry() {
		return country;
	}

	public String getLang() {
		return lang;
	}

	public String getEntity() {
		return entity;
	}

	public String getSystem() {
		return system;
	}

	public String getSubsystem() {
		return subsystem;
	}

	public String getOriginator() {
		return originator;
	}

	public String getSender() {
		return sender;
	}

	public String getUserId() {
		return userId;
	}

	public String getWsId() {
		return wsId;
	}

	public String getWsIp() {
		return wsIp;
	}

	public String getOperation() {
		return operation;
	}

	public String getDestination() {
		return destination;
	}

	public String getExecId() {
		return execId;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setWsId(String wsId) {
		this.wsId = wsId;
	}

	public void setWsIp(String wsIp) {
		this.wsIp = wsIp;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getReniecConsultaClienteUri() {
		return reniecConsultaClienteUri;
	}

	public void setReniecConsultaClienteUri(String reniecConsultaClienteUri) {
		this.reniecConsultaClienteUri = reniecConsultaClienteUri;
	}

	public String getReniecConsultarClienteApiId() {
		return reniecConsultarClienteApiId;
	}

	public void setReniecConsultarClienteApiId(String reniecConsultarClienteApiId) {
		this.reniecConsultarClienteApiId = reniecConsultarClienteApiId;
	}

	public String getReniecConsultarClienteApiSecret() {
		return reniecConsultarClienteApiSecret;
	}

	public void setReniecConsultarClienteApiSecret(String reniecConsultarClienteApiSecret) {
		this.reniecConsultarClienteApiSecret = reniecConsultarClienteApiSecret;
	}

	public String getCmsConsultaEstadoUri() {
		return cmsConsultaEstadoUri;
	}

	public void setCmsConsultaEstadoUri(String cmsConsultaEstadoUri) {
		this.cmsConsultaEstadoUri = cmsConsultaEstadoUri;
	}

	public String getAtisConsultaEstadoUri() {
		return atisConsultaEstadoUri;
	}

	public void setAtisConsultaEstadoUri(String atisConsultaEstadoUri) {
		this.atisConsultaEstadoUri = atisConsultaEstadoUri;
	}

	public String getCmsConsultarEstadoApiId() {
		return cmsConsultarEstadoApiId;
	}

	public void setCmsConsultarEstadoApiId(String cmsConsultarEstadoApiId) {
		this.cmsConsultarEstadoApiId = cmsConsultarEstadoApiId;
	}

	public String getCmsConsultarEstadoApiSecret() {
		return cmsConsultarEstadoApiSecret;
	}

	public void setCmsConsultarEstadoApiSecret(String cmsConsultarEstadoApiSecret) {
		this.cmsConsultarEstadoApiSecret = cmsConsultarEstadoApiSecret;
	}

	public String getAtisConsultarEstadoApiId() {
		return atisConsultarEstadoApiId;
	}

	public void setAtisConsultarEstadoApiId(String atisConsultarEstadoApiId) {
		this.atisConsultarEstadoApiId = atisConsultarEstadoApiId;
	}

	public String getAtisConsultarEstadoApiSecret() {
		return atisConsultarEstadoApiSecret;
	}

	public void setAtisConsultarEstadoApiSecret(String atisConsultarEstadoApiSecret) {
		this.atisConsultarEstadoApiSecret = atisConsultarEstadoApiSecret;
	}
}