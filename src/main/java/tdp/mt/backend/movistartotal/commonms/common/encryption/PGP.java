package tdp.mt.backend.movistartotal.commonms.common.encryption;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;

import java.io.*;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;

public class PGP {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public static String encrypt(String msgText) throws IOException, PGPException, NoSuchProviderException {
		byte[] clearData = msgText.getBytes();

		PGPPublicKey encKey = PGPUtil.readPublicKey(new FileInputStream("/ms/server/pgp_venta_fija/public.gpg"));
		ByteArrayOutputStream encOut = new ByteArrayOutputStream();
		OutputStream out = new ArmoredOutputStream(encOut);
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(PGPCompressedDataGenerator.ZIP);
		OutputStream cos = comData.open(bOut);
		PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
		OutputStream pOut = lData.open(cos, PGPLiteralData.BINARY, PGPLiteralData.CONSOLE, clearData.length,
				new Date());
		pOut.write(clearData);
		lData.close();
		comData.close();
		PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(PGPEncryptedData.AES_128, true,
				new SecureRandom(), "BC");
		if (encKey != null) {
			encGen.addMethod(encKey);
			byte[] bytes = bOut.toByteArray();
			OutputStream cOut = encGen.open(out, bytes.length);
			cOut.write(bytes);
			cOut.close();
		}
		out.close();
		return new String(encOut.toByteArray());
	}

	public static String decrypt(String encryptedText, String password) throws Exception {
		byte[] encrypted = encryptedText.getBytes();
		InputStream in = new ByteArrayInputStream(encrypted);
		in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);
		PGPObjectFactory pgpF = new PGPObjectFactory(in);
		PGPEncryptedDataList enc;
		Object o = pgpF.nextObject();
		if (o instanceof PGPEncryptedDataList) {
			enc = (PGPEncryptedDataList) o;
		} else {
			enc = (PGPEncryptedDataList) pgpF.nextObject();
		}
		PGPPrivateKey sKey = null;
		PGPPublicKeyEncryptedData pbe = null;
		while (sKey == null && enc.getEncryptedDataObjects().hasNext()) {
			pbe = (PGPPublicKeyEncryptedData) enc.getEncryptedDataObjects().next();
			sKey = PGPUtil.findSecretKey(new FileInputStream("/ms/server/pgp_venta_fija/private.gpg"), pbe.getKeyID(),
					password.toCharArray());
		}
		if (pbe != null) {
			InputStream clear = pbe.getDataStream(sKey, "BC");
			PGPObjectFactory pgpFact = new PGPObjectFactory(clear);
			PGPCompressedData cData = (PGPCompressedData) pgpFact.nextObject();
			pgpFact = new PGPObjectFactory(cData.getDataStream());
			PGPLiteralData ld = (PGPLiteralData) pgpFact.nextObject();
			InputStream unc = ld.getInputStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int ch;
			while ((ch = unc.read()) >= 0) {
				out.write(ch);
			}
			byte[] returnBytes = out.toByteArray();
			out.close();
			return new String(returnBytes, "UTF-8");
		}
		return null;
	}
}
