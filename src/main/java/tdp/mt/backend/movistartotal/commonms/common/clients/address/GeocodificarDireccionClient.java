package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.main.util.ServiceUrlUtil;

import java.io.IOException;

public class GeocodificarDireccionClient extends AbstractClient<BodyIn, BodyOut> {

    private static final String urlendpoint ="ws-geopointEngine/";
    private static final String urlmethod = "GeocodificarDireccion";

    public GeocodificarDireccionClient(ClientConfig config) {
        super(config);
        this.config = new ClientConfig.ClientConfigBuilder()
                .setUrl(ServiceUrlUtil.BASE_SERVICE_URL2 + urlendpoint + urlmethod)
                .setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY2)
                .setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET2)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_GD)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_GD).build();
    }

    @Override
    protected String getServiceCode() {
        return "NORMALIZADOR";
    }

    @Override
    protected ApiResponse<BodyOut> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                new TypeReference<ApiResponse<BodyOut>>() {
                });
    }

}

