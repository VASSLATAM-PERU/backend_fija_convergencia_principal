package tdp.mt.backend.movistartotal.commonms.common.clients.dto.fault;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class Detail {

	@JsonProperty("QuerySubscriptionGroupValueFault")
	private TefFault querySubscriptionGroupValueFault;

	@JsonProperty("QuerySubscriptionGroupValueFault")
	public TefFault getQuerySubscriptionGroupValueFault() {
		return querySubscriptionGroupValueFault;
	}

	@JsonProperty("QuerySubscriptionGroupValueFault")
	public void setQuerySubscriptionGroupValueFault(TefFault querySubscriptionGroupValueFault) {
		this.querySubscriptionGroupValueFault = querySubscriptionGroupValueFault;
	}
}
