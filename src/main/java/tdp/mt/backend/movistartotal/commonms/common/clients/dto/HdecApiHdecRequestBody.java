package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"CodigoUnico", "Campana", "NombreDeCliente", "ApellidoPaterno", "ApellidoMaterno",
        "TipoDeDocumento", "NumeroDeDocumento", "TelefonoDeContacto1", "TelefonoDeContacto2", "Email",
        "EnvioDeContratos", "ProteccionDeDatos", "CanalDeVenta", "DetalleDeCanal", "NombreVendedor", "CodVendedorAtis",
        "CodVendedorCms", "ZonalDepartamentoVendedor", "RegionProvinciaDistritoVendedor", "TipoDeProducto",
        "SubProducto", "Departamento", "Provincia", "Distrito", "Direccion", "OperacionComercial", "TelefonoAMigrar",
        "DesafiliacionDePaginasBlancas", "AfiliacionAFacturaDigital", "TecnologiaDeInternet", "CodigoExperto",
        "TieneGrabacion", "FechaRegistro", "HoraRegistroWeb", "ModalidadDePago", "IdGrabacionNativo",
        "TecnologiaTelevision", "Paquetizacion", "AltasTv", "TipoEquipamientoDeco", "DniVendedor", "TelefonoOrigen",
        "TipoServicio", "ClienteCms", "DistritoVendedor", "DecosSd", "DecosHd", "DecosDvr", "BloqueTv", "SvaInternet",
        "SvaLinea", "DescuentoWinback", "CodigoDeServicioCms", "BloqueProducto", "CoordenadasX", "CoordenadasY",
        "WebParental", "MontoContado", "CodigoPostal", "PublicarGuia"})
public class HdecApiHdecRequestBody {

    @JsonProperty("CodigoUnico")
    private String codigoUnico;
    @JsonProperty("Campana")
    private String campana;
    @JsonProperty("NombreDeCliente")
    private String nombreDeCliente;
    @JsonProperty("ApellidoPaterno")
    private String apellidoPaterno;
    @JsonProperty("ApellidoMaterno")
    private String apellidoMaterno;
    @JsonProperty("TipoDeDocumento")
    private String tipoDeDocumento;
    @JsonProperty("NumeroDeDocumento")
    private String numeroDeDocumento;
    @JsonProperty("TelefonoDeContacto1")
    private String telefonoDeContacto1;
    @JsonProperty("TelefonoDeContacto2")
    private String telefonoDeContacto2;
    @JsonProperty("Email")
    private String email;
    @JsonProperty("EnvioDeContratos")
    private String envioDeContratos;
    @JsonProperty("ProteccionDeDatos")
    private String proteccionDeDatos;
    @JsonProperty("CanalDeVenta")
    private String canalDeVenta;
    @JsonProperty("DetalleDeCanal")
    private String detalleDeCanal;
    @JsonProperty("NombreVendedor")
    private String nombreVendedor;
    @JsonProperty("CodVendedorAtis")
    private String codVendedorAtis;
    @JsonProperty("CodVendedorCms")
    private String codVendedorCms;
    @JsonProperty("ZonalDepartamentoVendedor")
    private String zonalDepartamentoVendedor;
    @JsonProperty("RegionProvinciaDistritoVendedor")
    private String regionProvinciaDistritoVendedor;
    @JsonProperty("TipoDeProducto")
    private String tipoDeProducto;
    @JsonProperty("SubProducto")
    private String subProducto;
    @JsonProperty("Departamento")
    private String departamento;
    @JsonProperty("Provincia")
    private String provincia;
    @JsonProperty("Distrito")
    private String distrito;
    @JsonProperty("Direccion")
    private String direccion;
    @JsonProperty("OperacionComercial")
    private String operacionComercial;
    @JsonProperty("TelefonoAMigrar")
    private String telefonoAMigrar;
    @JsonProperty("DesafiliacionDePaginasBlancas")
    private String desafiliacionDePaginasBlancas;
    @JsonProperty("AfiliacionAFacturaDigital")
    private String afiliacionAFacturaDigital;
    @JsonProperty("TecnologiaDeInternet")
    private String tecnologiaDeInternet;
    @JsonProperty("CodigoExperto")
    private String codigoExperto;
    @JsonProperty("TieneGrabacion")
    private String tieneGrabacion;
    @JsonProperty("FechaRegistro")
    private String fechaRegistro;
    @JsonProperty("HoraRegistroWeb")
    private String horaRegistroWeb;
    @JsonProperty("ModalidadDePago")
    private String modalidadDePago;
    @JsonProperty("IdGrabacionNativo")
    private String idGrabacionNativo;
    @JsonProperty("TecnologiaTelevision")
    private String tecnologiaTelevision;
    @JsonProperty("Paquetizacion")
    private String paquetizacion;
    @JsonProperty("AltasTv")
    private String altasTv;
    @JsonProperty("TipoEquipamientoDeco")
    private String tipoEquipamientoDeco;
    @JsonProperty("DniVendedor")
    private String dniVendedor;
    @JsonProperty("TelefonoOrigen")
    private String telefonoOrigen;
    @JsonProperty("TipoServicio")
    private String tipoServicio;
    @JsonProperty("ClienteCms")
    private String clienteCms;
    @JsonProperty("DistritoVendedor")
    private String distritoVendedor;
    @JsonProperty("DecosSd")
    private String decosSd;
    @JsonProperty("DecosHd")
    private String decosHd;
    @JsonProperty("DecosDvr")
    private String decosDvr;
    @JsonProperty("BloqueTv")
    private String bloqueTv;
    @JsonProperty("SvaInternet")
    private String svaInternet;
    @JsonProperty("SvaLinea")
    private String svaLinea;
    @JsonProperty("DescuentoWinback")
    private String descuentoWinback;
    @JsonProperty("CodigoDeServicioCms")
    private String codigoDeServicioCms;
    @JsonProperty("BloqueProducto")
    private String bloqueProducto;
    @JsonProperty("CoordenadasX")
    private String coordenadasX;
    @JsonProperty("CoordenadasY")
    private String coordenadasY;
    @JsonProperty("WebParental")
    private String webParental;
    @JsonProperty("MontoContado")
    private String montoContado;
    @JsonProperty("CodigoPostal")
    private String codigoPostal;
    @JsonProperty("PublicarGuia")
    private String publicarGuia;
    @JsonProperty("RepetidorSmartWifi")
    private Integer repetidorSmartWifi;
    @JsonProperty("ModoVenta")
    private String modoVenta;
    @JsonProperty("Nacionalidad")
    private String nacionalidad;

    /**
     * @return The codigoUnico
     */
    @JsonProperty("CodigoUnico")
    public String getCodigoUnico() {
        return codigoUnico;
    }

    /**
     * @param codigoUnico The CodigoUnico
     */
    @JsonProperty("CodigoUnico")
    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    /**
     * @return The campana
     */
    @JsonProperty("Campana")
    public String getCampana() {
        return campana;
    }

    /**
     * @param campana The Campana
     */
    @JsonProperty("Campana")
    public void setCampana(String campana) {
        this.campana = campana;
    }

    /**
     * @return The nombreDeCliente
     */
    @JsonProperty("NombreDeCliente")
    public String getNombreDeCliente() {
        return nombreDeCliente;
    }

    /**
     * @param nombreDeCliente The NombreDeCliente
     */
    @JsonProperty("NombreDeCliente")
    public void setNombreDeCliente(String nombreDeCliente) {
        this.nombreDeCliente = nombreDeCliente;
    }

    /**
     * @return The apellidoPaterno
     */
    @JsonProperty("ApellidoPaterno")
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno The ApellidoPaterno
     */
    @JsonProperty("ApellidoPaterno")
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return The apellidoMaterno
     */
    @JsonProperty("ApellidoMaterno")
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno The ApellidoMaterno
     */
    @JsonProperty("ApellidoMaterno")
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return The tipoDeDocumento
     */
    @JsonProperty("TipoDeDocumento")
    public String getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    /**
     * @param tipoDeDocumento The TipoDeDocumento
     */
    @JsonProperty("TipoDeDocumento")
    public void setTipoDeDocumento(String tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    /**
     * @return The numeroDeDocumento
     */
    @JsonProperty("NumeroDeDocumento")
    public String getNumeroDeDocumento() {
        return numeroDeDocumento;
    }

    /**
     * @param numeroDeDocumento The NumeroDeDocumento
     */
    @JsonProperty("NumeroDeDocumento")
    public void setNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
    }

    /**
     * @return The telefonoDeContacto1
     */
    @JsonProperty("TelefonoDeContacto1")
    public String getTelefonoDeContacto1() {
        return telefonoDeContacto1;
    }

    /**
     * @param telefonoDeContacto1 The TelefonoDeContacto1
     */
    @JsonProperty("TelefonoDeContacto1")
    public void setTelefonoDeContacto1(String telefonoDeContacto1) {
        this.telefonoDeContacto1 = telefonoDeContacto1;
    }

    /**
     * @return The telefonoDeContacto2
     */
    @JsonProperty("TelefonoDeContacto2")
    public String getTelefonoDeContacto2() {
        return telefonoDeContacto2;
    }

    /**
     * @param telefonoDeContacto2 The TelefonoDeContacto2
     */
    @JsonProperty("TelefonoDeContacto2")
    public void setTelefonoDeContacto2(String telefonoDeContacto2) {
        this.telefonoDeContacto2 = telefonoDeContacto2;
    }

    /**
     * @return The email
     */
    @JsonProperty("Email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The Email
     */
    @JsonProperty("Email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The envioDeContratos
     */
    @JsonProperty("EnvioDeContratos")
    public String getEnvioDeContratos() {
        return envioDeContratos;
    }

    /**
     * @param envioDeContratos The EnvioDeContratos
     */
    @JsonProperty("EnvioDeContratos")
    public void setEnvioDeContratos(String envioDeContratos) {
        this.envioDeContratos = envioDeContratos;
    }

    /**
     * @return The proteccionDeDatos
     */
    @JsonProperty("ProteccionDeDatos")
    public String getProteccionDeDatos() {
        return proteccionDeDatos;
    }

    /**
     * @param proteccionDeDatos The ProteccionDeDatos
     */
    @JsonProperty("ProteccionDeDatos")
    public void setProteccionDeDatos(String proteccionDeDatos) {
        this.proteccionDeDatos = proteccionDeDatos;
    }

    /**
     * @return The canalDeVenta
     */
    @JsonProperty("CanalDeVenta")
    public String getCanalDeVenta() {
        return canalDeVenta;
    }

    /**
     * @param canalDeVenta The CanalDeVenta
     */
    @JsonProperty("CanalDeVenta")
    public void setCanalDeVenta(String canalDeVenta) {
        this.canalDeVenta = canalDeVenta;
    }

    /**
     * @return The detalleDeCanal
     */
    @JsonProperty("DetalleDeCanal")
    public String getDetalleDeCanal() {
        return detalleDeCanal;
    }

    /**
     * @param detalleDeCanal The DetalleDeCanal
     */
    @JsonProperty("DetalleDeCanal")
    public void setDetalleDeCanal(String detalleDeCanal) {
        this.detalleDeCanal = detalleDeCanal;
    }

    /**
     * @return The nombreVendedor
     */
    @JsonProperty("NombreVendedor")
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     * @param nombreVendedor The NombreVendedor
     */
    @JsonProperty("NombreVendedor")
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    /**
     * @return The codVendedorAtis
     */
    @JsonProperty("CodVendedorAtis")
    public String getCodVendedorAtis() {
        return codVendedorAtis;
    }

    /**
     * @param codVendedorAtis The CodVendedorAtis
     */
    @JsonProperty("CodVendedorAtis")
    public void setCodVendedorAtis(String codVendedorAtis) {
        this.codVendedorAtis = codVendedorAtis;
    }

    /**
     * @return The codVendedorCms
     */
    @JsonProperty("CodVendedorCms")
    public String getCodVendedorCms() {
        return codVendedorCms;
    }

    /**
     * @param codVendedorCms The CodVendedorCms
     */
    @JsonProperty("CodVendedorCms")
    public void setCodVendedorCms(String codVendedorCms) {
        this.codVendedorCms = codVendedorCms;
    }

    /**
     * @return The zonalDepartamentoVendedor
     */
    @JsonProperty("ZonalDepartamentoVendedor")
    public String getZonalDepartamentoVendedor() {
        return zonalDepartamentoVendedor;
    }

    /**
     * @param zonalDepartamentoVendedor The ZonalDepartamentoVendedor
     */
    @JsonProperty("ZonalDepartamentoVendedor")
    public void setZonalDepartamentoVendedor(String zonalDepartamentoVendedor) {
        this.zonalDepartamentoVendedor = zonalDepartamentoVendedor;
    }

    /**
     * @return The regionProvinciaDistritoVendedor
     */
    @JsonProperty("RegionProvinciaDistritoVendedor")
    public String getRegionProvinciaDistritoVendedor() {
        return regionProvinciaDistritoVendedor;
    }

    /**
     * @param regionProvinciaDistritoVendedor The RegionProvinciaDistritoVendedor
     */
    @JsonProperty("RegionProvinciaDistritoVendedor")
    public void setRegionProvinciaDistritoVendedor(String regionProvinciaDistritoVendedor) {
        this.regionProvinciaDistritoVendedor = regionProvinciaDistritoVendedor;
    }

    /**
     * @return The tipoDeProducto
     */
    @JsonProperty("TipoDeProducto")
    public String getTipoDeProducto() {
        return tipoDeProducto;
    }

    /**
     * @param tipoDeProducto The TipoDeProducto
     */
    @JsonProperty("TipoDeProducto")
    public void setTipoDeProducto(String tipoDeProducto) {
        this.tipoDeProducto = tipoDeProducto;
    }

    /**
     * @return The subProducto
     */
    @JsonProperty("SubProducto")
    public String getSubProducto() {
        return subProducto;
    }

    /**
     * @param subProducto The SubProducto
     */
    @JsonProperty("SubProducto")
    public void setSubProducto(String subProducto) {
        this.subProducto = subProducto;
    }

    /**
     * @return The departamento
     */
    @JsonProperty("Departamento")
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento The Departamento
     */
    @JsonProperty("Departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return The provincia
     */
    @JsonProperty("Provincia")
    public String getProvincia() {
        return provincia;
    }

    /**
     * @param provincia The Provincia
     */
    @JsonProperty("Provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /**
     * @return The distrito
     */
    @JsonProperty("Distrito")
    public String getDistrito() {
        return distrito;
    }

    /**
     * @param distrito The Distrito
     */
    @JsonProperty("Distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    /**
     * @return The direccion
     */
    @JsonProperty("Direccion")
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion The Direccion
     */
    @JsonProperty("Direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return The operacionComercial
     */
    @JsonProperty("OperacionComercial")
    public String getOperacionComercial() {
        return operacionComercial;
    }

    /**
     * @param operacionComercial The OperacionComercial
     */
    @JsonProperty("OperacionComercial")
    public void setOperacionComercial(String operacionComercial) {
        this.operacionComercial = operacionComercial;
    }

    /**
     * @return The telefonoAMigrar
     */
    @JsonProperty("TelefonoAMigrar")
    public String getTelefonoAMigrar() {
        return telefonoAMigrar;
    }

    /**
     * @param telefonoAMigrar The TelefonoAMigrar
     */
    @JsonProperty("TelefonoAMigrar")
    public void setTelefonoAMigrar(String telefonoAMigrar) {
        this.telefonoAMigrar = telefonoAMigrar;
    }

    /**
     * @return The desafiliacionDePaginasBlancas
     */
    @JsonProperty("DesafiliacionDePaginasBlancas")
    public String getDesafiliacionDePaginasBlancas() {
        return desafiliacionDePaginasBlancas;
    }

    /**
     * @param desafiliacionDePaginasBlancas The DesafiliacionDePaginasBlancas
     */
    @JsonProperty("DesafiliacionDePaginasBlancas")
    public void setDesafiliacionDePaginasBlancas(String desafiliacionDePaginasBlancas) {
        this.desafiliacionDePaginasBlancas = desafiliacionDePaginasBlancas;
    }

    /**
     * @return The afiliacionAFacturaDigital
     */
    @JsonProperty("AfiliacionAFacturaDigital")
    public String getAfiliacionAFacturaDigital() {
        return afiliacionAFacturaDigital;
    }

    /**
     * @param afiliacionAFacturaDigital The AfiliacionAFacturaDigital
     */
    @JsonProperty("AfiliacionAFacturaDigital")
    public void setAfiliacionAFacturaDigital(String afiliacionAFacturaDigital) {
        this.afiliacionAFacturaDigital = afiliacionAFacturaDigital;
    }

    /**
     * @return The tecnologiaDeInternet
     */
    @JsonProperty("TecnologiaDeInternet")
    public String getTecnologiaDeInternet() {
        return tecnologiaDeInternet;
    }

    /**
     * @param tecnologiaDeInternet The TecnologiaDeInternet
     */
    @JsonProperty("TecnologiaDeInternet")
    public void setTecnologiaDeInternet(String tecnologiaDeInternet) {
        this.tecnologiaDeInternet = tecnologiaDeInternet;
    }

    /**
     * @return The codigoExperto
     */
    @JsonProperty("CodigoExperto")
    public String getCodigoExperto() {
        return codigoExperto;
    }

    /**
     * @param codigoExperto The CodigoExperto
     */
    @JsonProperty("CodigoExperto")
    public void setCodigoExperto(String codigoExperto) {
        this.codigoExperto = codigoExperto;
    }

    /**
     * @return The tieneGrabacion
     */
    @JsonProperty("TieneGrabacion")
    public String getTieneGrabacion() {
        return tieneGrabacion;
    }

    /**
     * @param tieneGrabacion The TieneGrabacion
     */
    @JsonProperty("TieneGrabacion")
    public void setTieneGrabacion(String tieneGrabacion) {
        this.tieneGrabacion = tieneGrabacion;
    }

    /**
     * @return The fechaRegistro
     */
    @JsonProperty("FechaRegistro")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * @param fechaRegistro The FechaRegistro
     */
    @JsonProperty("FechaRegistro")
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     * @return The horaRegistroWeb
     */
    @JsonProperty("HoraRegistroWeb")
    public String getHoraRegistroWeb() {
        return horaRegistroWeb;
    }

    /**
     * @param horaRegistroWeb The HoraRegistroWeb
     */
    @JsonProperty("HoraRegistroWeb")
    public void setHoraRegistroWeb(String horaRegistroWeb) {
        this.horaRegistroWeb = horaRegistroWeb;
    }

    /**
     * @return The modalidadDePago
     */
    @JsonProperty("ModalidadDePago")
    public String getModalidadDePago() {
        return modalidadDePago;
    }

    /**
     * @param modalidadDePago The ModalidadDePago
     */
    @JsonProperty("ModalidadDePago")
    public void setModalidadDePago(String modalidadDePago) {
        this.modalidadDePago = modalidadDePago;
    }

    /**
     * @return The idGrabacionNativo
     */
    @JsonProperty("IdGrabacionNativo")
    public String getIdGrabacionNativo() {
        return idGrabacionNativo;
    }

    /**
     * @param idGrabacionNativo The IdGrabacionNativo
     */
    @JsonProperty("IdGrabacionNativo")
    public void setIdGrabacionNativo(String idGrabacionNativo) {
        this.idGrabacionNativo = idGrabacionNativo;
    }

    /**
     * @return The tecnologiaTelevision
     */
    @JsonProperty("TecnologiaTelevision")
    public String getTecnologiaTelevision() {
        return tecnologiaTelevision;
    }

    /**
     * @param tecnologiaTelevision The TecnologiaTelevision
     */
    @JsonProperty("TecnologiaTelevision")
    public void setTecnologiaTelevision(String tecnologiaTelevision) {
        this.tecnologiaTelevision = tecnologiaTelevision;
    }

    /**
     * @return The paquetizacion
     */
    @JsonProperty("Paquetizacion")
    public String getPaquetizacion() {
        return paquetizacion;
    }

    /**
     * @param paquetizacion The Paquetizacion
     */
    @JsonProperty("Paquetizacion")
    public void setPaquetizacion(String paquetizacion) {
        this.paquetizacion = paquetizacion;
    }

    /**
     * @return The altasTv
     */
    @JsonProperty("AltasTv")
    public String getAltasTv() {
        return altasTv;
    }

    /**
     * @param altasTv The AltasTv
     */
    @JsonProperty("AltasTv")
    public void setAltasTv(String altasTv) {
        this.altasTv = altasTv;
    }

    /**
     * @return The tipoEquipamientoDeco
     */
    @JsonProperty("TipoEquipamientoDeco")
    public String getTipoEquipamientoDeco() {
        return tipoEquipamientoDeco;
    }

    /**
     * @param tipoEquipamientoDeco The TipoEquipamientoDeco
     */
    @JsonProperty("TipoEquipamientoDeco")
    public void setTipoEquipamientoDeco(String tipoEquipamientoDeco) {
        this.tipoEquipamientoDeco = tipoEquipamientoDeco;
    }

    /**
     * @return The dniVendedor
     */
    @JsonProperty("DniVendedor")
    public String getDniVendedor() {
        return dniVendedor;
    }

    /**
     * @param dniVendedor The DniVendedor
     */
    @JsonProperty("DniVendedor")
    public void setDniVendedor(String dniVendedor) {
        this.dniVendedor = dniVendedor;
    }

    /**
     * @return The telefonoOrigen
     */
    @JsonProperty("TelefonoOrigen")
    public String getTelefonoOrigen() {
        return telefonoOrigen;
    }

    /**
     * @param telefonoOrigen The TelefonoOrigen
     */
    @JsonProperty("TelefonoOrigen")
    public void setTelefonoOrigen(String telefonoOrigen) {
        this.telefonoOrigen = telefonoOrigen;
    }

    /**
     * @return The tipoServicio
     */
    @JsonProperty("TipoServicio")
    public String getTipoServicio() {
        return tipoServicio;
    }

    /**
     * @param tipoServicio The TipoServicio
     */
    @JsonProperty("TipoServicio")
    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    /**
     * @return The clienteCms
     */
    @JsonProperty("ClienteCms")
    public String getClienteCms() {
        return clienteCms;
    }

    /**
     * @param clienteCms The ClienteCms
     */
    @JsonProperty("ClienteCms")
    public void setClienteCms(String clienteCms) {
        this.clienteCms = clienteCms;
    }

    /**
     * @return The distritoVendedor
     */
    @JsonProperty("DistritoVendedor")
    public String getDistritoVendedor() {
        return distritoVendedor;
    }

    /**
     * @param distritoVendedor The DistritoVendedor
     */
    @JsonProperty("DistritoVendedor")
    public void setDistritoVendedor(String distritoVendedor) {
        this.distritoVendedor = distritoVendedor;
    }

    /**
     * @return The decosSd
     */
    @JsonProperty("DecosSd")
    public String getDecosSd() {
        return decosSd;
    }

    /**
     * @param decosSd The DecosSd
     */
    @JsonProperty("DecosSd")
    public void setDecosSd(String decosSd) {
        this.decosSd = decosSd;
    }

    /**
     * @return The decosHd
     */
    @JsonProperty("DecosHd")
    public String getDecosHd() {
        return decosHd;
    }

    /**
     * @param decosHd The DecosHd
     */
    @JsonProperty("DecosHd")
    public void setDecosHd(String decosHd) {
        this.decosHd = decosHd;
    }

    /**
     * @return The decosDvr
     */
    @JsonProperty("DecosDvr")
    public String getDecosDvr() {
        return decosDvr;
    }

    /**
     * @param decosDvr The DecosDvr
     */
    @JsonProperty("DecosDvr")
    public void setDecosDvr(String decosDvr) {
        this.decosDvr = decosDvr;
    }

    /**
     * @return The bloqueTv
     */
    @JsonProperty("BloqueTv")
    public String getBloqueTv() {
        return bloqueTv;
    }

    /**
     * @param bloqueTv The BloqueTv
     */
    @JsonProperty("BloqueTv")
    public void setBloqueTv(String bloqueTv) {
        this.bloqueTv = bloqueTv;
    }

    /**
     * @return The svaInternet
     */
    @JsonProperty("SvaInternet")
    public String getSvaInternet() {
        return svaInternet;
    }

    /**
     * @param svaInternet The SvaInternet
     */
    @JsonProperty("SvaInternet")
    public void setSvaInternet(String svaInternet) {
        this.svaInternet = svaInternet;
    }

    /**
     * @return The svaLinea
     */
    @JsonProperty("SvaLinea")
    public String getSvaLinea() {
        return svaLinea;
    }

    /**
     * @param svaLinea The SvaLinea
     */
    @JsonProperty("SvaLinea")
    public void setSvaLinea(String svaLinea) {
        this.svaLinea = svaLinea;
    }

    /**
     * @return The descuentoWinback
     */
    @JsonProperty("DescuentoWinback")
    public String getDescuentoWinback() {
        return descuentoWinback;
    }

    /**
     * @param descuentoWinback The DescuentoWinback
     */
    @JsonProperty("DescuentoWinback")
    public void setDescuentoWinback(String descuentoWinback) {
        this.descuentoWinback = descuentoWinback;
    }

    /**
     * @return The codigoDeServicioCms
     */
    @JsonProperty("CodigoDeServicioCms")
    public String getCodigoDeServicioCms() {
        return codigoDeServicioCms;
    }

    /**
     * @param codigoDeServicioCms The CodigoDeServicioCms
     */
    @JsonProperty("CodigoDeServicioCms")
    public void setCodigoDeServicioCms(String codigoDeServicioCms) {
        this.codigoDeServicioCms = codigoDeServicioCms;
    }

    /**
     * @return The bloqueProducto
     */
    @JsonProperty("BloqueProducto")
    public String getBloqueProducto() {
        return bloqueProducto;
    }

    /**
     * @param bloqueProducto The BloqueProducto
     */
    @JsonProperty("BloqueProducto")
    public void setBloqueProducto(String bloqueProducto) {
        this.bloqueProducto = bloqueProducto;
    }

    /**
     * @return The coordenadasX
     */
    @JsonProperty("CoordenadasX")
    public String getCoordenadasX() {
        return coordenadasX;
    }

    /**
     * @param coordenadasX The coordenadasX
     */
    @JsonProperty("CoordenadasX")
    public void setCoordenadasX(String coordenadasX) {
        this.coordenadasX = coordenadasX;
    }

    /**
     * @return The coordenadasY
     */
    @JsonProperty("CoordenadasY")
    public String getCoordenadasY() {
        return coordenadasY;
    }

    /**
     * @param coordenadasY The coordenadasY
     */
    @JsonProperty("CoordenadasY")
    public void setCoordenadasY(String coordenadasY) {
        this.coordenadasY = coordenadasY;
    }

    /**
     * @return The webParental
     */
    @JsonProperty("WebParental")
    public String getWebParental() {
        return webParental;
    }

    /**
     * @param webParental The webParental
     */
    @JsonProperty("WebParental")
    public void setWebParental(String webParental) {
        this.webParental = webParental;
    }

    /**
     * @return The montoContado
     */
    @JsonProperty("MontoContado")
    public String getMontoContado() {
        return montoContado;
    }

    /**
     * @param montoContado The montoContado
     */
    @JsonProperty("MontoContado")
    public void setMontoContado(String montoContado) {
        this.montoContado = montoContado;
    }

    /**
     * @return The codigoPostal
     */
    @JsonProperty("CodigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal The codigoPostal
     */
    @JsonProperty("CodigoPostal")
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return The publicarGuia
     */
    @JsonProperty("PublicarGuia")
    public String getPublicarGuia() {
        return publicarGuia;
    }

    /**
     * @param publicarGuia The publicarGuia
     */
    @JsonProperty("PublicarGuia")
    public void setPublicarGuia(String publicarGuia) {
        this.publicarGuia = publicarGuia;
    }

    /**
     * @return The repetidorSmartWifi
     */
    @JsonProperty("RepetidorSmartWifi")
    public Integer getRepetidorSmartWifi() {
        return repetidorSmartWifi;
    }

    /**
     * @param repetidorSmartWifi The repetidorSmartWifi
     */
    @JsonProperty("RepetidorSmartWifi")
    public void setRepetidorSmartWifi(Integer repetidorSmartWifi) {
        this.repetidorSmartWifi = repetidorSmartWifi;
    }

    /**
     * @return The modoVenta
     */
    @JsonProperty("ModoVenta")
    public String getModoVenta() {
        return modoVenta;
    }

    /**
     * @param modoVenta The modoVenta
     */
    @JsonProperty("ModoVenta")
    public void setModoVenta(String modoVenta) {
        this.modoVenta = modoVenta;
    }

    /**
     * @return The nacionalidad
     */
    @JsonProperty("Nacionalidad")
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * @param nacionalidad The nacionalidad
     */
    @JsonProperty("Nacionalidad")
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
}
