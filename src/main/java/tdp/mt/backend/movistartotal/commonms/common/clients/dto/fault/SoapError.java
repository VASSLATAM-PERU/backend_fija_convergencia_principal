package tdp.mt.backend.movistartotal.commonms.common.clients.dto.fault;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SoapError {

    @JsonProperty("faultcode")
    private String faultcode;
    @JsonProperty("faultstring")
    private String faultstring;
    @JsonProperty("faultactor")
    private String faultactor;
    @JsonProperty("detail")
    private Detail detail;

    @JsonProperty("faultcode")
    public String getFaultcode() {
        return faultcode;
    }

    @JsonProperty("faultcode")
    public void setFaultcode(String faultcode) {
        this.faultcode = faultcode;
    }

    @JsonProperty("faultstring")
    public String getFaultstring() {
        return faultstring;
    }

    @JsonProperty("faultstring")
    public void setFaultstring(String faultstring) {
        this.faultstring = faultstring;
    }

    @JsonProperty("faultactor")
    public String getFaultactor() {
        return faultactor;
    }

    @JsonProperty("faultactor")
    public void setFaultactor(String faultactor) {
        this.faultactor = faultactor;
    }

    @JsonProperty("detail")
    public Detail getDetail() {
        return detail;
    }

    @JsonProperty("detail")
    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    @JsonIgnore
    public String getExceptionDescription(){
        StringBuilder m = new StringBuilder();
        if (this.getDetail()==null) {
            return null;
        }
        try {
            if (this.getDetail().getQuerySubscriptionGroupValueFault() != null) {
                String e = this.getDetail().getQuerySubscriptionGroupValueFault().getError().getCode().getError();
                String d = this.getDetail().getQuerySubscriptionGroupValueFault().getError().getDescription();
                String c = this.getDetail().getQuerySubscriptionGroupValueFault().getError().getDetails().getCause();
                m.append(" |Ex QuerySubscriptionGroupValueFault: ").append(d).append(" Cause: ").append(c).append("(").append(e).append(")");
            }

        } catch (NullPointerException e) {
            //e.printStackTrace();
            return null;
        } catch(Exception ex){
            //TODO:ADD EX LAYER
        }
        return m.length() != 0?m.toString():null;
    }
}
