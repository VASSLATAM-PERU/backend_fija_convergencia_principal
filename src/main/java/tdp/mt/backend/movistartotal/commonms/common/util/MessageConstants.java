package tdp.mt.backend.movistartotal.commonms.common.util;

public class MessageConstants {

	public static final String ORDER_NOT_SAVED = "order.not.saved";
	public static final String ORDER_PRODUCT_DATA_NOT_FOUND = "order.product_data.notfound";
	public static final String AUDIO_NOT_VALID = "audio.not.valid";
	public static final String ORDER_PAGO_EFECTIVO_CONCEPTO_PAGO = "order.pagoefectivo.conceptopago";
}