package tdp.mt.backend.movistartotal.commonms.domain.firebase;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.math.BigDecimal;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"type", "address", "address_additional_info",
        "atis_migration_phone", "client_doc_number", "client_document",
        "client_email", "client_has_email", "client_lastname", "client_mobile_phone", "client_mother_lastname",
        "client_names", "client_operator_line", "client_secondary_phone", "client_birth_date", "client_nationality", "cms_customer", "cms_service_code",
        "department", "district", "input_doc_mode", "latitude_installation",
        "latitude_sales", "longitude_installation", "longitude_sales", "modified", "offering_expert_code",
        "product_cash_price", "product_commercial_operation", "product_equipment_type", "product_financing_cost", "product_financing_month",
        "product_has_discount", "product_has_equipment", "product_id", "product_install_cost", "product_line_type",
        "product_name", "product_payment_method", "product_price", "product_prom_price", "product_return_month",
        "product_return_period", "province", "reason_close_sale", "requiere_acception",
        "success", "sva", "vendor", "vendor_id", "month_period", "legacy_code",
        "prom_speed", "period_prom_speed", "product_equipamiento_linea", "product_equipamiento_internet", "product_equipamiento_tv", "price_flat",
        // Automatizador GIS
        "msx_cbr_voi_ges_in", "msx_cbr_voi_gis_in", "msx_ind_snl_gis_cd", "msx_ind_gpo_gis_cd", "cod_ind_sen_cms", "cod_cab_cms", "cod_fac_tec_cd",
        // Automatizador Normalizador
        "address_referencia", "address_ubigeoGeocodificado", "address_descripcionUbigeo", "address_direccionGeocodificada", "address_tipoVia", "address_nombreVia", "address_numeroPuerta1", "address_numeroPuerta2", "address_cuadra", "address_tipoInterior", "address_numeroInterior", "address_piso", "address_tipoVivienda", "address_nombreVivienda", "address_tipoUrbanizacion", "address_nombreUrbanizacion", "address_manzana", "address_lote", "address_kilometro", "address_nivelConfianza",
        // Condiciones de Venta
        "shipping_contracts_for_email", "affiliation_electronic_invoice", "disaffiliation_white_pages_guide", "publication_white_pages_guide", "affiliation_data_protection", "parental_protection", "datetime_sales_condition"
})


public class SaleApiFirebaseResponseBody {

    @JsonProperty("id")
    private String id;

    @JsonProperty("type")
    private String type;


    @JsonProperty("shipping_contracts_for_email")
    private Boolean shipping_contracts_for_email;
    @JsonProperty("affiliation_electronic_invoice")
    private Boolean affiliation_electronic_invoice;
    @JsonProperty("disaffiliation_white_pages_guide")
    private Boolean disaffiliation_white_pages_guide;
    @JsonProperty("publication_white_pages_guide")
    private Boolean publication_white_pages_guide;
    @JsonProperty("affiliation_data_protection")
    private Boolean affiliation_data_protection;
    @JsonProperty("parental_protection")
    private Boolean parental_protection;
    @JsonProperty("datetime_sales_condition")
    private String datetime_sales_condition;

    @JsonProperty("address")
    private String address;
    @JsonProperty("address_additional_info")
    private String address_additional_info;
    @JsonProperty("atis_migration_phone")
    private String atis_migration_phone;
    @JsonProperty("client_doc_number")
    private String client_doc_number;
    @JsonProperty("client_document")
    private String client_document;
    @JsonProperty("client_email")
    private String client_email;
    @JsonProperty("client_has_email")
    private Boolean client_has_email;
    @JsonProperty("client_lastname")
    private String client_lastname;
    @JsonProperty("client_mobile_phone")
    private String client_mobile_phone;
    @JsonProperty("client_mother_lastname")
    private String client_mother_lastname;
    @JsonProperty("client_names")
    private String client_names;
    @JsonProperty("client_operator_line")
    private String client_operator_line;
    @JsonProperty("client_secondary_phone")
    private String client_secondary_phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "GMT-5")
    @JsonProperty("client_birth_date")
    private Date client_birth_date;

    @JsonProperty("client_nationality")
    private String client_nationality;

    @JsonProperty("cms_customer")
    private String cms_customer;
    @JsonProperty("cms_service_code")
    private String cms_service_code;
    @JsonProperty("department")
    private String department;
    @JsonProperty("district")
    private String district;
    @JsonProperty("input_doc_mode")
    private String input_doc_mode;
    @JsonProperty("latitude_installation")
    private Double latitude_installation;
    @JsonProperty("latitude_sales")
    private Double latitude_sales;
    @JsonProperty("longitude_installation")
    private Double longitude_installation;
    @JsonProperty("longitude_sales")
    private Double longitude_sales;
    @JsonProperty("modified")
    private Long modified;
    @JsonProperty("offering_expert_code")
    private String offering_expert_code;
    @JsonProperty("product_cash_price")
    private BigDecimal product_cash_price;
    @JsonProperty("product_commercial_operation")
    private String product_commercial_operation;
    @JsonProperty("product_equipment_type")
    private String product_equipment_type;
    @JsonProperty("product_financing_cost")
    private BigDecimal product_financing_cost;
    @JsonProperty("product_financing_month")
    private Integer product_financing_month;
    @JsonProperty("product_has_discount")
    private Boolean product_has_discount;
    @JsonProperty("product_has_equipment")
    private Boolean product_has_equipment;
    @JsonProperty("product_id")
    private String product_id;
    @JsonProperty("product_install_cost")
    private BigDecimal product_install_cost;
    @JsonProperty("product_line_type")
    private String product_line_type;
    @JsonProperty("product_name")
    private String product_name;
    @JsonProperty("product_payment_method")
    private String product_payment_method;
    @JsonProperty("product_price")
    private BigDecimal product_price;
    @JsonProperty("product_prom_price")
    private BigDecimal product_prom_price;
    @JsonProperty("product_return_month")
    private Integer product_return_month;
    @JsonProperty("product_return_period")
    private String product_return_period;
    @JsonProperty("province")
    private String province;
    @JsonProperty("reason_close_sale")
    private String reason_close_sale;
    @JsonProperty("requiere_acception")
    private Integer requiere_acception;
    @JsonProperty("success")
    private Boolean success;
    @JsonProperty("sva")
    private List<String> sva = new ArrayList<String>();
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("vendor_id")
    private String vendor_id;


    @JsonProperty("product_campaign")
    private String product_campaign;
    @JsonProperty("product_category")
    private String product_category;
    @JsonProperty("product_code")
    private String product_code;
    @JsonProperty("product_type")
    private String product_type;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("month_period")
    private String month_period;
    @JsonProperty("legacy_code")
    private String legacy_code;

    @JsonIgnore
    private String originOrder;

    @JsonProperty("prom_speed")
    private Integer prom_speed;

    @JsonProperty("period_prom_speed")
    private Integer period_prom_speed;

    @JsonProperty("product_equipamiento_linea")
    private String product_equipamiento_linea;

    @JsonProperty("product_equipamiento_internet")
    private String product_equipamiento_internet;

    @JsonProperty("product_equipamiento_tv")
    private String product_equipamiento_tv;

    @JsonProperty("price_flat")
    private BigDecimal price_flat;

    // Automatizador GIS
    @JsonProperty("msx_cbr_voi_ges_in")
    private String msx_cbr_voi_ges_in;
    @JsonProperty("msx_cbr_voi_gis_in")
    private String msx_cbr_voi_gis_in;
    @JsonProperty("msx_ind_snl_gis_cd")
    private String msx_ind_snl_gis_cd;
    @JsonProperty("msx_ind_gpo_gis_cd")
    private String msx_ind_gpo_gis_cd;
    @JsonProperty("cod_ind_sen_cms")
    private String cod_ind_sen_cms;
    @JsonProperty("cod_cab_cms")
    private String cod_cab_cms;
    @JsonProperty("cod_fac_tec_cd")
    private String cod_fac_tec_cd;

    @JsonProperty("cobre_blq_vta")
    private String cobre_blq_vta;
    @JsonProperty("cobre_blq_trm")
    private String cobre_blq_trm;

    @JsonProperty("address_referencia")
    private String address_referencia;
    @JsonProperty("address_ubigeoGeocodificado")
    private String address_ubigeoGeocodificado;
    @JsonProperty("address_descripcionUbigeo")
    private String address_descripcionUbigeo;
    @JsonProperty("address_direccionGeocodificada")
    private String address_direccionGeocodificada;
    @JsonProperty("address_tipoVia")
    private String address_tipoVia;
    @JsonProperty("address_nombreVia")
    private String address_nombreVia;
    @JsonProperty("address_numeroPuerta1")
    private String address_numeroPuerta1;
    @JsonProperty("address_numeroPuerta2")
    private String address_numeroPuerta2;
    @JsonProperty("address_cuadra")
    private String address_cuadra;
    @JsonProperty("address_tipoInterior")
    private String address_tipoInterior;
    @JsonProperty("address_numeroInterior")
    private String address_numeroInterior;
    @JsonProperty("address_piso")
    private String address_piso;
    @JsonProperty("address_tipoVivienda")
    private String address_tipoVivienda;
    @JsonProperty("address_nombreVivienda")
    private String address_nombreVivienda;
    @JsonProperty("address_tipoUrbanizacion")
    private String address_tipoUrbanizacion;
    @JsonProperty("address_nombreUrbanizacion")
    private String address_nombreUrbanizacion;
    @JsonProperty("address_manzana")
    private String address_manzana;
    @JsonProperty("address_lote")
    private String address_lote;
    @JsonProperty("address_kilometro")
    private String address_kilometro;
    @JsonProperty("address_nivelConfianza")
    private String address_nivelConfianza;

    /**
     * @return The month_period
     */
    @JsonProperty("month_period")
    public String getMonth_period() {
        return month_period;
    }

    /**
     * @param month_period The month_period
     */
    @JsonProperty("month_period")
    public void setMonth_period(String month_period) {
        this.month_period = month_period;
    }


    /**
     * @return The legacy_code
     */
    public String getLegacy_code() {
        return legacy_code;

    }

    /**
     * @param legacy_code The legacy_code
     */
    public void setLegacy_code(String legacy_code) {
        this.legacy_code = legacy_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The address
     */
    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The address_additional_info
     */
    @JsonProperty("address_additional_info")
    public String getAddress_additional_info() {
        return address_additional_info;
    }

    /**
     * @param address_additional_info The address_additional_info
     */
    @JsonProperty("address_additional_info")
    public void setAddress_additional_info(String address_additional_info) {
        this.address_additional_info = address_additional_info;
    }

    /**
     * @return The affiliation_data_protection
     */
    @JsonProperty("affiliation_data_protection")
    public Boolean getAffiliation_data_protection() {
        return affiliation_data_protection;
    }

    /**
     * @param affiliation_data_protection The affiliation_data_protection
     */
    @JsonProperty("affiliation_data_protection")
    public void setAffiliation_data_protection(Boolean affiliation_data_protection) {
        this.affiliation_data_protection = affiliation_data_protection;
    }

    /**
     * @return The affiliation_electronic_invoice
     */
    @JsonProperty("affiliation_electronic_invoice")
    public Boolean getAffiliation_electronic_invoice() {
        return affiliation_electronic_invoice;
    }

    /**
     * @param affiliation_electronic_invoice The affiliation_electronic_invoice
     */
    @JsonProperty("affiliation_electronic_invoice")
    public void setAffiliation_electronic_invoice(Boolean affiliation_electronic_invoice) {
        this.affiliation_electronic_invoice = affiliation_electronic_invoice;
    }

    /**
     * @return The atis_migration_phone
     */
    @JsonProperty("atis_migration_phone")
    public String getAtis_migration_phone() {
        return atis_migration_phone;
    }

    /**
     * @param atis_migration_phone The atis_migration_phone
     */
    @JsonProperty("atis_migration_phone")
    public void setAtis_migration_phone(String atis_migration_phone) {
        this.atis_migration_phone = atis_migration_phone;
    }

    /**
     * @return The client_doc_number
     */
    @JsonProperty("client_doc_number")
    public String getClient_doc_number() {
        return client_doc_number;
    }

    /**
     * @param client_doc_number The client_doc_number
     */
    @JsonProperty("client_doc_number")
    public void setClient_doc_number(String client_doc_number) {
        this.client_doc_number = client_doc_number;
    }

    /**
     * @return The client_document
     */
    @JsonProperty("client_document")
    public String getClient_document() {
        return client_document;
    }

    /**
     * @param client_document The client_document
     */
    @JsonProperty("client_document")
    public void setClient_document(String client_document) {
        this.client_document = client_document;
    }

    /**
     * @return The client_email
     */
    @JsonProperty("client_email")
    public String getClient_email() {
        return client_email;
    }

    /**
     * @param client_email The client_email
     */
    @JsonProperty("client_email")
    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    /**
     * @return The client_has_email
     */
    @JsonProperty("client_has_email")
    public Boolean getClient_has_email() {
        return client_has_email;
    }

    /**
     * @param client_has_email The client_has_email
     */
    @JsonProperty("client_has_email")
    public void setClient_has_email(Boolean client_has_email) {
        this.client_has_email = client_has_email;
    }

    /**
     * @return The client_lastname
     */
    @JsonProperty("client_lastname")
    public String getClient_lastname() {
        return client_lastname;
    }

    /**
     * @param client_lastname The client_lastname
     */
    @JsonProperty("client_lastname")
    public void setClient_lastname(String client_lastname) {
        this.client_lastname = client_lastname;
    }

    /**
     * @return The client_mobile_phone
     */
    @JsonProperty("client_mobile_phone")
    public String getClient_mobile_phone() {
        return client_mobile_phone;
    }

    /**
     * @param client_mobile_phone The client_mobile_phone
     */
    @JsonProperty("client_mobile_phone")
    public void setClient_mobile_phone(String client_mobile_phone) {
        this.client_mobile_phone = client_mobile_phone;
    }

    /**
     * @return The client_mother_lastname
     */
    @JsonProperty("client_mother_lastname")
    public String getClient_mother_lastname() {
        return client_mother_lastname;
    }

    /**
     * @param client_mother_lastname The client_mother_lastname
     */
    @JsonProperty("client_mother_lastname")
    public void setClient_mother_lastname(String client_mother_lastname) {
        this.client_mother_lastname = client_mother_lastname;
    }

    /**
     * @return The client_names
     */
    @JsonProperty("client_names")
    public String getClient_names() {
        return client_names;
    }

    /**
     * @param client_names The client_names
     */
    @JsonProperty("client_names")
    public void setClient_names(String client_names) {
        this.client_names = client_names;
    }

    /**
     * @return The client_operator_line
     */
    @JsonProperty("client_operator_line")
    public String getClient_operator_line() {
        return client_operator_line;
    }

    /**
     * @param client_operator_line The client_operator_line
     */
    @JsonProperty("client_operator_line")
    public void setClient_operator_line(String client_operator_line) {
        this.client_operator_line = client_operator_line;
    }

    /**
     * @return The client_secondary_phone
     */
    @JsonProperty("client_secondary_phone")
    public String getClient_secondary_phone() {
        return client_secondary_phone;
    }

    /**
     * @param client_secondary_phone The client_secondary_phone
     */

    @JsonProperty("client_secondary_phone")
    public void setClient_secondary_phone(String client_secondary_phone) {
        this.client_secondary_phone = client_secondary_phone;
    }


    @JsonProperty("client_birth_date")
    public Date getClient_birth_date() {
        return client_birth_date;
    }

    @JsonProperty("client_birth_date")
    public void setClient_birth_date(Date client_birth_date) {
        this.client_birth_date = client_birth_date;
    }

    /**
     * @return The client_nationality
     */
    @JsonProperty("client_nationality")
    public String getClient_nationality() {
        return client_nationality;
    }

    /**
     * @param client_nationality The client_nationality
     */
    @JsonProperty("client_nationality")
    public void setClient_nationality(String client_nationality) {
        this.client_nationality = client_nationality;
    }

    /**
     * @return The cms_customer
     */
    @JsonProperty("cms_customer")
    public String getCms_customer() {
        return cms_customer;
    }

    /**
     * @param cms_customer The cms_customer
     */
    @JsonProperty("cms_customer")
    public void setCms_customer(String cms_customer) {
        this.cms_customer = cms_customer;
    }

    /**
     * @return The cms_service_code
     */
    @JsonProperty("cms_service_code")
    public String getCms_service_code() {
        return cms_service_code;
    }

    /**
     * @param cms_service_code The cms_service_code
     */
    @JsonProperty("cms_service_code")
    public void setCms_service_code(String cms_service_code) {
        this.cms_service_code = cms_service_code;
    }

    /**
     * @return The department
     */
    @JsonProperty("department")
    public String getDepartment() {
        return department;
    }

    /**
     * @param department The department
     */
    @JsonProperty("department")
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return The disaffiliation_white_pages_guide
     */
    @JsonProperty("disaffiliation_white_pages_guide")
    public Boolean getDisaffiliation_white_pages_guide() {
        return disaffiliation_white_pages_guide;
    }

    /**
     * @param disaffiliation_white_pages_guide The disaffiliation_white_pages_guide
     */
    @JsonProperty("disaffiliation_white_pages_guide")
    public void setDisaffiliation_white_pages_guide(Boolean disaffiliation_white_pages_guide) {
        this.disaffiliation_white_pages_guide = disaffiliation_white_pages_guide;
    }

    /**
     * @return The district
     */
    @JsonProperty("district")
    public String getDistrict() {
        return district;
    }

    /**
     * @param district The district
     */
    @JsonProperty("district")
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return The input_doc_mode
     */
    @JsonProperty("input_doc_mode")
    public String getInput_doc_mode() {
        return input_doc_mode;
    }

    /**
     * @param input_doc_mode The input_doc_mode
     */
    @JsonProperty("input_doc_mode")
    public void setInput_doc_mode(String input_doc_mode) {
        this.input_doc_mode = input_doc_mode;
    }

    /**
     * @return The latitude_installation
     */
    @JsonProperty("latitude_installation")
    public Double getLatitude_installation() {
        return latitude_installation;
    }

    /**
     * @param latitude_installation The latitude_installation
     */
    @JsonProperty("latitude_installation")
    public void setLatitude_installation(Double latitude_installation) {
        this.latitude_installation = latitude_installation;
    }

    /**
     * @return The latitude_sales
     */
    @JsonProperty("latitude_sales")
    public Double getLatitude_sales() {
        return latitude_sales;
    }

    /**
     * @param latitude_sales The latitude_sales
     */
    @JsonProperty("latitude_sales")
    public void setLatitude_sales(Double latitude_sales) {
        this.latitude_sales = latitude_sales;
    }

    /**
     * @return The longitude_installation
     */
    @JsonProperty("longitude_installation")
    public Double getLongitude_installation() {
        return longitude_installation;
    }

    /**
     * @param longitude_installation The longitude_installation
     */
    @JsonProperty("longitude_installation")
    public void setLongitude_installation(Double longitude_installation) {
        this.longitude_installation = longitude_installation;
    }

    /**
     * @return The longitude_sales
     */
    @JsonProperty("longitude_sales")
    public Double getLongitude_sales() {
        return longitude_sales;
    }

    /**
     * @param longitude_sales The longitude_sales
     */
    @JsonProperty("longitude_sales")
    public void setLongitude_sales(Double longitude_sales) {
        this.longitude_sales = longitude_sales;
    }

    /**
     * @return The modified
     */
    @JsonProperty("modified")
    public Long getModified() {
        return modified;
    }

    /**
     * @param modified The modified
     */
    @JsonProperty("modified")
    public void setModified(Long modified) {
        this.modified = modified;
    }

    /**
     * @return The offering_expert_code
     */
    @JsonProperty("offering_expert_code")
    public String getOffering_expert_code() {
        return offering_expert_code;
    }

    /**
     * @param offering_expert_code The offering_expert_code
     */
    @JsonProperty("offering_expert_code")
    public void setOffering_expert_code(String offering_expert_code) {
        this.offering_expert_code = offering_expert_code;
    }

    /**
     * @return The product_cash_price
     */
    @JsonProperty("product_cash_price")
    public BigDecimal getProduct_cash_price() {
        return product_cash_price;
    }

    /**
     * @param product_cash_price The product_cash_price
     */
    @JsonProperty("product_cash_price")
    public void setProduct_cash_price(BigDecimal product_cash_price) {
        this.product_cash_price = product_cash_price;
    }

    /**
     * @param product_commercial_operation The product_commercial_operation
     */
    @JsonProperty("product_commercial_operation")
    public void setProduct_commercial_operation(String product_commercial_operation) {
        this.product_commercial_operation = product_commercial_operation;
    }

    /**
     * @return The product_equipment_type
     */
    @JsonProperty("product_commercial_operation")
    public String getProduct_commercial_operation() {
        return product_commercial_operation;
    }

    /**
     * @param product_equipment_type The product_equipment_type
     */
    @JsonProperty("product_equipment_type")
    public void setProduct_equipment_type(String product_equipment_type) {
        this.product_equipment_type = product_equipment_type;
    }

    /**
     * @return The product_equipment_type
     */
    @JsonProperty("product_equipment_type")
    public String getProduct_equipment_type() {
        return product_equipment_type;
    }

    /**
     * @return The product_financing_cost
     */
    @JsonProperty("product_financing_cost")
    public BigDecimal getProduct_financing_cost() {
        return product_financing_cost;
    }

    /**
     * @param product_financing_cost The product_financing_cost
     */
    @JsonProperty("product_financing_cost")
    public void setProduct_financing_cost(BigDecimal product_financing_cost) {
        this.product_financing_cost = product_financing_cost;
    }

    /**
     * @return The product_financing_month
     */
    @JsonProperty("product_financing_month")
    public Integer getProduct_financing_month() {
        return product_financing_month;
    }

    /**
     * @param product_financing_month The product_financing_month
     */
    @JsonProperty("product_financing_month")
    public void setProduct_financing_month(Integer product_financing_month) {
        this.product_financing_month = product_financing_month;
    }

    /**
     * @return The product_has_discount
     */
    @JsonProperty("product_has_discount")
    public Boolean getProduct_has_discount() {
        return product_has_discount;
    }

    /**
     * @param product_has_discount The product_has_discount
     */
    @JsonProperty("product_has_discount")
    public void setProduct_has_discount(Boolean product_has_discount) {
        this.product_has_discount = product_has_discount;
    }

    /**
     * @return The product_has_equipment
     */
    @JsonProperty("product_has_equipment")
    public Boolean getProduct_has_equipment() {
        return product_has_equipment;
    }

    /**
     * @param product_has_equipment The product_has_equipment
     */
    @JsonProperty("product_has_equipment")
    public void setProduct_has_equipment(Boolean product_has_equipment) {
        this.product_has_equipment = product_has_equipment;
    }

    /**
     * @return The product_id
     */
    @JsonProperty("product_id")
    public String getProduct_id() {
        return product_id;
    }

    /**
     * @param product_id The product_id
     */
    @JsonProperty("product_id")
    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    /**
     * @return The product_install_cost
     */
    @JsonProperty("product_install_cost")
    public BigDecimal getProduct_install_cost() {
        return product_install_cost;
    }

    /**
     * @param product_install_cost The product_install_cost
     */
    @JsonProperty("product_install_cost")
    public void setProduct_install_cost(BigDecimal product_install_cost) {
        this.product_install_cost = product_install_cost;
    }

    /**
     * @return The product_line_type
     */
    @JsonProperty("product_line_type")
    public String getProduct_line_type() {
        return product_line_type;
    }

    /**
     * @param product_line_type The product_line_type
     */
    @JsonProperty("product_line_type")
    public void setProduct_line_type(String product_line_type) {
        this.product_line_type = product_line_type;
    }

    /**
     * @return The product_name
     */
    @JsonProperty("product_name")
    public String getProduct_name() {
        return product_name;
    }

    /**
     * @param product_name The product_name
     */
    @JsonProperty("product_name")
    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    /**
     * @return The product_payment_method
     */
    @JsonProperty("product_payment_method")
    public String getProduct_payment_method() {
        return product_payment_method;
    }

    /**
     * @param product_payment_method The product_payment_method
     */
    @JsonProperty("product_payment_method")
    public void setProduct_payment_method(String product_payment_method) {
        this.product_payment_method = product_payment_method;
    }

    /**
     * @return The product_price
     */
    @JsonProperty("product_price")
    public BigDecimal getProduct_price() {
        return product_price;
    }

    /**
     * @param product_price The product_price
     */
    @JsonProperty("product_price")
    public void setProduct_price(BigDecimal product_price) {
        this.product_price = product_price;
    }

    /**
     * @return The product_prom_price
     */
    @JsonProperty("product_prom_price")
    public BigDecimal getProduct_prom_price() {
        return product_prom_price;
    }

    /**
     * @param product_prom_price The product_prom_price
     */
    @JsonProperty("product_prom_price")
    public void setProduct_prom_price(BigDecimal product_prom_price) {
        this.product_prom_price = product_prom_price;
    }

    /**
     * @return The product_return_month
     */
    @JsonProperty("product_return_month")
    public Integer getProduct_return_month() {
        return product_return_month;
    }

    /**
     * @param product_return_month The product_return_month
     */
    @JsonProperty("product_return_month")
    public void setProduct_return_month(Integer product_return_month) {
        this.product_return_month = product_return_month;
    }

    /**
     * @return The product_return_period
     */
    @JsonProperty("product_return_period")
    public String getProduct_return_period() {
        return product_return_period;
    }

    /**
     * @param product_return_period The product_return_period
     */
    @JsonProperty("product_return_period")
    public void setProduct_return_period(String product_return_period) {
        this.product_return_period = product_return_period;
    }

    /**
     * @return The province
     */
    @JsonProperty("province")
    public String getProvince() {
        return province;
    }

    /**
     * @param province The province
     */
    @JsonProperty("province")
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return The publication_white_pages_guide
     */
    @JsonProperty("publication_white_pages_guide")
    public Boolean getPublication_white_pages_guide() {
        return publication_white_pages_guide;
    }

    /**
     * @param publication_white_pages_guide The publication_white_pages_guide
     */
    @JsonProperty("publication_white_pages_guide")
    public void setPublication_white_pages_guide(Boolean publication_white_pages_guide) {
        this.publication_white_pages_guide = publication_white_pages_guide;
    }

    /**
     * @return The reason_close_sale
     */
    @JsonProperty("reason_close_sale")
    public String getReason_close_sale() {
        return reason_close_sale;
    }

    /**
     * @param reason_close_sale The reason_close_sale
     */
    @JsonProperty("reason_close_sale")
    public void setReason_close_sale(String reason_close_sale) {
        this.reason_close_sale = reason_close_sale;
    }

    /**
     * @return The requiere_acception
     */
    @JsonProperty("requiere_acception")
    public Integer getRequiere_acception() {
        return requiere_acception;
    }

    /**
     * @param requiere_acception The requiere_acception
     */
    @JsonProperty("requiere_acception")
    public void setRequiere_acception(Integer requiere_acception) {
        this.requiere_acception = requiere_acception;
    }

    /**
     * @return The shipping_contracts_for_email
     */
    @JsonProperty("shipping_contracts_for_email")
    public Boolean getShipping_contracts_for_email() {
        return shipping_contracts_for_email;
    }

    /**
     * @param shipping_contracts_for_email The shipping_contracts_for_email
     */
    @JsonProperty("shipping_contracts_for_email")
    public void setShipping_contracts_for_email(Boolean shipping_contracts_for_email) {
        this.shipping_contracts_for_email = shipping_contracts_for_email;
    }

    /**
     * @return The success
     */
    @JsonProperty("success")
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    @JsonProperty("success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * @return The sva
     */
    @JsonProperty("sva")
    public List<String> getSva() {
        return sva;
    }

    /**
     * @param sva The sva
     */
    @JsonProperty("sva")
    public void setSva(List<String> sva) {
        this.sva = sva;
    }

    /**
     * @return The vendor
     */
    @JsonProperty("vendor")
    public String getVendor() {
        return vendor;
    }

    /**
     * @param vendor The vendor
     */
    @JsonProperty("vendor")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * @return The vendor_id
     */
    @JsonProperty("vendor_id")
    public String getVendor_id() {
        return vendor_id;
    }

    /**
     * @param vendor_id The vendor_id
     */
    @JsonProperty("vendor_id")
    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    /**
     * @return The parental_protection
     */
    @JsonProperty("parental_protection")
    public Boolean getParental_protection() {
        return parental_protection;
    }

    /**
     * @param parental_protection The parental_protection
     */
    @JsonProperty("parental_protection")
    public void setParental_protection(Boolean parental_protection) {
        this.parental_protection = parental_protection;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getProduct_campaign() {
        return product_campaign;
    }

    public void setProduct_campaign(String product_campaign) {
        this.product_campaign = product_campaign;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getOriginOrder() {
        return originOrder;
    }

    public void setOriginOrder(String originOrder) {
        this.originOrder = originOrder;
    }

    public Integer getProm_speed() {
        return prom_speed;
    }

    public void setProm_speed(Integer prom_speed) {
        this.prom_speed = prom_speed;
    }

    public Integer getPeriod_prom_speed() {
        return period_prom_speed;
    }

    public void setPeriod_prom_speed(Integer period_prom_speed) {
        this.period_prom_speed = period_prom_speed;
    }

    public String getProduct_equipamiento_linea() {
        return product_equipamiento_linea;
    }

    public void setProduct_equipamiento_linea(String product_equipamiento_linea) {
        this.product_equipamiento_linea = product_equipamiento_linea;
    }

    public String getProduct_equipamiento_internet() {
        return product_equipamiento_internet;
    }

    public void setProduct_equipamiento_internet(String product_equipamiento_internet) {
        this.product_equipamiento_internet = product_equipamiento_internet;
    }

    public String getProduct_equipamiento_tv() {
        return product_equipamiento_tv;
    }

    public void setProduct_equipamiento_tv(String product_equipamiento_tv) {
        this.product_equipamiento_tv = product_equipamiento_tv;
    }

    public BigDecimal getPrice_flat() {
        return price_flat;
    }

    public void setPrice_flat(BigDecimal price_flat) {
        this.price_flat = price_flat;
    }

    @JsonProperty("msx_cbr_voi_ges_in")
    public String getMsx_cbr_voi_ges_in() {
        return msx_cbr_voi_ges_in;
    }

    @JsonProperty("msx_cbr_voi_ges_in")
    public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
        this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
    }

    public String getMsx_cbr_voi_gis_in() {
        return msx_cbr_voi_gis_in;
    }

    public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
        this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
    }

    public String getMsx_ind_snl_gis_cd() {
        return msx_ind_snl_gis_cd;
    }

    public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
        this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
    }

    public String getMsx_ind_gpo_gis_cd() {
        return msx_ind_gpo_gis_cd;
    }

    public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
        this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
    }

    public String getCod_ind_sen_cms() {
        return cod_ind_sen_cms;
    }

    public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
        this.cod_ind_sen_cms = cod_ind_sen_cms;
    }

    public String getCod_cab_cms() {
        return cod_cab_cms;
    }

    public void setCod_cab_cms(String cod_cab_cms) {
        this.cod_cab_cms = cod_cab_cms;
    }

    public String getCod_fac_tec_cd() {
        return cod_fac_tec_cd;
    }

    public String getCobre_blq_vta() {
        return cobre_blq_vta;
    }

    public void setCobre_blq_vta(String cobre_blq_vta) {
        this.cobre_blq_vta = cobre_blq_vta;
    }

    public String getCobre_blq_trm() {
        return cobre_blq_trm;
    }

    public void setCobre_blq_trm(String cobre_blq_trm) {
        this.cobre_blq_trm = cobre_blq_trm;
    }

    public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
        this.cod_fac_tec_cd = cod_fac_tec_cd;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getAddress_referencia() {
        return address_referencia;
    }

    public void setAddress_referencia(String address_referencia) {
        this.address_referencia = address_referencia;
    }

    public String getAddress_ubigeoGeocodificado() {
        return address_ubigeoGeocodificado;
    }

    public void setAddress_ubigeoGeocodificado(String address_ubigeoGeocodificado) {
        this.address_ubigeoGeocodificado = address_ubigeoGeocodificado;
    }

    public String getAddress_descripcionUbigeo() {
        return address_descripcionUbigeo;
    }

    public void setAddress_descripcionUbigeo(String address_descripcionUbigeo) {
        this.address_descripcionUbigeo = address_descripcionUbigeo;
    }

    public String getAddress_direccionGeocodificada() {
        return address_direccionGeocodificada;
    }

    public void setAddress_direccionGeocodificada(String address_direccionGeocodificada) {
        this.address_direccionGeocodificada = address_direccionGeocodificada;
    }

    public String getAddress_tipoVia() {
        return address_tipoVia;
    }

    public void setAddress_tipoVia(String address_tipoVia) {
        this.address_tipoVia = address_tipoVia;
    }

    public String getAddress_nombreVia() {
        return address_nombreVia;
    }

    public void setAddress_nombreVia(String address_nombreVia) {
        this.address_nombreVia = address_nombreVia;
    }

    public String getAddress_numeroPuerta1() {
        return address_numeroPuerta1;
    }

    public void setAddress_numeroPuerta1(String address_numeroPuerta1) {
        this.address_numeroPuerta1 = address_numeroPuerta1;
    }

    public String getAddress_numeroPuerta2() {
        return address_numeroPuerta2;
    }

    public void setAddress_numeroPuerta2(String address_numeroPuerta2) {
        this.address_numeroPuerta2 = address_numeroPuerta2;
    }

    public String getAddress_cuadra() {
        return address_cuadra;
    }

    public void setAddress_cuadra(String address_cuadra) {
        this.address_cuadra = address_cuadra;
    }

    public String getAddress_tipoInterior() {
        return address_tipoInterior;
    }

    public void setAddress_tipoInterior(String address_tipoInterior) {
        this.address_tipoInterior = address_tipoInterior;
    }

    public String getAddress_numeroInterior() {
        return address_numeroInterior;
    }

    public void setAddress_numeroInterior(String address_numeroInterior) {
        this.address_numeroInterior = address_numeroInterior;
    }

    public String getAddress_piso() {
        return address_piso;
    }

    public void setAddress_piso(String address_piso) {
        this.address_piso = address_piso;
    }

    public String getAddress_tipoVivienda() {
        return address_tipoVivienda;
    }

    public void setAddress_tipoVivienda(String address_tipoVivienda) {
        this.address_tipoVivienda = address_tipoVivienda;
    }

    public String getAddress_nombreVivienda() {
        return address_nombreVivienda;
    }

    public void setAddress_nombreVivienda(String address_nombreVivienda) {
        this.address_nombreVivienda = address_nombreVivienda;
    }

    public String getAddress_tipoUrbanizacion() {
        return address_tipoUrbanizacion;
    }

    public void setAddress_tipoUrbanizacion(String address_tipoUrbanizacion) {
        this.address_tipoUrbanizacion = address_tipoUrbanizacion;
    }

    public String getAddress_nombreUrbanizacion() {
        return address_nombreUrbanizacion;
    }

    public void setAddress_nombreUrbanizacion(String address_nombreUrbanizacion) {
        this.address_nombreUrbanizacion = address_nombreUrbanizacion;
    }

    public String getAddress_manzana() {
        return address_manzana;
    }

    public void setAddress_manzana(String address_manzana) {
        this.address_manzana = address_manzana;
    }

    public String getAddress_lote() {
        return address_lote;
    }

    public void setAddress_lote(String address_lote) {
        this.address_lote = address_lote;
    }

    public String getAddress_kilometro() {
        return address_kilometro;
    }

    public void setAddress_kilometro(String address_kilometro) {
        this.address_kilometro = address_kilometro;
    }

    public String getAddress_nivelConfianza() {
        return address_nivelConfianza;
    }

    public void setAddress_nivelConfianza(String address_nivelConfianza) {
        this.address_nivelConfianza = address_nivelConfianza;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatetime_sales_condition() {
        return datetime_sales_condition;
    }

    public void setDatetime_sales_condition(String datetime_sales_condition) {
        this.datetime_sales_condition = datetime_sales_condition;
    }
}