package tdp.mt.backend.movistartotal.commonms.common.clients;

import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiRequestHeader;

import java.util.Locale;

public class ApiRequestHeaderFactory {
    private ApiHeaderConfig config;

    public ApiRequestHeaderFactory(ApiHeaderConfig config) {
        this.config = config;
    }

    public ApiRequestHeader getHeader() {
        ApiRequestHeader header = new ApiRequestHeader();
        header.setCountry(config.getCountry());
        header.setLang(config.getLang());
        header.setEntity(config.getEntity());
        header.setSystem(config.getSystem());
        header.setSubsystem(config.getSubsystem());
        header.setOriginator(config.getOriginator());
        header.setSender(config.getSender());
        header.setUserId(config.getUserId());
        header.setWsId(config.getWsId());
        header.setWsIp(config.getWsIp());
        header.setOperation(config.getOperation());
        header.setDestination(config.getDestination());
        header.setExecId(config.getExecId());
        //header.setTimestamp("2016-10-04T10:52:47.233-05:00"); // TODO hacerlo dinamico
        //header.setTimestamp(sdf.format(new java.util.Date()));
        //java.util.Date ahora = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        header.setTimestamp(sdf.format(new java.util.Date()) + "-05:00");
        header.setMsgType(ApiHeaderConfig.MSG_TYPE_REQUEST);
        return header;
    }
}
