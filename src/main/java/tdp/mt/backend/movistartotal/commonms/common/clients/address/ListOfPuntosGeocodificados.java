package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListOfPuntosGeocodificados {

    @JsonProperty("puntoGeocodificado")
    private List<PuntoGeocodificado> puntoGeocodificado = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("puntoGeocodificado")
    public List<PuntoGeocodificado> getPuntoGeocodificado() {
        return puntoGeocodificado;
    }

    @JsonProperty("puntoGeocodificado")
    public void setPuntoGeocodificado(List<PuntoGeocodificado> puntoGeocodificado) {
        this.puntoGeocodificado = puntoGeocodificado;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

