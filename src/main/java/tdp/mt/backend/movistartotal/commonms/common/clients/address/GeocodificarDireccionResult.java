package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeocodificarDireccionResult {

    @JsonProperty("idUnico")
    private String idUnico;
    @JsonProperty("codigoOriginal")
    private String codigoOriginal;
    @JsonProperty("ubigeoOriginal")
    private String ubigeoOriginal;
    @JsonProperty("direccionOriginal")
    private String direccionOriginal;
    @JsonProperty("ubigeoGeocodificado")
    private String ubigeoGeocodificado;
    @JsonProperty("direccionGeocodificada")
    private String direccionGeocodificada;
    @JsonProperty("xGeocodificado")
    private String xGeocodificado;
    @JsonProperty("yGeocodificado")
    private String yGeocodificado;
    @JsonProperty("ubigeoValidado")
    private String ubigeoValidado;
    @JsonProperty("direccionNormalizada")
    private String direccionNormalizada;
    @JsonProperty("flagGeocodificacion")
    private String flagGeocodificacion;
    @JsonProperty("metodoGeocodificacion")
    private String metodoGeocodificacion;
    @JsonProperty("numeroRespuestas")
    private String numeroRespuestas;
    @JsonProperty("motivoNoGeocodificacion")
    private String motivoNoGeocodificacion;
    @JsonProperty("listOfPuntosGeocodificados")
    private ListOfPuntosGeocodificados listOfPuntosGeocodificados;
    @JsonProperty("parsingAddress")
    private ParsingAddress parsingAddress;
    @JsonProperty("geocodedAddress")
    private GeocodedAddress geocodedAddress;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("idUnico")
    public String getIdUnico() {
        return idUnico;
    }

    @JsonProperty("idUnico")
    public void setIdUnico(String idUnico) {
        this.idUnico = idUnico;
    }

    @JsonProperty("codigoOriginal")
    public String getCodigoOriginal() {
        return codigoOriginal;
    }

    @JsonProperty("codigoOriginal")
    public void setCodigoOriginal(String codigoOriginal) {
        this.codigoOriginal = codigoOriginal;
    }

    @JsonProperty("ubigeoOriginal")
    public String getUbigeoOriginal() {
        return ubigeoOriginal;
    }

    @JsonProperty("ubigeoOriginal")
    public void setUbigeoOriginal(String ubigeoOriginal) {
        this.ubigeoOriginal = ubigeoOriginal;
    }

    @JsonProperty("direccionOriginal")
    public String getDireccionOriginal() {
        return direccionOriginal;
    }

    @JsonProperty("direccionOriginal")
    public void setDireccionOriginal(String direccionOriginal) {
        this.direccionOriginal = direccionOriginal;
    }

    @JsonProperty("ubigeoGeocodificado")
    public String getUbigeoGeocodificado() {
        return ubigeoGeocodificado;
    }

    @JsonProperty("ubigeoGeocodificado")
    public void setUbigeoGeocodificado(String ubigeoGeocodificado) {
        this.ubigeoGeocodificado = ubigeoGeocodificado;
    }

    @JsonProperty("direccionGeocodificada")
    public String getDireccionGeocodificada() {
        return direccionGeocodificada;
    }

    @JsonProperty("direccionGeocodificada")
    public void setDireccionGeocodificada(String direccionGeocodificada) {
        this.direccionGeocodificada = direccionGeocodificada;
    }

    @JsonProperty("xGeocodificado")
    public String getXGeocodificado() {
        return xGeocodificado;
    }

    @JsonProperty("xGeocodificado")
    public void setXGeocodificado(String xGeocodificado) {
        this.xGeocodificado = xGeocodificado;
    }

    @JsonProperty("yGeocodificado")
    public String getYGeocodificado() {
        return yGeocodificado;
    }

    @JsonProperty("yGeocodificado")
    public void setYGeocodificado(String yGeocodificado) {
        this.yGeocodificado = yGeocodificado;
    }

    @JsonProperty("ubigeoValidado")
    public String getUbigeoValidado() {
        return ubigeoValidado;
    }

    @JsonProperty("ubigeoValidado")
    public void setUbigeoValidado(String ubigeoValidado) {
        this.ubigeoValidado = ubigeoValidado;
    }

    @JsonProperty("direccionNormalizada")
    public String getDireccionNormalizada() {
        return direccionNormalizada;
    }

    @JsonProperty("direccionNormalizada")
    public void setDireccionNormalizada(String direccionNormalizada) {
        this.direccionNormalizada = direccionNormalizada;
    }

    @JsonProperty("flagGeocodificacion")
    public String getFlagGeocodificacion() {
        return flagGeocodificacion;
    }

    @JsonProperty("flagGeocodificacion")
    public void setFlagGeocodificacion(String flagGeocodificacion) {
        this.flagGeocodificacion = flagGeocodificacion;
    }

    @JsonProperty("metodoGeocodificacion")
    public String getMetodoGeocodificacion() {
        return metodoGeocodificacion;
    }

    @JsonProperty("metodoGeocodificacion")
    public void setMetodoGeocodificacion(String metodoGeocodificacion) {
        this.metodoGeocodificacion = metodoGeocodificacion;
    }

    @JsonProperty("numeroRespuestas")
    public String getNumeroRespuestas() {
        return numeroRespuestas;
    }

    @JsonProperty("numeroRespuestas")
    public void setNumeroRespuestas(String numeroRespuestas) {
        this.numeroRespuestas = numeroRespuestas;
    }

    @JsonProperty("motivoNoGeocodificacion")
    public String getMotivoNoGeocodificacion() {
        return motivoNoGeocodificacion;
    }

    @JsonProperty("motivoNoGeocodificacion")
    public void setMotivoNoGeocodificacion(String motivoNoGeocodificacion) {
        this.motivoNoGeocodificacion = motivoNoGeocodificacion;
    }

    @JsonProperty("listOfPuntosGeocodificados")
    public ListOfPuntosGeocodificados getListOfPuntosGeocodificados() {
        return listOfPuntosGeocodificados;
    }

    @JsonProperty("listOfPuntosGeocodificados")
    public void setListOfPuntosGeocodificados(ListOfPuntosGeocodificados listOfPuntosGeocodificados) {
        this.listOfPuntosGeocodificados = listOfPuntosGeocodificados;
    }

    @JsonProperty("parsingAddress")
    public ParsingAddress getParsingAddress() {
        return parsingAddress;
    }

    @JsonProperty("parsingAddress")
    public void setParsingAddress(ParsingAddress parsingAddress) {
        this.parsingAddress = parsingAddress;
    }

    @JsonProperty("geocodedAddress")
    public GeocodedAddress getGeocodedAddress() {
        return geocodedAddress;
    }

    @JsonProperty("geocodedAddress")
    public void setGeocodedAddress(GeocodedAddress geocodedAddress) {
        this.geocodedAddress = geocodedAddress;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
