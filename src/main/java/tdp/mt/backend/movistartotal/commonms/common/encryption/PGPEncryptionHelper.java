package tdp.mt.backend.movistartotal.commonms.common.encryption;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;

import java.io.*;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;

public class PGPEncryptionHelper {

    private static final Logger logger = LogManager.getLogger();
    private static final int PGP_ENC_DATA = PGPEncryptedData.AES_128;
    private static final String PGP_PROVIDER = "BC";

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private InputStream privateKey = null;
    private InputStream publicKey = null;
    private String password = null;
    private boolean armor = false;
    private boolean withIntegrityCheck = true;

    private PGPPrivateKey pgpPrivateKey;

    /**
     * Constructor used when decrypt
     *
     * @param privateKey
     * @param password
     */
    public PGPEncryptionHelper(InputStream privateKey, String password) {
        super();
        this.privateKey = privateKey;
        this.password = password;
    }

    /**
     * Constructor used when encryption
     *
     * @param publicKey
     */
    public PGPEncryptionHelper(InputStream publicKey) {
        super();
        this.publicKey = publicKey;
    }

    public String encrypt(String msgText) throws IOException, PGPException, NoSuchProviderException {
        byte[] clearData = msgText.getBytes("UTF-8");

        PGPPublicKey encKey = readPublicKey(publicKey);
        ByteArrayOutputStream encOut = new ByteArrayOutputStream();
        OutputStream out = new ArmoredOutputStream(encOut);
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(PGPCompressedDataGenerator.ZIP);
        OutputStream cos = comData.open(bOut);
        PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
        OutputStream pOut = lData.open(cos, PGPLiteralData.BINARY, PGPLiteralData.CONSOLE, clearData.length,
                new Date());
        pOut.write(clearData);
        lData.close();
        comData.close();
        PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(PGP_ENC_DATA, true, new SecureRandom(),
                PGP_PROVIDER);
        if (encKey != null) {
            encGen.addMethod(encKey);
            byte[] bytes = bOut.toByteArray();
            OutputStream cOut = encGen.open(out, bytes.length);
            cOut.write(bytes);
            cOut.close();
        }
        out.close();
        return new String(encOut.toByteArray());
    }

    public File encrypt(File file) throws IOException, NoSuchProviderException, PGPException {
        File dest = File.createTempFile("encripted-", ".pgp");
        OutputStream out = new FileOutputStream(dest);

        if (armor) {
            out = new ArmoredOutputStream(out);
        }

        ByteArrayOutputStream bOut = new ByteArrayOutputStream();

        PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);

        org.bouncycastle.openpgp.PGPUtil.writeFileToLiteralData(comData.open(bOut), PGPLiteralData.BINARY, file);

        comData.close();

        PGPEncryptedDataGenerator cPk = new PGPEncryptedDataGenerator(PGP_ENC_DATA, withIntegrityCheck,
                new SecureRandom(), "BC");

        cPk.addMethod(readPublicKey(publicKey));

        byte[] bytes = bOut.toByteArray();

        OutputStream cOut = cPk.open(out, bytes.length);

        cOut.write(bytes);
        cOut.close();
        out.close();

        return dest;
    }

    public String decrypt(String encryptedText) throws Exception {
        byte[] encrypted = encryptedText.getBytes();
        InputStream in = new ByteArrayInputStream(encrypted);
        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);
        PGPObjectFactory pgpF = new PGPObjectFactory(in);
        PGPEncryptedDataList enc;
        Object o = pgpF.nextObject();
        if (o instanceof PGPEncryptedDataList) {
            enc = (PGPEncryptedDataList) o;
        } else {
            enc = (PGPEncryptedDataList) pgpF.nextObject();
        }
        PGPPrivateKey sKey = null;
        PGPPublicKeyEncryptedData pbe = null;
        while (sKey == null && enc.getEncryptedDataObjects().hasNext()) {
            pbe = (PGPPublicKeyEncryptedData) enc.getEncryptedDataObjects().next();
            sKey = findSecretKey(privateKey, pbe.getKeyID(), password.toCharArray());
        }
        if (pbe != null) {
            InputStream clear = pbe.getDataStream(sKey, PGP_PROVIDER);
            PGPObjectFactory pgpFact = new PGPObjectFactory(clear);
            PGPCompressedData cData = (PGPCompressedData) pgpFact.nextObject();
            pgpFact = new PGPObjectFactory(cData.getDataStream());
            PGPLiteralData ld = (PGPLiteralData) pgpFact.nextObject();
            InputStream unc = ld.getInputStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int ch;
            while ((ch = unc.read()) >= 0) {
                out.write(ch);
            }
            byte[] returnBytes = out.toByteArray();
            out.close();
            in.close();
            return new String(returnBytes, "UTF-8");
        }
        in.close();
        return null;
    }

    public File decrypt(File file) throws Exception {
        InputStream in = new FileInputStream(file);
        File dest = File.createTempFile("decrypted-", ".original");
        OutputStream out = new FileOutputStream(dest);

        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

        PGPObjectFactory pgpF = new PGPObjectFactory(in);
        PGPEncryptedDataList enc;

        Object o = pgpF.nextObject();
        //
        // the first object might be a PGP marker packet.
        //
        if (o instanceof PGPEncryptedDataList) {
            enc = (PGPEncryptedDataList) o;
        } else {
            enc = (PGPEncryptedDataList) pgpF.nextObject();
        }

        //
        // find the secret key
        //
        Iterator<PGPPublicKeyEncryptedData> it = enc.getEncryptedDataObjects();
        PGPPrivateKey sKey = null;
        PGPPublicKeyEncryptedData pbe = null;

        while (sKey == null && it.hasNext()) {
            pbe = it.next();

            sKey = findSecretKey(privateKey, pbe.getKeyID(), password.toCharArray());
        }

        if (sKey == null) {
            out.close();
            throw new IllegalArgumentException("Secret key for message not found.");
        }

        InputStream clear = pbe.getDataStream(sKey, PGP_PROVIDER);

        PGPObjectFactory plainFact = new PGPObjectFactory(clear);

        Object message = plainFact.nextObject();

        if (message instanceof PGPCompressedData) {
            PGPCompressedData cData = (PGPCompressedData) message;
            PGPObjectFactory pgpFact = new PGPObjectFactory(cData.getDataStream());

            message = pgpFact.nextObject();
        }

        if (message instanceof PGPLiteralData) {
            PGPLiteralData ld = (PGPLiteralData) message;

            InputStream unc = ld.getInputStream();
            int ch;

            while ((ch = unc.read()) >= 0) {
                out.write(ch);
            }
        } else if (message instanceof PGPOnePassSignatureList) {
            out.close();
            throw new PGPException("Encrypted message contains a signed message - not literal data.");
        } else {
            out.close();
            throw new PGPException("Message is not a simple encrypted file - type unknown.");
        }

        if (pbe.isIntegrityProtected()) {
            if (!pbe.verify()) {
                out.close();
                throw new PGPException("Message failed integrity check");
            }
        }
        out.close();
        return dest;
    }

    public File decrypt(File file, String id) throws Exception {
        InputStream in = new FileInputStream(file);
        File dest = null;
        OutputStream out = null;
        try {
            dest = File.createTempFile("decrypted-" + id, ".original");
            out = new FileOutputStream(dest);
        } catch (Exception e) {
            if (dest != null)
                if (dest.exists())
                    if (!dest.delete())
                        logger.info("Delete on PGPEncryptionHelper: " + dest.getAbsolutePath() + " failed!");
            if (out != null)
                out.close();
            logger.info("Delete on out:  failed!");
        }
        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);
        PGPObjectFactory pgpF = new PGPObjectFactory(in);
        PGPEncryptedDataList enc;
        Object o = pgpF.nextObject();
        //
        // the first object might be a PGP marker packet.
        //
        if (o instanceof PGPEncryptedDataList) {
            enc = (PGPEncryptedDataList) o;
        } else {
            enc = (PGPEncryptedDataList) pgpF.nextObject();
        }
        //
        // find the secret key
        //
        Iterator<PGPPublicKeyEncryptedData> it = enc.getEncryptedDataObjects();
        PGPPrivateKey sKey = null;
        PGPPublicKeyEncryptedData pbe = null;
        while (sKey == null && it.hasNext()) {
            pbe = it.next();
            sKey = findSecretKey(privateKey, pbe.getKeyID(), password.toCharArray());
        }
        if (sKey == null) {
            if (dest != null && dest.exists() && !dest.isDirectory()) {
                if (!dest.delete())
                    logger.info("Delete on sKey Null: " + dest.getAbsolutePath() + " failed!");
            }
            out.close();
            throw new IllegalArgumentException("Secret key for message not found.");
        }
        InputStream clear = pbe.getDataStream(sKey, PGP_PROVIDER);
        PGPObjectFactory plainFact = new PGPObjectFactory(clear);
        Object message = plainFact.nextObject();
        if (message instanceof PGPCompressedData) {
            PGPCompressedData cData = (PGPCompressedData) message;
            PGPObjectFactory pgpFact = new PGPObjectFactory(cData.getDataStream());
            message = pgpFact.nextObject();
        }
        if (message instanceof PGPLiteralData) {
            PGPLiteralData ld = (PGPLiteralData) message;
            InputStream unc = ld.getInputStream();
            int ch;
            while ((ch = unc.read()) >= 0) {
                out.write(ch);
            }
        } else if (message instanceof PGPOnePassSignatureList) {
            if (dest != null && dest.exists() && !dest.isDirectory()) {
                if (!dest.delete())
                    logger.info("Delete on Encrypted message : " + dest.getAbsolutePath() + " failed!");
            }
            out.close();
            throw new PGPException("Encrypted message contains a signed message - not literal data.");
        } else {
            if (dest != null && dest.exists() && !dest.isDirectory()) {
                if (!dest.delete())
                    logger.info("Message is not a simple encrypted file: " + dest.getAbsolutePath() + " failed!");
            }
            out.close();
            throw new PGPException("Message is not a simple encrypted file - type unknown.");
        }
        if (pbe.isIntegrityProtected()) {
            if (!pbe.verify()) {
                if (!dest.delete())
                    logger.info("Delete on Message failed integrity checks: " + dest.getAbsolutePath() + " failed!");
                out.close();
                throw new PGPException("Message failed integrity check");
            }
        }
        out.close();
        return dest;
    }

    private PGPPrivateKey findSecretKey(String publicKeyLocation, long keyID, char[] pass)
            throws NoSuchProviderException, FileNotFoundException, IOException, PGPException {
        return findSecretKey(new FileInputStream(publicKeyLocation), keyID, pass);
    }

    private PGPPrivateKey findSecretKey(InputStream keyIn, long keyID, char[] pass)
            throws IOException, PGPException, NoSuchProviderException {
        PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(
                org.bouncycastle.openpgp.PGPUtil.getDecoderStream(keyIn));

        PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

        if (pgpSecKey == null) {
            return null;
        }

        return pgpSecKey.extractPrivateKey(pass, PGP_PROVIDER);
    }

    public PGPPublicKey readPublicKey(String publicKeySource) throws FileNotFoundException, IOException, PGPException {
        return readPublicKey(new FileInputStream(publicKeySource));
    }

    public PGPPublicKey readPublicKey(InputStream in) throws IOException, PGPException {
        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

        PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(in);

        //
        // we just loop through the collection till we find a key suitable for
        // encryption, in the real
        // world you would probably want to be a bit smarter about this.
        //
        PGPPublicKey key = null;

        //
        // iterate through the key rings.
        //
        Iterator<PGPPublicKeyRing> rIt = pgpPub.getKeyRings();

        while (key == null && rIt.hasNext()) {
            PGPPublicKeyRing kRing = rIt.next();
            Iterator<PGPPublicKey> kIt = kRing.getPublicKeys();
            while (key == null && kIt.hasNext()) {
                PGPPublicKey k = kIt.next();

                if (k.isEncryptionKey()) {
                    key = k;
                }
            }
        }

        if (key == null) {
            throw new IllegalArgumentException("Can't find encryption key in key ring.");
        }

        return key;
    }

}
