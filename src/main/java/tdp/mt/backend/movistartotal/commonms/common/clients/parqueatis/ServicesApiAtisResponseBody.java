package tdp.mt.backend.movistartotal.commonms.common.clients.parqueatis;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

import java.util.List;

public class ServicesApiAtisResponseBody {
	@JsonProperty("DesbordeDetalleParque")
	private String DesbordeDetalleParque;
	@JsonProperty("DetalleParque")
	private List<ServicesApiAtisResponseBodyParkDetail> DetalleParque;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getDesbordeDetalleParque() {
		return DesbordeDetalleParque;
	}

	public void setDesbordeDetalleParque(String desbordeDetalleParque) {
		DesbordeDetalleParque = desbordeDetalleParque;
	}

	public List<ServicesApiAtisResponseBodyParkDetail> getDetalleParque() {
		return DetalleParque;
	}

	public void setDetalleParque(List<ServicesApiAtisResponseBodyParkDetail> detalleParque) {
		DetalleParque = detalleParque;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
