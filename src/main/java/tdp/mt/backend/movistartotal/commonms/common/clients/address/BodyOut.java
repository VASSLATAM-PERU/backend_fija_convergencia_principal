package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyOut {

    @JsonProperty("GeocodificarDireccionResponse")
    private GeocodificarDireccionResponse geocodificarDireccionResponse;

    @JsonProperty("GeocodificarDireccionResponse")
    public GeocodificarDireccionResponse getGeocodificarDireccionResponse() {
        return geocodificarDireccionResponse;
    }

    @JsonProperty("GeocodificarDireccionResponse")
    public void setGeocodificarDireccionResponse(GeocodificarDireccionResponse geocodificarDireccionResponse) {
        this.geocodificarDireccionResponse = geocodificarDireccionResponse;
    }

}

