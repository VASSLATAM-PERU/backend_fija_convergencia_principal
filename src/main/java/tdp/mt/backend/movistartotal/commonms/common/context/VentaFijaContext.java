package tdp.mt.backend.movistartotal.commonms.common.context;

import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;

import java.io.Serializable;

public interface VentaFijaContext extends Serializable {
    void setServiceCallEvent(ServiceCallEvent event);

    ServiceCallEvent getServiceCallEvent();
}
