package tdp.mt.backend.movistartotal.commonms.common.clients.gis;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OffersApiGisRequestBody {

    @JsonInclude(Include.NON_NULL)
    @JsonProperty("NUM_NUM_TLF")
    private String num_NUM_TLF;
    @JsonInclude(Include.NON_NULL)
    @JsonProperty("NUM_COD_CDX")
    private String num_COD_CDX;
    @JsonInclude(Include.NON_NULL)
    @JsonProperty("NUM_COD_CDY")
    private String num_COD_CDY;

    public String getNum_NUM_TLF() {
        return num_NUM_TLF;
    }

    public void setNum_NUM_TLF(String num_NUM_TLF) {
        this.num_NUM_TLF = num_NUM_TLF;
    }

    public String getNum_COD_CDX() {
        return num_COD_CDX;
    }

    public void setNum_COD_CDX(String num_COD_CDX) {
        this.num_COD_CDX = num_COD_CDX;
    }

    public String getNum_COD_CDY() {
        return num_COD_CDY;
    }

    public void setNum_COD_CDY(String num_COD_CDY) {
        this.num_COD_CDY = num_COD_CDY;
    }
}
