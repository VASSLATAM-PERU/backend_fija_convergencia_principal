package tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadocms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuplicateApiCmsRequestBody {

	private String CodigoRequerimiento;

	@JsonProperty("CodigoRequerimiento")
	public String getCodigoRequerimiento() {
		return CodigoRequerimiento;
	}

	public void setCodigoRequerimiento(String codigoRequerimiento) {
		CodigoRequerimiento = codigoRequerimiento;
	}
}
