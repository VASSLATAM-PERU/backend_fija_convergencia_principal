package tdp.mt.backend.movistartotal.commonms.common.clients.parquecms;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.constant.LegadosConstants;
import tdp.mt.backend.movistartotal.main.util.ServiceUrlUtil;

import java.io.IOException;

//import org.springframework.boot.json.JsonParseException;

public class ParqueCmsClient extends AbstractClient<ServicesApiCmsRequestBody, ServicesApiCmsResponseBody> {

	private static final String urlendpoint ="consulta-parque-cms/";
	private static final String urlmethod = "consultaParque";

	public ParqueCmsClient(ClientConfig config) {
		super(config);
		this.config = new ClientConfig.ClientConfigBuilder()
				.setUrl(ServiceUrlUtil.BASE_SERVICE_URL2 + urlendpoint+urlmethod)
				.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY2)
				.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET2)
				.setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_CMS)
				.setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS)
				.build();
	}

	@Override
	protected String getServiceCode() {
		return "PARQUECMS";
	}

	@Override
	protected ApiResponse<ServicesApiCmsResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<ServicesApiCmsResponseBody> objCms = mapper.readValue(json,
				new TypeReference<ApiResponse<ServicesApiCmsResponseBody>>() {
				});
		return objCms;
	}

}
