package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@Service
//@JsonPropertyOrder({"HeaderOut","BodyOut"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<E> {
  @JsonProperty("HeaderOut")
  private ApiResponseHeader HeaderOut;
  @JsonProperty("BodyOut")
  private E BodyOut;
  @JsonProperty("ClientException")
  private ClientException clientException;

  @JsonProperty("httpCode")
  private String httpCode;
  @JsonProperty("httpMessage")
  private String httpMessage;
  @JsonProperty("moreInformation")
  private String moreInformation;

  public ApiResponseHeader getHeaderOut() {
    return HeaderOut;
  }

  public void setHeaderOut(ApiResponseHeader headerOut) {
    HeaderOut = headerOut;
  }

  public E getBodyOut() {
    return BodyOut;
  }

  public void setBodyOut(E bodyOut) {
    BodyOut = bodyOut;
  }

  public ClientException getClientException() {
    return clientException;
  }

  public void setClientException(ClientException clientException) {
    this.clientException = clientException;
  }

  public String getHttpCode() {
    return httpCode;
  }

  public void setHttpCode(String httpCode) {
    this.httpCode = httpCode;
  }

  public String getHttpMessage() {
    return httpMessage;
  }

  public void setHttpMessage(String httpMessage) {
    this.httpMessage = httpMessage;
  }

  public String getMoreInformation() {
    return moreInformation;
  }

  public void setMoreInformation(String moreInformation) {
    this.moreInformation = moreInformation;
  }
}
