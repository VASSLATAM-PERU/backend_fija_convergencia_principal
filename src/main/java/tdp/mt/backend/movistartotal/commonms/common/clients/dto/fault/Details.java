package tdp.mt.backend.movistartotal.commonms.common.clients.dto.fault;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Details {

	@JsonProperty("timeStamp")
	private String timeStamp;
	@JsonProperty("cause")
	private String cause;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The timeStamp
	 */
	@JsonProperty("timeStamp")
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * 
	 * @param timeStamp
	 *            The timeStamp
	 */
	@JsonProperty("timeStamp")
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * 
	 * @return The cause
	 */
	@JsonProperty("cause")
	public String getCause() {
		return cause;
	}

	/**
	 * 
	 * @param cause
	 *            The cause
	 */
	@JsonProperty("cause")
	public void setCause(String cause) {
		this.cause = cause;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

