package tdp.mt.backend.movistartotal.commonms.common.clients.gis;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


public class OffersApiGisResponseBody {

    @JsonInclude(Include.NON_NULL)
    @JsonProperty("WSSIGFFTT_FFTT_AVEResult")
    private WSSIGFFTT_FFTT_AVEResult wSSIGFFTT_FFTT_AVEResult;

    public WSSIGFFTT_FFTT_AVEResult getwSSIGFFTT_FFTT_AVEResult() {
        return wSSIGFFTT_FFTT_AVEResult;
    }

    public void setwSSIGFFTT_FFTT_AVEResult(WSSIGFFTT_FFTT_AVEResult wSSIGFFTT_FFTT_AVEResult) {
        this.wSSIGFFTT_FFTT_AVEResult = wSSIGFFTT_FFTT_AVEResult;
    }
}
