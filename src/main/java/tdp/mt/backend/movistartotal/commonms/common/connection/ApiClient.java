package tdp.mt.backend.movistartotal.commonms.common.connection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import tdp.mt.backend.movistartotal.commonms.common.logging.ApplicationLogMarker;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Date;

/**
 * Api Client
 * @author jvilcayp
 *
 */
public class ApiClient {
	private static final Logger logger = LogManager.getLogger(ApiClient.class);

	public ApiClient() {
		super();
	}

	/**
	 * 
	 * @param uri
	 * @return
	 */
	public String jerseyGET(URI uri) {
		Date timeStamp = new Date();
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s peticion a %s", timeStamp.getTime(), uri));
		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String response = target.request().accept(MediaType.TEXT_PLAIN).get(String.class).toString();
		logger.info(response);
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, response));
		return response;
	}

	/**
	 * 
	 * @param uri
	 * @param apiRequest
	 * @return
	 */
	public String jerseyPOST(URI uri, Object apiRequest, String apiId, String apiSecret) {
		Date timeStamp = new Date();
		ObjectMapper mapper = new ObjectMapper();
		String requestJson;
		try {
			requestJson = mapper.writeValueAsString(apiRequest);
			logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s consulta a %s: %s", timeStamp.getTime(), uri, requestJson));
		} catch (JsonProcessingException e) {}
		Client client = ClientBuilder.newClient();
		client.register(new LoggingFeature());
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String json = "";

		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("x-ibm-client-id", apiId);
		invocationBuilder.header("x-ibm-client-secret", apiSecret);
		invocationBuilder.header("content-type", "application/json");
		Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
		json = response.readEntity(String.class);

		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, json));
		return json;
	}
}
