package tdp.mt.backend.movistartotal.commonms.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {
    private static final Logger logger = LogManager.getLogger();

    private FileUtils() {
    }

    public static File zip(File f) {
        return zip(f, f.getName());
    }

    public static File zip(File f, String contentFileName) {
        File zip = null;
        FileInputStream in = null;
        if (f != null) {
            try {
                in = new FileInputStream(f);
                zip = zip(in, contentFileName);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error(e);
                    }
                }
            }
        }
        return zip;
    }

    public static File zip(InputStream in, String contentFileName) throws IOException {
        File zip = File.createTempFile("compressed" + new Date().getTime(), ".zip");
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip))) {
            ZipEntry ze = new ZipEntry(contentFileName);
            zos.putNextEntry(ze);

            byte[] buffer = new byte[1024];
            int len;

            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();
        } catch (Exception e) {
            logger.error(e);
        }

        return zip;
    }

    public static File writeLines(List<String> lines) {
        File f = null;
        try {
            f = File.createTempFile("file" + new Date().getTime(), ".txt");
            try (PrintWriter writer = new PrintWriter(f, "UTF-8")) {
                for (String line : lines) {
                    writer.println(line);
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return f;
    }
}
