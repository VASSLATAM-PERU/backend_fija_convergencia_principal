package tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadoatis;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

public class DuplicateApiAtisResponseBody {

	@JsonProperty("EstadoPeticion")
	private String estadoPeticion;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault clientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault serverException;

	public String getEstadoPeticion() {
		return estadoPeticion;
	}

	public void setEstadoPeticion(String estadoPeticion) {
		this.estadoPeticion = estadoPeticion;
	}

	public ApiResponseBodyFault getClientException() {
		return clientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		this.clientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return serverException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		this.serverException = serverException;
	}

}
