package tdp.mt.backend.movistartotal.commonms.common.clients.avve;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

public class OffersApiAvveResponseBody {

	@JsonInclude(Include.NON_NULL)
	private String tecTV;
	@JsonInclude(Include.NON_NULL)
	private String tecInternet;
	@JsonInclude(Include.NON_NULL)
	private String velocidadMax;
	@JsonInclude(Include.NON_NULL)
	private String tipoSenal;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getTecTV() {
		return tecTV;
	}

	public void setTecTV(String tecTV) {
		this.tecTV = tecTV;
	}

	public String getTecInternet() {
		return tecInternet;
	}

	public void setTecInternet(String tecInternet) {
		this.tecInternet = tecInternet;
	}

	public String getVelocidadMax() {
		return velocidadMax;
	}

	public void setVelocidadMax(String velocidadMax) {
		this.velocidadMax = velocidadMax;
	}

	public String getTipoSenal() {
		return tipoSenal;
	}

	public void setTipoSenal(String tipoSenal) {
		this.tipoSenal = tipoSenal;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
