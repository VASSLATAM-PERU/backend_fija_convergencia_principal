package tdp.mt.backend.movistartotal.commonms.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class DatabasePropertiesSource {
	private static Logger logger = LogManager.getLogger(DatabasePropertiesSource.class);
	private DataSource datasource;
	
	public DatabasePropertiesSource (DataSource datasource) {
		this.datasource = datasource;
	}
	
	public Map<String, Object> loadProperties () {
		Map<String, Object> props = new HashMap<>();
		try (Connection con = datasource.getConnection()) {
			//sprint 3 - se adiciona nombre de esquema para la consulta en la tabla parameters
			String query = "select element, strValue from ibmx_a07e6d02edaf552.parameters where domain = 'CONFIG' and category = 'PARAMETER'";
			logger.info("query: "+query);
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						String key = rs.getString(1);
						String value = rs.getString(2);
						props.put(key, value);
						logger.info(String.format("Parametro %s: %s", key, value));
					}
				}
			}
		} catch (Exception e) {
			logger.error("waa :/", e);
		}
		return props;
	}

}
