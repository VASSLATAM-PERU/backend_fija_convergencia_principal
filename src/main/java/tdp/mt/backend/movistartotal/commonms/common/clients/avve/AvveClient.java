package tdp.mt.backend.movistartotal.commonms.common.clients.avve;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.io.IOException;

public class AvveClient extends AbstractClient<OffersApiAvveRequestBody, OffersApiAvveResponseBody> {

	public AvveClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "AVVE";
	}

	@Override
	protected ApiResponse<OffersApiAvveResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, 
	    		new TypeReference<ApiResponse<OffersApiAvveResponseBody>>() {
	    	});
	}

}
