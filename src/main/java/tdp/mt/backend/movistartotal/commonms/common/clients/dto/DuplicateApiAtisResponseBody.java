package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuplicateApiAtisResponseBody {

	@JsonProperty("EstadoPeticion")
	private String EstadoPeticion;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getEstadoPeticion() {
		return EstadoPeticion;
	}

	public void setEstadoPeticion(String estadoPeticion) {
		EstadoPeticion = estadoPeticion;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
