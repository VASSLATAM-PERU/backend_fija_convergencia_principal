package tdp.mt.backend.movistartotal.commonms.common.clients.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "Mensaje", "TelefonoDestino" })
public class RegisterApiSmsRequestBody {

  private String Mensaje;

  private String TelefonoDestino;

  @JsonProperty("Mensaje")
  public String getMensaje() {
    return Mensaje;
  }

  public void setMensaje(String mensaje) {
    Mensaje = mensaje;
  }

  @JsonProperty("TelefonoDestino")
  public String getTelefonoDestino() {
    return TelefonoDestino;
  }

  public void setTelefonoDestino(String telefonoDestino) {
    TelefonoDestino = telefonoDestino;
  }
}
