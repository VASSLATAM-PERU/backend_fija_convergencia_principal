package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "HeaderIn", "PreventaRequestBody" })
public class ApiRequest<E> {

  private ApiRequestHeader HeaderIn;
  private E BodyIn;

  @JsonProperty("HeaderIn")
  public ApiRequestHeader getHeaderIn() {
    return HeaderIn;
  }

  public void setHeaderIn(ApiRequestHeader headerIn) {
    HeaderIn = headerIn;
  }

  @JsonProperty("BodyIn")
  public E getBodyIn() {
    return BodyIn;
  }

  public void setBodyIn(E bodyIn) {
    BodyIn = bodyIn;
  }
}
