package tdp.mt.backend.movistartotal.commonms.common.util;

public class CustomerConstants {

    private CustomerConstants() {
    }

    public static final String DUPLICATE_RESPONSE_CODE_OK = "00";
    public static final String DUPLICATE_RESPONSE_DATA_OK = "PROCEDE";
    public static final String DUPLICATE_RESPONSE_CODE_ERROR = "01";
    public static final String DUPLICATE_RESPONSE_DATA_ERROR = "NO PROCEDE";
    public static final String DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME = "{productoName}";
    public static final String DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE = "{recordingDate}";
    public static final String DUPLICATE_RESPONSE_MESSAGE = "Ya cuenta con un " + DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME + " ingresado el " + DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE + ".";

    public static final String ANIO = "2016";
    public static final String DIA = "28";
    public static final String MES = "08";
    public static final String MONTO = "100.00";
    public static final String PRECIO = "199.99";
    public static final String PRODUCTO = "TRIO HD";
    public static final String DNI_RUC = "12345678";

    public static final String PARAMETRO_ANIO = "[ANIO]";
    public static final String PARAMETRO_DIA = "[DIA]";
    public static final String PARAMETRO_MES = "[MES]";
    public static final String PARAMETRO_MONTO = "[MONTO]";
    public static final String PARAMETRO_PRECIO = "[PRECIO]";
    public static final String PARAMETRO_PRODUCTO = "[PRODUCTO]";
    public static final String PARAMETRO_DNI_RUC = "[DNI-RUC]";

    //FIJOMT en vuelo
    public static final String SIN_MT="00";
    public static final String FIJO_MT_CANCELADO="01";
    public static final String FIJO_MT_EN_VUELO="02";
    public static final String FIJO_MT="03";

    public static final String ESTADO_SOLICITUD_PENDIENTE = "PENDIENTE";
    public static final String ESTADO_SOLICITUD_CAIDA = "CAIDA";
    public static final String ESTADO_SOLICITUD_GENERANDO_CIP = "GENERANDO_CIP";
    public static final String ESTADO_SOLICITUD_INGRESADO = "INGRESADO";

}
