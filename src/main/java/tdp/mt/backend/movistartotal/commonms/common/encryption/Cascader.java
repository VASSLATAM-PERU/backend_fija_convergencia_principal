package tdp.mt.backend.movistartotal.commonms.common.encryption;

import javax.crypto.Cipher;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Cascader {

    private static final int KEY_SIZE = 3072;
    private static final String TRANSFORMATION = "RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING";

    public static KeyPair generateRSAKeyPair() {
        try {
            KeyPair pair = new KeyPair(getPublicKey(), getPrivateKey());
            return pair;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] encrypt(byte[] data, byte[] publicKey) {
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKey);
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey pk = kf.generatePublic(publicKeySpec);
            Cipher rsa = Cipher.getInstance(TRANSFORMATION);
            rsa.init(Cipher.ENCRYPT_MODE, pk);
            return rsa.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] decrypt(byte[] encryptedData, byte[] privateKey) {
        try {
            PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privateKey);
            RSAPrivateKey pk = (RSAPrivateKey) KeyFactory.getInstance("RSA")
                    .generatePrivate(privSpec);

            Cipher rsaCipher = Cipher.getInstance(TRANSFORMATION);
            rsaCipher.init(Cipher.DECRYPT_MODE, pk);
            return rsaCipher.doFinal(encryptedData);

        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static PrivateKey getPrivateKey() throws Exception {

		byte[] keyBytes = Files.readAllBytes(Paths.get("/ms/server/private_key.der"));

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	public static PublicKey getPublicKey() throws Exception {
		byte[] keyBytes = Files.readAllBytes(Paths.get("/ms/server/public_key.der"));

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

}