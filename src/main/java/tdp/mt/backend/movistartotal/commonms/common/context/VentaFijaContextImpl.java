package tdp.mt.backend.movistartotal.commonms.common.context;

import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;

public class VentaFijaContextImpl implements VentaFijaContext {
	private static final long serialVersionUID = 1L;
	private ServiceCallEvent event;

	@Override
	public void setServiceCallEvent(ServiceCallEvent event) {
		this.event = event;
	}

	@Override
	public ServiceCallEvent getServiceCallEvent() {
		return event;
	}

}
