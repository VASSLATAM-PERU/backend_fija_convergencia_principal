package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeocodificarDireccionResponse {

    @JsonProperty("GeocodificarDireccionResult")
    private GeocodificarDireccionResult geocodificarDireccionResult;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("GeocodificarDireccionResult")
    public GeocodificarDireccionResult getGeocodificarDireccionResult() {
        return geocodificarDireccionResult;
    }

    @JsonProperty("GeocodificarDireccionResult")
    public void setGeocodificarDireccionResult(GeocodificarDireccionResult geocodificarDireccionResult) {
        this.geocodificarDireccionResult = geocodificarDireccionResult;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

