package tdp.mt.backend.movistartotal.commonms.common.connection;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tdp.mt.backend.movistartotal.commonms.common.encryption.PGPEncryptionHelper;
import tdp.mt.backend.movistartotal.commonms.domain.firebase.SaleApiFirebaseResponseBody;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class Firebase {
    private static final Logger logger = LogManager.getLogger();

    private String PGP_HEADER = "-----BEGIN PGP MESSAGE-----";

    private String token;

    private String apiKey;
    private String urlDetail;
    private String pgpPassphrase;

    private byte[] pgpPrivateKeyBytes;

    public Firebase(String apiKey, String urlDetail, InputStream pgpPrivateKey, String pgpPassphrase) throws IOException {
        this.apiKey = apiKey;
        this.urlDetail = urlDetail;
        this.pgpPassphrase = pgpPassphrase;

        pgpPrivateKeyBytes = IOUtils.toByteArray(pgpPrivateKey);

    }

//	private static void loadMeta () throws IOException {
//		if (API_KEY == null || URL_DETAIL == null) {
//			Properties prop = new Properties();
//			prop.load(new FileInputStream(new File("/ms/config/services.properties")));
//			API_KEY = prop.getProperty("firebase.api.auth.key");
//			URL_DETAIL = prop.getProperty("firebase.api.salesdetail.url");
//		}
//	}

    public String generateToken() {
        try {
            Map<String, Object> authPayload = new HashMap<String, Object>();
            authPayload.put("uid", "1");
            authPayload.put("some", "arbitrary");
            authPayload.put("data", "here");

            TokenOptions tokenOptions = new TokenOptions();
            tokenOptions.setAdmin(true);

            TokenGenerator tokenGenerator = new TokenGenerator(apiKey);
            token = tokenGenerator.createToken(authPayload, tokenOptions);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    /**
     * Loads the firebase object given its id
     * @param firebaseId id to retrive the firebase object
     * @return the firebase object, null when an error ocurred or not found
     */
    public SaleApiFirebaseResponseBody loadFirebaseObject(String firebaseId) {
        logger.info("load firebase object " + firebaseId);
        if (firebaseId == null) {
            return null;
        }
        String token = generateToken();
        URI uri;
        SaleApiFirebaseResponseBody firebaseObj = null;
        String jsonFirebase = null;
        try {
            uri = new URI(urlDetail.replace("{?id}", firebaseId).replace("{?auth}", token));
            logger.info(uri);
            jsonFirebase = Api.jerseyGET(uri);

            System.out.println(jsonFirebase);

            if (jsonFirebase != null) {
                ObjectMapper mapper = new ObjectMapper();
                if (jsonFirebase.contains(PGP_HEADER)) {
                    jsonFirebase = mapper.readValue(jsonFirebase, String.class);
                    PGPEncryptionHelper pgpDecryptionHelper = new PGPEncryptionHelper(new ByteArrayInputStream(pgpPrivateKeyBytes), pgpPassphrase);
                    jsonFirebase = pgpDecryptionHelper.decrypt(jsonFirebase);
                }
                firebaseObj = mapper.readValue(jsonFirebase, SaleApiFirebaseResponseBody.class);
            }

        } catch (URISyntaxException e) {
            logger.error("Error when creating firebase service", e);
        } catch (JsonParseException e) {
            logger.error("Error when parsing firebase response", e);
        } catch (JsonMappingException e) {
            logger.error("Error when mapping json to bean", e);
        } catch (IOException e) {
            logger.error("Error when reading json", e);
        } catch (Exception e) {
            logger.error("Error al desencriptar", e);
        }
        logger.info("loaded object: " + jsonFirebase);
        return firebaseObj;
    }

    public String getPgpPassphrase() {
        return pgpPassphrase;
    }

    public void setPgpPassphrase(String pgpPassphrase) {
        this.pgpPassphrase = pgpPassphrase;
    }

    public byte[] getPgpPrivateKeyBytes() {
        return pgpPrivateKeyBytes;
    }

    public void setPgpPrivateKeyBytes(byte[] pgpPrivateKeyBytes) {
        this.pgpPrivateKeyBytes = pgpPrivateKeyBytes;
    }


}
