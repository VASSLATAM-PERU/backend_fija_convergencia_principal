package tdp.mt.backend.movistartotal.commonms.common.exception;

public class ApiClientException extends ApplicationException {

	public ApiClientException (String apiMessage) {
		super(apiMessage);
	}
}
