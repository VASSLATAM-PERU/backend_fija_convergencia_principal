package tdp.mt.backend.movistartotal.commonms.common.util.enums;

public enum UploadWebFileResponse {
    UPLOAD_OK,
    UPLOAD_ENVIANDO_HDEC,
    UPLOAD_ERROR;
}
