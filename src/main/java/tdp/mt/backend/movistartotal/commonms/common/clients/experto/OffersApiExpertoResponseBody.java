package tdp.mt.backend.movistartotal.commonms.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

import java.util.List;

public class OffersApiExpertoResponseBody {
	@JsonProperty("CodConsulta")
	private String codConsulta;
	@JsonProperty("Accion")
	private String accion;
	@JsonProperty("DeudaAtis")
	private String deudaAtis;
	@JsonProperty("DeudaCms")
	private String deudaCms;
	@JsonProperty("OperacionComercial")
	private List<OffersApiExpertoResponseBodyOperacion> operacionComercial;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault clientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault serverException;

	public String getCodConsulta() {
		return codConsulta;
	}

	public void setCodConsulta(String codConsulta) {
		this.codConsulta = codConsulta;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getDeudaAtis() {
		return deudaAtis;
	}

	public void setDeudaAtis(String deudaAtis) {
		this.deudaAtis = deudaAtis;
	}

	public String getDeudaCms() {
		return deudaCms;
	}

	public void setDeudaCms(String deudaCms) {
		this.deudaCms = deudaCms;
	}

	public List<OffersApiExpertoResponseBodyOperacion> getOperacionComercial() {
		return operacionComercial;
	}

	public void setOperacionComercial(List<OffersApiExpertoResponseBodyOperacion> operacionComercial) {
		this.operacionComercial = operacionComercial;
	}

	public ApiResponseBodyFault getClientException() {
		return clientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		this.clientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return serverException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		this.serverException = serverException;
	}
}
