package tdp.mt.backend.movistartotal.commonms.common.clients;

import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.HdecApiHdecRequestBody;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.HdecApiHdecResponseBody;
import tdp.mt.backend.movistartotal.commonms.common.connection.ApiClient;
import tdp.mt.backend.movistartotal.commonms.common.exception.ApiClientException;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;

public class HdecClient extends SimpleClientImpl<HdecApiHdecRequestBody, HdecApiHdecResponseBody> {

	public HdecClient (ApiHeaderConfig config) {
		super(config, "", "",
				"", Constants.API_REQUEST_HEADER_OPERATION_HDEC,
				Constants.API_REQUEST_HEADER_DESTINATION_HDEC, HdecApiHdecResponseBody.class);
	}

	public HdecClient (ApiHeaderConfig config, ApiClient apiClient, ApiRequestHeaderFactory factory) {
		super(config, "", "",
				"", Constants.API_REQUEST_HEADER_OPERATION_HDEC,
				Constants.API_REQUEST_HEADER_DESTINATION_HDEC, HdecApiHdecResponseBody.class, apiClient, factory);
	}

	@Override
	public HdecApiHdecResponseBody sendData(HdecApiHdecRequestBody request) throws ApiClientException {
		HdecApiHdecResponseBody response = super.sendData(request);
		if (response == null) {
			throw new ApiClientException("Error client HDEC");
		}
		
		if (response.getClientException() != null) {
			String exceptionDetail = response.getClientException().getExceptionDetail();
			String appMessage = response.getClientException().getAppDetail() == null ? "Error de cliente HDEC" : response.getClientException().getAppDetail().getExceptionAppMessage();
			throw new ApiClientException(exceptionDetail + " " + appMessage);
		}
		return response;
	}
}
