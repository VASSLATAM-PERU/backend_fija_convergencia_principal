package tdp.mt.backend.movistartotal.commonms.common.constant;

public class ParametersConstants {

    private ParametersConstants() {
    }

    public static final String PARAMETERS_DOMAIN_TIMER = "TIMER";
    public static final String PARAMETERS_DOMAIN_TRY = "TRY";
    public static final String PARAMETERS_DOMAIN_TRANSLATE = "TRANSLATE";
    public static final String PARAMETERS_DOMAIN_CHECK_IN_FLIGHT_ORDER_LIMIT = "CHECK_IN_FLIGHT_ORDER";

    public static final String PARAMETERS_CATEGORY_TRANSLATE_ATIS = "ATIS";
    public static final String PARAMETERS_CATEGORY_TRANSLATE_CMSS = "CMSS";
    public static final String PARAMETERS_CATEGORY_TRANSLATE_CMSR = "CMSR";
    public static final String PARAMETERS_CATEGORY_CHECK_IN_FLIGHT_ORDER_LIMIT = "CALENDAR_LIMIT";

    public static final String PARAMETERS_ELEMENT_CHECK_IN_FLIGHT_ORDER_TYPE = "inflight.calendar.typelimit";
    public static final String PARAMETERS_ELEMENT_CHECK_IN_FLIGHT_ORDER_DAYS = "inflight.calendar.dayslimit";
    public static final String PARAMETERS_ELEMENT_CHECK_IN_FLIGHT_ORDER_MONTHS = "inflight.calendar.monthslimit";

}
