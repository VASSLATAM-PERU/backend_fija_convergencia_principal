package tdp.mt.backend.movistartotal.commonms.common.clients.experto;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.main.util.ServiceUrlUtil;

import java.io.IOException;

public class ExpertoClient extends AbstractClient<OffersApiExpertoRequestBody, OffersApiExpertoResponseBody> {

	private static final String urlendpoint ="consulta-estrategia-comercial/";
	private static final String urlmethod = "consultaScoring";

	public ExpertoClient(ClientConfig config) {
		super(config);
		this.config = new ClientConfig.ClientConfigBuilder()
				.setUrl(ServiceUrlUtil.BASE_SERVICE_URL2 + urlendpoint + urlmethod)
				.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY2)
				.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET2)
				.setOperation(Constants.API_REQUEST_HEADER_OPERATION_EXPERTO)
				.setDestination(Constants.API_REQUEST_HEADER_DESTINATION_EXPERTO).build();
	}

	@Override
	protected String getServiceCode() {
		return "EXPERTO";
	}

	@Override
	protected ApiResponse<OffersApiExpertoResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<OffersApiExpertoResponseBody> objExperto = mapper.readValue(json, new TypeReference<ApiResponse<OffersApiExpertoResponseBody>>() {
			});
		return objExperto;
	}
}
