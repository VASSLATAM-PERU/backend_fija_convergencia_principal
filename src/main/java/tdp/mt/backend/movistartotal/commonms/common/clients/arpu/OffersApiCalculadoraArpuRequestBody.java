package tdp.mt.backend.movistartotal.commonms.common.clients.arpu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OffersApiCalculadoraArpuRequestBody {
  @JsonInclude(Include.NON_NULL)
	private String documento;
  @JsonInclude(Include.NON_NULL)
	private String telefono;
	

  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
  public String getDocumento() {
	return documento;
  }
  
  public void setDocumento(String documento) {
	this.documento = documento;
  }
  
  

}
