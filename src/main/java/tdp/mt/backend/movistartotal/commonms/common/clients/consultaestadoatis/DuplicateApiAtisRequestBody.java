package tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadoatis;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuplicateApiAtisRequestBody {

	private String Peticion;

	@JsonProperty("Peticion")
	public String getPeticion() {
		return Peticion;
	}

	public void setPeticion(String peticion) {
		Peticion = peticion;
	}

}
