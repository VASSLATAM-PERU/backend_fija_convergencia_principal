package tdp.mt.backend.movistartotal.commonms.common.clients.reniec;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

public class IdentifyApiReniecResponseBody {

  private String apePaterno;
  private String apeMaterno;
  private String nombres;
  private String nacUbiDepartamento;
  private String nacUbiProvincia;
  private String nacUbiDistrito;
  private String fecNacimiento;
  private String nomPadre;
  private String nomMadre;
  private String fecExpedicion;
  private String restriccion;
  @JsonProperty("ClientException")
  private ApiResponseBodyFault ClientException;
  @JsonProperty("ServerException")
  private ApiResponseBodyFault ServerException;

  public String getApePaterno() {
    return apePaterno;
  }

  public void setApePaterno(String apePaterno) {
    this.apePaterno = apePaterno;
  }

  public String getApeMaterno() {
    return apeMaterno;
  }

  public void setApeMaterno(String apeMaterno) {
    this.apeMaterno = apeMaterno;
  }

  public String getNombres() {
    return nombres;
  }

  public void setNombres(String nombres) {
    this.nombres = nombres;
  }

  public String getNacUbiDepartamento() {
    return nacUbiDepartamento;
  }

  public void setNacUbiDepartamento(String nacUbiDepartamento) {
    this.nacUbiDepartamento = nacUbiDepartamento;
  }

  public String getNacUbiProvincia() {
    return nacUbiProvincia;
  }

  public void setNacUbiProvincia(String nacUbiProvincia) {
    this.nacUbiProvincia = nacUbiProvincia;
  }

  public String getNacUbiDistrito() {
    return nacUbiDistrito;
  }

  public void setNacUbiDistrito(String nacUbiDistrito) {
    this.nacUbiDistrito = nacUbiDistrito;
  }

  public String getFecNacimiento() {
    return fecNacimiento;
  }

  public void setFecNacimiento(String fecNacimiento) {
    this.fecNacimiento = fecNacimiento;
  }

  public String getNomPadre() {
    return nomPadre;
  }

  public void setNomPadre(String nomPadre) {
    this.nomPadre = nomPadre;
  }

  public String getNomMadre() {
    return nomMadre;
  }

  public void setNomMadre(String nomMadre) {
    this.nomMadre = nomMadre;
  }

  public String getFecExpedicion() {
    return fecExpedicion;
  }

  public void setFecExpedicion(String fecExpedicion) {
    this.fecExpedicion = fecExpedicion;
  }

  public String getRestriccion() {
    return restriccion;
  }

  public void setRestriccion(String restriccion) {
    this.restriccion = restriccion;
  }
  public ApiResponseBodyFault getClientException() {
    return ClientException;
  }

  public void setClientException(ApiResponseBodyFault clientException) {
    ClientException = clientException;
  }

  public ApiResponseBodyFault getServerException() {
    return ServerException;
  }

  public void setServerException(ApiResponseBodyFault serverException) {
    ServerException = serverException;
  }

}
