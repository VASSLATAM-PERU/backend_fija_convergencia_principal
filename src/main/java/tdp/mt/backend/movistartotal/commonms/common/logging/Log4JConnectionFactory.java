package tdp.mt.backend.movistartotal.commonms.common.logging;/*package pe.com.tdp.ventafija.microservices.common.logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class Log4JConnectionFactory {
	private static final Logger logger = LogManager.getLogger();
	private final HikariDataSource datasource;
	static Properties prop = new Properties();

	private static interface Singleton {
		final Log4JConnectionFactory INSTANCE = new Log4JConnectionFactory();
	}

	private Log4JConnectionFactory() {

		HikariConfig config = new HikariConfig();
		config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
		config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
		config.setPassword(System.getenv("TDP_FIJA_DB_PW"));
		config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
		config.setMaximumPoolSize(3);
		datasource = new HikariDataSource(config);
	}

	public static Connection getConnection() throws SQLException {
		return Singleton.INSTANCE.datasource.getConnection();
	}
}
*/