package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.main.restclient.domain.VarArg;

public class ApiResponseHeader {

  @JsonProperty("originator")
  private String originator;
  @JsonProperty("destination")
  private String destination;
  @JsonProperty("execId")
  private String execId;
  @JsonProperty("timestamp")
  private String timestamp;
  @JsonProperty("msgType")
  private String msgType;
  @JsonProperty("varArg")
  private VarArg varArg;

  public String getOriginator() {
    return originator;
  }

  public void setOriginator(String originator) {
    this.originator = originator;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getExecId() {
    return execId;
  }

  public void setExecId(String execId) {
    this.execId = execId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getMsgType() {
    return msgType;
  }

  public void setMsgType(String msgType) {
    this.msgType = msgType;
  }

  @JsonProperty("varArg")
  public VarArg getVarArg() {
    return varArg;
  }

  @JsonProperty("varArg")
  public void setVarArg(VarArg varArg) {
    this.varArg = varArg;
  }



}
