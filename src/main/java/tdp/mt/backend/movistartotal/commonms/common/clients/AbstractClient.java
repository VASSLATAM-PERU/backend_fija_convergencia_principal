package tdp.mt.backend.movistartotal.commonms.common.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiRequest;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiRequestHeader;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.connection.Api;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextHolder;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonms.common.logging.ApplicationLogMarker;
import tdp.mt.backend.movistartotal.commonms.common.util.Constants;
import tdp.mt.backend.movistartotal.main.domain.Response;
import tdp.mt.backend.movistartotal.main.util.UuidUtil;

import javax.ws.rs.client.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class AbstractClient<T, R> {
    public static final String SERVICE_CODE_CONSULTA_ESTADO_CMS = "ESTADOCMS";
    public static final String SERVICE_CODE_CONSULTA_ESTADO_ATIS = "ESTADOATIS";
    public static final String SERVICE_CODE_RENIEC = "RENIEC";
    public static final String SERVICE_CODE_BIOMETRIC = "BIOMETRIC";
    public static final String SERVICE_CODE_TGESTIONA = "TGESTIONA_MT";
    public static final String SERVICE_CODE_TGESTIONA2 = "TGESTIONA2";

    protected ClientConfig config;
    protected ServiceCallEvent event;

    protected AbstractClient(ClientConfig config) {
        super();
        this.config = config;
    }

    public ClientResult<ApiResponse<R>> post(T body) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {

            String json = doRequest(body);

            ApiResponse<R> apiResponse = getResponse(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    public ClientResult<ApiResponse<R>> post3(T body) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {

            String json = doRequest2(body);

            ApiResponse<R> apiResponse = getResponse(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }


    public ClientResult<ApiResponse<R>> post2(T body) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {
            String json = doRequest(body);
            String jsonNew = "";
            if (json.contains("$")) {
                json = json + "12345";
                int n = json.length();
                int k = 0;
                for (int i = 0; i < n; i++) {
                    if (json.substring(i, i + 5).equals("12345")) {
                        i = i + 6;
                    } else {
                        if (json.substring(i, i + 5).equals("{\"$\":")) {
                            k = i + 5;
                            jsonNew = jsonNew + "";
                            while (!json.substring(k, k + 1).equals("}")) {
                                jsonNew = jsonNew + json.substring(k, k + 1);
                                k++;
                            }
                            if (json.substring(k, k + 1).equals("}")) {
                                jsonNew = jsonNew + "";
                                i = k;
                            }
                        } else {
                            jsonNew = jsonNew + json.substring(i, i + 1);
                        }
                    }
                }
            } else {
                jsonNew = json;
            }
            ApiResponse<R> apiResponse = getResponse(jsonNew);
            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    protected String doRequest(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    protected String doRequest2(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest2(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    protected ApiRequest<T> getApiRequest(T body) {
        ApiRequest<T> apiRequest = new ApiRequest<>();
        ApiRequestHeader apiRequestHeader = getHeader(config.getOperation(), config.getDestination());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }

    protected ApiRequest<T> getApiRequest2(T body) {
        ApiRequest<T> apiRequest = new ApiRequest<>();
        ApiRequestHeader apiRequestHeader = getHeader2(config.getOperation());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }

    protected ApiRequestHeader getHeader(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
        ApiRequestHeader apiRequestHeader = new ApiRequestHeader();
        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG);
        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM);
        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM);
        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR);
        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER);
        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USER_ID);
        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WS_ID);
        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WS_IP);
        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestHeader.setTimestamp(sdf.format(new Date()) + "-05:00");
        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);

        return apiRequestHeader;
    }

    protected ApiRequestHeader getHeader2(String API_REQUEST_HEADER_OPERATION) {
        ApiRequestHeader header = new ApiRequestHeader();
        header.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        header.setLang(Constants.API_REQUEST_HEADER_LANG);
        header.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        header.setSystem(Constants.API_REQUEST_HEADER_SYSTEM_MOBILE);
        header.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM_MOBILE);
        header.setOriginator(header.getCountry() + ":" + header.getEntity() + ":" + header.getSystem() + ":"
                + header.getSubsystem());
        header.setSender(Constants.API_REQUEST_HEADER_SENDER);
        header.setUserId(Constants.API_REQUEST_HEADER_USER_ID); // DNI VENDEDOR
        header.setWsId(Constants.API_REQUEST_HEADER_WS_ID); // IBMBluemix? --> host del consumer.
        header.setWsIp(Constants.API_REQUEST_HEADER_WS_IP); // IP de bluemix
        header.setOperation(API_REQUEST_HEADER_OPERATION);
        header.setDestination(header.getCountry() + ":" + header.getEntity() + ":" + header.getSubsystem() + ":"
                + header.getSystem());
        String execId = UuidUtil.generarExecId();
        header.setExecId(execId); // Identificador de la ejecucion del servicio, segun formato
        //header.setPid(execId);
        Date d = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm:ss.SSSZ");
        String str = dt.format(d);
        str += "T" + dt1.format(d);
        String aux = str.substring(0, str.length() - 2) + ":" + str.substring(str.length() - 2);
        header.setTimestamp(aux); // Timestamp de la ejecucion del servicio. -->
        // formato "2015-07-15T14:53:47.003-05:00"
        header.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);
        return header;
    }

    protected ServiceCallEvent getEvent() {
        if (event == null) {
            if (VentaFijaContextHolder.getContext().getServiceCallEvent()==null) {
                event = new ServiceCallEvent();
            } else {
                event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
            }
            event.setServiceUrl(config.getUrl());
            event.setServiceCode(getServiceCode());
        }
        return event;
    }

    protected abstract String getServiceCode();

    protected abstract ApiResponse<R> getResponse(String json) throws Exception;

    //protected abstract R getResponseNoApi(String json) throws Exception;

    protected String doRequestNoApi(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOSTFija(uri, body, config.getApiId(), config.getApiSecret(), config.getAppSource(), config.getAuthorization());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    public ClientResult<Response<R>> postNoApi(T body) {
        ClientResult<Response<R>> result = new ClientResult<>();

        try {

            String json = doRequestNoApi(body);

            Response<R> noApiResponse = getResponseNoApi(json);

            result.setResult(noApiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    protected Response<R> getResponseNoApi(String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Response<R> response = mapper.readValue(json, new TypeReference<Response<R>>() {
        });

        return response;
    }



}
