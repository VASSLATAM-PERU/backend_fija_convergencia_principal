package tdp.mt.backend.movistartotal.commonms.common.clients.reniec;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdentifyApiReniecRequestBody {

  private String numDNI;

  @JsonProperty("NumDNI")
  public String getNumDNI() {
    return numDNI;
  }

  public void setNumDNI(String numDNI) {
    this.numDNI = numDNI;
  }

}
