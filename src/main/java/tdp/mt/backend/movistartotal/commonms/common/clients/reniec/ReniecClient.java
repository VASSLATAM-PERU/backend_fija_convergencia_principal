package tdp.mt.backend.movistartotal.commonms.common.clients.reniec;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.io.IOException;

public class ReniecClient extends AbstractClient<IdentifyApiReniecRequestBody, IdentifyApiReniecResponseBody> {

	public ReniecClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_RENIEC;
	}

	@Override
	protected ApiResponse<IdentifyApiReniecResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<IdentifyApiReniecResponseBody> objReniec = mapper.readValue(json, new TypeReference<ApiResponse<IdentifyApiReniecResponseBody>>() {});
		return objReniec;
	}

}
