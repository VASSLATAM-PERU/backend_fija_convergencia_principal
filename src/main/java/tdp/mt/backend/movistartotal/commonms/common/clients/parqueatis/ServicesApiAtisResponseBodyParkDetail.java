package tdp.mt.backend.movistartotal.commonms.common.clients.parqueatis;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServicesApiAtisResponseBodyParkDetail {
  @JsonProperty("Telefono")
  private String Telefono;
  @JsonProperty("ProductoPrincipal")
  private String ProductoPrincipal;
  @JsonProperty("ProductoLinea")
  private String ProductoLinea;
  @JsonProperty("ProductoInternet")
  private String ProductoInternet;
  @JsonProperty("ProductoTV")
  private String ProductoTV;
  @JsonProperty("TipoOrigen")
  private String TipoOrigen;
  @JsonProperty("Departamento")
  private String Departamento;
  @JsonProperty("Provincia")
  private String Provincia;
  @JsonProperty("Distrito")
  private String Distrito;
  @JsonProperty("Direccion")
  private String Direccion;
  @JsonProperty("CodigoUbigeo")
  private String CodigoUbigeo;
  @JsonProperty("DescripcionComercial")
  private String DescripcionComercial;
  @JsonProperty("EstadoPC")
  private String EstadoPC;
public String getTelefono() {
	return Telefono;
}
public void setTelefono(String telefono) {
	Telefono = telefono;
}
public String getProductoPrincipal() {
	return ProductoPrincipal;
}
public void setProductoPrincipal(String productoPrincipal) {
	ProductoPrincipal = productoPrincipal;
}
public String getProductoLinea() {
	return ProductoLinea;
}
public void setProductoLinea(String productoLinea) {
	ProductoLinea = productoLinea;
}
public String getProductoInternet() {
	return ProductoInternet;
}
public void setProductoInternet(String productoInternet) {
	ProductoInternet = productoInternet;
}
public String getProductoTV() {
	return ProductoTV;
}
public void setProductoTV(String productoTV) {
	ProductoTV = productoTV;
}
public String getTipoOrigen() {
	return TipoOrigen;
}
public void setTipoOrigen(String tipoOrigen) {
	TipoOrigen = tipoOrigen;
}
public String getDepartamento() {
	return Departamento;
}
public void setDepartamento(String departamento) {
	Departamento = departamento;
}
public String getProvincia() {
	return Provincia;
}
public void setProvincia(String provincia) {
	Provincia = provincia;
}
public String getDistrito() {
	return Distrito;
}
public void setDistrito(String distrito) {
	Distrito = distrito;
}
public String getDireccion() {
	return Direccion;
}
public void setDireccion(String direccion) {
	Direccion = direccion;
}
public String getCodigoUbigeo() {
	return CodigoUbigeo;
}
public void setCodigoUbigeo(String codigoUbigeo) {
	CodigoUbigeo = codigoUbigeo;
}
public String getDescripcionComercial() {
	return DescripcionComercial;
}
public void setDescripcionComercial(String descripcionComercial) {
	DescripcionComercial = descripcionComercial;
}
public String getEstadoPC() {
	return EstadoPC;
}
public void setEstadoPC(String estadoPC) {
	EstadoPC = estadoPC;
}


}
