package tdp.mt.backend.movistartotal.commonms.common.clients.consultaestadocms;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.io.IOException;

public class ConsultaEstadoCmsClient extends AbstractClient<DuplicateApiCmsRequestBody, DuplicateApiCmsResponseBody> {

	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_CONSULTA_ESTADO_CMS;
	}

	@Override
	protected ApiResponse<DuplicateApiCmsResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<DuplicateApiCmsResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<DuplicateApiCmsResponseBody>>() {});
		return objCms;
	}

	public ConsultaEstadoCmsClient(ClientConfig config) {
		super(config);
	}

}
