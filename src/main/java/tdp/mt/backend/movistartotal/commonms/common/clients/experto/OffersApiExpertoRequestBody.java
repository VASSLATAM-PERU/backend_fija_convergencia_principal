package tdp.mt.backend.movistartotal.commonms.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "ApellidoPaterno", "ApellidoMaterno", "Nombres", "TipoDeDocumento", "NumeroDeDocumento",
		"Departamento", "Provincia", "Distrito", "Ubigeo", "CodVendedor", "Zonal" })
public class OffersApiExpertoRequestBody {

	@JsonProperty("ApellidoPaterno")
	private String apellidoPaterno;

	@JsonProperty("ApellidoMaterno")
	private String apellidoMaterno;

	@JsonProperty("Nombres")
	private String nombres;

	@JsonProperty("TipoDeDocumento")
	private String tipoDeDocumento;

	@JsonProperty("NumeroDeDocumento")
	private String numeroDeDocumento;

	@JsonProperty("Departamento")
	private String departamento;

	@JsonProperty("Provincia")
	private String provincia;

	@JsonProperty("Distrito")
	private String distrito;

	@JsonProperty("Ubigeo")
	private String ubigeo;

	@JsonProperty("CodVendedor")
	private String codVendedor;

	@JsonProperty("Zonal")
	private String zonal;

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTipoDeDocumento() {
		return tipoDeDocumento;
	}

	public void setTipoDeDocumento(String tipoDeDocumento) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	public String getNumeroDeDocumento() {
		return numeroDeDocumento;
	}

	public void setNumeroDeDocumento(String numeroDeDocumento) {
		this.numeroDeDocumento = numeroDeDocumento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getCodVendedor() {
		return codVendedor;
	}

	public void setCodVendedor(String codVendedor) {
		this.codVendedor = codVendedor;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}
}
