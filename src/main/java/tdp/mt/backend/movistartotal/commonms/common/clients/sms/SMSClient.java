package tdp.mt.backend.movistartotal.commonms.common.clients.sms;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.io.IOException;

public class SMSClient extends AbstractClient<RegisterApiSmsRequestBody, RegisterApiSmsResponseBody> {

	public SMSClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "SMS";
	}

	@Override
	protected ApiResponse<RegisterApiSmsResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<RegisterApiSmsResponseBody> apiResponse = mapper.readValue(json,
	            new TypeReference<ApiResponse<RegisterApiSmsResponseBody>>() {
	            });
		return apiResponse;
	}
}
