package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HdecApiHdecResponseBody {
	@JsonProperty("Mensaje")
	private String Mensaje;
	@JsonProperty("ClientException")
	private ClientException ClientException;

	public String getMensaje() {
		return Mensaje;
	}

	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}

	public ClientException getClientException() {
		return ClientException;
	}

	public void setClientException(ClientException clientException) {
		ClientException = clientException;
	}

}
