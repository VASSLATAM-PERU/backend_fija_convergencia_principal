package tdp.mt.backend.movistartotal.commonms.common.clients.gis;

import com.fasterxml.jackson.annotation.JsonProperty;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponseBodyFault;

public class WSSIGFFTT_FFTT_AVEResult {

    @JsonProperty("COBRE_TEC")
    private String COBRE_TEC;
    @JsonProperty("COBRE_SAT")
    private String COBRE_SAT;
    @JsonProperty("COBRE_TRM")
    private String COBRE_TRM;
    @JsonProperty("COAXIAL_TEC")
    private String COAXIAL_TEC;
    @JsonProperty("COBRE_BLQ_VTA")
    private String COBRE_BLQ_VTA;
    @JsonProperty("COBRE_BLQ_TRM")
    private String COBRE_BLQ_TRM;
    @JsonProperty("COAXIAL_SEN")
    private String COAXIAL_SEN;
    @JsonProperty("COAXIAL_TRM")
    private String COAXIAL_TRM;
    @JsonProperty("HFC_TEC")
    private String HFC_TEC;
    @JsonProperty("HFC_VEL")
    private String HFC_VEL;
    @JsonProperty("GPON_TEC")
    private String GPON_TEC;
    @JsonProperty("GPON_VEL")
    private String GPON_VEL;
    @JsonProperty("GPON_TRM")
    private String GPON_TRM;
    @JsonProperty("XDSL_TEC")
    private String XDSL_TEC;
    @JsonProperty("XDSL_SAT")
    private String XDSL_SAT;
    @JsonProperty("XDSL_VEL_TLF")
    private String XDSL_VEL_TLF;
    @JsonProperty("XDSL_VEL_TRM")
    private String XDSL_VEL_TRM;
    @JsonProperty("EXC_CABLE_HFC")
    private String EXC_CABLE_HFC;
    @JsonProperty("EXC_CABLE_ACO")
    private String EXC_CABLE_ACO;
    @JsonProperty("EDF_NOCABLE_EXISTE")
    private String EDF_NOCABLE_EXISTE;
    @JsonProperty("EDF_NOCABLE_METROS")
    private String EDF_NOCABLE_METROS;
    @JsonProperty("PST_APOYO")
    private String PST_APOYO;
    @JsonProperty("NAP_TEC")
    private String NAP_TEC;
    @JsonProperty("S4G_TEC")
    private String S4G_TEC;
    @JsonProperty("EST_BASE")
    private String EST_BASE;
    @JsonProperty("CIR_DIGITAL")
    private String CIR_DIGITAL;
    @JsonProperty("DTH_TEC")
    private String DTH_TEC;
    @JsonProperty("DTH_BLQ")
    private String DTH_BLQ;
    @JsonProperty("ZAR_TEC")
    private String ZAR_TEC;
    @JsonProperty("ZAR_BLQ")
    private String ZAR_BLQ;
    @JsonProperty("COOR_X")
    private String COOR_X;
    @JsonProperty("COOR_Y")
    private String COOR_Y;
    @JsonProperty("WS_STATUS")
    private String WS_STATUS;
    @JsonProperty("WS_ERROR")
    private String WS_ERROR;
    @JsonProperty("ClientException")
    private ApiResponseBodyFault ClientException;
    @JsonProperty("ServerException")
    private ApiResponseBodyFault ServerException;

    public String getCOBRE_TEC() {
        return COBRE_TEC;
    }
    public void setCOBRE_TEC(String cOBRE_TEC) {
        COBRE_TEC = cOBRE_TEC;
    }
    public String getCOBRE_SAT() {
        return COBRE_SAT;
    }
    public void setCOBRE_SAT(String cOBRE_SAT) {
        COBRE_SAT = cOBRE_SAT;
    }
    public String getCOBRE_TRM() {
        return COBRE_TRM;
    }
    public void setCOBRE_TRM(String cOBRE_TRM) {
        COBRE_TRM = cOBRE_TRM;
    }
    public String getCOAXIAL_TEC() {
        return COAXIAL_TEC;
    }
    public void setCOAXIAL_TEC(String cOAXIAL_TEC) {
        COAXIAL_TEC = cOAXIAL_TEC;
    }

    public String getCOBRE_BLQ_VTA() {
        return COBRE_BLQ_VTA;
    }

    public void setCOBRE_BLQ_VTA(String COBRE_BLQ_VTA) {
        this.COBRE_BLQ_VTA = COBRE_BLQ_VTA;
    }

    public String getCOBRE_BLQ_TRM() {
        return COBRE_BLQ_TRM;
    }

    public void setCOBRE_BLQ_TRM(String COBRE_BLQ_TRM) {
        this.COBRE_BLQ_TRM = COBRE_BLQ_TRM;
    }

    public String getCOAXIAL_SEN() {
        return COAXIAL_SEN;
    }
    public void setCOAXIAL_SEN(String cOAXIAL_SEN) {
        COAXIAL_SEN = cOAXIAL_SEN;
    }
    public String getCOAXIAL_TRM() {
        return COAXIAL_TRM;
    }
    public void setCOAXIAL_TRM(String cOAXIAL_TRM) {
        COAXIAL_TRM = cOAXIAL_TRM;
    }
    public String getHFC_TEC() {
        return HFC_TEC;
    }
    public void setHFC_TEC(String hFC_TEC) {
        HFC_TEC = hFC_TEC;
    }
    public String getHFC_VEL() {
        return HFC_VEL;
    }
    public void setHFC_VEL(String hFC_VEL) {
        HFC_VEL = hFC_VEL;
    }
    public String getGPON_TEC() {
        return GPON_TEC;
    }
    public void setGPON_TEC(String gPON_TEC) {
        GPON_TEC = gPON_TEC;
    }
    public String getGPON_VEL() {
        return GPON_VEL;
    }
    public void setGPON_VEL(String gPON_VEL) {
        GPON_VEL = gPON_VEL;
    }
    public String getGPON_TRM() {
        return GPON_TRM;
    }
    public void setGPON_TRM(String gPON_TRM) {
        GPON_TRM = gPON_TRM;
    }
    public String getXDSL_TEC() {
        return XDSL_TEC;
    }
    public void setXDSL_TEC(String xDSL_TEC) {
        XDSL_TEC = xDSL_TEC;
    }
    public String getXDSL_SAT() {
        return XDSL_SAT;
    }
    public void setXDSL_SAT(String xDSL_SAT) {
        XDSL_SAT = xDSL_SAT;
    }
    public String getXDSL_VEL_TLF() {
        return XDSL_VEL_TLF;
    }
    public void setXDSL_VEL_TLF(String xDSL_VEL_TLF) {
        XDSL_VEL_TLF = xDSL_VEL_TLF;
    }
    public String getXDSL_VEL_TRM() {
        return XDSL_VEL_TRM;
    }
    public void setXDSL_VEL_TRM(String xDSL_VEL_TRM) {
        XDSL_VEL_TRM = xDSL_VEL_TRM;
    }
    public String getEXC_CABLE_HFC() {
        return EXC_CABLE_HFC;
    }
    public void setEXC_CABLE_HFC(String eXC_CABLE_HFC) {
        EXC_CABLE_HFC = eXC_CABLE_HFC;
    }
    public String getEXC_CABLE_ACO() {
        return EXC_CABLE_ACO;
    }
    public void setEXC_CABLE_ACO(String eXC_CABLE_ACO) {
        EXC_CABLE_ACO = eXC_CABLE_ACO;
    }
    public String getEDF_NOCABLE_EXISTE() {
        return EDF_NOCABLE_EXISTE;
    }
    public void setEDF_NOCABLE_EXISTE(String eDF_NOCABLE_EXISTE) {
        EDF_NOCABLE_EXISTE = eDF_NOCABLE_EXISTE;
    }
    public String getEDF_NOCABLE_METROS() {
        return EDF_NOCABLE_METROS;
    }
    public void setEDF_NOCABLE_METROS(String eDF_NOCABLE_METROS) {
        EDF_NOCABLE_METROS = eDF_NOCABLE_METROS;
    }
    public String getPST_APOYO() {
        return PST_APOYO;
    }
    public void setPST_APOYO(String pST_APOYO) {
        PST_APOYO = pST_APOYO;
    }
    public String getNAP_TEC() {
        return NAP_TEC;
    }
    public void setNAP_TEC(String nAP_TEC) {
        NAP_TEC = nAP_TEC;
    }
    public String getS4G_TEC() {
        return S4G_TEC;
    }
    public void setS4G_TEC(String s4g_TEC) {
        S4G_TEC = s4g_TEC;
    }
    public String getEST_BASE() {
        return EST_BASE;
    }
    public void setEST_BASE(String eST_BASE) {
        EST_BASE = eST_BASE;
    }
    public String getCIR_DIGITAL() {
        return CIR_DIGITAL;
    }

    public String getDTH_TEC() {
        return DTH_TEC;
    }

    public void setDTH_TEC(String DTH_TEC) {
        this.DTH_TEC = DTH_TEC;
    }

    public String getDTH_BLQ() {
        return DTH_BLQ;
    }

    public void setDTH_BLQ(String DTH_BLQ) {
        this.DTH_BLQ = DTH_BLQ;
    }

    public String getZAR_TEC() {
        return ZAR_TEC;
    }

    public void setZAR_TEC(String ZAR_TEC) {
        this.ZAR_TEC = ZAR_TEC;
    }

    public String getZAR_BLQ() {
        return ZAR_BLQ;
    }

    public void setZAR_BLQ(String ZAR_BLQ) {
        this.ZAR_BLQ = ZAR_BLQ;
    }

    public String getCOOR_X() {
        return COOR_X;
    }

    public void setCOOR_X(String COOR_X) {
        this.COOR_X = COOR_X;
    }

    public String getCOOR_Y() {
        return COOR_Y;
    }

    public void setCOOR_Y(String COOR_Y) {
        this.COOR_Y = COOR_Y;
    }

    public void setCIR_DIGITAL(String cIR_DIGITAL) {
        CIR_DIGITAL = cIR_DIGITAL;
    }
    public String getWS_STATUS() {
        return WS_STATUS;
    }
    public void setWS_STATUS(String wS_STATUS) {
        WS_STATUS = wS_STATUS;
    }
    public String getWS_ERROR() {
        return WS_ERROR;
    }
    public void setWS_ERROR(String wS_ERROR) {
        WS_ERROR = wS_ERROR;
    }

    public ApiResponseBodyFault getClientException() {
        return ClientException;
    }

    public void setClientException(ApiResponseBodyFault clientException) {
        ClientException = clientException;
    }

    public ApiResponseBodyFault getServerException() {
        return ServerException;
    }

    public void setServerException(ApiResponseBodyFault serverException) {
        ServerException = serverException;
    }

}
