package tdp.mt.backend.movistartotal.commonms.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpFrontResponseBody {
	@JsonProperty("PagoEfectivoCodigoCIP")
	private String PagoEfectivoCodigoCIP;
	@JsonProperty("PagoEfectivoFechaExpiracion")
	private String PagoEfectivoFechaExpiracion;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

	public String getPagoEfectivoCodigoCIP() {
		return PagoEfectivoCodigoCIP;
	}

	public void setPagoEfectivoCodigoCIP(String pagoEfectivoCodigoCIP) {
		PagoEfectivoCodigoCIP = pagoEfectivoCodigoCIP;
	}

	public String getPagoEfectivoFechaExpiracion() {
		return PagoEfectivoFechaExpiracion;
	}

	public void setPagoEfectivoFechaExpiracion(String pagoEfectivoFechaExpiracion) {
		PagoEfectivoFechaExpiracion = pagoEfectivoFechaExpiracion;
	}

}
