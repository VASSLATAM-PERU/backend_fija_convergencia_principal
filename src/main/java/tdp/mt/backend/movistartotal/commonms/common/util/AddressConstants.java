package tdp.mt.backend.movistartotal.commonms.common.util;

public class AddressConstants {

    private AddressConstants() {
    }

    public static final String VALIDAR_ATIS_CODE_OK = "0";
    public static final String VALIDAR_ATIS_MESSAGE_OK = "Dirección validada correctamente.";

}
