package tdp.mt.backend.movistartotal.commonms.common.context;

import org.springframework.util.Assert;

public class ThreadLocalVentaFijaContextHolderStrategy implements VentaFijaContextHolderStrategy {
	private static final ThreadLocal<VentaFijaContext> contextHolder = new ThreadLocal<VentaFijaContext>();

	@Override
	public void clearContext() {
		contextHolder.remove();
	}

	@Override
	public VentaFijaContext getContext() {
		VentaFijaContext ctx = contextHolder.get();

		if (ctx == null) {
			ctx = createEmptyContext();
			contextHolder.set(ctx);
		}

		return ctx;
	}

	@Override
	public void setContext(VentaFijaContext context) {
		Assert.notNull(context, "Only non-null SecurityContext instances are permitted");
		contextHolder.set(context);
	}

	@Override
	public VentaFijaContext createEmptyContext() {
		return new VentaFijaContextImpl();
	}

}
