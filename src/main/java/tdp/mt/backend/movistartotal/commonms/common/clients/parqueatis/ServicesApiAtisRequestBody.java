package tdp.mt.backend.movistartotal.commonms.common.clients.parqueatis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "TipoDocumento", "NumeroDocumento" })
public class ServicesApiAtisRequestBody {
	private String TipoDocumento;
	private String NumeroDocumento;

	@JsonProperty("TipoDocumento")
	public String getTipoDocumento() {
		return TipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		TipoDocumento = tipoDocumento;
	}

	@JsonProperty("NumeroDocumento")
	public String getNumeroDocumento() {
		return NumeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		NumeroDocumento = numeroDocumento;
	}
}
