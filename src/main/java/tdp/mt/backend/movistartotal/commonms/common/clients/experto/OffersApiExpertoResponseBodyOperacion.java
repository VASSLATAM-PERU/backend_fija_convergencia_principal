package tdp.mt.backend.movistartotal.commonms.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OffersApiExpertoResponseBodyOperacion {
	@JsonProperty("CodOpe")
	private String CodOpe;
	@JsonProperty("Renta")
	private String Renta;
	@JsonProperty("Segmento")
	private String Segmento;
	@JsonProperty("ContadorDetalle")
	private float ContadorDetalle;
	@JsonProperty("Detalle")
	private List<OffersApiExpertoResponseBodyOperacionDetalle> Detalle;

	public String getCodOpe() {
		return CodOpe;
	}

	public void setCodOpe(String codOpe) {
		CodOpe = codOpe;
	}

	public String getRenta() {
		return Renta;
	}

	public void setRenta(String renta) {
		Renta = renta;
	}

	public String getSegmento() {
		return Segmento;
	}

	public void setSegmento(String segmento) {
		Segmento = segmento;
	}

	public float getContadorDetalle() {
		return ContadorDetalle;
	}

	public void setContadorDetalle(float contadorDetalle) {
		ContadorDetalle = contadorDetalle;
	}

	public List<OffersApiExpertoResponseBodyOperacionDetalle> getDetalle() {
		return Detalle;
	}

	public void setDetalle(List<OffersApiExpertoResponseBodyOperacionDetalle> detalle) {
		Detalle = detalle;
	}

}
