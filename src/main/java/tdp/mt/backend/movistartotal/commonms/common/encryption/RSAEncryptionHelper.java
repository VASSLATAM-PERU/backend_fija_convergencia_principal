package tdp.mt.backend.movistartotal.commonms.common.encryption;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource.PSpecified;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAEncryptionHelper {
	private static final int ENCRYPTION_BUFFER_SIZE = 190;
	private static final int DECRYPTION_BUFFER_SIZE = 256;
	private static final String ENCRYPTION_FORM = "RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING";
	
	public void encrypt(File file, File destFile) throws Exception {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		OutputStream os = new FileOutputStream(destFile);
		byte[] buffer = new byte[ENCRYPTION_BUFFER_SIZE];
		
		Cipher oaepFromAlgo = Cipher.getInstance(ENCRYPTION_FORM);
		oaepFromAlgo.init(Cipher.ENCRYPT_MODE, getPublicKey());
		
		while(is.read(buffer) != -1){
			os.write(oaepFromAlgo.doFinal(buffer));
		}
		
		is.close();
		os.close();
	}
	
	public void decrypt(File file, File destFile) throws Exception {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		OutputStream os = new FileOutputStream(destFile);
		byte[] buffer = new byte[DECRYPTION_BUFFER_SIZE];

		Cipher oaepFromInit = Cipher.getInstance("RSA/ECB/OAEPPadding");
		OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", new MGF1ParameterSpec("SHA-1"), PSpecified.DEFAULT);
		oaepFromInit.init(Cipher.DECRYPT_MODE, getPrivateKey(), oaepParams);

		while(is.read(buffer) != -1){
			os.write(oaepFromInit.doFinal(buffer));
		}
		
		is.close();
		os.close();
	}
	
	/**
	 * Encrypts the plaintext and generates a base64 string
	 * @param plaintext
	 * @return
	 * @throws Exception
	 */
	public String encryptAndBase64(String plaintext, String srcEncoding) throws Exception {
		byte[] ct = encrypt(plaintext.getBytes(srcEncoding));
		byte[] base64Bytes = org.apache.commons.codec.binary.Base64.encodeBase64(ct);
		return new String(base64Bytes);
	}
	
	/**
	 * Decrypts a base64 ciphertext
	 * @param base64Ciphertext
	 * @return
	 * @throws Exception when error occurs
	 */
	public String decrypt(String base64Ciphertext, String destEncoding) throws Exception {
		// "ISO-8859-1"
		byte[] encoded = Base64.decodeBase64(base64Ciphertext);
		byte[] bts = decrypt(encoded);
		return new String(bts, destEncoding);
	}

	public byte[] decrypt(byte[] ct) throws Exception {
		InputStream is = new BufferedInputStream(new ByteArrayInputStream(ct));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buffer = new byte[DECRYPTION_BUFFER_SIZE];

//		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
//		OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", new MGF1ParameterSpec("SHA-1"), PSpecified.DEFAULT);
		OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSpecified.DEFAULT);
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(), oaepParams);
//		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey());

		while(is.read(buffer) != -1){
			os.write(cipher.doFinal(buffer));
		}
		
		is.close();
		os.close();
		
		return os.toByteArray();
	}

	public byte[] encrypt(byte[] pt) throws Exception {
		InputStream is = new BufferedInputStream(new ByteArrayInputStream(pt));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buffer = new byte[ENCRYPTION_BUFFER_SIZE];
		
		Cipher oaepFromAlgo = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
		oaepFromAlgo.init(Cipher.ENCRYPT_MODE, getPublicKey());
		
		while(is.read(buffer) != -1){
			os.write(oaepFromAlgo.doFinal(buffer));
		}
		
		is.close();
		os.close();
		
		byte[] ct = os.toByteArray();
		return ct;
	}

	public PrivateKey getPrivateKey() throws Exception {

		byte[] keyBytes = Files.readAllBytes(Paths.get("/ms/server/private_key.der"));

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	public PublicKey getPublicKey() throws Exception {
		byte[] keyBytes = Files.readAllBytes(Paths.get("/ms/server/public_key.der"));

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}
}
