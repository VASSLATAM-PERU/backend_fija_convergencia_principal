package tdp.mt.backend.movistartotal.commonms.common.exception;

public class ApplicationException extends Exception {
	private static final long serialVersionUID = 1L;

	public ApplicationException (Exception e) {
		super(e);
	}
	
	public ApplicationException (String exceptionCode) {
		super(exceptionCode);
	}
	
	protected ApplicationException () {
		super();
	}
}
