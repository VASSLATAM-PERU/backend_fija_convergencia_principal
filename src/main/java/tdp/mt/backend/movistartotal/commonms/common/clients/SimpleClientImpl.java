package tdp.mt.backend.movistartotal.commonms.common.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiHeaderConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiRequest;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiRequestHeader;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.connection.ApiClient;
import tdp.mt.backend.movistartotal.commonms.common.exception.ApiClientException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

//import pe.com.tdp.ventafija.microservices.common.connection.Api;

public abstract class SimpleClientImpl<T, R> implements Client<T, R> {
	private static final Logger logger = LogManager.getLogger(SimpleClientImpl.class);
	
	private ApiHeaderConfig config;
	private String apiUri;
	private String apiId;
	private String apiSecret;
	private String operation;
	private String destination;
	
	private Class<R> responseClass;
	private ApiClient apiClient;
	private ApiRequestHeaderFactory factory;
	
	protected SimpleClientImpl (ApiHeaderConfig config, String apiUri, String apiId, String apiSecret, String operation, String destination, Class<R> responseClass) {
		this.config = config;
		this.apiUri = apiUri;
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.operation = operation;
		this.destination = destination;
		this.responseClass = responseClass;
		this.apiClient = new ApiClient();
		this.factory = new ApiRequestHeaderFactory(config);
	}
	
	protected SimpleClientImpl (ApiHeaderConfig config, String apiUri, String apiId, String apiSecret, String operation, String destination, Class<R> responseClass, ApiClient apiClient, ApiRequestHeaderFactory factory) {
		this.config = config;
		this.apiUri = apiUri;
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.operation = operation;
		this.destination = destination;
		this.responseClass = responseClass;
		this.apiClient = apiClient;
		this.factory = factory;
	}
	

	@Override
	public R sendData(T request) throws ApiClientException {
		ApiResponse<R> apiResponse = null;
		try {
			URI uri = new URI(apiUri);
			ApiRequest<T> apiRequest = new ApiRequest<>();
			ApiRequestHeader headerIn = this.factory.getHeader();
			headerIn.setOperation(this.operation);
			headerIn.setDestination(this.destination);
			apiRequest.setHeaderIn(headerIn);
            
            apiRequest.setBodyIn(request);
            
            ObjectMapper mapper = new ObjectMapper();
            logger.info(mapper.writeValueAsString(apiRequest));
            
            String json = this.apiClient.jerseyPOST(uri, apiRequest, apiId, apiSecret);
            logger.info(json);
            JavaType type = mapper.getTypeFactory().constructParametricType(ApiResponse.class, responseClass);
//            apiResponse = mapper.readValue(json, new TypeReference<ApiResponse<R>>() {});
            apiResponse = mapper.readValue(json, type);
		} catch (URISyntaxException e) {
			logger.error("error uri", e);
		} catch (JsonProcessingException e) {
			logger.error("error json", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (IOException e) {
			logger.error("error io", e);
		}
		return apiResponse == null ? null : apiResponse.getBodyOut();
	}

}
