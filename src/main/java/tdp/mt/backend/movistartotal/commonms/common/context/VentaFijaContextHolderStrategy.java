package tdp.mt.backend.movistartotal.commonms.common.context;

public interface VentaFijaContextHolderStrategy {
	void clearContext();
	VentaFijaContext getContext();
	void setContext(VentaFijaContext context);
	VentaFijaContext createEmptyContext();
}
