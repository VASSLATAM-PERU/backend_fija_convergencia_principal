package tdp.mt.backend.movistartotal.commonms.common.clients.dto.fault;

import javax.xml.bind.annotation.XmlElement;

public class Code {
	
	@XmlElement(name="service")
	private String service;
	@XmlElement(name="operation")
	private String operation;
	@XmlElement(name="layer")
	private String layer;
	@XmlElement(name="tamSystem")
	private String tamSystem;
	@XmlElement(name="legacySystem")
	private String legacySystem;
	@XmlElement(name="api")
	private String api;
	@XmlElement(name="error")
	private String error;
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getLayer() {
		return layer;
	}
	public void setLayer(String layer) {
		this.layer = layer;
	}
	public String getTamSystem() {
		return tamSystem;
	}
	public void setTamSystem(String tamSystem) {
		this.tamSystem = tamSystem;
	}
	public String getLegacySystem() {
		return legacySystem;
	}
	public void setLegacySystem(String legacySystem) {
		this.legacySystem = legacySystem;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

}
