package tdp.mt.backend.movistartotal.commonms.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParsingAddress {

    @JsonProperty("idUnico")
    private String idUnico;
    @JsonProperty("codigoOriginal")
    private String codigoOriginal;
    @JsonProperty("ubigeoOriginal")
    private String ubigeoOriginal;
    @JsonProperty("direccionOriginal")
    private String direccionOriginal;
    @JsonProperty("ubigeoCorregido")
    private String ubigeoCorregido;
    @JsonProperty("tipoVia")
    private String tipoVia;
    @JsonProperty("nombreVia")
    private String nombreVia;
    @JsonProperty("cuadra")
    private String cuadra;
    @JsonProperty("numeroPuertaVia1")
    private String numeroPuertaVia1;
    @JsonProperty("numeroPuertaVia21")
    private String numeroPuertaVia21;
    @JsonProperty("tipoVia2")
    private String tipoVia2;
    @JsonProperty("nombreVia2")
    private String nombreVia2;
    @JsonProperty("numeroPuertaVia2")
    private String numeroPuertaVia2;
    @JsonProperty("numeroPuertaVia22")
    private String numeroPuertaVia22;
    @JsonProperty("numeroPuertaVia3")
    private String numeroPuertaVia3;
    @JsonProperty("numeroPuertaVia4")
    private String numeroPuertaVia4;
    @JsonProperty("tipoInterior")
    private String tipoInterior;
    @JsonProperty("numeroInterior")
    private String numeroInterior;
    @JsonProperty("piso")
    private String piso;
    @JsonProperty("tipoInterior2")
    private String tipoInterior2;
    @JsonProperty("numeroInterior2")
    private String numeroInterior2;
    @JsonProperty("tipoVivienda")
    private String tipoVivienda;
    @JsonProperty("nombreVivienda")
    private String nombreVivienda;
    @JsonProperty("tipoVivienda2")
    private String tipoVivienda2;
    @JsonProperty("nombreVivienda2")
    private String nombreVivienda2;
    @JsonProperty("tipoAgregacion")
    private String tipoAgregacion;
    @JsonProperty("nombreAgregacion")
    private String nombreAgregacion;
    @JsonProperty("numeroPuertaAgregacion1")
    private String numeroPuertaAgregacion1;
    @JsonProperty("numeroPuertaAgregacion2")
    private String numeroPuertaAgregacion2;
    @JsonProperty("numeroPuertaAgregacion3")
    private String numeroPuertaAgregacion3;
    @JsonProperty("numeroPuertaAgregacion4")
    private String numeroPuertaAgregacion4;
    @JsonProperty("manzana")
    private String manzana;
    @JsonProperty("lote")
    private String lote;
    @JsonProperty("barrio")
    private String barrio;
    @JsonProperty("etapa")
    private String etapa;
    @JsonProperty("sector")
    private String sector;
    @JsonProperty("comite")
    private String comite;
    @JsonProperty("grupo")
    private String grupo;
    @JsonProperty("zona")
    private String zona;
    @JsonProperty("parcela")
    private String parcela;
    @JsonProperty("kilometro")
    private String kilometro;
    @JsonProperty("departamento")
    private String departamento;
    @JsonProperty("otro")
    private String otro;
    @JsonProperty("resto")
    private String resto;
    @JsonProperty("referencia")
    private String referencia;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("idUnico")
    public String getIdUnico() {
        return idUnico;
    }

    @JsonProperty("idUnico")
    public void setIdUnico(String idUnico) {
        this.idUnico = idUnico;
    }

    @JsonProperty("codigoOriginal")
    public String getCodigoOriginal() {
        return codigoOriginal;
    }

    @JsonProperty("codigoOriginal")
    public void setCodigoOriginal(String codigoOriginal) {
        this.codigoOriginal = codigoOriginal;
    }

    @JsonProperty("ubigeoOriginal")
    public String getUbigeoOriginal() {
        return ubigeoOriginal;
    }

    @JsonProperty("ubigeoOriginal")
    public void setUbigeoOriginal(String ubigeoOriginal) {
        this.ubigeoOriginal = ubigeoOriginal;
    }

    @JsonProperty("direccionOriginal")
    public String getDireccionOriginal() {
        return direccionOriginal;
    }

    @JsonProperty("direccionOriginal")
    public void setDireccionOriginal(String direccionOriginal) {
        this.direccionOriginal = direccionOriginal;
    }

    @JsonProperty("ubigeoCorregido")
    public String getUbigeoCorregido() {
        return ubigeoCorregido;
    }

    @JsonProperty("ubigeoCorregido")
    public void setUbigeoCorregido(String ubigeoCorregido) {
        this.ubigeoCorregido = ubigeoCorregido;
    }

    @JsonProperty("tipoVia")
    public String getTipoVia() {
        return tipoVia;
    }

    @JsonProperty("tipoVia")
    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    @JsonProperty("nombreVia")
    public String getNombreVia() {
        return nombreVia;
    }

    @JsonProperty("nombreVia")
    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    @JsonProperty("cuadra")
    public String getCuadra() {
        return cuadra;
    }

    @JsonProperty("cuadra")
    public void setCuadra(String cuadra) {
        this.cuadra = cuadra;
    }

    @JsonProperty("numeroPuertaVia1")
    public String getNumeroPuertaVia1() {
        return numeroPuertaVia1;
    }

    @JsonProperty("numeroPuertaVia1")
    public void setNumeroPuertaVia1(String numeroPuertaVia1) {
        this.numeroPuertaVia1 = numeroPuertaVia1;
    }

    @JsonProperty("numeroPuertaVia21")
    public String getNumeroPuertaVia21() {
        return numeroPuertaVia21;
    }

    @JsonProperty("numeroPuertaVia21")
    public void setNumeroPuertaVia21(String numeroPuertaVia21) {
        this.numeroPuertaVia21 = numeroPuertaVia21;
    }

    @JsonProperty("tipoVia2")
    public String getTipoVia2() {
        return tipoVia2;
    }

    @JsonProperty("tipoVia2")
    public void setTipoVia2(String tipoVia2) {
        this.tipoVia2 = tipoVia2;
    }

    @JsonProperty("nombreVia2")
    public String getNombreVia2() {
        return nombreVia2;
    }

    @JsonProperty("nombreVia2")
    public void setNombreVia2(String nombreVia2) {
        this.nombreVia2 = nombreVia2;
    }

    @JsonProperty("numeroPuertaVia2")
    public String getNumeroPuertaVia2() {
        return numeroPuertaVia2;
    }

    @JsonProperty("numeroPuertaVia2")
    public void setNumeroPuertaVia2(String numeroPuertaVia2) {
        this.numeroPuertaVia2 = numeroPuertaVia2;
    }

    @JsonProperty("numeroPuertaVia22")
    public String getNumeroPuertaVia22() {
        return numeroPuertaVia22;
    }

    @JsonProperty("numeroPuertaVia22")
    public void setNumeroPuertaVia22(String numeroPuertaVia22) {
        this.numeroPuertaVia22 = numeroPuertaVia22;
    }

    @JsonProperty("numeroPuertaVia3")
    public String getNumeroPuertaVia3() {
        return numeroPuertaVia3;
    }

    @JsonProperty("numeroPuertaVia3")
    public void setNumeroPuertaVia3(String numeroPuertaVia3) {
        this.numeroPuertaVia3 = numeroPuertaVia3;
    }

    @JsonProperty("numeroPuertaVia4")
    public String getNumeroPuertaVia4() {
        return numeroPuertaVia4;
    }

    @JsonProperty("numeroPuertaVia4")
    public void setNumeroPuertaVia4(String numeroPuertaVia4) {
        this.numeroPuertaVia4 = numeroPuertaVia4;
    }

    @JsonProperty("tipoInterior")
    public String getTipoInterior() {
        return tipoInterior;
    }

    @JsonProperty("tipoInterior")
    public void setTipoInterior(String tipoInterior) {
        this.tipoInterior = tipoInterior;
    }

    @JsonProperty("numeroInterior")
    public String getNumeroInterior() {
        return numeroInterior;
    }

    @JsonProperty("numeroInterior")
    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    @JsonProperty("piso")
    public String getPiso() {
        return piso;
    }

    @JsonProperty("piso")
    public void setPiso(String piso) {
        this.piso = piso;
    }

    @JsonProperty("tipoInterior2")
    public String getTipoInterior2() {
        return tipoInterior2;
    }

    @JsonProperty("tipoInterior2")
    public void setTipoInterior2(String tipoInterior2) {
        this.tipoInterior2 = tipoInterior2;
    }

    @JsonProperty("numeroInterior2")
    public String getNumeroInterior2() {
        return numeroInterior2;
    }

    @JsonProperty("numeroInterior2")
    public void setNumeroInterior2(String numeroInterior2) {
        this.numeroInterior2 = numeroInterior2;
    }

    @JsonProperty("tipoVivienda")
    public String getTipoVivienda() {
        return tipoVivienda;
    }

    @JsonProperty("tipoVivienda")
    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    @JsonProperty("nombreVivienda")
    public String getNombreVivienda() {
        return nombreVivienda;
    }

    @JsonProperty("nombreVivienda")
    public void setNombreVivienda(String nombreVivienda) {
        this.nombreVivienda = nombreVivienda;
    }

    @JsonProperty("tipoVivienda2")
    public String getTipoVivienda2() {
        return tipoVivienda2;
    }

    @JsonProperty("tipoVivienda2")
    public void setTipoVivienda2(String tipoVivienda2) {
        this.tipoVivienda2 = tipoVivienda2;
    }

    @JsonProperty("nombreVivienda2")
    public String getNombreVivienda2() {
        return nombreVivienda2;
    }

    @JsonProperty("nombreVivienda2")
    public void setNombreVivienda2(String nombreVivienda2) {
        this.nombreVivienda2 = nombreVivienda2;
    }

    @JsonProperty("tipoAgregacion")
    public String getTipoAgregacion() {
        return tipoAgregacion;
    }

    @JsonProperty("tipoAgregacion")
    public void setTipoAgregacion(String tipoAgregacion) {
        this.tipoAgregacion = tipoAgregacion;
    }

    @JsonProperty("nombreAgregacion")
    public String getNombreAgregacion() {
        return nombreAgregacion;
    }

    @JsonProperty("nombreAgregacion")
    public void setNombreAgregacion(String nombreAgregacion) {
        this.nombreAgregacion = nombreAgregacion;
    }

    @JsonProperty("numeroPuertaAgregacion1")
    public String getNumeroPuertaAgregacion1() {
        return numeroPuertaAgregacion1;
    }

    @JsonProperty("numeroPuertaAgregacion1")
    public void setNumeroPuertaAgregacion1(String numeroPuertaAgregacion1) {
        this.numeroPuertaAgregacion1 = numeroPuertaAgregacion1;
    }

    @JsonProperty("numeroPuertaAgregacion2")
    public String getNumeroPuertaAgregacion2() {
        return numeroPuertaAgregacion2;
    }

    @JsonProperty("numeroPuertaAgregacion2")
    public void setNumeroPuertaAgregacion2(String numeroPuertaAgregacion2) {
        this.numeroPuertaAgregacion2 = numeroPuertaAgregacion2;
    }

    @JsonProperty("numeroPuertaAgregacion3")
    public String getNumeroPuertaAgregacion3() {
        return numeroPuertaAgregacion3;
    }

    @JsonProperty("numeroPuertaAgregacion3")
    public void setNumeroPuertaAgregacion3(String numeroPuertaAgregacion3) {
        this.numeroPuertaAgregacion3 = numeroPuertaAgregacion3;
    }

    @JsonProperty("numeroPuertaAgregacion4")
    public String getNumeroPuertaAgregacion4() {
        return numeroPuertaAgregacion4;
    }

    @JsonProperty("numeroPuertaAgregacion4")
    public void setNumeroPuertaAgregacion4(String numeroPuertaAgregacion4) {
        this.numeroPuertaAgregacion4 = numeroPuertaAgregacion4;
    }

    @JsonProperty("manzana")
    public String getManzana() {
        return manzana;
    }

    @JsonProperty("manzana")
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @JsonProperty("lote")
    public String getLote() {
        return lote;
    }

    @JsonProperty("lote")
    public void setLote(String lote) {
        this.lote = lote;
    }

    @JsonProperty("barrio")
    public String getBarrio() {
        return barrio;
    }

    @JsonProperty("barrio")
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    @JsonProperty("etapa")
    public String getEtapa() {
        return etapa;
    }

    @JsonProperty("etapa")
    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @JsonProperty("sector")
    public String getSector() {
        return sector;
    }

    @JsonProperty("sector")
    public void setSector(String sector) {
        this.sector = sector;
    }

    @JsonProperty("comite")
    public String getComite() {
        return comite;
    }

    @JsonProperty("comite")
    public void setComite(String comite) {
        this.comite = comite;
    }

    @JsonProperty("grupo")
    public String getGrupo() {
        return grupo;
    }

    @JsonProperty("grupo")
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    @JsonProperty("zona")
    public String getZona() {
        return zona;
    }

    @JsonProperty("zona")
    public void setZona(String zona) {
        this.zona = zona;
    }

    @JsonProperty("parcela")
    public String getParcela() {
        return parcela;
    }

    @JsonProperty("parcela")
    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    @JsonProperty("kilometro")
    public String getKilometro() {
        return kilometro;
    }

    @JsonProperty("kilometro")
    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    @JsonProperty("departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("otro")
    public String getOtro() {
        return otro;
    }

    @JsonProperty("otro")
    public void setOtro(String otro) {
        this.otro = otro;
    }

    @JsonProperty("resto")
    public String getResto() {
        return resto;
    }

    @JsonProperty("resto")
    public void setResto(String resto) {
        this.resto = resto;
    }

    @JsonProperty("referencia")
    public String getReferencia() {
        return referencia;
    }

    @JsonProperty("referencia")
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
