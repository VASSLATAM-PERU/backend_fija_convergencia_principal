package tdp.mt.backend.movistartotal.commonms.common.util;

public class ConstantsBIO {

    private ConstantsBIO() {
    }

    public static final String API_REQUEST_HEADER_SESSION_CODE = "550e8400-e29b-41d4-a716-446655440003";
    public static final String API_REQUEST_HEADER_APPLICATION = "APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_ID_MESSAGE = "550e8400-e29b-41d4-a716-446655440003";
    public static final String API_REQUEST_HEADER_IP_ADDRESS = "10.11.11.11";
    public static final String API_REQUEST_HEADER_FUNCIONALITY_CODE = "BiometricService";
    public static final String API_REQUEST_HEADER_SERVICE_NAME = "validateCustomerIdentity";
    public static final String API_REQUEST_HEADER_VERSION = "1.0";


}
