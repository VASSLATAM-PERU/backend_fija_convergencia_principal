package tdp.mt.backend.movistartotal.commonms.common.clients.arpu;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;

import java.util.List;

public class CalculadoraClient extends AbstractClient<OffersApiCalculadoraArpuRequestBody, OffersApiCalculadoraArpuResponseBody> {
    public final static String CLIENTE_NO_ENCONTRADO = "2";

    public CalculadoraClient(ClientConfig config) {
        super(config);
    }

    @Override
    protected String getServiceCode() {
        return "ARPU";
    }

    @Override
    protected ApiResponse<OffersApiCalculadoraArpuResponseBody> getResponse(String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora = null;
        try {
            objCalculadora = mapper.readValue(json,
                    new TypeReference<ApiResponse<List<OffersApiCalculadoraArpuResponseBody>>>() {
                    });
        } catch (JsonMappingException e) {
            objCalculadora = mapper.readValue(json,
                    new TypeReference<ApiResponse<OffersApiCalculadoraArpuResponseBody>>() {
                    });
            if (objCalculadora.getHeaderOut() != null && "ERROR".equals(objCalculadora.getHeaderOut().getMsgType())) {
                objCalculadora.setBodyOut(null);
            } else {
                throw e;
            }
				/*if (CLIENTE_NO_ENCONTRADO.equals(objCalculadora.getBodyOut().getClientException().getAppDetail().getExceptionAppCode())){
					objCalculadora.setBodyOut(null);
				}else if (objCalculadora.getBodyOut() != null && objCalculadora.getBodyOut().getServerException() != null) {
					throw new Exception(objCalculadora.getBodyOut().getServerException().getExceptionDetail());
				} else {
					throw e;
				}
			} else {
				throw e;
			}*/
        }

        return objCalculadora;
    }

}
