package tdp.mt.backend.movistartotal.commonms.common.clients.parqueatis;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import tdp.mt.backend.movistartotal.commonms.common.clients.AbstractClient;
import tdp.mt.backend.movistartotal.commonms.common.clients.ClientConfig;
import tdp.mt.backend.movistartotal.commonms.common.clients.dto.ApiResponse;
import tdp.mt.backend.movistartotal.commonms.common.constant.LegadosConstants;
import tdp.mt.backend.movistartotal.main.util.ServiceUrlUtil;

import java.io.IOException;

public class ParqueAtisClient extends AbstractClient<ServicesApiAtisRequestBody, ServicesApiAtisResponseBody> {

	private static final String urlendpoint ="consulta-parque-atis/";
	private static final String urlmethod = "consultaParque";

	public ParqueAtisClient(ClientConfig config) {
		super(config);
		this.config = new ClientConfig.ClientConfigBuilder()
				.setUrl(ServiceUrlUtil.BASE_SERVICE_URL2 + urlendpoint+urlmethod)
				.setApiId(ServiceUrlUtil.BASE_SERVICE_API_KEY2)
				.setApiSecret(ServiceUrlUtil.BASE_SERVICE_API_SECRET2)
				.setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_ATIS)
				.setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS)
				.build();
	}

	@Override
	protected String getServiceCode() {
		return "PARQUEATIS";
	}

	@Override
	protected ApiResponse<ServicesApiAtisResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<ServicesApiAtisResponseBody> objAtis = mapper.readValue(json, new TypeReference<ApiResponse<ServicesApiAtisResponseBody>>() {});
		return objAtis;
	}

}
