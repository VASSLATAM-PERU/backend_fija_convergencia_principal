package tdp.mt.backend.movistartotal.commonms.common.clients;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tdp.mt.backend.movistartotal.commonms.common.connection.Api;
import tdp.mt.backend.movistartotal.commonms.common.context.VentaFijaContextHolder;
import tdp.mt.backend.movistartotal.commonms.common.dto.ServiceCallEvent;
import tdp.mt.backend.movistartotal.commonms.common.exception.ApiClientException;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class ThirdpartyPagoEfectivoClient implements Client<Map<String, String>, Map<String, String>> {
	private static final Logger logger = LogManager.getLogger(ThirdpartyPagoEfectivoClient.class);
	private String url;
	private String apiId;
	private String apiSecret;
	
	public ThirdpartyPagoEfectivoClient (String url, String apiId, String apiSecret) {
		this.url = url;
		this.apiId = apiId;
		this.apiSecret = apiSecret;
	}

	@Override
	public Map<String, String> sendData(Map<String, String> request) throws ApiClientException {
		ServiceCallEvent event = VentaFijaContextHolder.getContext().getServiceCallEvent();
		
		Map<String, String> headers = new HashMap<>();
		headers.put("x-ibm-client-id", apiId);
		headers.put("x-ibm-client-secret", apiSecret);
		headers.put("X_HTTP_USUARIO", event.getUsername());
		headers.put("X_HTTP_ORDERID", event.getOrderId());
		headers.put("X_HTTP_DOCIDENT", event.getDocNumber());
		headers.put("X_HTTP_APPSOURCE", event.getSourceApp());
		headers.put("X_HTTP_APPVERSION", event.getSourceAppVersion());
		
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> responseMap = null;
		try {
			URI uri = new URI(url);
			String response = Api.jerseyPOST(uri, request, headers);
			responseMap = mapper.readValue(response, new TypeReference<Map<String, String>>(){});
		} catch (JsonParseException e) {
			logger.error("json error", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (JsonMappingException e) {
			logger.error("json error", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (IOException e) {
			logger.error("json error", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (Exception e) {
			logger.error("otro error", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		}
		return responseMap;
	}

}
