package tdp.mt.backend.movistartotal.commonms.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Cryptography {
	
	private Cryptography() {}
	
	public static String encryptMd5(String clear) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] b = md.digest(clear.getBytes());

		int size = b.length;
		StringBuilder h = new StringBuilder(size);
		for (int i = 0; i < size; i++) {
			int u = b[i] & 255;
			if (u < 16) {
				h.append("0" + Integer.toHexString(u));
			} else {
				h.append(Integer.toHexString(u));
			}
		}
		return h.toString();
	}
}
