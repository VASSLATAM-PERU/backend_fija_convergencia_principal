package tdp.mt.backend.movistartotal.commonms.common.clients;

import tdp.mt.backend.movistartotal.commonms.common.exception.ApiClientException;

/**
 * Client interface for external apis
 * @author jvilcayp
 *
 * @param <T> the request type that will be send to the service
 * @param <R> the type of response that will be returned
 */
public interface Client<T, R> {
	
	/**
	 * method responsable for sending data
	 * @param request to be send
	 * @return null when error ocurrs, the ResponseType (R) when available 
	 */
	public R sendData(T request) throws ApiClientException;
}
