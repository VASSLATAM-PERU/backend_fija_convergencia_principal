package tdp.mt.backend.movistartotal.commonms.common.clients;

public class ClientConfig {
	private String url;
	private String apiId;
	private String apiSecret;
	private String operation;
	private String destination;

	//WebFija
	private String appSource;
	private String authorization;

	public ClientConfig(){super();}

	public ClientConfig(String url, String apiId, String apiSecret, String operation, String destination) {
		super();
		this.url = url;
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.operation = operation;
		this.destination = destination;
	}

	public ClientConfig(String url, String apiId, String apiSecret, String appSource, String authorization, String operation, String destination) {
		super();
		this.url = url;
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.appSource = appSource;
		this.authorization = authorization;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getAppSource() {
		return appSource;
	}

	public void setAppSource(String appSource) {
		this.appSource = appSource;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public static class ClientConfigBuilder {
		private String url;
		private String apiId;
		private String apiSecret;
		private String operation;
		private String destination;
		
		public ClientConfigBuilder() {
			super();
		}

		public ClientConfigBuilder setUrl(String url) {
			this.url = url;
			return this;
		}

		public ClientConfigBuilder setApiId(String apiId) {
			this.apiId = apiId;
			return this;
		}

		public ClientConfigBuilder setApiSecret(String apiSecret) {
			this.apiSecret = apiSecret;
			return this;
		}

		public ClientConfigBuilder setOperation(String operation) {
			this.operation = operation;
			return this;
		}

		public ClientConfigBuilder setDestination(String destination) {
			this.destination = destination;
			return this;
		}
		
		public ClientConfig build () {
			return new ClientConfig(url, apiId, apiSecret, operation, destination);
		}
	}
}
